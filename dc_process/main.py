import os,sys
import datetime, time
import argparse
from optparse import OptionParser
from modules import odyssey
from modules import daisy
import configparser 
from modules import genConfig

global cf;cf = configparser.ConfigParser()
cf.optionxform=str

def _clean_config(config):
	# remove the annotation:
	for firstLevel in config.keys():
		for secondLevel in config[firstLevel]:
			if '#' in config[firstLevel][secondLevel]:
				config[firstLevel][secondLevel] = config[firstLevel][secondLevel][:config[firstLevel][secondLevel].index('#')-1].rstrip()
	return(config)
	
def main():
	try:
		parser = argparse.ArgumentParser(description="""synchronous information from with daisy, and process samples in odyssey""")
		# parser.add_argument( '-c', dest='config', type=str, required=True, help='the path of config file')
		sub_parsers = parser.add_subparsers(help="sub-command help", dest="sub_command")
		parser_run = sub_parsers.add_parser("run", help="run chilin automatically",
	                                        description="ChiLin-run: run pipeline")
		parser_run.add_argument( '-c', dest='conf', type=str, required=True, help='the path of config file')
		parser_config = sub_parsers.add_parser("genConf", help="generate config file",
	                                        description="config file generation")

		args = parser.parse_args()

		if args.sub_command == 'run':
			#read config
			cf.read(args.conf)
			config = cf._sections
			config=_clean_config(config)
			config['configPath']={}
			config['configPath']['path'] = args.conf
			
			# check the mode and folders
			if (config['mode']['odyssey'] == 'True') and (config['mode']['daisy'] == 'False'):
				for p in config['odysseyFolders'].keys():
					if not os.path.exists(config['odysseyFolders'][p]):
						if p == 'status':
							os.open(config['odysseyFolders'][p], os.O_RDWR|os.O_CREAT)
							os.system('echo "OK" > %s'%config['odysseyFolders'][p])
							continue
						os.mkdir(config['odysseyFolders'][p])
				#copy writeSbatch to qc
				os.system('cp ./modules/writeSbatch.py ./modules/qc/')
				print('+++start++++')
				return(odyssey._running(config))

			elif (config['mode']['odyssey'] == 'False') and (config['mode']['daisy'] == 'True'):
				for p in config['daisyFolders'].keys():
					if not os.path.exists(p):
						if p == 'status':
							os.open(config['daisyFolders'][p], os.O_RDWR|os.O_CREAT)
							os.system('echo "OK" > %s'%config['daisyFolders'][p])
							continue
						os.mkdir(config['daisyFolders'][p])
					else:
						print(p)
				return(daisy._running(config))
			else:
				sys.stderr.write("setting problem in mode\n")
				sys.exit(0)
		elif args.sub_command == 'genConf':
			genConfig.run()
			sys.exit(0)
		else:
			sys.stderr.write("Please give [run or genConf or -h]!\n")
			sys.exit(0)


	except KeyboardInterrupt:
		sys.stderr.write("User interrupted me!\n")
		sys.exit(0)


if __name__ == '__main__':
        # t = main()
	while 1:
		t=main()
		if t == 'no new samples, please wait!':
			time.sleep(43200) # wait for 12h when no new sample sync in last round checking
			#t = main()
#

## The README of cistrome DC data process pipeline ##

## Preparation

Installation:

1. build miniconda2 environment
2. install chilin and requested software in chilin, and you must need to test chilin demo and make sure that you can run chilin smoothly
3. download sratools and copy the tools into miniconda2 environment, you can do like this:
```
$ln -s ./sratools/bin/* ~miniconda2/bin/
```
4. make sure that R is available in your environment, and seqLogo package is installed and can work smoothly
5. make sure that your png() can write out smoothly, if something reports to you about X11 problems, you can try to resolve by:
```
$echo "options(bitmapType='cairo')" >> ~/.Rprofile
```
## Running
* The main script is main.py
```shell
python main.py -h
usage: main.py [-h] {run,genConf} ...

synchronous information from with daisy, and process samples in odyssey

positional arguments:
  {run,genConf}  sub-command help
    run          run chilin automatically
    genConf      generate config file

optional arguments:
  -h, --help     show this help message and exit
```
* Revise the dc.conf file. If there is no dc.conf file, please generate it by this command line. Then revise the parameters inside dc.conf
```shell
python main.py genConf
```
* make sure the source environment (like source /n/home04/xiaoleliu/MEI/DC_env3/bin/activate, or your miniconda2 environment) and softwares (this could be changed in different server, i.e. "module load texlive/2018.06.15-fasrc01" , and R) have been added to the sbatch file and title file.
* start to run:
```shell
python main.py run -c dc.conf
# usually, this is submitted by either run_cistromedc.sbatch (O2) or immortal_job.sbatch (Odyssey)
```

* once you start to run, several folders will be created:

1. mainPlace: the main work path, includes major Python scripts. Details in the folder.
2. syncDaisy: sync sample imformttion from Daisy where collects new samples from GEO.
3. sbatch_files: Odyssey require user to submit jobs by sbatch files, this folder will save all the sbatch files will be used in the whole process.
4. log_files: save all the log files, so that we can check what happened if something goes wrong.
5. tmp_sra_fastq: this folder saves SRA files which are raw data of high-throughput sequencing data, SRA will be converted to fastq file by fastq-dump command. Multiple sub-fastq files for one sample will also in this folder.
6. final_fastq: save the final fastq which is the input of ChiLin.
7. record: save some important tables. For example, a table with information of single or paired-end, SRA download link, etc.
8. tmp_chilin_result: a folder to save chilin output temporarily, after the checkChilin.py, the OK result will move to final_chilin_result folder.
9. final_chilin_result: save chilin result finally.

* Under record folders, several tables will created for recording the data processing and sample information:

1. chilin_OK_samples.txt: records the processed and completed samples    
2. failure_gsm_nolinkinfo.xls: records sampels that cannot get raw data
3. Input_ChIP_samples.txt: records Input samples, this sample won't be processed
4. motif_problem_samples.txt: records motif problem samples  
5. probably_badANDfailed_samples.txt: records samples which peak number is 0
6. fail_generate_chilin.xls: records samples that the chilin command line cannot be generated, probably fastq file is empty
7. gsm_info_cmd.xls: records all processedd sample information and command lines            
8. json_problem.txt: records the samples with json file problems        
9. None_Factor_samples.txt: samples without factor information, cannot be processed    
10. rerun_chilin_samples.txt: samples were processed but failed (not pass checking)




#odyssey
import os,sys
import argparse
import datetime, time
import commands
import configparser 

global cf;cf = configparser.ConfigParser()
cf.optionxform=str

global conf; conf = sys.argv[1] # the config file give in main.py run

def _clean_config(config):
	# remove the annotation:
	for firstLevel in config.keys():
		for secondLevel in config[firstLevel]:
			if '#' in config[firstLevel][secondLevel]:
				config[firstLevel][secondLevel] = config[firstLevel][secondLevel][:config[firstLevel][secondLevel].index('#')-1].rstrip()
	return(config)
	
def _check_md5(pathDir):
        """check the completeness of transferring
        """
        ID = os.path.basename(pathDir).replace('dataset', '')
        md5file = os.path.join(pathDir, ID+'.md5')
        if not os.path.exists(md5file):
                return None
        os.chdir(pathDir)
        check_res = commands.getoutput('md5sum -c %s'%md5file).replace('%s.md5: FAILED'%ID, '')
        os.chdir(pathDir.replace('final_chilin_result/dataset%s'%ID, ''))
        if ('FAILED' in check_res) or ('No such file' in check_res):
                return None
        else:
                return True
			
def _again_md5(pathDir):
	""" remove the old md5 and get a new
	"""
	ID = os.path.basename(pathDir).replace('dataset', '')
	md5file = os.path.join(pathDir, ID+'.md5')
	os.system('rm -fr %s'%md5file)
	os.chdir(pathDir)
	os.system('find ./ -type f -print0 | xargs -0 md5sum > %s.md5'%ID)
        os.chdir(pathDir.replace('final_chilin_result/dataset%s'%ID, 'mainPlace'))
def main():
	cf.read(conf)
	config = cf._sections
	config=_clean_config(config)
	
	Daisy_DC_path = ['/data6/DC_results/DC_new']
	Daisy_DC_path.append(config['odysseyParameter']['toDaisyPath'])

	for p in Daisy_DC_path:
		os.system("rsync -raP -e 'ssh -p 33001' rongbin@cistrome.org:%s %s"%(os.path.join(p, 'sampleList.txt'), './sampleListDaisy.txt'))
		os.system("rsync -raP -e 'ssh -p 33001' rongbin@cistrome.org:%s %s"%(os.path.join(p, 'again.txt'), './againDaisy.txt'))
		if os.path.exists('./againDaisy.txt'):
			againSample = list(set([x.rstrip() for x in open('./againDaisy.txt')]))
			for sa in againSample:
				if not os.path.exists(os.path.join('./final_chilin_result', sa)):
					continue
				check_res = _check_md5(os.path.join(os.getcwd(), 'final_chilin_result/'+sa))
				os.system('touch scp_ok')
				if check_res:
					os.system("rsync -raP -e 'ssh -p 33001' %s rongbin@cistrome.org:%s"%(os.path.join('./final_chilin_result', sa), p))
					os.system("rsync -raP -e 'ssh -p 33001' scp_ok rongbin@cistrome.org:%s"%(os.path.join(p, sa)))
				else:
					_again_md5(os.path.join(os.getcwd(), 'final_chilin_result/'+sa))
					os.system("rsync -raP -e 'ssh -p 33001' %s rongbin@cistrome.org:%s"%(os.path.join('./final_chilin_result', sa), p))
					os.system("rsync -raP -e 'ssh -p 33001' scp_ok rongbin@cistrome.org:%s"%(os.path.join(p, sa)))
		daisy_sample = [x.rstrip() for x in open('sampleListDaisy.txt')] #the samples that have checked by md5sum on daisy
		current_samples = [x for x in os.listdir('./final_chilin_result') if x.startswith('dataset')]
		needRemove = [x for x in current_samples if x in daisy_sample]
		for s in needRemove:
			os.system('rm -rf %s'%os.path.join('./final_chilin_result', s))
while 1:
	main()
	os.system('echo "++Finish check at %s, and will sleep 1 hours.\n" >> connect_daisy.log'%str(datetime.datetime.now()))
	time.sleep(3600)# wait for 12h to start a new round


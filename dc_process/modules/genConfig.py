import os,sys
import time
import configparser 


global config; config = configparser.ConfigParser()

def run():
	config = configparser.ConfigParser()
	config._sections['mode'] = {}
	config._sections['environment'] = {}
	config._sections['odysseyFolders'] = {}
	config._sections['odysseyParameter'] = {}
	config._sections['daisyFolders'] = {}
	config._sections['daisyParameter'] = {}
	config._sections['GEO'] = {}
	config._sections['chilin'] = {}
	config._sections['hg38'] = {}
	config._sections['mm10'] = {}

	config._sections['mode']['daisy']="False"
	config._sections['mode']['odyssey']="True"

	config._sections['environment']['path'] = '/home/rz92/miniconda3/bin/activate chilin'

	config._sections['odysseyFolders']['syncDaisy']='./syncDaisy #please give the relative path'
	config._sections['odysseyFolders']['fastq_final']='./fastq_final'
	config._sections['odysseyFolders']['log_files']='./log_files'
	config._sections['odysseyFolders']['sbatch_files']='./sbatch_files'
	config._sections['odysseyFolders']['tmp_chilin_result']='./tmp_chilin_result'
	config._sections['odysseyFolders']['final_chilin_result']='./final_chilin_result'
	config._sections['odysseyFolders']['record']='./record'
	config._sections['odysseyFolders']['tmp_sra_fastq']='./tmp_sra_fastq'
	config._sections['odysseyFolders']['status']='./status'


	config._sections['odysseyParameter']['localMetaTable']=''
	config._sections['odysseyParameter']['fromDaisyMetaTable']='/data6/DC_results/test.xls'
	config._sections['odysseyParameter']['trasferIP'] = 'rongbin@cistrome.org'
	config._sections['odysseyParameter']['cols']='0,1,2,4 # DCIDcol, speciescol, factorcol, gsmcol in localMetaTable or fromDaisyMetaTable for GEO samples, DCIDcol, speciescol, factorcol, fastq_link for encode samples'
	config._sections['odysseyParameter']['maxJob']='10'
	config._sections['odysseyParameter']['sbatchTitle']='./title'
	config._sections['odysseyParameter']['toDaisyPath']='/data6/DC_results/test'
	config._sections['odysseyParameter']['clusterType']='o2 # one of [o2, odyssey]'
	config._sections['odysseyParameter']['dataSource']='geo # one of [geo, encode]'

	# please give relative path under your current working path
	config._sections['daisyFolders']['fastq_final']='./fastq_final'
	config._sections['daisyFolders']['tmp_chilin_result']='./tmp_chilin_result'
	config._sections['daisyFolders']['final_chilin_result']='./final_chilin_result'
	config._sections['daisyFolders']['record']='./record'
	config._sections['daisyFolders']['tmp_sra_fastq']='./tmp_sra_fastq'

	# please either give localMetaTable or fromDaisyMetaTable
	config._sections['daisyParameter']['localMetaTable']=''
	config._sections['daisyParameter']['cols']='0,1,2,4 # DCIDcol, speciescol, factorcol, gsmcol in localMetaTable'

	config._sections['GEO']['ftp']='ftp://ftp-trace.ncbi.nih.gov/sra/sra-instant/reads/ByRun/sra/SRR'
	config._sections['ENCODE']['ftp']='https://www.encodeproject.org'
	config._sections['chilin']['broad']='h3k27me3,h3k36me3,h3k9me3 # please use the lower letter'

	config._sections['hg38']['genome_index'] = '/home/rz92/software/chilin/db/hg38/hg38.fa'
	config._sections['hg38']['genome_dir'] = '/home/rz92/software/chilin/db/hg38_mask'
	config._sections['hg38']['genetable'] = '/home/rz92/software/chilin/db/hg38/hg38.refGene'
	config._sections['hg38']['chrom_len'] = '/home/rz92/software/chilin/db/hg38/chromInfo_hg38.txt'
	config._sections['hg38']['dhs'] = '/home/rz92/software/chilin/db/hg38/DHS_hg38.bed'
	config._sections['hg38']['conservation'] = '/home/rz92/software/chilin/db/hg38/phastcon.bw'
	config._sections['hg38']['promoter']='./modules/qc/hg38/gene.bed_promoter'
	config._sections['hg38']['exon']='./modules/qc/hg38/gene.bed_exon'

	config._sections['mm10']['genome_index'] = '/home/rz92/software/chilin/db/mm10/mm10.fa'
	config._sections['mm10']['genome_dir'] = '/home/rz92/software/chilin/db/mm10_mask'
	config._sections['mm10']['genetable'] = '/home/rz92/software/chilin/db/mm10/mm10.refGene'
	config._sections['mm10']['chrom_len'] = '/home/rz92/software/chilin/db/mm10/chromInfo_mm10.txt'
	config._sections['mm10']['dhs'] = '/home/rz92/software/chilin/db/mm10/DHS_mm10.bed'
	config._sections['mm10']['conservation'] = '/home/rz92/software/chilin/db/mm10/phastcon.bw'
	config._sections['mm10']['promoter']='./modules/qc/mm10/gene.bed_promoter'
	config._sections['mm10']['exon']='./modules/qc/mm10/gene.bed_exon'

	with open('dc.conf', 'w') as configfile:
		config.write(configfile)

# if __name__ == '__main__':
# 	main()

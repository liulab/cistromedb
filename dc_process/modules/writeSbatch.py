import datetime
import os,sys


def _writeSbatch(config, sbatchTitle, core, time, memery, job):
        '''since the title format of each cluster type of server is different, we can handle title modification in this function.
        the commom partL #SBATCH -t 
        time is in minutes
        '''
        jobID = job
        job = os.path.join(config['odysseyFolders']['log_files'], jobID)
        if config['odysseyParameter']['clusterType'] == 'o2':
                sbatchTitle = sbatchTitle.replace('core', str(core)).replace('memery', str(memery)).replace('joblog', str(job))
                d = datetime.datetime(1,1,1)+datetime.timedelta(minutes=int(time))
                time = "%s-%s:%s"%(d.day-1, d.hour, d.minute)
                if (d.hour <= 12 and (d.day-1) == 0) or ((d.day-1) == 0 and d.hour == 12 and d.minute == 0):
                        partition = 'short'
                else:
                        partition = 'medium'
                sbatchTitle = sbatchTitle.replace('partition', str(partition)).replace('time', str(time))
        elif config['odysseyParameter']['clusterType'] == 'odyssey':
                sbatchTitle = sbatchTitle.replace('core', str(core)).replace('memery', str(memery)).replace('joblog', str(job)).replace('time', str(time)).replace('jobid', jobID)
        else:
                sys.stderr.write('please give o2 or odyssey type of server!')
                sys.exit(1)
        return(sbatchTitle)
		





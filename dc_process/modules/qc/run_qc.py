import argparse
import os,sys
import pandas as pd
import urllib2
import re
#sys.path.append("..")
from writeSbatch import _writeSbatch
# server = sys.argv[1] # daisy or odyssey
# inputFile = sys.argv[2] # the input file contains dcid, gsm, bamfile path, peak file path, species, dhs
# outputDir = sys.argv[3] # the output dirtory path

def _estimate_time_mem(path):
    try:
        if os.path.exists(path):
            fastq_size = os.path.getsize(path)/1024/1024
            # if fastq_size == 0:
            #     fail_file.write(gsm+' size error\n')
        if fastq_size < 1024:
            time = "300"
            fmemory ="1200"
        elif fastq_size < 2000:
            time = "400"
            fmemory ="2500"
        elif fastq_size < 3000:
            time = "500"
            fmemory ="3500"
        elif fastq_size < 4000:
            time = "500"
            fmemory ="4500"
        elif fastq_size < 5000:
            time = "500"
            fmemory ="8500"
        else:
            time = "500"
            fmemory ="18000"
    except:
        time = '400'
        fmemory = '5000'
    return [time, fmemory]
def parse_end(gsmid):
	"""get seq type from geo
	"""
	try:
		gsm_url = 'http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=%s'%gsm
		gsm_handler = urllib2.urlopen(gsm_url)
		gsm_html = gsm_handler.read()
		gsm_html = gsm_html.decode('utf-8')
		srx_regexp = re.compile('ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByExp/sra/SRX/SRX\S*"')
		srx_infor = srx_regexp.search(gsm_html)
		srx_infor = srx_infor.group().rstrip('"')
		srx = srx_infor.split('/')[-1]
		# get the SRR id('>SRR1588518</a></td><td') and find the type of layout
		srx_url = 'http://www.ncbi.nlm.nih.gov/sra?term=%s'%srx
		srx_handler = urllib2.urlopen(srx_url)
		srx_html = srx_handler.read()
		srx_html = srx_html.decode('utf-8')
		# find the layout type (<div>Layout: <span>SINGLE</span>)
		lay_infor = re.compile('<div>Layout: <span>.{6}</span>')
		lay_type = lay_infor.search(srx_html)
		lay_type = lay_type.group()
		lay_type = lay_type[-13:-7]
	except:
		return None
	return lay_type
def write_out(config, Command, server_type, cmdFile, ID = None, estimate = ['400', '1000']):
	"""write command line into file
	"""
	if not ID:
		ID = str(len([x for x in os.listdir('./') if x.endswith('.sbatch')])+1)
	if server_type == 'daisy':
		out = open('run_qc.sh', 'a')
		out.write(Command+'\n')
		out.close()
	if server_type == 'odyssey':
		sbatchTitlePath = config['odysseyParameter']['sbatchTitle']
		sbatchTitle = open(sbatchTitlePath).read()
		sbatch_file = open('%s_qc.sbatch'%(os.path.join(cmdFile, ID)), 'w')
		core, time, memery, job = 1, 1000, 3000, ID+'_qc'
		sbatch_file.write(_writeSbatch(config, sbatchTitle, core, time, memery, job))
		sbatch_file.write('\n'+Command+'\n')
		sbatch_file.close()
		# out = open('run_sbatch.sh', 'a')
		# print >>out, 'sbatch %s_qc.sbatch'%(ID)
		# out.close()
		return 'sbatch %s_qc.sbatch'%(os.path.join(cmdFile, ID))


#id_col, gsm_col, bam_col, peak_col, species_col, dhs_col, frip_col, pbc_col = 0,2,12,4,1,9,10,11
# gsmInf = [x.rstrip().split('\t') for x in open('ody_getInf.xls')]
# known_gsms = [x[1] for x in gsmInf]
def find_path(sampleInf, gsmCol, dirCol, speciesCol):
	"""find path based on input information
	"""
	gsmid = sampleInf[int(gsmCol)]
	dataset_path = sampleInf[int(dirCol)]
	species = sampleInf[int(speciesCol)]
	ID = dataset_path.split('dataset')[-1].strip('_').strip('/')
	bamFile = os.path.join(dataset_path, 'attic/', ID+'_treat_rep1.bam')
	if not os.path.exists(bamFile):
		return None
	peak=[i1 for i1 in os.listdir(dataset_path) if i1.endswith('sort_peaks.narrowPeak') or i1.endswith('sort_peaks.narrowPeak.bed') or i1.endswith('sort_peaks.broadPeak.bed') or i1.endswith('sort_peaks.broadPeak.bed') and i1.find('rep') == -1]
	if peak:
		peakFile = os.path.join(dataset_path, peak[0])
	else:
		return None
	json_path = os.path.join(dataset_path, 'attic', 'json')
	dhsFile = os.path.join(json_path, ID + '_dhs.json')
	if not os.path.exists(dhsFile):
		dhsFile = 'NA'
	fripFile = os.path.join(json_path, ID + '_frip.json')
	if not os.path.exists(fripFile):
		fripFile = 'NA'	
	pbcFile = os.path.join(json_path, ID + '_pbc.json')
	if not os.path.exists(pbcFile):
		pbcFile = 'NA'
	enrichFile = os.path.join(json_path, ID + '_enrich_meta.json')
	if not os.path.exists(enrichFile):
		enrichFile = 'NA'
	return [ID, gsmid, species, dataset_path, bamFile, peakFile, dhsFile, fripFile, pbcFile, enrichFile]

def _run(main_path, config, server, sampleType, gsm, chilin, end, sp, inputFile, cols, outputDir, enrich_orNot, cmdFile = './'):
#def _run(config):
	"""generate command line based on the provided sample information
	"""
	recordFolder = os.path.join(main_path, config['odysseyFolders']['record'])
	configPath = config['configPath']['path']
	if sampleType.lower() == 'encode':
		return None
	else:
		if inputFile and cols:	
			cols = cols.split(',')
			f = [x.rstrip().split('\t') for x in open(inputFile)]
		elif gsm and chilin and sp:
			f = [[gsm, chilin, sp]]
			cols = [0,1,2]
		else:
			return None
	#with open(inputFile) as f:
	# gsmInf = pd.read_table('../record/gsm_info_cmd.xls', header = None)
	for line in f:
		print(line)
		#line = line.rstrip().split('\t')
		#ID, gsm, bam, peak, species, dhs = line[id_col], line[gsm_col], line[bam_col], line[peak_col], line[species_col], line[dhs_col]
		#dataset_path = peak.replace(peak.split('/')[-1], '')
		INF = find_path(sampleInf=line, gsmCol=cols[0], dirCol=cols[1], speciesCol=cols[2])
		print(INF)
		if not INF:
			sys.stderr.write("problem in failure to locate files, peak file or bam file\n")
			sys.exit(0)
		ID, gsm, species, dataset_path, bam, peak, dhs, frip, pbc, enrich_meta = tuple(INF)
		if enrich_orNot:
			#means do not generate enrich_meta.json file
			enrich_meta = 'skip'
		if (enrich_meta.lower() not in ['na', 'none']) and (dhs.lower() not in ['na', 'none']) and (frip.lower() not in ['na', 'none']) and (pbc.lower() not in ['na', 'none']):
			continue
		if species in ['Mus musculus', 'mm', 'mm10']:
			species = 'mm'
		else:
			species = 'hs'
		# if gsm in list(gsmInf[1]):
		# 	end = gsmInf[gsmInf[1] == gsm].iat[0,2]
		if end:
			pass
		else:
			end = parse_end(gsm)
			if end:
				out = open(recordFolder+'/ody_getInf.xls.new', 'a')
				out.write('\t'.join([ID, gsm, end])+'\n')
				out.close()
			else:
				out = open(recordFolder+'/parser_problem.xls', 'a')
				out.write('\t'.join([ID, gsm])+'\n')
				out.close()
				continue
		if not os.path.exists(outputDir):
			os.mkdir(outputDir)
		cmd_cp = 'mv %s*.json %s'%(os.path.join(outputDir, ID), os.path.join(dataset_path, 'attic/json/'))
		estimate = _estimate_time_mem(bam)
		if end == 'SINGLE':
			preCMD = 'python '+os.path.join(main_path+'/modules/qc/qc_sperately.py')+' run -cf {cf} -n {ID} -b {bam} -p {peak} -O {output} -s {species}'.format(cf=configPath, ID = ID,
					bam = bam, peak = peak, output = outputDir, species = species)
			if (dhs.lower() in ['na', 'none']) and (frip.lower() not in ['na', 'none']) and (pbc.lower() not in ['na', 'none']) and (enrich_meta.lower() not in ['na', 'none']):
				cmd = preCMD + ' --dhs'
				estimate = ['400', '1000']
			elif (dhs.lower() in ['na', 'none']) and (frip.lower() in ['na', 'none']) and (pbc.lower() not in ['na', 'none']) and (enrich_meta.lower() not in ['na', 'none']):
				cmd = preCMD + ' --dhs --frip'
			elif (dhs.lower() in ['na', 'none']) and (frip.lower() in ['na', 'none']) and (pbc.lower() in ['na', 'none']) and (enrich_meta.lower() not in ['na', 'none']):
				cmd = preCMD + ' --dhs --frip --pbc'
			elif (dhs.lower() not in ['na', 'none']) and (frip.lower() in ['na', 'none']) and (pbc.lower() not in ['na', 'none']) and (enrich_meta.lower() not in ['na', 'none']):
				cmd = preCMD + ' --frip'	
			elif (dhs.lower() not in ['na', 'none']) and (frip.lower() in ['na', 'none']) and (pbc.lower() in ['na', 'none']) and (enrich_meta.lower() not in ['na', 'none']):
				cmd = preCMD + ' --frip --pbc'
			elif (dhs.lower() not in ['na', 'none']) and (frip.lower() in ['na', 'none']) and (pbc.lower() in ['na', 'none']) and (enrich_meta.lower() in ['na', 'none']):
				cmd = preCMD + ' --frip --pbc --enrich'
			elif (dhs.lower() not in ['na', 'none']) and (frip.lower() not in ['na', 'none']) and (pbc.lower() in ['na', 'none']) and (enrich_meta.lower() not in ['na', 'none']):
				cmd = preCMD + ' --pbc'
			elif (dhs.lower() not in ['na', 'none']) and (frip.lower() not in ['na', 'none']) and (pbc.lower() in ['na', 'none']) and (enrich_meta.lower() in ['na', 'none']):
				cmd = preCMD + ' --pbc --enrich'
			elif (dhs.lower() not in ['na', 'none']) and (frip.lower() not in ['na', 'none']) and (pbc.lower() not in ['na', 'none']) and (enrich_meta.lower() in ['na', 'none']):
				cmd = preCMD + ' --enrich'
			else:
				cmd = preCMD + ' --dhs --frip --pbc --enrich'
			w = write_out(config, Command = cmd+'\n'+cmd_cp+'\n', server_type = server, ID = ID, estimate = estimate, cmdFile = cmdFile)
		else:
			preCMD = 'python '+os.path.join(main_path+'/modules/qc/qc_sperately.py')+' run -n {ID} -b {bam} -p {peak} -O {output} -s {species} --pe'.format(ID = ID,
					bam = bam, peak = peak, output = outputDir, species = species)
			if (dhs.lower() in ['na', 'none']) and (frip.lower() not in ['na', 'none']) and (pbc.lower() not in ['na', 'none']) and (enrich_meta.lower() not in ['na', 'none']):
				cmd = preCMD + ' --dhs'
				estimate = ['400', '1000']
			elif (dhs.lower() in ['na', 'none']) and (frip.lower() in ['na', 'none']) and (pbc.lower() not in ['na', 'none']) and (enrich_meta.lower() not in ['na', 'none']):
				cmd = preCMD + ' --dhs --frip'
			elif (dhs.lower() in ['na', 'none']) and (frip.lower() in ['na', 'none']) and (pbc.lower() in ['na', 'none']) and (enrich_meta.lower() not in ['na', 'none']):
				cmd = preCMD + ' --dhs --frip --pbc'
			elif (dhs.lower() not in ['na', 'none']) and (frip.lower() in ['na', 'none']) and (pbc.lower() not in ['na', 'none']) and (enrich_meta.lower() not in ['na', 'none']):
				cmd = preCMD + ' --frip'	
			elif (dhs.lower() not in ['na', 'none']) and (frip.lower() in ['na', 'none']) and (pbc.lower() in ['na', 'none']) and (enrich_meta.lower() not in ['na', 'none']):
				cmd = preCMD + ' --frip --pbc'
			elif (dhs.lower() not in ['na', 'none']) and (frip.lower() in ['na', 'none']) and (pbc.lower() in ['na', 'none']) and (enrich_meta.lower() in ['na', 'none']):
				cmd = preCMD + ' --frip --pbc --enrich'
			elif (dhs.lower() not in ['na', 'none']) and (frip.lower() not in ['na', 'none']) and (pbc.lower() in ['na', 'none']) and (enrich_meta.lower() not in ['na', 'none']):
				cmd = preCMD + ' --pbc'
			elif (dhs.lower() not in ['na', 'none']) and (frip.lower() not in ['na', 'none']) and (pbc.lower() in ['na', 'none']) and (enrich_meta.lower() in ['na', 'none']):
				cmd = preCMD + ' --pbc --enrich'
			elif (dhs.lower() not in ['na', 'none']) and (frip.lower() not in ['na', 'none']) and (pbc.lower() not in ['na', 'none']) and (enrich_meta.lower() in ['na', 'none']):
				cmd = preCMD + ' --enrich'
			else:
				cmd = preCMD + ' --dhs --frip --pbc --enrich'
			w = write_out(config, Command = cmd+'\n'+cmd_cp+'\n', server_type = server, ID = ID, estimate = estimate, cmdFile = cmdFile)
		if server == 'odyssey' and not inputFile:
			return w
		if server == 'daisy' and not inputFile:
			return cmd+'\n'+cmd_cp

def main():
	try:
		parser = argparse.ArgumentParser(description="""get chilin command, and submit""")
		#parser.add_argument( '-p', dest='path', type=str, required=False, help='the cistromeDC path, default is ./')
		parser.add_argument( '-t', dest='type', type=str, required=True,
		 help='select server type in [daisy, odyssey]')
		parser.add_argument( '-st', dest='sampleType', type=str, required=True,
		 help='select sample type in [GEO, ENCODE]')
		parser.add_argument( '-f', dest='file', type=str, required=False,
		 help='the file which contaitns sample informtation')
		parser.add_argument( '-c', dest='columns', type=str, required=False,
		 help='a vector of column number for gsmid, chilin output path, and species, split by comma and start with 0, like 0,1,2')
		
		parser.add_argument( '-o', dest='output', type=str, required=True,
		 help='the output path for saving the temp files and results.')

		parser.add_argument( '-g', dest='gsm', type=str, required=False,
		 help='the gsm number of the sample.') 
		parser.add_argument( '-ch', dest='chilin', type=str, required=False,
		 help='the path of chilin result.')
		parser.add_argument( '-s', dest='species', type=str, required=False,
		 help='the species of the sample. one of [hs, mm], hs for human, mm for mouse')

		parser.add_argument("--dont_enrich", dest="enrich", action="store_true", default=False,
			help="skip run meta enrichment in promoter, exon, dhs region, default is to automatically check and run") 
		args = parser.parse_args()
		# config = {"server":args.type, 'sampleType':args.sampleType, "inputFile":args.file, "cols":args.columns, 
		# "outputDir":args.output, 'gsm':args.gsm, 'chilin':args.chilin, 'species':args.species, "enrich_orNot":args.enrich}
		# if config['inputFile'] and config['cols']:
		_run(server=args.type, sampleType = args.sampleType, gsm=args.gsm, chilin=args.chilin, sp = args.species, inputFile=args.file, cols=args.columns, outputDir=args.output, enrich_orNot=args.enrich)
		# if (not config['inputFile']) and (not config['cols']) and config['gsm'] and config['chilin'] and config['species']:
		# 	_run(server=args.type, gsm=args.gsm, chilin=args.chilin, sp = args.species, outputDir=args.output, enrich_orNot=args.enrich)

	except KeyboardInterrupt:
		sys.stderr.write("User interrupted me T_T \n")
		sys.exit(0)

if __name__ == '__main__':
    main()










import sys
import os
import argparse
from string import Template
import time
import json
import random
import configparser 

global cf;cf = configparser.ConfigParser()
cf.optionxform=str

def _clean_config(config):
	# remove the annotation:
	for firstLevel in config.keys():
		for secondLevel in config[firstLevel]:
			if '#' in config[firstLevel][secondLevel]:
				config[firstLevel][secondLevel] = config[firstLevel][secondLevel][:config[firstLevel][secondLevel].index('#')-1].rstrip()
	return(config)
class FriendlyArgumentParser(argparse.ArgumentParser):
    """
    Override argparse to show full length help information
    """

    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(1)

def json_dump(json_dict):   # json
    """
    dump out uniform json files for collecting statistics
    :param json_dict: output python dict to json
    :return: json_file name
    """
    json_file = json_dict["output"]["json"]
    with open(json_file, "w") as f:
        json.dump(json_dict, f, indent=4)
    return(json_file)

# def has_dhs(conf):
#     """total reads enrichment in union DHS regions
#     """
#     t = conf['prefix_path']
#     c = """dhs=$(bedtools intersect -f {param[p]} -wa -u -abam {input[bam]} -b {param[dhs]} -bed | wc -l)
# total=$(samtools flagstat {input[bam]} | head -1 | cut -d" " -f1)
# echo $dhs,$total > {output[dhs]}
# """.format(tool = "coverageBed",
# input = {"bam": conf['reads'],
#         "dhs":conf.get_path(conf.get("basics", "species"), "dhs")},
# output = {"dhs": t+".enrich.dhs"}, param = {"p": "1E-9","dhs": conf.get_path(conf.get("basics", "species"), "dhs")},
# )
def has_dhs(conf):
    """top 5000 peaks in union DHS regions
    """
    files = {'hs' : conf['DHS_hg38'], 'mm' : conf['DHS_mm10']}
    t = conf['prefix_path']
    c = """head -5000 {input[peak]} > {input[topPeak]}
    dhs=$(bedtools intersect -f {param[p]}.5000 -wa -u -a {input[topPeak]} -b {param[dhs]} -bed |wc -l)
    total=$(wc -l {input[peak]} | head -1 | cut -d" " -f1)
    echo $dhs,$total > {output[dhs]}
    """.format(tool = 'coverageBed',
        input = {"peak" : conf["peak"], "topPeak" : t+'_peak.5000'},
        output = {'dhs' : t+".enrich.dhs"}, param = {"p": "1E-9","dhs": files[conf['species']]},
        )
    print(c)
    os.system(c)
    json_dict = {"stat": {}, "input": {'dhs' : conf["peak"], "top_peaks":5000},
     "output": {"json":t+'_dhs.json'},
     "param": {}}

    i, s = t+".enrich.dhs", conf['prefix'] 
    inf = open(i).read().strip().split(",")
    json_dict["stat"][s] = {}
    json_dict["stat"][s]["number"] = int(inf[1])
    json_dict["stat"][s]["overlap"] = int(inf[0])
    json_dump(json_dict)

def enrich_in_meta(conf, input = {'meta':''}, output = {"json": ""}, param = {'dhs': '', 'has_dhs':'', 'samples':""}):
    """ enrichment in meta regions
    """
    json_dict = {"stat": {}, "input": input, "output": output, "param":param}
    #for n, s in enumerate(param['samples']):
    s = conf['prefix'] 
    ## total mapped reads

    #mapped = float(open(input["mapped"][n]).readlines()[2].split()[0])
    json_dict['stat'][s] = {}

    meta = open(input['meta']).read().strip().split(",")
    meta = map(float, meta)
    # if not param["down"]:
    #     json_dict['stat'][s]['exon'] = meta[0]/mapped
    #     json_dict['stat'][s]['promoter'] = meta[1]/mapped ## use all mapped reads
    # else:
    json_dict['stat'][s]['exon'] = meta[0]/meta[2]
    json_dict['stat'][s]['promoter'] = meta[1]/meta[2] ## use 4M reads

    if param['has_dhs']:
        dhs = open(param["dhs"]).read().strip().split(",")
        dhs = map(float, dhs)
        # if not param["down"]:
        #     json_dict['stat'][s]['dhs'] = dhs[0]/mapped
        # else:
        json_dict['stat'][s]['dhs'] = dhs[0]/dhs[1]

    json_dump(json_dict)

def _enrich_meta(conf):
    """calculate meta enrichment
    """
    t = conf['prefix_path']
    promtoer_files = {'hs' : conf['promoter_hg38'], 'mm' : conf['promoter_mm10']} # need files
    exon_files = {'hs' : conf['exon_hg38'], 'mm' : conf['exon_mm10']}
    c = """exon=$(bedtools intersect -f {param[p]} -wa -u -abam {input[bam]} -b {param[exon]} -bed | wc -l)
promoter=$(bedtools intersect -f {param[p]} -wa -u -abam {input[bam]} -b {param[promoter]} -bed | wc -l)
total=$(samtools flagstat {input[bam]} | head -1 | cut -d" " -f1)
echo $exon,$promoter,$total > {output[meta]}
""".format(tool = "coverageBed",
input = {"bam": conf['reads']},
output = {"meta":t+".enrich.meta"},
param = {"promoter": promtoer_files[conf['species']], 
         "p": "1E-9",
         "exon": exon_files[conf['species']]})
    os.system(c)

    ##dhs enrichment
    files = {'hs' : conf['DHS_hg38'], 'mm' : conf['DHS_mm10']}
    c1 = """dhs=$(bedtools intersect -f {param[p]} -wa -u -abam {input[bam]} -b {param[dhs]} -bed | wc -l)
total=$(samtools flagstat {input[bam]} | head -1 | cut -d" " -f1)
echo $dhs,$total > {output[dhs]}
""".format(tool = "coverageBed",
input = {"bam": conf['reads'],
        "dhs":files[conf['species']]},
output = {"dhs": t+".enrich_meta.dhs"}, param = {"p": "1E-9","dhs": files[conf['species']]},
)
    os.system(c1)
    enrich_in_meta(conf, input = {"meta":t+".enrich.meta"}, ## use 4M reads for down sampling ones, and all reads instead
        output = {"json": t + "_enrich_meta.json"},
        param = {"samples": conf['prefix'], "has_dhs":True,
                 "dhs": t+".enrich_meta.dhs"})


def PBC(conf):  # PBC1
    """
    Introduce ENCODE II library complexity assessment methods
    N1 / Nd, N1 is the location with exact one read, Nd is distinct location number
    :param workflow: samflow class
    :param conf: parsed config
    :return: void
    """
    t = conf['prefix_path']
    c = """bamToBed -i {input[bam]} | {tool} \'{{l[$1"\\t"$2"\\t"$3"\\t"$6]+=1}} END {{for(i in l) print l[i]}}\' \\
 | awk \'{{n[$1]+=1}} END {{for (i in n) print i"\\t"n[i]}}\'  \\
 | sort -k1n -  > {output[hist]}
awk '{{
if (NR==1) {{N1=$2}}
Nd+=$2
}} END {{print N1,Nd,N1/Nd}}' {output[hist]} > {output[pbc]}
""".format(
tool = "awk",
input = {"bam": conf['reads']},#t + "_4000000.bam" if conf.down else t + ".bam"},
output = {"pbc": t + ".pbc",
         "hist": t + ".hist"},
name = "PBC")
    print(c)
    os.system(c)

def stat_pbc(conf): # collect pbc value
    """
    statistics collected from *.pbc
    """
    c=json_pbc(input = {"pbc": conf['prefix_path'] + ".pbc"},
        output = {"json": conf['prefix_path'] + "_pbc.json"},
        param = {"samples":conf['prefix']})


def json_pbc(input={}, output={}, param={}): # convert to json format
    """
    input is the target + ".pbc"
    output is the json files conf.json_prefix + "_pbc.json"
    param for matching samples order
    """
    json_dict = {"stat": {}, "input": input, "output": output, "param": param}

    i, s = input["pbc"], param["samples"]
    inl = open(i).readlines()[0].strip().split()
    json_dict["stat"][s] = {}
    json_dict["stat"][s]["N1"] = int(inl[0])
    json_dict["stat"][s]["Nd"] = int(inl[1])
    json_dict["stat"][s]["PBC"] = round(float(inl[2]), 3)

    json_dump(json_dict)

def stat_frip(conf):    # collect frip score
    """
    collect FRiP informative tag number and effective peaks number
    """
    c = json_frip(input={"frip": conf['prefix_path'] +".frip"},
        output={"json": conf['prefix_path']+"_frip.json"},
        param={"samples": conf['prefix']})

def json_frip(input={}, output={}, param={}):    # convert to json
    """
    input is *.frip
    output is conf.json_prefix + "_frip.json"
    param for matching samples
    """
    json_dict = {"stat": {}, "input": input, "output": output, "param": param}
    i,s = input["frip"], param["samples"]
    inf = open(i).read().strip().split(",")
    json_dict["stat"][s] = {}
    json_dict["stat"][s]["info_tag"] = int(inf[0])
    json_dict["stat"][s]["total_tag"] = int(inf[1])
    json_dict["stat"][s]["frip"] = float(int(inf[0]))/int(inf[1])
    json_dump(json_dict)


def FRiP(conf):  # FRiP
    """
    Fraction of Reads in Peaks regions at 4M reads level
    For example: 2 treat, 2 control
    modify: without down sampling read peaks calling, use merged peaks for comparison
    """
    ## use merged peaks for evaluation after removing chrM reads 

    t = conf['prefix_path']
    c = """fr=$(bedtools intersect -f {param[p]} -wa -u -abam {input[reads]} -b {input[peaks]} -bed | wc -l)
total=$(samtools flagstat {input[reads]} | head -1 | cut -d" " -f1)
echo $fr,$total > {output[frip]}
""".format(tool="intersectBed",
input={"reads": conf['reads'], "peaks": conf['peak']}, #reads if conf.down else t+"_nochrM.bam", "peaks": conf.prefix + "_sort_peaks.narrowPeak" if conf.get("macs2", "type") in ["both", "narrow"] else conf.prefix + "_b_sort_peaks.broadPeak"},
output={"frip": t + ".frip"},
param={"p": "1E-9"},
name="FRiP score")  
    os.system(c)
    #c.invoke()
    ## QC prefix_path   
    stat_frip(conf)


def sampling(conf): # call fastq_sampling
    """
    prepare sampling fastq files for library contamination and fastqc
    rand: the number of random selected fastq reads
    use lh3's https://github.com/lh3/seqtk/ to sample fastq and fastq.gz
    """
    ## samtools sampling
    ## add judge condition
    c = """{tool} view -h {input[bamfile]} |grep -v chrMT > {input[samfile]}
count=$({tool} view -Sc {input[samfile]})
## judge mapped reads number less than sampling number
if [ $count -le {param[random_number]} ]
then
    ln -f {input[samfile]} {input[samfile]}.{param[random_number]}
    {tool} view -bS {input[samfile]}.{param[random_number]} > {output[samp]}
else
    python sampling_pe_sam.py {input[samfile]} {param[random_number]} {input[End]}
    {tool} view -bS {input[samfile]}.{param[random_number]} > {output[samp]}
fi
""".format(tool = "samtools",
input={"bamfile": conf['bam'], 'samfile': conf['sam'], 'End':conf['pair']},
output={"samp": conf['reads']},
param={"random_number": 4000000},
name = "sampling bam")
    print(c)
    os.system(c)

    #c.invoke()
def clean_up(conf):
    """remove the templete files which generated during the calculation
    """
    if conf['sam']:
        tmplete_files = {"samfile":conf['sam'],
            "samfile_sampling":conf['sam'] +'.4000000',
            "samplingBam":conf['reads'],
            "fripTemp":conf['prefix_path'] +".frip",
            "pbcTemp":conf['prefix_path'] +".pbc",
            "hist":conf['prefix_path'] +".hist",
            'DHS':conf['prefix_path']+".enrich.dhs",
            "topPeak":conf['prefix_path']+'_peak.5000',
            "enrich":conf['prefix_path']+'.enrich.meta',
            "enrich_dhs":conf['prefix_path']+'.enrich_meta.dhs'
            }
    else:
        tmplete_files = {"fripTemp":conf['prefix_path'] +".frip",
            "pbcTemp":conf['prefix_path'] +".pbc",
            "hist":conf['prefix_path'] +".hist",
            'DHS':conf['prefix_path']+".enrich.dhs",
            "topPeak":conf['prefix_path']+'_peak.5000'
            }
    cmd = 'rm -rf {files}'.format(files=' '.join([x for x in tmplete_files.values() if os.path.exists(x)]))
    os.system(cmd)

def parse_args(args=None):
    """
    parse input arguments
    """
    parser = FriendlyArgumentParser(description=__doc__)
    parser.add_argument('-V', '--version', action='version',
                        version="%(prog)s (code version 2.0, db version 1.0)")
    sub_parsers = parser.add_subparsers(help="sub-command help", dest="sub_command")
    parser_run = sub_parsers.add_parser("run", help="run pipeline to calculater FRiP scroe",
                                        description="ChiLin-run: run pipeline to calculater FRiP scroe")
    parser_run.add_argument("-cf", "--config", dest="config", required=True,
                            help="the path of config file")
    parser_run.add_argument("-n", "--name", dest="ID", required=True,
                            help="the name will be used on json file, usually it is the ID of cistromeDC")
    parser_run.add_argument("-b", "--BAM", dest="BAM_file", required=False,
                            help="the BAM file for a ChIP-seq sample")
    parser_run.add_argument("-p", "--peak", dest="peak_file", required=False,
                            help="the narrow or broad peak file generated by MACS")
    parser_run.add_argument("-O", "--output", dest="output_directory", required=True,
                            help="output directory for saving FRiP json file")
    parser_run.add_argument("-t", "--type", dest="peak_type", required=False,
                            help="the type will be used when calling peak by MACS, including broad, narrow, both")    
    parser_run.add_argument("-s", "--species", dest="species", required=False,
                            help="what species? [hs, mm], hs means human, mm means mouse")
    parser_run.add_argument("--pe", dest="pe", action="store_true", default=False,
                       help="default single end mode, turn on for pair end sequencing")  
    parser_run.add_argument("--frip", dest="frip", action="store_true", default=False,
                       help="default do not run frip")  
    parser_run.add_argument("--pbc", dest="pbc", action="store_true", default=False,
                       help="default do not run PBC") 
    parser_run.add_argument("--dhs", dest="dhs", action="store_true", default=False,
                       help="default do not run DHS intersection ratio")
    parser_run.add_argument("--enrich", dest="enrich", action="store_true", default=False,
                       help="default do not run meta enrichment in promoter, exon, intron region")  
    return(parser.parse_args(args), parser)

def main(args=None):
    """
    connect with args module, and generate a config using dictinary,
    so that it will be easy to get each items.
    """
    args, parser = parse_args(args)

    os.system("echo start++++++%s"%time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time())))

    cf.read(args.config)
    config = cf._sections
    config = _clean_config(config)

    # generate config
    if args.sub_command == 'run':
        if args.BAM_file:
            samFile = os.path.join(args.output_directory, os.path.basename(args.BAM_file).replace('bam', 'sam'))
            readsFile = os.path.join(args.output_directory, os.path.basename(args.BAM_file) + '_4M.bam')
        else:
            samFile = None
            readsFile = None
        conf = {"frip":args.frip,
        "PBC":args.pbc,
        "pair" : args.pe,
        "bam" : args.BAM_file,
        "sam" : samFile,
        "reads" : readsFile,
        "peak" : args.peak_file,
        "prefix" : args.ID,
        "output" : args.output_directory,
        "type" : args.peak_type,
        "prefix_path" : os.path.join(args.output_directory, args.ID),
        "species" : args.species,
        'dhs' : args.dhs,
        'enrich' : args.enrich,
        'DHS_hg38': config['hg38']['dhs'],
        'DHS_mm10': config['mm10']['dhs'],
        'promoter_hg38':config['hg38']['promoter'],
        'promoter_mm10':config['mm10']['promoter'],
        'exon_hg38':config['hg38']['exon'],
        'exon_mm10':config['mm10']['exon']
        }
    print(conf)
    # start to excute
    if conf['dhs'] and conf['peak']:
        has_dhs(conf)
    if conf['bam']:
        if os.path.exists(conf['reads']):
            os.system("echo +++++++sampling result existed")
        else:
            sampling(conf)
        if conf['frip']:
            print "+++++Frip:"
            FRiP(conf)
            stat_frip(conf)
        if conf['PBC']:
            print "++++++PBC:"
            PBC(conf)
            stat_pbc(conf)
        if conf['enrich']:
            print "+++++++meta enrich:"
            _enrich_meta(conf)

    os.system("echo clean up ")
    clean_up(conf)
    os.system("echo +++++wolkflow successfull++++++++%s"%time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time())))

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        sys.stderr("User interrupt:) \n")















#2017-06-26, Rongbin Zheng
#use vdb-validate to verify SRA file which download from GEO, and return signal to shell for further judgement

import os,sys
import commands

fsra = sys.argv[1] # the path of sra file for checking

def main(fsra):
	res = []
	try:
		f = os.path.basename(fsra)
		cmd_output = commands.getoutput('vdb-validate %s'%fsra)
		if ("'%s' is consistent"%f in cmd_output) or ("'%s' metadata: md5 ok"%f in cmd_output):
			print("sra file OK: %s"%fsra)
			return(True)
		else:
			print("sra file problem: %s"%fsra)
			return(False)

	except:
		return(False)

	return(False)

if __name__ == '__main__':
	i = main(fsra)
	if i:
		sys.exit(0) # check sra ok
	else:
		sys.exit(1) # check sra fail



	


import os, sys
# from urllib import request as urllib2
import urllib2
import re
import argparse
from modules.writeSbatch import _writeSbatch

# transfer the sra files to fastqfiles and cat the multiple fastq files
def catfastq_prefectch(config, main_path, gsm,srx_infor,srr,lay_type):
    """download fastq using prefetch
    """
    cmd = ''
    cat_file1 = ''
    cat_file2 = ''
    #srx_infor = config['GEO']['ftp'] # the ftp for GEO sra file
    tmpsrafastqFolder = os.path.join(main_path, config['odysseyFolders']['tmp_sra_fastq'])
    finalfastqFolder = os.path.join(main_path, config['odysseyFolders']['fastq_final'])
    recordFolder = os.path.join(main_path,config['odysseyFolders']['record'])
    
    if lay_type == 'SINGLE':
        for i in range(len(srr)):
            #srr[i] = srr[i][1:-12]
            #ftp = srx_infor + '//' + srr[i] + '/' + srr[i] + '.sra'
            #ftp = srx_infor + '/' + srr[i][:6] + '/' + srr[i] +'/' + srr[i]+ '.sra'
            # download the sra files and transmit them into fastq files
            fsra = '%s/%s_%s.sra'%(tmpsrafastqFolder, gsm, i+1)
            #cmd = cmd + 'wget %s -O %s \n'%(ftp, fsra) 
            cmd = cmd+'prefetch {srrId} -o {fsra}\n'.format(srrId = srr[i], fsra = fsra)
            cmd = cmd + 'python '+os.path.join(main_path+'/modules/checkSRA.py')+' %s\nif [ $? == 1 ];then\necho %s >> %s/SRAchecking_fail.xls\nexit\nfi\n'%(fsra, gsm+','+ftp+','+fsra, recordFolder)
            cmd = cmd + 'fastq-dump %s/%s_%s.sra -O %s \n'%(tmpsrafastqFolder, gsm,i+1, tmpsrafastqFolder) 
            cat_file1 = cat_file1 + '%s/%s_%s.fastq '%(tmpsrafastqFolder, gsm,i+1) 
        cmd = cmd + 'cat %s> %s/%s.fastq \n'%(cat_file1,finalfastqFolder,gsm) 
        cmd = cmd + 'rm %s/%s_* \n'%(tmpsrafastqFolder, gsm)
    elif lay_type == 'PAIRED':
        for i in range(len(srr)):
            #srr[i] = srr[i][1:-12]
            #ftp = srx_infor + '/' + srr[i][:6] + '/' + srr[i] +'/' + srr[i]+ '.sra'
            fsra = '%s/%s_%s.sra'%(tmpsrafastqFolder, gsm, i+1)
            #cmd = cmd + 'wget %s -O %s \n'%(ftp, fsra) 
            cmd = cmd+'prefetch {srrId} -o {fsra}\n'.format(srrId = srr[i], fsra = fsra)
            cmd = cmd + 'python '+os.path.join(main_path+'/modules/checkSRA.py')+' %s\nif [ $? == 1 ];then\necho %s >> %s/SRAchecking_fail.xls\nexit\nfi\n'%(fsra, gsm+','+ftp+','+fsra, recordFolder)
            cmd = cmd + 'fastq-dump --split-files %s/%s_%s.sra -O %s \n'%(tmpsrafastqFolder, gsm,i+1, tmpsrafastqFolder) 
            cat_file1 = cat_file1 + '%s/%s_%s_1.fastq '%(tmpsrafastqFolder, gsm,i+1)
            cat_file2 = cat_file2 + '%s/%s_%s_2.fastq '%(tmpsrafastqFolder, gsm,i+1)
        cmd = cmd + 'cat %s> %s/%s.fastq_R1 \n'%(cat_file1, finalfastqFolder,gsm) 
        cmd = cmd + 'cat %s> %s/%s.fastq_R2 \n'%(cat_file2, finalfastqFolder,gsm) 
        cmd = cmd + 'rm %s/%s_* \n'%(tmpsrafastqFolder, gsm)
    return(cmd)

def catfastq(config, main_path, gsm,srx_infor,srr,lay_type):
    """download using wget and ftp link
    """
    cmd = ''
    cat_file1 = ''
    cat_file2 = ''
    srx_infor = config['GEO']['ftp'] # the ftp for GEO sra file
    tmpsrafastqFolder = os.path.join(main_path, config['odysseyFolders']['tmp_sra_fastq'])
    finalfastqFolder = os.path.join(main_path, config['odysseyFolders']['fastq_final'])
    recordFolder = os.path.join(main_path,config['odysseyFolders']['record'])
    
    if lay_type == 'SINGLE':
        for i in range(len(srr)):
            ftp = srx_infor + '/' + srr[i][:6] + '/' + srr[i] +'/' + srr[i]+ '.sra'
            # download the sra files and transmit them into fastq files
            fsra = '%s/%s_%s.sra'%(tmpsrafastqFolder, gsm, i+1)
            cmd = cmd + 'wget %s -O %s \n'%(ftp, fsra) 
            cmd = cmd + 'python '+os.path.join(main_path+'/modules/checkSRA.py')+' %s\nif [ $? == 1 ];then\necho %s >> %s/SRAchecking_fail.xls\nexit\nfi\n'%(fsra, gsm+','+ftp+','+fsra, recordFolder)
            cmd = cmd + 'fastq-dump %s/%s_%s.sra -O %s \n'%(tmpsrafastqFolder, gsm,i+1, tmpsrafastqFolder) 
            cat_file1 = cat_file1 + '%s/%s_%s.fastq '%(tmpsrafastqFolder, gsm,i+1) 
        cmd = cmd + 'cat %s> %s/%s.fastq \n'%(cat_file1,finalfastqFolder,gsm) 
        cmd = cmd + 'rm %s/%s_* \n'%(tmpsrafastqFolder, gsm)
    elif lay_type == 'PAIRED':
        for i in range(len(srr)):
            ftp = srx_infor + '/' + srr[i][:6] + '/' + srr[i] +'/' + srr[i]+ '.sra'
            fsra = '%s/%s_%s.sra'%(tmpsrafastqFolder, gsm, i+1)
            cmd = cmd + 'wget %s -O %s \n'%(ftp, fsra) 
            cmd = cmd + 'python '+os.path.join(main_path+'/modules/checkSRA.py')+' %s\nif [ $? == 1 ];then\necho %s >> %s/SRAchecking_fail.xls\nexit\nfi\n'%(fsra, gsm+','+ftp+','+fsra, recordFolder)
            cmd = cmd + 'fastq-dump --split-files %s/%s_%s.sra -O %s \n'%(tmpsrafastqFolder, gsm,i+1, tmpsrafastqFolder) 
            cat_file1 = cat_file1 + '%s/%s_%s_1.fastq '%(tmpsrafastqFolder, gsm,i+1)
            cat_file2 = cat_file2 + '%s/%s_%s_2.fastq '%(tmpsrafastqFolder, gsm,i+1)
        cmd = cmd + 'cat %s> %s/%s.fastq_R1 \n'%(cat_file1, finalfastqFolder,gsm) 
        cmd = cmd + 'cat %s> %s/%s.fastq_R2 \n'%(cat_file2, finalfastqFolder,gsm) 
        cmd = cmd + 'rm %s/%s_* \n'%(tmpsrafastqFolder, gsm)
    return(cmd)
    
    
def LinkPlusDownload(config, DCID, gsm, species,factor, path, refresh = True):
    """get link sequece type and return the path of sbatch file
    """
    tmpsrafastqFolder = os.path.join(path, config['odysseyFolders']['tmp_sra_fastq'])
    finalfastqFolder = os.path.join(path, config['odysseyFolders']['fastq_final'])
    recordFolder = os.path.join(path, config['odysseyFolders']['record'])
    sbatchFolder = os.path.join(path, config['odysseyFolders']['sbatch_files'])
    tmpchilinresultFolder = os.path.join(path, config['odysseyFolders']['tmp_chilin_result'])
    finalchilinresultFolder = os.path.join(path, config['odysseyFolders']['final_chilin_result'])

    sbatchTitlePath = config['odysseyParameter']['sbatchTitle']
    sbatchTitle = open(sbatchTitlePath).read()

    os.system('echo %s'%gsm)
    if species in ['Homo sapiens', 'hg38', 'hg19']:
        species = 'hg38'
    elif species in ['Mus musculus', 'mm10', 'mm9']:
        species = 'mm10'
        # gsm_url is the link of the input GSM data
    try:
        gsm_url = 'http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=%s'%gsm
        gsm_handler = urllib2.urlopen(gsm_url)
        gsm_html = gsm_handler.read()
        gsm_html = gsm_html.decode('utf-8')
        # get the ftp location of SRX file and the SRX id
        srx_regexp = re.compile('https://www.ncbi.nlm.nih.gov/sra\S*"')
        srx_infor = srx_regexp.search(gsm_html)
        if srx_infor:
            srx = srx_infor.group().rstrip('"').lstrip('https://www.ncbi.nlm.nih.gov/sra?term=')
        else:
            os.system('echo "no srx of %s"'%gsm)
            failure = '%s/failure_gsm_nolinkinfo.xls'%recordFolder
            exist = open(failure).readlines()
            if os.path.exists(failure):
                exist = open(failure).readlines()
            else:
                exist=''
            if DCID+'\t'+species+'\t'+factor+'\t'+gsm+'\n' not in exist:
                out = open('%s/failure_gsm_nolinkinfo.xls'%recordFolder, 'a')
                out.write(DCID+'\t'+species+'\t'+factor+'\t'+gsm+'\n')
                out.close()
            return(None, None)
        # get the SRR id('>SRR1588518</a></td><td') and find the type of layout
        srx_url = 'http://www.ncbi.nlm.nih.gov/sra?term=%s'%srx
        srx_handler = urllib2.urlopen(srx_url)
        srx_html = srx_handler.read()
        srx_html = srx_html.decode('utf-8')
        # find the layout type (<div>Layout: <span>SINGLE</span>)
        lay_infor = re.compile('<div>Layout: <span>.{6}</span>')
        lay_type = lay_infor.search(srx_html)
        lay_type = lay_type.group()
        lay_type = lay_type[-13:-7]
        # get the srr id and download the sra files
        #srr_regexp = re.compile('>SRR[0-9]*</a></td><td')
        srr_regexp = re.compile('//trace.ncbi.nlm.nih.gov/Traces/sra/\S*">') # DRR098306 or SRR6792712 type of ID
        srr = [x.rstrip('">').lstrip('//trace.ncbi.nlm.nih.gov/Traces/sra/?run=') for x in srr_regexp.findall(srx_html)]
        #srr = srr_regexp.findall(srx_html)
        if lay_type == 'SINGLE':
            fastq_file = '%s/%s.fastq'%(finalfastqFolder,gsm)
             # single.append(gsm)
             # print >>SingleEnd_gsm, gsm
        elif lay_type == 'PAIRED':
            fastq_file = '%s/%s.fastq_R1,%s/%s.fastq_R2'%(finalfastqFolder,gsm,finalfastqFolder,gsm)
        else:
            print('+++neither PAIRED nor SINGLE end sequencing: %s+++'%gsm)
            return(None, None)

        sbatch_file = open("%s/%s_down.sbatch"%(sbatchFolder,DCID),"w")
        core, time, memery, job = 1, 1000, 3000, DCID+'_down'
        sbatchTitle_new = _writeSbatch(config, sbatchTitle, core, time, memery, job)
        sbatch_file.write(sbatchTitle_new)
        
        def checkFastq(X):
            X = X.split(',')
            for q in X:
                if not os.path.exists(q):
                    return(False)
                elif os.path.exists(q) and (os.path.getsize(q) == 0):
                    return(False)
                else:
                    pass
            return(True) # return correct fastq flag
        # check whether need to download fastq again, by fastq existence and the size ?= 0
        fastCmd=''
        if refresh: # new samples need to download fastq
            fastCmd = catfastq(config, path, gsm,srx_infor,srr,lay_type)
            sbatch_file.write('\n'+fastCmd+'\n')
        elif (not refresh) and (not checkFastq(fastq_file)):# rerun samples but fastq is not correct.
            fastCmd = catfastq(config, path, gsm,srx_infor,srr,lay_type)
            sbatch_file.write('\n'+fastCmd+'\n')
        else:
            pass      
        # get chilin command and the sbatch file, and submit
        if not refresh:
            sbatch_file.write('rm -r %s/dataset%s \n'%(tmpchilinresultFolder,DCID))# remove the old result
        # generate chilin command, and the generated chilin sbatch job will be submitted in modules/chilinComand.py
        if config['chilin']['broad'] != '':
            sbatch_file.write('python '+os.path.join(path,'modules/chilinComand.py')+' -s odyssey -c %s -g %s -spe %s -factor %s -end %s -fastq %s -out %s/dataset%s -broad %s\n'%(config['configPath']['path'],gsm,species,factor.replace(' ', ''),lay_type,fastq_file,tmpchilinresultFolder,DCID, config['chilin']['broad']))
        else:
            sbatch_file.write('python '+os.path.join(path, 'modules/chilinComand.py')+' -s odyssey -c %s -g %s -spe %s -factor %s -end %s -fastq %s -out %s/dataset%s\n'%(config['configPath']['path'],gsm,species,factor.replace(' ', ''),lay_type,fastq_file,tmpchilinresultFolder,DCID))
        sbatch_file.close()
        if refresh:
            gsm_info = open('%s/gsm_info_cmd.xls'%recordFolder, 'a') # record the sample infomation
            receive_file = open('%s/recivedSamples.xls'%os.path.join(path, config['odysseyFolders']['syncDaisy']),'a')
            gsm_info.write('%s\t%s\t%s\t%s\t%s\t%s\t%s\n'%(DCID, species, factor, gsm, lay_type, fastq_file, fastCmd.replace('\n',',')))
            gsm_info.close()
            receive_file.write(DCID+'\t'+species+'\t'+factor+'\t'+gsm+'\n')
            receive_file.close()
        return('%s/%s_down.sbatch'%(sbatchFolder,DCID),lay_type)
    except:
        print(sys.exc_info())
    #    sys.exit(0)
        failure = '%s/failure_gsm_nolinkinfo.xls'%recordFolder
        if os.path.exists(failure):
            exist = open(failure).readlines()
        else:
            exist = ''
        if DCID+'\t'+species+'\t'+factor+'\t'+gsm+'\n' not in exist:
            out = open('%s/failure_gsm_nolinkinfo.xls'%recordFolder, 'a')
            out.write(DCID+'\t'+species+'\t'+factor+'\t'+gsm+'\n')
            out.close()
        return(None, None)



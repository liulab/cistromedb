import os, sys
# from urllib import request as urllib2
import urllib2
import re
import argparse
from modules.writeSbatch import _writeSbatch

def _getcmd(config, DCID, fastqlinks, ftp, main_path):
    """get fastq produce command of encode
    """
    cmd = ''
    cat_file1 = ''
    cat_file2 = ''
    tmpsrafastqFolder = os.path.join(main_path, config['odysseyFolders']['tmp_sra_fastq'])
    finalfastqFolder = os.path.join(main_path, config['odysseyFolders']['fastq_final'])
    recordFolder = os.path.join(main_path,config['odysseyFolders']['record'])
    
    finalfastqfile = os.path.join(finalfastqFolder, DCID+'.fastq')
    if ';' in fastqlinks:
        lay_type = 'PAIRED'
        end1 = fastqlinks.split(';')[0].split(',') # first end fastqs
        end2 = fastqlinks.split(';')[1].split(',') # first end fastqs
        if len(end1) != len(end2):
            return(None, None, lay_type)
        for i in range(len(end1)): # some samples have different run, have to cat
            fname1 = os.path.join(tmpsrafastqFolder, DCID+'_'+str(i+1)+'.fastq.gz')
            cmd += 'wget {ftp}/{link} -O {fsave}\ngunzip {fsave2}\n'.format(ftp=ftp, link = end1[i], fsave=fname1, fsave2=fname1)
            fname1 = fname1.rstrip('.gz')
            cat_file1 = fname1+' '
        cat_file1 = 'cat %s > %s\n'%(cat_file1, finalfastqfile+'_R1')
        for j in range(len(end2)):
            fname2 = os.path.join(tmpsrafastqFolder, DCID+'_'+str(i+1))
            cmd += 'wget {ftp}/{link} -O {fsave}\ngunzip {fsave2}\n'.format(ftp=ftp, link = end2[i], fsave=fname2, fsave2=fname2)
            fname2 = fname2.rstrip('.gz')
            cat_file2 = fname2+' '
        cat_file2 = 'cat %s > %s\n'%(cat_file2, finalfastqfile+'_R2')
        cmd = cmd + cat_file1 + cat_file2
        finalfastqfile = finalfastqfile+'_R1,'+finalfastqfile+'_R2'
    else:
        lay_type = 'SINGLE'
        end1 = fastqlinks.split(',')
        for i in range(len(end1)): # some samples have different run, have to cat
            fname1 = os.path.join(tmpsrafastqFolder, DCID+'_'+str(i+1)+'.fastq.gz')
            cmd += 'wget {ftp}/{link} -O {fsave}\ngunzip {fsave2}\n'.format(ftp=ftp, link = end1[i], fsave=fname1, fsave2=fname1)
            fname1 = fname1.rstrip('.gz')
            cat_file1 = fname1+' '
        cat_file1 = 'cat %s > %s\n'%(cat_file1, finalfastqfile)  
        cmd = cmd + cat_file1
    return(cmd, lay_type, finalfastqfile)
        
    
def LinkPlusDownload(config, DCID, fastqlinks, species, factor, path, refresh = True):
    """ based on information, get fastq download link and command
    """
    tmpsrafastqFolder = os.path.join(path, config['odysseyFolders']['tmp_sra_fastq'])
    finalfastqFolder = os.path.join(path, config['odysseyFolders']['fastq_final'])
    recordFolder = os.path.join(path, config['odysseyFolders']['record'])
    sbatchFolder = os.path.join(path, config['odysseyFolders']['sbatch_files'])
    tmpchilinresultFolder = os.path.join(path, config['odysseyFolders']['tmp_chilin_result'])
    finalchilinresultFolder = os.path.join(path, config['odysseyFolders']['final_chilin_result'])
    ftp = config['ENCODE']['ftp']
    
    sbatchTitlePath = config['odysseyParameter']['sbatchTitle']
    sbatchTitle = open(sbatchTitlePath).read()
    
    if species in ['Homo sapiens', 'hg38', 'hg19']:
        species = 'hg38'
    elif species in ['Mus musculus', 'mm10', 'mm9']:
        species = 'mm10'
    else:
        species = 'hg38'
    # generate command
    fastCmd, lay_type, fastq_file = _getcmd(config, DCID, fastqlinks, ftp, path)
    if not fastCmd:
        failure = '%s/failure_gsm_nolinkinfo.xls'%recordFolder
        if os.path.exists(failure):
            exist = open(failure).readlines()
        else:
            exist = ''
        if DCID+'\t'+species+'\t'+factor+'\t'+DCID+'\n' not in exist:
            out = open('%s/failure_gsm_nolinkinfo.xls'%recordFolder, 'a')
            out.write(DCID+'\t'+species+'\t'+factor+'\t'+DCID+'\n')
            out.close()
        return(None, None)
    sbatch_file = open("%s/%s_down.sbatch"%(sbatchFolder,DCID),"w")
    core, time, memery, job = 1, 1000, 3000, DCID+'_down'
    sbatchTitle_new = _writeSbatch(config, sbatchTitle, core, time, memery, job)
    sbatch_file.write(sbatchTitle_new)
    def checkFastq(X):
            X = X.split(',')
            for q in X:
                if not os.path.exists(q):
                    return(False)
                elif os.path.exists(q) and (os.path.getsize(q) == 0):
                    return(False)
                else:
                    pass
            return(True) # return correct fastq flag
    if refresh: # new samples need to download fastq
        sbatch_file.write('\n'+fastCmd+'\n')
    elif (not refresh) and (not checkFastq(fastq_file)):# rerun samples but fastq is not correct.
        sbatch_file.write('\n'+fastCmd+'\n')
    else:
        pass 
    if not refresh:
        sbatch_file.write('rm -r %s/dataset%s \n'%(tmpchilinresultFolder,DCID))# remove the old result
    if config['chilin']['broad'] != '':
        sbatch_file.write('python '+os.path.join(path,'modules/chilinComand.py')+' -s odyssey -c %s -g %s -spe %s -factor %s -end %s -fastq %s -out %s/dataset%s -broad %s\n'%(config['configPath']['path'],DCID,species,factor.replace(' ', ''),lay_type,fastq_file,tmpchilinresultFolder,DCID, config['chilin']['broad']))
    else:
        sbatch_file.write('python '+os.path.join(path, 'modules/chilinComand.py')+' -s odyssey -c %s -g %s -spe %s -factor %s -end %s -fastq %s -out %s/dataset%s\n'%(config['configPath']['path'],DCID,species,factor.replace(' ', ''),lay_type,fastq_file,tmpchilinresultFolder,DCID))
        sbatch_file.close()
    if refresh:
        gsm_info = open('%s/gsm_info_cmd.xls'%recordFolder, 'a') # record the sample infomation
        receive_file = open('%s/recivedSamples.xls'%os.path.join(path, config['odysseyFolders']['syncDaisy']),'a')
        gsm_info.write('%s\t%s\t%s\t%s\t%s\t%s\t%s\n'%(DCID, species, factor, fastqlinks, lay_type, fastq_file, fastCmd.replace('\n',',')))
        gsm_info.close()
        receive_file.write(DCID+'\t'+species+'\t'+factor+'\t'+DCID+'\n')
        receive_file.close()
    return('%s/%s_down.sbatch'%(sbatchFolder,DCID),lay_type)
    
    
    
    
    
    
    
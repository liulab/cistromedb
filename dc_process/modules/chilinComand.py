import os
import argparse
import re
import configparser 

global cf;cf = configparser.ConfigParser()
cf.optionxform=str
from writeSbatch import _writeSbatch

def _clean_config(config):
	# remove the annotation:
	for firstLevel in config.keys():
		for secondLevel in config[firstLevel]:
			if '#' in config[firstLevel][secondLevel]:
				config[firstLevel][secondLevel] = config[firstLevel][secondLevel][:config[firstLevel][secondLevel].index('#')-1].rstrip()
	return(config)

###need to further change for any mistakes
def chilinCommand(server, config, gsm, species,factor,output,fastq_file,lay_type,broad):
    '''
    generate chilin command for every gsm id samples, the gsm here means a unique id, both gsm and dcid are ok
    '''
    # get the main workplace path
    if 'tmp_chilin_result/dataset' in output:
        path, DCID = output.split('tmp_chilin_result/dataset')
        DCID = DCID.strip().strip('_')
    else:
        path = os.getcwd().replace('modules', '')
        DCID = gsm
    try:
        if re.match(r"^H\d\w*\d",factor):
             factortype="histone"
             motif_option=" --skip 12 "
             extension = 100
             #       continue ## focus on TF first
        elif factor == "DNase" or factor == 'ATAC-seq':
             factortype="dnase"
             motif_option=" --skip 12 "
             extension = 100
            #        continue ## focus on TF first
        else:
             factortype="tf"
             motif_option=" "
             extension = 146
                #    continue
        ftype = factor.strip().lower() 
        if ftype in broad:
            peaktype = "broad"
        else:
            peaktype = "narrow"
        fastq_one = fastq_file.split(',')[0]
        if os.path.exists(fastq_one):
            fastq_size = os.path.getsize(fastq_one)/1024/1024
            # if fastq_size == 0:
            #     fail_file.write(gsm+' size error\n')
            if fastq_size > 7000:
                time1= "2800"
                time2 = "3600"
                fmemory1= "20000"
                fmemory2="22222"
            elif fastq_size > 3500:
                time1= "1200"
                time2 = "1800"
                fmemory1= "15000"
                fmemory2="16000"
            elif fastq_size > 2000:
                time1= "1000"
                time2 = "1500"
                fmemory1= "11000"
                fmemory2="13200"
            elif fastq_size > 1000:
                time1= "800"
                time2 = "1200"
                fmemory1= "10000"
                fmemory2="12000"
            elif fastq_size > 500:
                time1= "500"
                time2 = "600"
                fmemory1= "8900"
                fmemory2="10680"
            else:
                time1= "400"
                time2 = "500"
                fmemory1= "7900"
                fmemory2="9480"

        if lay_type == 'SINGLE':
            core, time, memery, job = 8, time1, fmemory1, DCID+'_run'
            cmd = 'chilin simple -u rongbin -s %s --threads 8 -i %s -o %s -t %s -p %s -r %s %s \n'%(species, DCID, output, fastq_file, peaktype, factortype, motif_option)
        else:
            core, time, memery, job = 8, time2, fmemory2, DCID+'_run'
           # step1 = 'samtools rmdup -S %s'%(bamfile + ' ' + bamfile.replace(DCID, 'dpt_'+DCID)) # use this step to remove the duplication in the original bam files.
            cmd = 'chilin simple --pe -u rongbin -s %s --threads 12 -i %s -o %s -t %s -p %s -r %s %s \n'%(species, DCID, output, fastq_file, peaktype, factortype, motif_option)
        if server == 'odyssey':
            sbatchTitlePath = config['odysseyParameter']['sbatchTitle']
            recordFolder = os.path.join(path, config['odysseyFolders']['record'])
            sbatchFolder = os.path.join(path, config['odysseyFolders']['sbatch_files'])
            finalchilinresultFolder = os.path.join(path, config['odysseyFolders']['final_chilin_result'])
            sbatchTitle = open(sbatchTitlePath).read()
            sbatch_file = open("%s/%s_run.sbatch"%(sbatchFolder, DCID), "w")
            sbatchTitle_new = _writeSbatch(config, sbatchTitle, core, time, memery, job)
            sbatch_file.write(sbatchTitle_new)
            sbatch_file.write('\n'+cmd)
            sbatch_file.write('python '+os.path.join(path+'/modules/checkChilin.py')+' -cf %s -d %s -p %s -c "%s"\n'%(config['configPath']['path'], output, finalchilinresultFolder, cmd.rstrip()))
            sbatch_file.close()
            if os.path.exists("%s/%s_run.sbatch"%(sbatchFolder, DCID)):
                os.system('sbatch %s/%s_run.sbatch'%(sbatchFolder, DCID))
            else:
                print('run_sbatch file is miss')
        else:
            sh = open(path+"/%s_run.sh"%DCID, "w")
            sh.write(cmd)
            sh.write('python '+os.path.join(path+'/modules/checkChilin.py')+' -cf -d %s -p %s -c "%s"\n'%(config['configPath']['path'], output, finalchilinresultFolder, cmd.rstrip()))
            sh.close()
        #check bw file and reduce the resolution into 8bp using deeptools
        #bwfile = os.path.join(output, '/attic/'+DCID+'_treat.bw') # get the putative bam file path
        #step2 = 'samtools index %s'%(bamfile.replace(gsm, 'dpt_'+gsm)) # get index of bam file
        #step3 = 'bamCoverage -b %s -e %d --binSize 8 --scaleFactor 0.008 --normalizeUsingRPKM -o %s'%(bamfile.replace(gsm, 'dpt_'+gsm), extension, bamfile.replace(gsm, 'dpt_'+gsm))
        #cmd_bw = '\nif [ ! -x "%s" ]; then\n%sfi\n'%(bamfile, step1 + '\n' + step2 + '\n' + step3 + '\n')
        #sbatch_file.write(cmd_bw+'python checkChilin.py -d %s -p %s/final_chilin_result -c "%s"\n'%(output, path, cmd.rstrip()))
        #sbatch_file.write('python checkChilin.py -d %s -p %s/final_chilin_result -c "%s"\n'%(output, path, cmd.rstrip()))
        #sbatch_file.close()
    except:
        recordFolder = os.path.join(path, config['odysseyFolders']['record'])
        print('+++Trouble in generate the chilin command, this sample will be recorded into {0}/fail_generate_chilin.xls'.format(recordFolder))
        fail_file = open(recordFolder+'/fail_generate_chilin.xls','a')
        fail_file.write(DCID+'\t'+species+'\t'+factor+'\t'+gsm+'\t'+lay_type+'\t'+fastq_file)
        fail_file.close()

def main():
    try:
        parser = argparse.ArgumentParser(description="""get chilin command, and submit""")
        parser.add_argument( '-s', dest='server', type=str, required=True, help='which type of server from [daisy, odyssey]')
        parser.add_argument( '-c', dest='config', type=str, required=True, help='the path of config file')
        parser.add_argument( '-g', dest='gsmID', type=str, required=True, help='gsm ID of sample')
        parser.add_argument( '-spe', dest='specie', type=str, required=True, help='specie of sample')
        parser.add_argument( '-factor', dest='factortype', type=str, required=True, help='factor type of sample')
        parser.add_argument( '-end', dest='endtype', type=str, required=True, help='sequencing end type of sample')
        parser.add_argument( '-fastq', dest='fastqfile', type=str, required=True, help='path of fastq files of sample')
        parser.add_argument( '-out', dest='outputpath', type=str, required=True, help='output path of Chilin result')
        parser.add_argument( '-broad', dest='broad', type=str, required=False, help='optional, give the factor that will call broad peak, default is h3k27me3,h3k36me3,h3k9me3')

        args = parser.parse_args()
        # path = args.path
        # if not path:
        #     path = './'
        broad = args.broad
        if not broad:
            broad=['h3k27me3', 'h3k36me3', 'h3k9me3']
        elif broad and ',' not in broad:
            broad=[broad.lower()]
        elif broad and ',' in broad:
            broad=[x.lower() for x in broad.split(',')]
        else:
            broad=['h3k27me3', 'h3k36me3', 'h3k9me3']

        cf.read(args.config)
        config = cf._sections
        config=_clean_config(config)
        config['configPath'] = {}
        config['configPath']['path'] = args.config

        chilinCommand(args.server, config, args.gsmID, args.specie,args.factortype,args.outputpath,args.fastqfile,args.endtype, broad)
    except KeyboardInterrupt:
        sys.stderr.write("User interrupted me T_T \n")
        sys.exit(0)


if __name__ == '__main__':
    main()



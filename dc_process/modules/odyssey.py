import os,sys
import datetime, time
import commands
import configparser 
import argparse
from optparse import OptionParser
from modules import DC_dataProcess # the function togethered file, LinkPlusDownload, chilinCommand, checkChilin
from modules import encode
"""sync odyssey with daisy by transfering the parser output file, then get new samples which need to be processed by comparing with recived sample.
"""

def _submit_chilin(download_submited, mjobs, end = False):
	"""get chilin submiting based on download submit samples, check completeness for each downloading sample and submit
	chilin once downloading done. download_submited list contains all RUNNING or PENDING samples sbatch file path.
	"""
	leftJobs = checkCurrentJobs(int(mjobs))# result from the function
	download_submited_still = [] # record samples do not submit chilin in this function
	for d in download_submited:
		ID = os.path.basename(d).replace('_down.sbatch', '')
		status = commands.getoutput('sacct|grep %s_down'%ID)
		#print(status)
		if status == '': # this may because odyssey remove the COMPLETED record on start time of a day
			status = 'COMPLETED'
		chilin_sbatch = d.replace(ID+'_down', 'run_'+ID)
		if os.path.exists(chilin_sbatch) and (leftJobs > 0) and ('COMPLETED' in status):
			print(chilin_sbatch)
			os.system('sbatch %s'%chilin_sbatch)
		else:
			download_submited_still.append(d)
			#print('rest chilin: %s'%len(download_submited_still))
	if end and len(download_submited_still) > 0:
		time.sleep(1800)
		_submit_chilin(download_submited_still, mjobs, end = True)
	return(download_submited_still)


def checkCurrentJobs(maxJobs):
	"""summarize the current job number, including running and pending
	return the number that could be submitted
	"""
	sacct_runinng = int(int(commands.getoutput('sacct |grep RUNNING | wc -l'))/2)
	sacct_pending = int(commands.getoutput('sacct | grep PENDING | wc -l'))
	if (sacct_pending >= maxJobs):
		return(0)
	else:
		return(maxJobs - sacct_pending)
	return(maxJobs-(sacct_runinng + sacct_pending))

def _check_mode(config):
	'''check whether using local metatable or transfer from daisy
	'''
	daisylocal=config['odysseyParameter']['fromDaisyMetaTable']
	addFile=config['odysseyParameter']['localMetaTable']
	if daisylocal == '' and addFile == '':
		sys.stderr.write("no metaTable is given, please check config file\n")
		sys.exit(0)

	if daisylocal == '':
		daisylocal = False
	if addFile == '':
		addFile = False
	return(daisylocal, addFile)

# def _running(mjobs, daisylocal, cols, addFile=False, email=False):

def _running(config):
	"""the main script to handle submition of jobs in odyssey mode
	"""
	mjobs = config['odysseyParameter']['maxJob']
	daisylocal, addFile = _check_mode(config)
	cols = config['odysseyParameter']['cols'].split(',')
	recordFolder = config['odysseyFolders']['record']
	dataSource = config['odysseyParameter']['dataSource']

	main_ret = []
	if open('status').read().strip() != 'OK':
		return('+++Status file not OK')
	os.system('echo running > status')
	#DCIDcol, speciescol, factorcol, gsmcol = 0, 1, 2, 0
	if dataSource == 'geo':
		DCIDcol, speciescol, factorcol, gsmcol = int(cols[0]), int(cols[1]), int(cols[2]), int(cols[3])
	elif dataSource == 'encode':
		DCIDcol, speciescol, factorcol, fastqcol = int(cols[0]), int(cols[1]), int(cols[2]), int(cols[3])
	else:
		print('incorrect dataSource, must be geo or encode')
		sys.exit(1)
		
	newSamples = []
	if daisylocal and not addFile:
		refresh = True #give flag to DC_process function, True = record information, False=do not record again
		# 1. get the newest table from daisy
		daisyFile=config['odysseyParameter']['fromDaisyMetaTable']
		odysseyFile=os.path.join(config['odysseyFolders']['syncDaisy'], 'DC_new_collection.xls') # the path will save the samples need to be processed
		try:
			cmd = 'scp -P 33001 {IP}:{daisy} {here}'.format(IP=config['odysseyParameter']['trasferIP'],
				daisy=daisyFile,
				here=odysseyFile)
			print(cmd)
			r = commands.getoutput(cmd)
			if 'No such file or directory' in r:
				os.system('echo OK > status')
				print("scp failed, may be no such files.")
				sys.exit(1)
		except:
			os.system('echo OK > status')
			print("scp failed, may be no such files.")
			sys.exit(1)
		# 2. get those gsm which are new and haven't been processed or haven't being processed, and save to "newSamples
		try:
			receive_file=os.path.join(config['odysseyFolders']['syncDaisy'], 'recivedSamples.xls')# a one column file saved gsm which may have been processed or on the way.
			if os.path.exists(receive_file):
				with open(receive_file) as receiveSamples:
					receiveGsm = [x.rstrip().split('\t')[-1] for x in receiveSamples]
					newSamples = [x.rstrip().split('\t') for x in open(odysseyFile) if x.split('\t')[gsmcol] not in receiveGsm]
			else:
				newSamples = [x.rstrip().split('\t') for x in open(odysseyFile)]
		except:
			print("read file problem, then exit the job in odyssey!")
			sys.exit(1)

	elif not daisylocal and addFile and os.path.exists(addFile):
		refresh = False
		newSamples = [x.rstrip().split('\t') for x in open(addFile)]	
	else:
		print("parameter error, please check into the config file")
		os.system('echo OK > status')
		sys.exit(1)
	if not newSamples:# if no new samples, then wait for a new round
		os.system('echo OK > status')
		return('no new samples, please wait!')
	# 3. get all sbatch files for all new samples, and collect the sbatch file in 'LinkPlusDownload_CMD_all'"""
	
	def submit(cmd_list, download_submited_list):
		"""this function for submitting download job and record chilin job, if all download job are submitted will stop,
		so chilin job still need control the end samples outside this function
		"""
		ret = []
		## deal with the download submited samples, to submit the chilin sbatch.
		if len(download_submited_list) > 0:
			pass
			#download_submited_list = _submit_chilin(download_submited_list, mjobs, False)
		if len(cmd_list) == 0: #and len(download_submited_list) == 0: # stop, if no more sample need to run
			return download_submited_list
		# continue with download command
		leftJobs = checkCurrentJobs(int(mjobs))# result from the function
		collect = []
		""" 4. submit jobs just when current job number less 200, otherwise, keep checking"""
		if leftJobs>0 and (len(cmd_list) <= leftJobs):
			for one in cmd_list:# if need run jobs less than left job positions, submit all
				print("1: %s"%one)
				os.system('sbatch '+ one)
				collect.append(one)
				download_submited_list.append(one)
			ret = submit([], download_submited_list)
		elif leftJobs>0 and (len(cmd_list) > leftJobs): 
			Jobs = cmd_list[:leftJobs]
			for one in Jobs:
				print("2: %s"%one)
				os.system('sbatch ' + one)
				collect.append(one)
				download_submited_list.append(one)
			cmd_list = [x for x in cmd_list if x not in collect]
			ret = submit(cmd_list, download_submited_list)
		else:
			print("full jobs, waiting for space, default is 1 hour.")
			time.sleep(3600)
			ret = submit(cmd_list, download_submited_list)
		return(download_submited_list)
	LinkPlusDownload_CMD_all, chilinCommand_CMD_all, checkChilin_CMD_all, download_submited_list = [], [], [], []
	n = 1
	for sample in newSamples:
		if dataSource == 'geo':
			DCID, gsm, species, factor = sample[DCIDcol], sample[gsmcol], sample[speciescol], sample[factorcol] # get required information, gsm, species, and factor
			inf = DCID+'\t'+species+'\t'+factor+'\t'+gsm+'\t'+str(datetime.datetime.now())+'\n'
		else:
			DCID, fastqlinks, species, factor = sample[DCIDcol], sample[fastqcol], sample[speciescol], sample[factorcol] # get required information, gsm, species, and factor
			inf = DCID+'\t'+species+'\t'+factor+'\t'+fastqlinks+'\t'+str(datetime.datetime.now())+'\n'
		if factor.strip() in ['none', 'None', 'NA']:
			record = open(os.path.join(recordFolder, 'None_Factor_samples.txt'), 'a')
			record.write(inf)
			record.close()
			receive_file = open(os.path.join(config['odysseyFolders']['syncDaisy'], 'recivedSamples.xls'),'a') # provent from processing twice.
			receive_file.write(inf)
			receive_file.close()
			continue
		if factor.strip() in ['Input', 'input']:
			record = open(os.path.join(recordFolder, 'Input_ChIP_samples.txt'), 'a')
			record.write(inf)
			record.close()
			receive_file = open(os.path.join(config['odysseyFolders']['syncDaisy'], 'recivedSamples.xls'),'a')
			receive_file.write(inf)
			receive_file.close()
			continue
		"""sbatch file of each step, path = the cistromeDC path, first is get link and download sbatch, also include the fellowed sbatch files"""	
		if dataSource == 'geo':
			LinkPlusDownload_CMD, endType = DC_dataProcess.LinkPlusDownload(config, DCID, gsm, species, factor, path=os.getcwd(), refresh = refresh) #get link and sbatch file for command of download fastq, including sra-fastq convertion and cat multiple fastq, record single or paried and fastq path into a file
		else:
			LinkPlusDownload_CMD, endType= encode.LinkPlusDownload(config, DCID, fastqlinks, species, factor, path=os.getcwd(), refresh = refresh) #get link and sbatch file for command of download fastq, including sra-fastq convertion and cat multiple fastq, record single or paried and fastq path into a file
		if LinkPlusDownload_CMD:
			cmd_list = [LinkPlusDownload_CMD]
			download_submited_list = submit(cmd_list, download_submited_list) # submmit download job and chilin job, return back unsubmited chilin jobs.
			LinkPlusDownload_CMD_all.append(LinkPlusDownload_CMD)
	if len(download_submited_list) > 0: # download for all the samples, but chilin job havenot be all submited, please continue
		pass
		#chilin_end = _submit_chilin(download_submited_list, mjobs, end = True)

	print("new samples: %d"%len(LinkPlusDownload_CMD_all))

	os.system('echo OK > status')
	if not refresh:
		print('Finish the workflow for trying again samples.')
		sys.exit(1)
	return(main_ret)


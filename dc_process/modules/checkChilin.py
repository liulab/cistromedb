import sys,os
import argparse
import datetime
import commands
from qc.run_qc import _run
import configparser 

global cf;cf = configparser.ConfigParser()
cf.optionxform=str

def _clean_config(config):
	# remove the annotation:
	for firstLevel in config.keys():
		for secondLevel in config[firstLevel]:
			if '#' in config[firstLevel][secondLevel]:
				config[firstLevel][secondLevel] = config[firstLevel][secondLevel][:config[firstLevel][secondLevel].index('#')-1].rstrip()
	return(config)
	
def getMoreInf(config, main_path, ID):
	'''from the orignal file get information
	'''
	if config['mode']['odyssey'] == 'True':
		cols = config['odysseyParameter']['cols'].split(',')
		if config['odysseyParameter']['localMetaTable'] != '':
			list_infor = commands.getoutput('grep -w %s %s'%(ID, config['odysseyParameter']['localMetaTable'])).split('\t')
		elif config['odysseyParameter']['fromDaisyMetaTable'] != '':
			list_infor = commands.getoutput('grep -w %s %s'%(ID, os.path.join(main_path, config['odysseyFolders']['syncDaisy']+'/DC_new_collection.xls'))).split('\t')
		else:
			return(['NA', 'NA', 'NA', 'NA'])	
	elif config['mode']['daisy'] == 'True':
		cols = config['daisyParameter']['cols'].split(',')
		list_infor = commands.getoutput('grep -w %s %s/%s'%(ID, main_path, config['daisyParameter']['localMetaTable'])).split('\t')
	return([list_infor[int(i)] for i in cols])	
	
def check_motif(ID_path):
	"""make sure motif result is correct.
	"""
	denovo = 'denovo.xml'
	ID_file = commands.getoutput('ls '+ID_path).rstrip().split('\n')
	if (len(ID_file) == 7) or (len(ID_file) == 6 and denovo in ID_file):
		ID_json_file = os.path.join(ID_path, 'motif_list.json')
		with open(ID_json_file) as json:
			if len(json.readline()) == 0: # nothing in json file
				os.system('rm -rf {}'.format(ID_path))
				return False # None means wrong motif result
			return True
	os.system('rm -rf {}'.format(ID_path))
	return False
def _conser_cmd(config, main_path, prefix, cs_path, summit_path, cmd):
	"""get information from cmd and generate conservation running command
	"""
	pbw_mm = config['mm10']['conservation']
	pbw_hs = config['hg38']['conservation']
	if ('histone' in cmd) or ('dnase' in cmd):
		w = 4000
	else:
		w = 400 
	if 'hg38' in cmd:
		cmd1 = 'head -5000 %s > %s'%(summit_path, summit_path+'.top5k')
		#cmd2 = os.path.join(main_path+'/modules/conservation_onebw_plot.py')+" -d %s -w %d -o %s %s"%(pbw_hs, w, prefix, summit_path+'.top5k')
		cmd2 = '/conservation_onebw_plot.py'+" -d %s -w %d -o %s %s"%(pbw_hs, w, prefix, summit_path+'.top5k')
		cmd3 = 'rm -fr %s'%summit_path+'.top5k'
	else:
		cmd1 = 'head -5000 %s > %s'%(summit_path, summit_path+'.top5k')
		#cmd2 = os.path.join(main_path+'/modules/conservation_onebw_plot.py')+" -d %s -w %d -o %s %s"%(pbw_mm, w, prefix, summit_path+'.top5k')
		cmd2 = 'conservation_onebw_plot.py'+" -d %s -w %d -o %s %s"%(pbw_mm, w, prefix, summit_path+'.top5k')
		cmd3 = 'rm -fr %s'%summit_path+'.top5k'
	return cmd1 + '\n' + cmd2 + '\n' + cmd3

def check_filepath(config, dirpath,topath, cmd):
#"""first check the very necessary files and then check summit, json, motif, and conservation one by one"""

	get=[]
	ID = os.path.basename(dirpath).replace('dataset', '').strip('/')
	# end type
	if "--pe" in cmd:
		end = 'PAIRED'
	else:
		end = 'SINGLE'
	#path = '/'.join(dirpath.split('/')[:-2])
	if 'tmp_chilin_result' in dirpath:
		main_path =  dirpath.replace(os.path.basename(dirpath), '').replace('tmp_chilin_result', '').rstrip('/')
	else:
		main_path = os.getcwd()
	recordFolder = os.path.join(main_path, config['odysseyFolders']['record'])

	Inf = getMoreInf(config, main_path, ID)
	gsmid, sp = Inf[-1], Inf[1]
	#BamFiles = os.path.join(main_path, 'BamFiles')
	#if not os.path.exists(BamFiles):
	#	os.mkdir(BamFiles)
	if ';' in dirpath:
		folder = dirpath.split(';')
	else:
		folder = [dirpath]
	for path in folder:
		i = [path] # used to record the files status, if no, add NA
		if path and os.path.isdir(path):
			dataset = os.listdir(path)
			peak=[i1 for i1 in dataset if i1.endswith('sort_peaks.narrowPeak') or i1.endswith('sort_peaks.narrowPeak.bed') or i1.endswith('sort_peaks.broadPeak.bed') or i1.endswith('sort_peaks.broadPeak.bed') and i1.find('rep') == -1]
			if peak:
				path_peak = os.path.join(path, ''.join(peak))
				i.append(path_peak)
			else:
				i.append('NA')
			xls=[i1 for i1 in dataset if i1.endswith('peaks.xls') and i1.find('rep') == -1]
			if xls:
				path_xls = os.path.join(path, ''.join(xls))
				i.append(path_xls)
			else:
				i.append('NA')
			bw = [i1 for i1 in dataset if i1.endswith('treat.bw')]
			if bw:
				path_bw = os.path.join(path, ''.join(bw))
				i.append(path_bw)
			else:
				i.append('NA')
			p_attic = os.path.join(path, 'attic')
			if 'attic' in dataset and os.path.isdir(p_attic):
				attic = os.listdir(p_attic)
				if 'json' in attic and os.path.isdir(os.path.join(p_attic, 'json')):
					path_json = os.path.join(p_attic, 'json')
					i.append(path_json)
				else:
					i.append('NA')
				score = [i1 for i1 in attic if i1.endswith('_gene_score.txt')]
				if score:
					path_score = os.path.join(p_attic, ''.join(score))
					i.append(path_score)
				else:
					i.append('NA')
				treat=[i1 for i1 in attic if i1.endswith('bam') and i1.find('treat') != -1]
				if treat and len(treat) == 1:
					path_bam = os.path.join(p_attic, ''.join(treat))
					i.append(path_bam)
				elif treat and len(treat) > 1:
					treatment = [i2 for i2 in treat if i2.find('treatment') != -1]
					if treatment:
						path_bam = os.path.join(p_attic, ''.join(treatment))
						i.append(path_bam)
					else:
						i.append('NA')
				else:
					i.append('NA')
			else:
				i.append('NA')
		else:
			i.append('NA')
		get.append(i)
	for sample in get:
		l, now=[], str(datetime.datetime.now())
		#gsmid = os.path.basename(sample[0].rstrip('/')).replace('dataset', '').lstrip('_')
		for iterm in sample:
			if iterm == "NA":
				l.append('a')
		if not l:
			if os.path.isdir(topath):
				peak_number = int(commands.getoutput("wc -l {peak} |cut -d' ' -f1".format(peak = sample[1])))
#				bam_bdg_file = [sample[0]+'/attic/'+x for x in os.listdir(sample[0]+'/attic') if x.endswith('.bam') or x.endswith('.bdg') or x.startswith('dpt_')]
				# remove control bigwig file
				bw_file = [sample[0]+'/'+x for x in os.listdir(sample[0]) if x.endswith('_control.bw')]
				for cbw in bw_file:
					os.system('rm -rf %s'%cbw)
#				files_rm = bam_bdg_file + bw_file
#				if files_rm and ('histone' in cmd) or ('dnase' in cmd): # don't remove bam file of histone and ca data
#					for b in [x for x in files_rm if not x.endswith('.bam')]:
#						os.system('rm -f {}'.format(b))
#				else:
#					for b in files_rm:
#						os.system('rm -f {}'.format(b))
				os.system('mv %s %s'%(sample[0],topath))
				new_dir = sample[0].replace('tmp_chilin_result', 'final_chilin_result')
				#ID = new_dir.split('dataset')[-1]
				##move bam file to specific path
#				os.system('scp -P 33001 -r %s rongbin@cistrome.org:%s'%(os.path.join(topath, os.path.basename(sample[0]))), '/data6/DC_results/Result_2016_ATAC/')
				record1 = open(recordFolder+'/chilin_OK_samples.txt', 'a')
				record1.write('\t'.join(Inf)+'\t'+now+'\n')
				record1.close()
				motif_path = [os.path.join(new_dir, 'attic/'+x) for x in os.listdir(os.path.join(new_dir, 'attic')) if x.endswith('_seqpos')]
				summit_file = lambda x: os.path.join(x, ID+'_sort_summits.bed')
				if not motif_path and (peak_number > 500) and ('dnase' not in cmd) and ('histone' not in cmd):
					motif = False
				elif motif_path and (peak_number > 500):
					motif = check_motif(motif_path[0])
				else:
					motif = True
				if not motif:
					print("Find motif problem, please re-process motif seperately, and check file : "+recordFolder+'/motif_problem_samples.txt')
					record2 = open(recordFolder+'/motif_problem_samples.txt', 'a')
					record2.write('\t'.join(Inf)+'\t'+now+'\t'+summit_file(new_dir)+'\n') 
					record2.close()
				##check json file
				json_path = sample[4].replace('tmp_chilin_result', 'final_chilin_result')
				#ID = os.path.basename(sample[0]).split('dataset')[-1].strip('_')
				standard_json = ['fastqc.json', 'enrich_meta.json', 'dhs.json', 'pbc.json', 'map.json', 'macs2.json', 'frag.json', 'conserv.json', 'meta.json', 'frip.json']
				selective_json = ['seqpos.json']
				json_check = []
				for j in standard_json:
					json_check.append(os.path.exists(os.path.join(json_path, ID+'_'+j)))
				if False in json_check:
					json_check_out = open(recordFolder+'/json_problem.txt','a')
					json_check_out.write(ID+'\t'+'\t'.join([str(ii) for ii in json_check])+'\n')
					json_check_out.close()
					cmd_qc = _run(main_path = main_path, config=config, server = 'odyssey', sampleType = 'GEO', gsm = gsmid, chilin = new_dir, end=end,
					 sp = sp, inputFile = None, cols = None, outputDir = os.path.join(main_path, 'tmp_qc_process'), enrich_orNot = False, cmdFile = os.path.join(main_path, 'tmp_qc_process'))
				else:
					cmd_qc = None
				##check summit file
				if (not os.path.exists(summit_file(new_dir))) and ('narrowPeak' in sample[1]):
					print( '+++ generate summit file sperately')
					os.system("awk '{$2=$2+(int(($3-$2)/2)); $3=$2+1} {print $0}' %s > %s"%(sample[1], summit_file(new_dir)))
				##check conservation file
				conser_path = os.path.join(new_dir, 'attic/%s_conserv_img.png'%ID)
				if not os.path.exists(conser_path):
					print('conservation: %s'%conser_path)
					print( '+++ run conservation sperately')
					conser_cmd = _conser_cmd(config, main_path, new_dir+'/attic/'+ID, conser_path, summit_file(new_dir), cmd)
					os.system(conser_cmd)
				##everything are ok
				print( '%s files checked OK'%sample[0])
				fastq_file = cmd.split('-t ')[-1].split(' ')[0].split(',')
				for fq in fastq_file:
					print( '+++ remove fastq file: %s'%fq)
					os.system('rm -rf %s'%fq)

				## generate md5 file and start to transfer for odyssey mode
				if config['mode']['odyssey'] == 'True':
					daisy_folder = config['odysseyParameter']['toDaisyPath']
					IP = config['odysseyParameter']['trasferIP']
					transfer_cmd = 'rsync -raP -e "ssh -p 33001" %s %s:%s\n'%(new_dir, IP, daisy_folder)
					os.system('touch scp_ok')
					scp_ok = 'rsync -raP -e "ssh -p 33001" scp_ok %s:%s\n'%(IP, daisy_folder+'/'+new_dir.split('/')[-1])
					md5_cmd = '\nfind ./ -type f -print0 | xargs -0 md5sum > %s.md5\n'%(ID)
					if cmd_qc:
						sbatch_file = cmd_qc.replace('sbatch ', '')
						os.system("echo '%s' >> %s"%('\ncd '+new_dir+'\n'+md5_cmd, sbatch_file))
						os.system("echo '%s' >> %s"%(transfer_cmd, sbatch_file))
						os.system("echo 'cd %s' >> %s"%(main_path, sbatch_file))
						os.system("echo '%s' >> %s"%(scp_ok, sbatch_file))
						os.system(cmd_qc)
					else:
						# for bam in bamfiles:
						# 	cmd_bam = 'mv %s %s'%(bam, BamFiles)
						# 	os.system("echo '%s' >> %s"%(cmd_bam, sbatch_file))
						os.chdir(new_dir)
						os.system(md5_cmd)
						os.system(transfer_cmd)
						os.chdir(main_path)
						os.system(scp_ok)

		elif l and (sample[1] != 'NA') and (int(commands.getoutput("wc -l {peak} |cut -d' ' -f1".format(peak = sample[1]))) == 0):
			print('%s files checked failed, but no peak were found.'%sample[0])
			record4 = open(recordFolder+'/probably_badANDfailed_samples.txt', 'a')
			record4.write("%s\t%s\t%s\n"%('\t'.join(Inf), now, cmd))
			record4.close()
		else:
			print('%s files checked failed'%sample[0])
			print('check_result: %s'%sample)
			#os.system("rm -fr %s"%sample[0])
			record3 = open(recordFolder+'/rerun_chilin_samples.txt', 'a')
			record3.write("%s\t%s\t%s\n"%('\t'.join(Inf), now, cmd))
			record3.close()
			#os.system("echo '%s\t%s\t%s' >> ../record/rerun_chilin.txt"%(gsmid, now, cmd))
	fastq_file = cmd.split('-t ')[-1].split(' ')[0].split(',')
	for fq in fastq_file:
		print('+++ remove fastq file: %s'%fq)
		os.system('rm -rf %s'%fq)

def main():
	try:
		parser = argparse.ArgumentParser(description="""check chilin result and rerun if error happened""")
		parser.add_argument( '-cf', dest='config', type=str, required=True, help='the path of config file')
		parser.add_argument( '-d', dest='dirpath', type=str, required=True, help='dictory path need to check, separate by comma if a list')
		parser.add_argument('-p',dest='topath',type=str,required=True,help='dictory path the successful sample  will move to')
		parser.add_argument('-c', dest ='chilin_cmd', help='ChiLin command for rerun')
		args = parser.parse_args()

		cf.read(args.config)
		config=cf._sections
		config=_clean_config(config)
		config['configPath'] = {}
		config['configPath']['path'] = args.config
		check_filepath(config, args.dirpath, args.topath, args.chilin_cmd)

	except KeyboardInterrupt:
		sys.stderr.write("User interrupted me!\n")
		sys.exit(0)

if __name__ == '__main__':
	main()
	

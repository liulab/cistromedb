##+++ daisy
"""daisy side, check the md5 and remove bam file if sample have backup to umass
"""
import os,sys
import commands
import datetime, time

###check wheather there are new sample from odyssey.
def _check_md5(pathDir):
        """check the completeness of transferring
        """
        ID = os.path.basename(pathDir).replace('dataset', '')
        md5file = os.path.join(pathDir, ID+'.md5')
        if not os.path.exists(md5file):
                return None
        os.chdir(pathDir)
        check_res = commands.getoutput('md5sum -c %s'%md5file).replace('%s.md5: FAILED'%ID, '')
        os.chdir('/data6/DC_results/Files')
        if ('FAILED' in check_res) or ('No such file' in check_res):
                return None
        else:
                return True

def _findPath(dataset, pathSet):
        ID = dataset.replace('dataset', '')
        #pathSet = ['/data6/DC_results/Result_2018Collect', '/data6/DC_results/DC_new']
        for p in pathSet:
                if os.path.exists(os.path.join(p, dataset)):
                        bamFile = os.path.join(p, dataset, 'attic/'+ID+'_treat_rep1.bam')
                        if os.path.exists(bamFile):
                                return bamFile
        return None

def main(path):
        ##deal with odyssey comes samples 
        #DC_path = ['/data6/DC_results/DC_201812']
        if ',' in path:
                DC_path = path.split(',')
        else:
                DC_path = [path]
        for p in DC_path:
                current_samples = [x for x in os.listdir(p) if x.startswith('dataset')]
                if not os.path.exists(os.path.join(p, 'sampleList.txt')):
                        os.open(os.path.join(p, 'sampleList.txt'), os.O_RDWR|os.O_CREAT)
                if not os.path.exists(os.path.join(p, 'again.txt')):
                        os.open(os.path.join(p, 'again.txt'), os.O_RDWR|os.O_CREAT)
                record_samples = [x.rstrip() for x in open(os.path.join(p, 'sampleList.txt'))]
                new_samples = list(set(current_samples).difference(set(record_samples)))
                if new_samples:
                        os.system('echo "%s are new in %s\n" >>  connect_umass_odyssey.log'%(str(datetime.datetime.now())+': '+str(len(new_samples)), p))
                        for s in new_samples:
                                if 'scp_ok' not in os.listdir(os.path.join(p, s)):
                                continue
                                check_res = _check_md5(os.path.join(p, s))
                                if check_res:
                                        os.system('echo %s >> %s'%(s, os.path.join(p, 'sampleList.txt')))
                                        os.system('sed -i "/%s/d" %s'%(s, os.path.join(p, 'again.txt')))
                                else:
                                        os.system('rm -fr %s'%os.path.join(p, s))
                                        os.system('echo %s >> %s'%(s, os.path.join(p, 'again.txt')))
#       time.sleep(300)# wait 5 min, in case the umass side have not sync
        ##deal with umass part                  
        umassAlreadyPath = './sampleListUmass.txt' # the table records all samples which have backuped to umass
        noBamAlreadyPath = './noBamDaisy.txt' # the table records samples on daisy which have get ride of bam file already
        umassAlready = [x.rstrip() for x in open(umassAlreadyPath)]
        noBamAlready = [x.rstrip() for x in open(noBamAlreadyPath)]
        needRemoveBam = list(set(umassAlready).difference(set(noBamAlready)))
        for datab in needRemoveBam:
                bamPath = _findPath(datab, DC_path)
                if bamPath:
                        os.system('rm -f %s'%bamPath)
                        os.system('echo %s >> %s'%(datab, noBamAlreadyPath))
while 1:
        main(sys.argv[1])
        os.system('echo "++Finish check at %s, and will sleep 0.5 hours.\n" >> connect_umass_odyssey.log'%str(datetime.datetime.now()))
        time.sleep(1800)# wait for 12h to start a new round



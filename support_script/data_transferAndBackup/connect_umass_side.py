import os,sys
import datetime, time
"""
connect to Umass server for data backup
"""
def main():
        pathSet = ['/data6/DC_results/DC_201812'] ## the path that store chilin result on daisy
        toUmassPath = '/data/projects/Cistromesrc/2018/Result_201812' # the path of umass to store chilin result which transfer from daisy
        umassSampleList = '/data/projects/Cistromesrc/sampleListUmass.txt' # the table contains all the samples in umass
        backLabelToDaisy = '/data6/DC_results/Files/sampleListUmass.txt' # the path of daisy to recive new sample ID from umass, once umass recives new sample.
        umassAlready = [x.rstrip() for x in open(umassSampleList)]
        # umassPath = ['/data/projects/Cistromesrc/DC_result_2016All', '/data/projects/Cistromesrc/DC_results', '/data/projects/Cistromesrc/2018/Result_2018Collect']
        # umassAlready = []
        # for up in umassPath:
        #       umassAlready.extend(os.listdir(up))
        for p in pathSet:
                os.system("rsync -raP -e 'ssh -p 33001' rongbin@cistrome.org:%s ./sampleListDaisy.txt"%os.path.join(p, 'sampleList.txt'))
                sampleList = [x.rstrip() for x in open("sampleListDaisy.txt")]
                newSamples = list(set(sampleList).difference(set(umassAlready)))
                if newSamples:
                        print('%s are new in %s'%(str(len(newSamples)), p))
                        for n in newSamples:
                                cmd = "rsync -raP -e 'ssh -p 33001' rongbin@cistrome.org:%s %s"%(os.path.join(p, n), toUmassPath)
                                os.system(cmd)
                                os.system('echo %s >> %s'%(n, umassSampleList))
                                os.system("rsync -raP -e 'ssh -p 33001' %s rongbin@cistrome.org:%s"%(umassSampleList, backLabelToDaisy))
while 1:
        main()
        os.system('echo "++Finish check at %s, and will sleep 1 hours.\n" >> connect_daisy.log'%str(datetime.datetime.now()))
        time.sleep(3600)# wait for 6h to start a new round

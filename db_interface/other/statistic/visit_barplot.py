import os,sys
import datetime
import json
import re
import pandas as pd
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

os.system('rm -f all_visit.json')
os.system('wget -c "http://cistrome.org/~qqin/piwik/index.php?module=API&method=API.get&format=JSON&idSite=2&period=day&date=2015-05-29,today&filter_limit=false&format_metrics=1&period=day&expanded=1&translateColumnNames=1&language=en" -O all_visit.json')

j = open('all_visit.json')

try:
	j = json.load(j)
	visit = []
	for x in j.keys():
		visit.append([x, str(j[x]['nb_uniq_visitors']), str(j[x]['nb_visits'])])
except:
	print 1
	j = open('all_visit.json').read()
	all_date = re.findall(r'[0-9]*-[0-9]*-[0-9]*', j)
	visit = []
	for i, d in enumerate(all_date):
		if i == len(all_date) -1:
			one = re.findall(r'%s.*'%(d), j)
		else:
			one = re.findall(r'%s.*%s'%(d, all_date[i+1]), j)
		if one:
			one = one[0]#.replace(all_date[i+1], '')
		search = re.findall(r'nb_uniq_visitors":[0-9]*',one)
		if not search:
			visit.append([d, '0', '0'])
			continue
		nb_uniq_visitors = search[0].replace('nb_uniq_visitors":', '')
		nb_visits = re.findall(r'nb_visits":[0-9]*',one)[0].replace('nb_visits":', '')	
		visit.append([d, nb_uniq_visitors, nb_visits])
visit = pd.DataFrame(visit, columns = ['Date', 'Unique.visitors', 'Visits'])
visit[['Unique.visitors', 'Visits']] = visit[['Unique.visitors', 'Visits']].apply(pd.to_numeric)
visit.index = visit.iloc[:,0]
count_uniq_visit = {}
count_all_visit = {}
for i in visit.index:
	year = str(i).split('-')[0]
	if year in count_uniq_visit:
		count_uniq_visit[year] += visit.loc[i,'Unique.visitors']
	else:
		count_uniq_visit[year] = 0
	if year in count_all_visit:
		count_all_visit[year] += visit.loc[i,'Visits']
	else:
		count_all_visit[year] = 0
visit_count = pd.DataFrame([count_uniq_visit,count_all_visit], index = ['Unique_visits', 'All_visits'])
visit_count = visit_count.T

plt.figure()
fig = visit_count.plot.bar(rot=0)
plt.ylabel('# of visit')
plt.xlabel('Years')
fig = fig.get_figure()
fig.savefig("./visit_barplot.png")





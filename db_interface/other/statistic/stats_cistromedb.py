import os
import sys
import pandas as pd
import numpy as np
import re
if sys.argv[1] == 'all':
    import django
    import cPickle as p
      ## mkvirtualenv dc2, and pip install -r requirements.txt
    sys.path.append('/data/home/qqin/01_Projects/Programming/dc2/lib/python2.7/site-packages')
    sys.path.append('/data/home/qqin/01_Projects/Programming/dc2')
    sys.path.append('/data/home/qqin/01_Projects/Programming/dc2/dc2')
    sys.path.append('/data/home/qqin/01_Projects/Programming/dc2/datacollection')
    sys.path = sys.path[::-1]
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "dc2.settings")
    django.setup()
    from datacollection import models
    import commands

    from itertools import chain
    import json as json
    from django.core.paginator import Paginator
    from django.db.models import Q
    from django.http import HttpResponse
    from django.shortcuts import render_to_response
    from django.shortcuts import redirect
    from django.template import RequestContext
    from django.views.decorators.cache import cache_page
    from datacollection.models import Samples
    import re
    import pandas as pd
    import numpy as np

    inputdir = ['/data1/DC_results/Result_new', '/data5/DC_results/Result_new', '/data5/DC_results/Result_new3', '/data5/DC_results/Result_new2',
    '/data2/DC_results/Result_new', '/data3/DC_results/Result_new', '/data6/DC_results/Result_2016_ATAC', '/data6/DC_results/Result_2016_k27ac',
    '/data6/DC_results/Result_encode3', '/data6/DC_results/Result_2016All', '/data5/DC_results/Result_2016All2']
    datahub = '/data5/browser'
    scripts = '/data/home/qqin/01_Projects/Programming/dc2/scripts/'

    def get_query(query_string, search_fields):
        ''' Returns a query, that is a combination of Q objects. That combination
            aims to search keywords within a model by testing the given search fields.
        '''
        query = None # Query to search for every search term        
        terms = normalize_query(query_string)
        for term in terms:
            or_query = None # Query to search for a given term in each field
            for field_name in search_fields:
                q = Q(**{"%s__icontains" % field_name: term})
                if or_query is None:
                    or_query = q
                else:
                    or_query = or_query | q
            if query is None:
                query = or_query
            else:
                query = query & or_query
        return query

    def normalize_query(query_string,
                        findterms=re.compile(r'"([^"]+)"|(\S+)').findall,
                        normspace=re.compile(r'\s{2,}').sub):
        ''' Splits the query string in invidual keywords, getting rid of unecessary spaces
            and grouping quoted words together.
            Example:
            
            >>> normalize_query('  some random  words "with   quotes  " and   spaces')
            ['some', 'random', 'words', 'with quotes', 'and', 'spaces']
        '''
        return [normspace(' ', (t[0] or t[1]).strip()) for t in findterms(query_string)] 

    req_s = None #request.GET.get("species", None)
    req_c = None #request.GET.get("cellinfos", None)
    req_f = None #request.GET.get("factors", None)
    req_p = 1 #int(request.GET.get("page", 1))

    req_run = "false" #request.GET.get("run", "false")
    req_curated = "false" #request.GET.get("curated", "false")
    req_completed = "false" #request.GET.get("completed", "false")

    req_keyword = "" #request.GET.get("keyword", "")
    clicked = None #request.GET.get("clicked", None)

    if req_completed == "true" or req_run == "true" or req_curated == "true":
        if req_completed == "true" and req_run == "true" and req_curated == "true":
            q_common = Q(status="completed") | Q(status="run") | Q(status="curated")
        if req_completed == "false" and req_run == "true" and req_curated == "true":
            q_common = Q(status="run") | Q(status="curated")
        if req_completed == "false" and req_run == "false" and req_curated == "true":
            q_common = Q(status="curated")
        if req_completed == "true" and req_run == "true" and req_curated == "false":
            q_common = Q(status="completed") | Q(status="run")
        if req_completed == "true" and req_run == "false" and req_curated == "false":
            q_common = Q(status="completed")
        if req_completed == "true" and req_run == "false" and req_curated == "true":
            q_common = Q(status="completed") | Q(status="curated")
        if req_completed == "false" and req_run == "true" and req_curated == "false":
            q_common = Q(status="run")
    else:
        q_common = Q() ## all options are turned off

    # if request.is_ajax() and (not (req_s and req_c and req_f)):
    #     return HttpResponse("Request denied!")

    if req_s == "all" or req_s == None:
        q_s = Q()
    else:
        q_s = Q(species__name=req_s)

    if req_f == "all" or req_f == None:
        q_f = Q()
    elif req_f == "None":
        q_f = Q(factor__isnull=True)
    else:
        q_f = Q(factor__name=req_f)

    q_f = q_f & ~Q(factor__name__icontains='input') & ~Q(factor__name__icontains='igg')
    q_f = q_f & ~Q(factor__name__in=['552-SKD', '598-SKD', '5HMC', '5MC', '7SK'])

    if req_c == "all" or req_c == None:
        q_c = Q()
    else:
        clip = lambda x: x[3:]
        if req_c.startswith(ab_cellline):
            q_c = Q(cell_line__name=clip(req_c))
        #elif req_c.startswith(ab_cellpop):
        #    q_c = Q(cell_pop__name=clip(req_c))
        elif req_c.startswith(ab_celltype):
            q_c = Q(cell_type__name=clip(req_c))
        elif req_c.startswith(ab_tissue):
            q_c = Q(tissue_type__name=clip(req_c))
        #elif req_c.startswith(ab_strain):
        #    q_c = Q(strain__name=clip(req_c))

    ## s is the search term for samples
    # s = Samples.objects.filter(q_s & q_f & q_c & q_common & ~Q(status='hidden') & ~Q(status='new')) ## filter to get completed one
    s = Samples.objects.filter(q_s & q_f & q_c & q_common & (Q(status='completed') | Q(status='run')))

    # req_keyword = 'NR3A1'
    # s = Samples.objects.all()
    if req_keyword:
        try:
            req_keyword = int(req_keyword)
        except:
            pass
        if isinstance(req_keyword, int):
                #s = s.filter(full_text__iregex="[[:<:]]" + req_keyword)
                s = s.filter(id__iexact=req_keyword)
        else:
                if req_keyword.lower().startswith('gsm'):
                    s = s.filter(unique_id__iexact=req_keyword)
                elif req_keyword.lower().startswith('enc'):
                    encode_id = Q(unique_id__icontains=req_keyword)
                    s = s.filter(encode_id)
                else:
                    entry_query = get_query(req_keyword, ['factor__name', 'cell_line__name', 'cell_line__aliases', 'cell_type__name', 'cell_type__aliases', 'cell_pop__name', 'tissue_type__name', 'tissue_type__aliases', 'strain__name', 'species__name'])
                    # entry_query = get_query(req_keyword, ['factor__name', 'cell_line__name', 'cell_type__name', 'cell_pop__name', 'tissue_type__name', 'strain__name', 'species__name', 'paper__pub_summary', 'paper__reference'])
                    #abstract = Q(paper__abstract__iregex="[[:<:]]" + req_keyword)
                    #s = s.filter(entry_query | abstract)
                    factor_aliase = models.Aliases.objects.all()
                    factor_filter = [str(f.factor.name) for f in factor_aliase.filter(get_query(req_keyword, ['name']))]
                    if factor_filter:
                        for factor_query in factor_filter:
                            entry_query = entry_query | get_query(factor_query, ['factor__name'])
                    s = s.filter(entry_query)
    ## catalog sample types:
    human = s.filter(Q(species__name__iexact="Homo sapiens"))
    mouse = s.filter(Q(species__name__iexact="Mus musculus"))

    human_hm = human.filter(Q(factor__type__iexact='hm'))

    # human_hm_id = [str(x.unique_id) for x in human_hm]
    human_ca = human.filter(Q(factor__type__iexact='ca'))
    # human_ca_id = [str(x.unique_id) for x in human_ca]
    human_tf = human.filter(Q(factor__type__iexact='tf')|Q(factor__type__iexact='predicted transcription factor')|Q(factor__type__iexact='predicted chromatin regulator')|Q(factor__type__iexact='both predicted transcription factor and chromatin regulator')|Q(factor__type__iexact='cr'))
    # human_tf_id = [str(x.unique_id) for x in human_tf]
    human_other = human.filter(Q(factor__type__iexact='other')|Q(factor__type__iexact='')|Q(factor__type__iexact='None')|Q(factor__type__iexact='not sure'))
    # human_other_id = [str(x.unique_id) for x in human_other]
    # res = [x for x in human if str(x.unique_id) not in human_hm_id+human_ca_id+human_other_id+human_tf_id]

    mouse_hm = mouse.filter(Q(factor__type__iexact='hm')) #
    mouse_ca = mouse.filter(Q(factor__type__iexact='ca'))
    mouse_tf = mouse.filter(Q(factor__type__iexact='tf')|Q(factor__type__iexact='predicted transcription factor')|Q(factor__type__iexact='predicted chromatin regulator')|Q(factor__type__iexact='both predicted transcription factor and chromatin regulator')|Q(factor__type__iexact='cr'))
    mouse_other = mouse.filter(Q(factor__type__iexact='other')|Q(factor__type__iexact='')|Q(factor__type__iexact='None')|Q(factor__type__iexact='not sure'))

    sum_human = len(human_tf)+len(human_hm)+len(human_ca)+len(human_other)
    sum_mouse = len(mouse_tf)+len(mouse_hm)+len(mouse_ca)+len(mouse_other)
    summ = pd.DataFrame([[len(human_tf)+len(human_other), len(mouse_tf)+len(mouse_other), len(human_tf)+len(human_other)+len(mouse_other)+len(mouse_tf)],
                         [len(human_hm), len(mouse_hm), len(human_hm)+len(mouse_hm)],
                         [len(human_ca), len(mouse_ca), len(human_ca)+len(mouse_ca)],
                         #[len(human_other), len(mouse_other), len(human_other)+len(mouse_other)],
                         [sum_human, sum_mouse, sum_human+sum_mouse]], columns=['Homo sapiens','Mus musculus', 'Total'], index=['Factors', 'Histone Marks', 'Chromatin Accessibility', 'Total'])

    summ.to_csv('stat_db.csv')

    def type_count(sql):
        tmp = [str(x.factor) for x in sql]
        return len(set(tmp))

    print "Human HM types: %s"%type_count(human_hm) 
    print "Human Factor types: %s"%type_count(human_tf) + type_count(human_other)
    print "Mouse HM types: %s"%type_count(mouse_hm)
    print "Mouse Factor types: %s"%type_count(mouse_tf) + type_count(mouse_other) 
                    
if sys.argv[1] == 'table':
    input_table = sys.argv[2]
    cols = [int(x) for x in sys.argv[3].split(',')]
    input_inf = pd.read_table(input_table, sep = '\t', header = None)
    spCol, factorCol = cols[0], cols[1]
    human = input_inf[input_inf[spCol] == 'Homo sapiens']
    mouse = input_inf[input_inf[spCol] == 'Mus musculus']
    human_hm = [x for x in human[factorCol].tolist() if re.match(r"^H\d\w*\d",x)]
    mouse_hm = [x for x in mouse[factorCol].tolist() if re.match(r"^H\d\w*\d",x)]
    human_ca = [x for x in human[factorCol].tolist() if x in ['ATAC-seq', 'DNase']]
    mouse_ca = [x for x in mouse[factorCol].tolist() if x in ['ATAC-seq', 'DNase']]
    human_factor = [x for x in human[factorCol].tolist() if not re.match(r"^H\d\w*\d",x) and x not in ['ATAC-seq', 'DNase']]
    mouse_factor = [x for x in mouse[factorCol].tolist() if not re.match(r"^H\d\w*\d",x) and x not in ['ATAC-seq', 'DNase']]
    print "Human HM samples: %s, types: %s"%(len(human_hm), len(set(human_hm)))
    print "Human CA samples: %s, types: %s"%(len(human_ca), len(set(human_ca)))
    print "Human Factor samples: %s, types: %s"%(len(human_factor), len(set(human_factor)))
    print "Mouse HM samples: %s, types: %s"%(len(mouse_hm), len(set(mouse_hm)))
    print "Mouse CA samples: %s, types: %s"%(len(mouse_ca), len(set(mouse_ca)))
    print "Mouse Factor samples: %s, types: %s"%(len(mouse_factor), len(set(mouse_factor)))

if sys.argv[1] in ['help', '-h', '--help', '--h']:
    print "++++help informatin of this script:++++"
    print "all\nsummarize all data in MySQL database\n"
    print "table\nsummarize data in the given table give parameters like: table_path column_number\ncolumn_number looks like 0,1 which 0 mean species column and 1 means factor column.\n"


















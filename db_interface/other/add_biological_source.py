import os
import sys
import django
import cPickle as p
  ## mkvirtualenv dc2, and pip install -r requirements.txt
sys.path.append('/data/home/qqin/01_Projects/Programming/dc2/lib/python2.7/site-packages')
sys.path.append('/data/home/qqin/01_Projects/Programming/dc2')
sys.path.append('/data/home/qqin/01_Projects/Programming/dc2/dc2')
sys.path.append('/data/home/qqin/01_Projects/Programming/dc2/datacollection')
sys.path = sys.path[::-1]
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "dc2.settings")
django.setup()
#from datacollection import models
import models

"""
1. add biological_source column in datacollection_samples form
>>alter table datacollection_samples add biological_source varchar(225);
2. add biological_source in models.py
line 604: biological_source = models.DateTimeField(max_length=225, null=True, blank=True, default=None)
"""
reload(sys)
sys.setdefaultencoding("utf-8")
for s in models.Samples.objects.all():
	print s.id
	cell_line = str(s.cell_line)
	cell_type = str(s.cell_type)
	tissue_type = str(s.tissue_type)
	bg = cell_line+';'+cell_type+';'+tissue_type
	if bg == 'None;None;None':
		continue
	bg = bg.replace('None;', '').replace(';None', '')
	if str(s.biological_source) != bg:
		s.biological_source = bg
		s.save()

# s = s.order_by('biological_source')
# IDs = [str(x.id) for x in s]		

# foxa = s.filter(factor_id = '19')
# foxa = foxa.order_by('biological_source')
# # IDs = [str(x.id) for x in foxa]
# n = 0
# for x in foxa:
# 	if n == 10:
# 		break
# 	print(x.biological_source)
# 	n += 1



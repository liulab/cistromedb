#!/bin/bash

# Check command line and provide usage and version information
if [ $# -ne 3 ];
then echo "usage v2:  eap_narrowPeak_to_bigBed chromsizes  in.narrowPeak  out.narrowPeak.bigBed"
echo target is ucsc database ID - hg19 or hg38 or mm10 most commonly at time of writing.
echo Makes temp files so should be run in a freshly created directory .
exit -1; fi

# Convert bed narrowPeak in $2 to bigBed narrowPeak format in $3, checking chromosome coords vs $1

awk '{OFS="\t";IFS="\t"} {n+=1;$4=n;print $1,$2,$3,"peak_"$4,$5}' $2 > /data5/browser/$(basename $2).trim_name
/usr/local/bin/bedClip /data5/browser/$(basename $2).trim_name $1 /data5/browser/$(basename $2).trim_name.clip
R -e "peak=read.table('/data5/browser/$(basename $2).trim_name.clip');peak[,5]=round(peak[,5]/max(peak[,5])*1000,0);write.table(peak, file='/data5/browser/$(basename $2).trim_name', row.names=F, col.names=F, quote=F,sep='\t')"
sort -k1,1 -k2,2n /data5/browser/$(basename $2).trim_name > /data5/browser/$(basename $2).trim_name.clip
/usr/local/bin/bedToBigBed /data5/browser/$(basename $2).trim_name.clip $1 $3
rm /data5/browser/$(basename $2).trim_name.clip /data5/browser/$(basename $2).trim_name

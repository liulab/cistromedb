import os
import sys
import django
import cPickle as p
import argparse
  ## mkvirtualenv dc2, and pip install -r requirements.txt
sys.path.append('/data/home/qqin/01_Projects/Programming/dc2/lib/python2.7/site-packages')
sys.path.append('/data/home/qqin/01_Projects/Programming/dc2')
sys.path.append('/data/home/qqin/01_Projects/Programming/dc2/dc2')
# sys.path.append('/data/home/qqin/01_Projects/Programming/dc2/datacollection')
sys.path = sys.path[::-1]
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "dc2.settings")
django.setup()
from datacollection import models
import json

Finput = sys.argv[1] # the file which the first column is DC id


DC_paths = ['/data6/DC_results/DC_new/']

json_dir = '/data/home/qqin/12_data/json/'
motif_dir = '/data5/browser/motif_html/'

def _change(ID):
	sample_dir = None
	for path in DC_paths:
		tmp = os.path.join(path, 'dataset%s'%ID)
		if os.path.exists(tmp):
			sample_dir = tmp
			break
	if not sample_dir:
		continue
	# check if there is motif
	motif_path = os.path.join(sample_dir, 'attic/%s_seqpos'%ID)
	if os.path.exists(motif_path):
		os.system('mv %s %s'%(motif_path, motif_dir+'/'+ID))
	## run the qc
	if not os.path.exists(os.path.join(json_dir, ID+'.json'))
	os.system('python /data5/home/rongbin/cistromeDB/code/tojson_web_onejson.py %s %s'%(sample_dir, json_dir))
	## change status in mysql
	try:
		s = models.Samples.objects.filter(id = ID)[0]
		s.status = u'completed'
	except:
		continue
	try:
		# add biglogical source combination
		cell_line = str(s.cell_line)
		cell_type = str(s.cell_type)
		tissue_type = str(s.tissue_type)
		bg = cell_line+';'+cell_type+';'+tissue_type
		if bg == 'None;None;None':
			continue
		bg = bg.replace('None;', '').replace(';None', '')
		if str(s.biological_source) != bg:
			s.biological_source = bg
	except:
		pass
	try:
		# add qc judge for filter 
		json_f = open("%s/%s"%(json_dir, ID+'.json'), "r")
		qc = json.load(json_f)["judge"]
		json_f.close()
		qc.pop("motif_judge")
		qc = json.dumps(qc)
		s.qc_judge = qc
	except Exception, err:
		print(sys.exc_info())
		pass
	s.save()

def _run(config):
	if config['Finput']:
		IDs = [x.rstrip('\t').split('\t') for x in open(config['Finput'])]
		for ID in IDs:
			print(ID)
			_change(ID)
	else:
		_change(ID)

def main():
	try:
		parser = argparse.ArgumentParser(description="""release sample based on cistrome DC id""")
		parser.add_argument( '-f', dest='inputfile_path', type=str, required=False, help='the table with cistrome ID in the first column' )
		parser.add_argument( '-i', dest='id', type=str, required=False, help='given a cistrome DC id, this optional is exclusive to -f' )
		# parser.add_argument( '-o', dest='save', type=str, required=True, help='the file that save the meta annotation' )


		args = parser.parse_args()
		if args.inputfile_path and args.id:
			sys.stderr.write("Please either give -f or -i, not both!\n")
			sys.exit(0)

		_run({'Finput':args.inputfile_path, 'sID':args.id})


	except KeyboardInterrupt:
		sys.stderr.write("User interrupted me!\n")
		sys.exit(0)

if __name__ == '__main__':
	main()

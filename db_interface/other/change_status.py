import argparse
from optparse import OptionParser
import os, sys
import django
import cPickle as p
sys.path.append('/data/home/qqin/01_Projects/Programming/dc2/lib/python2.7/site-packages')
sys.path.append('/data/home/qqin/01_Projects/Programming/dc2')
sys.path.append('/data/home/qqin/01_Projects/Programming/dc2/dc2')
sys.path.append('/data/home/qqin/01_Projects/Programming/dc2/datacollection')
sys.path = sys.path[::-1]
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "dc2.settings")
django.setup()
#from datacollection import models
import models

# DC_path = ['/data1/DC_results/Result_new', '/data5/DC_results/Result_new', '/data5/DC_results/Result_new3', '/data5/DC_results/Result_new2',
# '/data2/DC_results/Result_new', '/data3/DC_results/Result_new', '/data6/DC_results/Result_2016_ATAC', '/data6/DC_results/Result_2016_k27ac',
# '/data6/DC_results/Result_encode3', '/data6/DC_results/Result_2016All', '/data5/DC_results/Result_2016All2', '/data6/DC_results/Result_2018Collect', '/data6/DC_results/DC_new']

 
types = sys.argv[1] # all, table
if types == "all":
	newSamples = models.Samples.objects.filter(status = 'new')

if types == 'table':
	newSamples = []
	#tablePath = sys.argv[2] ## table which contains sample ID in the first column
	with open(tablePath, 'r') as f:
		for item in f:
			ID = item.rstrip().split('\t')[0]
			s = models.Samples.objects.filter(id = ID)
			if s:
				newSamples.append(s[0])
print('total: %s'%len(newSamples))
if newSamples and (len(newSamples) >= 1):
	for s in newSamples:
		#gsm = str(s.unique_id)
		if (int(s.id) <= 65218) or (str(s.factor).lower() in ['input', 'none']) or (not os.path.exists('/data/home/qqin/12_data/json/%s.json'%str(s.id))):
			continue
		print(s.id)
		s.status = u'completed'
		s.save()
		# for p in DC_path:
		# 	samplePath = os.path.join(p, 'dataset'+str(s.id))
		# 	if os.path.exists(samplePath):
		# 		print str(s.id)
		# 		s.status = u'completed'
		# 		s.save()
		# 		motif_path = samplePath+'/attic/'+str(s.id)+'_seqpos'
		# 		if os.path.exists(motif_path):
		# 			cmd = 'mv %s /data5/browser/motif_html/%s'%(motif_path, str(s.id))
		# 			os.system(cmd)
		# 		break
				


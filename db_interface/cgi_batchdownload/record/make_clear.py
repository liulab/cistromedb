import os, sys

f = [x.rstrip().split('\t') for x in open('/data5/home/rongbin/public_html/release_interface/record/info.txt')]

get_types = {}
get_other = {}
for x in f:
	try:
		name = x[0]
		types = x[4]
		exist = [i.upper() for i in get_types.keys()]
		if name.upper() in exist:
			pre_name = get_types.keys()[exist.index(name.upper())]
			get_types[pre_name] +=  "; %s"%types
		else:
			get_types[name] =  types
			get_other[name] = x[1:4] + [x[-1]]
	except:
		continue
title = ['Name', 'PI', 'Institute', 'Email', 'Date_download', 'DataType_dowmload']
out = open('record_clear.txt', 'w')
print >>out, '\t'.join(title)
for x in get_types:
	ty = ';'.join(list(set(get_types[x].split(';'))))
	print >>out, x+'\t'+'\t'.join(get_other[x])+'\t'+get_types[x]
out.close()

os.system('Rscript make_clear.R')

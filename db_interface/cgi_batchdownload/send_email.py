#! /usr/bin/env python  
# -*- coding: UTF-8 -*-  
import smtplib  
from email.mime.text import MIMEText 
import json
import re


def send_mail(to_list,sub,content):
	# content += '\n\n'+'Term of usage:\n1. I will not share the download data to others.\n2. I will not build another website using the download data.\n3. I will cite "Mei S, Qin Q, Wu Q, Sun H, Zheng R, Zang C, Zhu M, Wu J, Shi X, Taing L, Liu T, Brown M, Meyer CA, Liu XS. Cistrome data browser: a data portal for ChIP-Seq and chromatin accessibility data in human and mouse. Nucleic Acids Res, 2017 Jan 4;45(D1):D658-D662" if I use the data for the publication.'
	content += '\n\n'+'Term of usage:\n\n' + open('content.txt', 'r').read()
	content += '\n\nBest!\nCistrome Datasets Browser Team'
	mail_host="smtp.gmail.com"            
	mail_user="cistromedb"                          
	mail_pass="Cistrome#17"                           
	mail_postfix="gmail.com"                     
	try:
		me="CistromeDB Team"#+"<"+mail_user+"@"+mail_postfix+">"  
		msg = MIMEText(content,_subtype='plain')  
		msg['Subject'] = sub  
		msg['From'] = me  
		msg['To'] = ";".join(to_list)                
		try:  
			server = smtplib.SMTP()  
			server.connect("smtp.gmail.com",587)
			server.ehlo()
			server.starttls()
			server.ehlo()
			server.login(mail_user,mail_pass)              
			server.sendmail(me, to_list, msg.as_string())  
			server.close()  
			return True  
		except Exception, e:  
			print str(e)  
			return False
	except:
		return False

def _validate_email(email_address):
	"""check email adress based on known university domain https://raw.githubusercontent.com/Hipo/university-domains-list/master/world_universities_and_domains.json
	"""
	# email_address_domain = email_address.split('@')
	match = None
	for x in json.load(open('world_universities_and_domains.json')):
		for e in x['domains']:
			match = re.findall(r'%s'%e, email_address.lower())
			if match:
				break
		if match:
			break
	if match:
		return True
	elif (not match) and ('.edu' in email_address.lower()):
		return True
	else:
		return False
	return False



# for i in range(1):                             
#     if send_mail(mailto_list,"电话","电话是XXX"):  
#         print "done!"  
#     else:  
#         print "failed!"  

#!/usr/bin/python
# -*- coding: UTF-8 -*-

# filename：test.py

import cgi, cgitb, sys, os
import traceback
import time
import smtplib  
from email.mime.text import MIMEText  
from send_email import send_mail
from send_email import _validate_email

# try:
# 	from send_email import send_mail
# except Exception, e:
# 	print "Content-type:text/html"
# 	print

# 	print "<html>"
# 	print "<head>"
# 	print "<meta charset=\"utf-8\">"
# 	print "<title>Cistrome data download</title>"
# 	print "</head>"
# 	print "<body>"
# 	# print "<h2>%s</h2>" % ('Please agree "Term of Usage"!')
# 	print "<h2>%s</h2>" % (e)
# 	print "</body>"
# 	print "</html>"

form = cgi.FieldStorage() 


try:
	agree = form.getvalue('checkbox')
	if not agree:
		print "Content-type:text/html"
		print

		print "<html>"
		print "<head>"
		print "<meta charset=\"utf-8\">"
		print "<title>Cistrome data download</title>"
		print "</head>"
		print "<body>"
		print "<h1>%s</h1>" % ('Not Found')
		print "The requested URL http://cistrome.org/cgi-bin/rongbin/validate2.py was not found on this server."
		# print "<h2>%s</h2>" % (os.path.abspath('.'))
		print "</body>"
		print "</html>"

	else:
		# get values from html
		user_name = form.getvalue('Username')
		user_PI  = form.getvalue('PI')
		user_affinity = form.getvalue('Institute')
		user_Email = form.getvalue('Email')

		# check their information
		if (not user_name) and (not user_PI) and (not user_affinity) and (not user_Email):
			wrong = "Please fill in your information!"
		elif (not user_name):
			wrong = 'Please fill in your name!'
		elif (not user_PI):
			wrong = 'Please fill in your lab PI!'
		elif not user_affinity:
			wrong = 'Please fill in your affinity!'
		elif not user_Email:
			wrong = 'Please fill in your Email!'
		else:
			wrong = None

		# check the email format
		if user_Email:
			if ('@' not in user_Email) or ("." not in user_Email) or (not _validate_email(user_Email.lower())):
				wrong = 'Unrecogonized Email format!'


		#check which type they want
		#check_box = [str(form.getvalue("checkbox1")), str(form.getvalue("checkbox2")),str(form.getvalue("checkbox3")),str(form.getvalue("checkbox4")),str(form.getvalue("checkbox5")),str(form.getvalue("checkbox6")),str(form.getvalue("checkbox7")),str(form.getvalue("checkbox8")),str(form.getvalue("checkbox9"))]
		if form.getvalue("checkbox1"):
			link = 'http://cistrome.org/db/batchdata/24KRO157XZ5Y204IEVFN.tar.gz'
			dtype = "Human FACTOR, 4.6G"
		elif form.getvalue("checkbox2"):
			link = 'http://cistrome.org/db/batchdata/GTYPP2KEMBOVQL3DDGS2.tar.gz'
			dtype = "Human HISTONE MARK AND VARIANT, 7.2G"
		elif form.getvalue("checkbox3"):
			link = 'http://cistrome.org/db/batchdata/R56Q7GGRZEY7L4PH4RA9.tar.gz'
			dtype = "Human CHROMATIN Accessibility, 3.0G"
		elif form.getvalue('checkbox5'):
			link = 'http://cistrome.org/db/batchdata/R9MXVUTB72SQ8FJLMWXU.tar.gz'
			dtype = "Mouse FACTOR, 3.0G"
		elif form.getvalue('checkbox6'):
			link = 'http://cistrome.org/db/batchdata/DPOUA6WA6SNLMRHVC7GW.tar.gz'
			dtype = "Mouse HISTONE MARK AND VARIANT, 5.2G"
		elif form.getvalue('checkbox7'):
			link = 'http://cistrome.org/db/batchdata/FGRVH30PLYTNOQPXMCUL.tar.gz'
			dtype = "Mouse CHROMATIN Accessibility, 1.5G"
		else:
			if not wrong:
				wrong = "Error: no data type selected!"

		print "Content-type:text/html"
		print

		# check captcha
		Captcha = form.getvalue("captcha")
		if not Captcha:
			if wrong:
				pass
			else:
				wrong = 'Please fill in Captcha'
		print "<html>"
		print "<head>"
		print "<meta charset=\"utf-8\">"
		print "<title>Cistrome data download</title>"
		print "</head>"
		print "<body>"
		print "<br/>"
		#print "<h2>%s：%s</h2>" % (user_name, user_PI)
		if wrong:
			print "<h2>%s</h2>" % ("<br/>"+wrong)
		else:
			# print '<meta http-equiv="refresh" content="0;url=%s" />' % link
			# infor = [user_name, user_PI, user_affinity, user_Email, dtype, str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time())))]
			# open('/data5/home/rongbin/public_html/release_interface/record/info.txt', 'a').write('\t'.join(infor)+'\n')
			content = "Hello {0},\n\nYou are REQUESTING CISTROME data on {1}: {2}".format(user_name, dtype, link)
			mail = send_mail([user_Email], 'CistromeDB data', content)
			if mail:
				infor = [user_name, user_PI, user_affinity, user_Email, dtype, str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time())))]
				open('./record/info.txt', 'a').write('\t'.join(infor)+'\n')
			print "<h2> %s</h2>" % ("Data you required is downloading"+"<br/>")
			print "<h2> %s: %s</h2>" % ("Your name", user_name+"<br/>")
			print "<h2> %s: %s</h2>" % ("Lab PI", user_PI+"<br/>")
			print "<h2> %s: %s</h2>" % ("Institute", user_affinity+"<br/>")
			print "<h2> %s: %s</h2>" % ("Email", user_Email+"<br/>")
			print "<h2> %s: %s</h2>" % ("Data type", dtype+"<br/>")

		print "</body>"
		print "</html>"
except:
	error = traceback.format_exc()


	# 

if error:
	print "Content-type:text/html"
	print

	print "<html>"
	print "<head>"
	print "<meta charset=\"utf-8\">"
	print "<title>Cistrome data download</title>"
	print "</head>"
	print "<body>"
	#print "<h2>%s：%s</h2>" % (user_name, user_PI)
	#print "<h2> %s: %s</h2>" % (user_name, error)
	print "<h2> %s <h2>"%('The page cannot be found')
	print "</body>"
	print "</html>"

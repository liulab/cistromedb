#!/usr/bin/python
# -*- coding: UTF-8 -*-

# filename：test.py


import cgi, cgitb, sys, os
import traceback
import time

form = cgi.FieldStorage() 

user_name = form.getvalue('name')
user_PI  = form.getvalue('institute')
comment = form.getvalue('textarea')
try:
	if not user_name and not user_PI and not comment:
		print "Content-type:text/html"
		print

		print "<html>"
		print "<head>"
		print "<meta charset=\"utf-8\">"
		print "<title>Cistrome data download</title>"
		print "</head>"
		print "<body>"
		print "<h1>%s</h1>" % ('Not Found')
		print "The requested URL http://cistrome.org/cgi-bin/rongbin/testimonial.py was not found on this server."

		# print "<h2>%s</h2>" % (os.path.abspath('.'))
		print "</body>"
		print "</html>"

	else:
		print "Content-type:text/html"
		print

		print "<html>"
		print "<body>"
		print "<br/>"
		now = str(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time())))
		try:
			all_inf = [x.rstrip().split('\t') for x in open('/var/www/html/db/testmonials.txt')]
			try:
				out = open('/var/www/html/db/testmonials.txt', 'w')
				inf = [all_inf[0], [str(user_name), str(user_PI), str(comment.replace('\r\n', ' ')), now]] + all_inf[1:]
				for x in inf:
					out.write('\t'.join(x)+'\n')
				out.close()
			except:
				out = open('/var/www/html/db/testmonials.txt', 'w')
				for x in all_inf:
					out.write('\t'.join(x)+'\n')
				out.close()	
			out = open('/data5/home/rongbin/public_html/release_interface/record/testmonial.txt', 'a')
			inf = [str(user_name), str(user_PI), str(comment.replace('\r\n', ' ')), now]
			out.write('\t'.join(inf)+'\n')
			out.close()	
		except:
			out = open('/data5/home/rongbin/public_html/release_interface/record/testmonial.txt', 'a')
			inf = [str(user_name), str(user_PI), str(comment.replace('\r\n', ' ')), now]
			out.write('\t'.join(inf)+'\n')
			out.close()	
	
		print "</body>"
		print "</html>"
except:
	error = traceback.format_exc()


	# 

if error:
	print "Content-type:text/html"
	print

	print "<html>"
	print "<head>"
	print "<meta charset=\"utf-8\">"
	print "<title>Cistrome data download</title>"
	print "</head>"
	print "<body>"
	#print "<h2>%s：%s</h2>" % (user_name, user_PI)
	#print "<h2> %s: %s</h2>" % (user_name, error)
	print "<h2> %s <h2>"%(error)
	print "</body>"
	print "</html>"

# How CistromeDB interface work? #
Language：AngularJS, coffeeScript, python 
* main things in daisy: /data/home/qqin/01_Projects/Programming/cistrangular, /data/home/qqin/01_Projects/Programming/dc2 
After revision, please do: 
```
$cd /data/home/qqin/01_Projects/Programming/cistrangular
$grunt build
$cp -r dist/* /var/www/html/db/
#'grunt build' works based on Yeoman（http://yeoman.io）
```
To install the whole platform 
```shell
#install dependencies
$cd cistrangular
$npm install
$bower install
#Serve Development Version
$grunt serve
$Serve Producton Version
$grunt serve:build
#Deploy to Web Server
#Change host and port to your own ones, change dest key to your deployed server path, then run,
$grunt sftp-deploy
```

Workflow:

1. fisrt go to /data/home/qqin/01_Projects/Programming/cistrangular/app/scripts/app.coffee if the cistrome.org/db be queried
2. refresh http://cistrome.org/db/#/, get html. As default, cistromeDB request all dc id / samples
3. /data/home/qqin/01_Projects/Programming/cistrangular/app/scripts/app.coffee receive the request, and then request /data/home/qqin/01_Projects/Programming/cistrangular/app/scripts/controllers/main_controller.coffee to interact with html
4. /data/home/qqin/01_Projects/Programming/cistrangular/app/scripts/main_service.coffee will query django by $http
5. /data/home/qqin/01_Projects/Programming/dc2/dc2/urls.py in dyango recieves the query from coffee
6. /data/home/qqin/01_Projects/Programming/dc2/datacollection/views.py, line 194 def inspector_ajax(request):, search in mysql
7. return the data using json format
8. get html showing data in json
9. /data/home/qqin/01_Projects/Programming/cistrangular/app/scripts/app.coffee returns the data
the HTML: http://getbootstrap.com/components/

Note: the scripts and files under ./other/batchmode_require is required in ./dc2/datacollection/views.py and  ./dc2/datacollection/batchmode.py for genome browser visulization in UCSC and WashU. 



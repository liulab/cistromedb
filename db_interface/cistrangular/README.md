# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is Cistrangular? ###

* The Front-end of Cistrome Dataset Browser

### Install Dependencies ###


```
#!shell

npm install
bower install
gem install compass

```

### Serve Development Version ###


```
#!shell

grunt serve

```

### Serve Producton Version ###


```
#!shell

grunt serve:build
```

### Deploy to Web Server ###

Change host and port to your own ones, change dest key to your deployed server path, then run,
```
#!shell

grunt sftp-deploy
```

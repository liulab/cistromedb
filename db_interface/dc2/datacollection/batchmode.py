import time
import urllib
import os
from .models import Samples
import time
import json
from django.db.models import Q
from django.shortcuts import redirect
from django.http import Http404, JsonResponse, HttpResponse
from datacollection.models import Samples
from django.core.paginator import Paginator
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

inputdir = ['/data1/DC_results/Result_new', '/data5/DC_results/Result_new', '/data5/DC_results/Result_new3', '/data5/DC_results/Result_new2',
'/data2/DC_results/Result_new', '/data3/DC_results/Result_new', '/data6/DC_results/Result_2016_ATAC', '/data6/DC_results/Result_2016_k27ac',
'/data6/DC_results/Result_encode3', '/data6/DC_results/Result_2016All', '/data5/DC_results/Result_2016All2', '/data6/DC_results/Result_2018Collect', '/data6/DC_results/DC_new']

#inputdir = '/data5/DC_results/Result_new'
#inputdir1 = '/data5/DC_results/Result_new3'
#inputdir2 = '/data5/DC_results/Result_new2'
#inputdir3 = '/data2/DC_results/Result_new'
#inputdir4 = '/data3/DC_results/Result_new'
#inputdir0 = '/data1/DC_results/Result_new'
#inputdir5 = '/data6/DC_results/Result_2016_ATAC'
#inputdir6 = '/data6/DC_results/Result_2016_k27ac'
#inputdir7 = '/data6/DC_results/Result_encode3'
#inputdir8 = '/data6/DC_results/Result_2016All'
#inputdir9 = '/data5/DC_results/Result_2016All2'

datahub = '/data5/browser'
scripts = '/data/home/qqin/01_Projects/Programming/dc2/scripts/'

def batch_filter(request):
    """
    query by both cell lines and factors in one form
    return all samples fit for the condition
    """
    if request.method == 'POST':
        data = json.loads(request.body)
        # cells = data.get('cells', None)
        # factors = data.get('factors', None)
        species = data.get('species', None)
        ids = data.get('ids', None)
        #if cells and factors and species:
        #    cells = cells.split('\n')
        #    factors = factors.split('\n')
        if ids:
            ids_f = []
            ids = ids.split('\n')
            for z in ids:
                z = z.split()
                ids_f += z
            if species == 'Human':
                s = 'Homo sapiens'
            else:
                s = 'Mus musculus'
        else:
            raise Http404

        q_s = Q(species__name=s)
        q = Q()
        # for f in factors:
        #     for c in cells:
        #         q |= Q(factor__name__iexact=f) & Q(cell_line__name__iexact=c)
        for i in ids_f:
            q |= Q(id = int(i))
        q = q_s & q & ~Q(status='hidden') & (Q(status='completed') | Q(status='run')) 
        sp = Samples.objects.filter(q)
        val = sp.values('species__name', 'factor__name', 'cell_line__name', 'id')

        return HttpResponse(json.dumps(list(val)), content_type='application/json')
    else:
        return HttpResponse('hello')

def washu_view(request, sp, ids, type, pos=None):
    """ datahub for WashU browser

    deposited temporary files: /data5/datahub
    input directory: /data5/DC_results/Result_new/
    use json up to WashU browser to transfer bed and bigwig 
    """
    if request.method == 'POST':
        obj = json.loads(request.body)
        ids = obj.get('ids', None)
        # position = obj.get('positon', None)
        species = obj.get('species', None)
        platform = obj.get('gb', None) # w: washu, u: ucsc
    else:
        if ids == None:
            raise Http404
        ids = map(int, ids.split('_'))
        species = sp
        platform = type # w: washu, u: ucsc
        position = pos

    if not ids:
        raise Http404

    if not position:
    	if species == 'h':
    		position = 'chr7:27066839-27266927'
    	else:
    		position = 'chr6:52003572-52426257'

    huburl = []
    anno_text = ''
    for sample_id in ids:
        folder = ''
        bed = ''
        bw = ''
        sample = Samples.objects.get(id=int(sample_id))
        sample_id = str(sample_id)
        for dir_path in inputdir:
            if os.path.exists(os.path.join(dir_path, 'dataset' + sample_id)):
                folder = os.path.join(dir_path, 'dataset' + sample_id)
                break
#        if os.path.exists(os.path.join(inputdir, 'dataset' + sample_id)):
#            folder = os.path.join(inputdir, 'dataset' + sample_id)
#        if os.path.exists(os.path.join(inputdir1, 'dataset' + sample_id)):
#            folder = os.path.join(inputdir1, 'dataset' + sample_id)
#        if os.path.exists(os.path.join(inputdir2, 'dataset' + sample_id)):
#            folder = os.path.join(inputdir2, 'dataset' + sample_id)
#        if os.path.exists(os.path.join(inputdir3, 'dataset' + sample_id)):
#            folder = os.path.join(inputdir3, 'dataset' + sample_id)
#        if os.path.exists(os.path.join(inputdir4, 'dataset' + sample_id)):
#            folder = os.path.join(inputdir4, 'dataset' + sample_id)
#        if os.path.exists(os.path.join(inputdir0, 'dataset' + sample_id)):
#            folder = os.path.join(inputdir0, 'dataset' + sample_id)
#        if os.path.exists(os.path.join(inputdir7, 'dataset' + sample_id)):
#            folder = os.path.join(inputdir7, 'dataset' + sample_id)
#        if os.path.exists(os.path.join(inputdir6, 'dataset' + sample_id)):
#            folder = os.path.join(inputdir6, 'dataset' + sample_id)
#        if os.path.exists(os.path.join(inputdir5, 'dataset' + sample_id)):
#            folder = os.path.join(inputdir5, 'dataset' + sample_id)
#        if os.path.exists(os.path.join(inputdir8, 'dataset' + sample_id)):
#            folder = os.path.join(inputdir8, 'dataset' + sample_id)
#        if os.path.exists(os.path.join(inputdir9, 'dataset' + sample_id)):
#            folder = os.path.join(inputdir9, 'dataset' + sample_id)

        if os.path.exists(os.path.join(folder, sample_id + '_b_sort_peaks.broadPeak.bed')):
            bed = os.path.join(folder, sample_id + '_b_sort_peaks.broadPeak.bed')
        if not os.path.exists(os.path.join(folder, sample_id + '_b_sort_peaks.broadPeak.bed')) and os.path.exists(os.path.join(folder, sample_id + '_sort_peaks.narrowPeak.bed')):
            bed = os.path.join(folder, sample_id + '_sort_peaks.narrowPeak.bed')

        if os.path.exists(os.path.join(folder, sample_id + '_treat.bw')):
            bw = os.path.join(folder, sample_id + '_treat.bw')

        if platform == 'w':
            tbed = os.path.join(datahub, os.path.basename(bed))
            tbw = os.path.join(datahub, os.path.basename(bw))
            if not os.path.exists(tbed):
                if os.path.exists(bed) and os.path.getsize(bed) > 0:
                    if 'narrow' in bed:
                        os.system('narrowpeak.py %s %s' % (bed, tbed))
                    else:
                        os.system('broadpeak.py %s %s' % (bed, tbed))
            if not os.path.exists(tbw):
                if os.path.exists(bw) and os.path.getsize(bw) > 0:
                    os.system('cp {bw} {tbw}'.format(**locals()))

            time.sleep(0.3)
            if os.path.exists(tbed + '.gz'):
                huburl += [{'type':'hammock',
                            'url': "http://dc2.cistrome.org" + tbed + '.gz',
                            'mode':'barplot',
                            'name': str(sample_id) + '_' + str(sample.cell_line).encode('utf-8') + '_' + str(sample.factor),
                            'showscoreidx':0,
                            'scorenamelst':["signal value", "P value (-log10)","Q value (-log10)"],
                            'boxcolor':'#210085',
                            'strokecolor':'#ff6600'}]
            if os.path.exists(tbw):
                huburl += [{"type": "bigwig", "url": "http://dc2.cistrome.org" + tbw, "mode": "show", "height": 50,
                            'name': str(sample_id) + '_' + str(sample.cell_line) + '_' + str(sample.factor)}]
        else:
            huburl = []
            if sp:
                if species == 'h':
                    db = 'hg38'
                    genome = os.path.join(scripts, 'hg38' + '.genome')
                else:
                    db = 'mm10'
                    genome = os.path.join(scripts, 'mm10' + '.genome')
            else:
                HttpResponse("no species specified")

            if os.path.exists(bed) and os.path.getsize(bed) > 0:
                tbed = os.path.join(datahub, os.path.basename(bed.replace('.bed','.bb')))
                tbw = os.path.join(datahub, os.path.basename(bw))
                if 'narrow' in bed:
                    os.system("{program} {genome} {bed} {bb}".format(
                        program=os.path.join(scripts, 'peak2bb.sh'),
                        genome=genome,
                        bed=bed,
                        bb=tbed))
                else:
                    os.system("{program} {genome} {bed} {bb}".format(
                        program=os.path.join(scripts, 'peak2bb.sh'),
                        genome=genome,
                        bed=bed,
                        bb=tbed))

            if os.path.exists(tbed):
                tbed_name = os.path.basename(tbed)
                anno_text += '''track  %s
	        bigDataUrl %s
	        type bigBed
	        shortLabel %s
	        longLabel %s
                visibility dense
                autoScale on
                viewLimits 0:13
                viewLimitsMax 0:50
                ''' % (str(sample_id) + '_' +  str(sample.cell_line), 'http://dc2.cistrome.org/' + tbed, str(sample.cell_line), str(sample.name))
                # anno_text += 'track visibility=2 type=bigBed bigDataUrl=http://dc2.cistrome.org{tbed} name="{tbed_name}" \n'.format(**locals())

            if os.path.exists(bw) and os.path.getsize(bw) > 0:
                os.system('cp {bw} {tbw}'.format(**locals()))
                bw_name = os.path.basename(bw)
            if os.path.exists(tbw):
                anno_text += '''
	        track %s
                visibility full
	        bigDataUrl %s
	        type bigWig
	        shortLabel %s
	        longLabel %s
                autoScale on
                viewLimits 0:13
                viewLimitsMax 0:50
                ''' % (str(sample_id) + '_' +  str(sample.cell_line), 'http://dc2.cistrome.org/' + tbw, str(sample.cell_line), str(sample.name))

    if platform == 'w':
        if species == 'h':
            s = 'hg38wugb'
        else:
            s = 'mm10'
        if huburl and len(huburl) <= 100:
            # in case too long file name, use time to represent
            # with open(os.path.join(datahub, '_'.join(map(str, ids))+'.json'), 'w') as inf: 
            stamp = time.time()
            with open(os.path.join(datahub, str(int(stamp)) + '.json'), 'w') as inf:
                json.dump(huburl, inf)
            # try internal browser http://cistrome.org/browser/, this only support hg38wugb
            if species == 'h':
                # return HttpResponse(json.dumps({'batchurl': "http://cistrome.org/browser/?genome=%s&datahub=http://dc2.cistrome.org" %(s) +os.path.join(datahub, str(int(stamp)) + '.json')+"&gftk=refGene,full"}), content_type = 'application/json')
                return redirect("http://cistrome.org/browser/?genome=%s&datahub=http://dc2.cistrome.org" %(s) +os.path.join(datahub, str(int(stamp)) + '.json')+"&gftk=refGene,full&coordinate=%s"%position)
            else:
                # return HttpResponse(json.dumps({'batchurl': "http://epigenomegateway.wustl.edu/browser/?genome=%s&datahub=http://dc2.cistrome.org" %(s) +os.path.join(datahub, str(int(stamp)) + '.json')+"&gftk=refGene,full"}), content_type = 'application/json')
                return redirect("http://epigenomegateway.wustl.edu/browser/?genome=%s&datahub=http://dc2.cistrome.org" %(s) +os.path.join(datahub, str(int(stamp)) + '.json')+"&gftk=refGene,full&coordinate=%s"%position)
        raise Http404, 'no data or beyond limited number'
    else:
        stamp = time.time()
        hub_text = '''
        hub Cistrome_Hub
        shortLabel Cistrome Analysis Hub
        longLabel Cistrome Analysis Data Hub
        genomesFile genomes_%s.txt
        email <a HREF="mailto:cistrome@cistrome.org" TARGET=_BLANK>cistrome</a>
        '''
        # genomes.txt
        genome_text ='''
        genome {0}
        trackDb {0}/trackDb_{1}.txt
        '''

        hub_file = open(os.path.join(datahub, 'hub_%s.txt' % str(int(stamp))), 'w')
        hub_file.write(hub_text % str(int(stamp)))
        hub_file.close()

        genome_file = open(os.path.join(datahub, 'genomes_%s.txt' % str(int(stamp))), 'w')
        genome_file.write(genome_text.format(db, str(int(stamp))))
        genome_file.close()

        track_file = open(os.path.join(datahub, db, 'trackDb_%s.txt' % str(int(stamp))), 'w')
        track_file.write(anno_text)
        track_file.close()

        # result_url = "http://genome.ucsc.edu/cgi-bin/hgTracks?db=" + db + "&hgct_customText="+urllib.quote_plus(anno_text)
        # http://genome.ucsc.edu/cgi-bin/hgTracks?db=hg19&hubUrl=http://ftp.ebi.ac.uk/pub/databases/ensembl/encode/integration_data_jan2011/hub.txt
        result_url = "http://genome.ucsc.edu/cgi-bin/hgTracks?db=" + db + "&hubUrl=http://dc2.cistrome.org/" + os.path.join(datahub, 'hub_' + str(int(stamp)) + '.txt&position=%s'%position)
        return redirect(result_url)



import os
import time
import urllib

from itertools import chain
import json as json
from django.core.paginator import Paginator
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.shortcuts import redirect
from django.template import RequestContext
from django.views.decorators.cache import cache_page
from datacollection.models import Samples
from datacollection.models import Aliases
from .utils import get_query


ab_cellline = "cl"
ab_celltype = "ct"
ab_tissue = "ti"
ab_cellpop = "cp"
ab_strain = "st"

## output dc results
## datahub for download and genome browser
##+++++
inputdir = ['/data1/DC_results/Result_new', '/data5/DC_results/Result_new', '/data5/DC_results/Result_new3', '/data5/DC_results/Result_new2',
'/data2/DC_results/Result_new', '/data3/DC_results/Result_new', '/data6/DC_results/Result_2016_ATAC', '/data6/DC_results/Result_2016_k27ac',
'/data6/DC_results/Result_encode3', '/data6/DC_results/Result_2016All', '/data5/DC_results/Result_2016All2', '/data6/DC_results/Result_2018Collect', '/data6/DC_results/DC_new']
# inputdir0 = '/data1/DC_results/Result_new'
# inputdir = '/data5/DC_results/Result_new'
# inputdir1 = '/data5/DC_results/Result_new3'
# inputdir2 = '/data5/DC_results/Result_new2'
# inputdir3 = '/data2/DC_results/Result_new'
# inputdir4 = '/data3/DC_results/Result_new'
# inputdir5 = '/data6/DC_results/Result_2016_ATAC'
# inputdir6 = '/data6/DC_results/Result_2016_k27ac'
# inputdir7 = '/data6/DC_results/Result_encode3'
# inputdir8 = '/data6/DC_results/Result_2016All'
# inputdir9 = '/data5/DC_results/Result_2016All2'

datahub = '/data5/browser'
scripts = '/data/home/qqin/01_Projects/Programming/dc2/scripts/'

def handler404(request):
    response = render_to_response('404.html', {},
                                  context_instance=RequestContext(request))
    response.status_code = 404
    return response

def handler500(request):
    response = render_to_response('404.html', {},
                                  context_instance=RequestContext(request))
    response.status_code = 500
    return response


@cache_page(60 * 60 * 24)
def main_filter_ng(request):
    """
    filter dc database information
    """
    req_s = request.GET.get("species", None)
    req_c = request.GET.get("cellinfos", None)
    req_f = request.GET.get("factors", None)
    req_p = int(request.GET.get("page", 1))

    req_run = request.GET.get("run", "false")
    req_curated = request.GET.get("curated", "false")
    req_completed = request.GET.get("completed", "false")

    req_keyword = request.GET.get("keyword", "")
    clicked = request.GET.get("clicked", None)
    req_sort = request.GET.get("colsort", None)

    req_allqc = request.GET.get("allqc", "false")
    req_peakqc = request.GET.get("peakqc", "false")

    if req_completed == "true" or req_run == "true" or req_curated == "true":
        if req_completed == "true" and req_run == "true" and req_curated == "true":
            q_common = Q(status="completed") | Q(status="run") | Q(status="curated")
        if req_completed == "false" and req_run == "true" and req_curated == "true":
            q_common = Q(status="run") | Q(status="curated")
        if req_completed == "false" and req_run == "false" and req_curated == "true":
            q_common = Q(status="curated")
        if req_completed == "true" and req_run == "true" and req_curated == "false":
            q_common = Q(status="completed") | Q(status="run")
        if req_completed == "true" and req_run == "false" and req_curated == "false":
            q_common = Q(status="completed")
        if req_completed == "true" and req_run == "false" and req_curated == "true":
            q_common = Q(status="completed") | Q(status="curated")
        if req_completed == "false" and req_run == "true" and req_curated == "false":
            q_common = Q(status="run")
    else:
        q_common = Q() ## all options are turned off

    if request.is_ajax() and (not (req_s and req_c and req_f)):
        return HttpResponse("Request denied!")

    if req_s == "all" or req_s == None:
        q_s = Q()
    else:
        q_s = Q(species__name=req_s)

    if req_f == "all" or req_f == None:
        q_f = Q()
    elif req_f == "None":
        q_f = Q(factor__isnull=True)
    else:
        q_f = Q(factor__name=req_f)

    q_f = q_f & ~Q(factor__name__icontains='input') & ~Q(factor__name__icontains='igg')
    q_f = q_f & ~Q(factor__name__in=['552-SKD', '598-SKD', '5HMC', '5MC', '7SK'])

    if req_c == "all" or req_c == None:
        q_c = Q()
    else:
        clip = lambda x: x[3:]
        if req_c.startswith(ab_cellline):
            q_c = Q(cell_line__name=clip(req_c))
        #elif req_c.startswith(ab_cellpop):
        #    q_c = Q(cell_pop__name=clip(req_c))
        elif req_c.startswith(ab_celltype):
            q_c = Q(cell_type__name=clip(req_c))
        elif req_c.startswith(ab_tissue):
            q_c = Q(tissue_type__name=clip(req_c))
        #elif req_c.startswith(ab_strain):
        #    q_c = Q(strain__name=clip(req_c))

    ## s is the search term for samples
    # s = Samples.objects.filter(q_s & q_f & q_c & q_common & ~Q(status='hidden') & ~Q(status='new')) ## filter to get completed one
    s = Samples.objects.filter(q_s & q_f & q_c & q_common & (Q(status='completed') | Q(status='run')))
    if req_keyword:
        try:
            req_keyword = int(req_keyword)
        except:
            pass
        if isinstance(req_keyword, int):
                #s = s.filter(full_text__iregex="[[:<:]]" + req_keyword)
                s = s.filter(id__iexact=req_keyword)
        else:
                if req_keyword.lower().startswith('gsm'):
                    s = s.filter(unique_id__iexact=req_keyword)
                elif req_keyword.lower().startswith('encode'):
                    s = s.filter(unique_id__icontains=req_keyword)
                elif req_keyword.lower().startswith('enc'):
                    encode_id = Q(unique_id__icontains=req_keyword)
                    s = s.filter(encode_id)
                else:
                    entry_query = get_query(req_keyword, ['factor__name', 'cell_line__name', 'cell_line__aliases', 'cell_type__name', 'cell_type__aliases', 'cell_pop__name', 'tissue_type__name', 'tissue_type__aliases', 'strain__name', 'species__name'])
                    # entry_query = get_query(req_keyword, ['factor__name', 'cell_line__name', 'cell_type__name', 'cell_pop__name', 'tissue_type__name', 'strain__name', 'species__name', 'paper__pub_summary', 'paper__reference'])
                    #abstract = Q(paper__abstract__iregex="[[:<:]]" + req_keyword)
                    #s = s.filter(entry_query | abstract)
                    factor_aliase = Aliases.objects.all()
                    factor_filter = [str(f.factor.name) for f in factor_aliase.filter(get_query(req_keyword, ['name']))]
                    if factor_filter:
                        for factor_query in factor_filter:
                            entry_query = entry_query | get_query(factor_query, ['factor__name'])
                    s = s.filter(entry_query)

    species = s.values_list("species__name", flat=True).order_by("species__name").distinct()

    if clicked != "factor_filter":
        factors = s.values_list("factor__name", flat=True).order_by("factor__name").distinct()
    else:
        factors = []

    if clicked != "cell_filter":
        # celllines = [(ab_cellline, i) for i in
        #              datasets.filter(cell_line__status="ok").values_list("cell_line__name", flat=True).distinct()]
        # celltypes = [(ab_celltype, i) for i in
        #              datasets.filter(cell_type__status="ok").values_list("cell_type__name", flat=True).distinct()]
        # cellpops = [(ab_cellpop, i) for i in
        #             datasets.filter(cell_pop__status="ok").values_list("cell_pop__name", flat=True).distinct()]
        # strains = [(ab_strain, i) for i in
        #            datasets.filter(strain__status="ok").values_list("strain__name", flat=True).distinct()]
        celllines = [(ab_cellline, i) for i in
                     s.values_list("cell_line__name", flat=True).distinct()]
        celltypes = [(ab_celltype, i) for i in
                     s.values_list("cell_type__name", flat=True).distinct()]
        tissues = [(ab_tissue, i) for i in s.values_list("tissue_type__name", flat=True).distinct()]
        #cellpops = [(ab_cellpop, i) for i in
        #            s.values_list("cell_pop__name", flat=True).distinct()]
        #strains = [(ab_strain, i) for i in
        #           s.values_list("strain__name", flat=True).distinct()]

        cellinfos = []
        for info in chain(celllines, celltypes, tissues):
            if info[1] and info[0]:
                cellinfos.append(info)
        cellinfos = sorted(cellinfos, key=lambda s: s[1].lower())

    else:
        cellinfos = []

    ## only visualize the treatment samples
    if req_sort == "specie":
        s= s.order_by("species__name")
    elif req_sort == "bs":
        #s = s.order_by("cell_line__name", "cell_type__name", "tissue_type__name")
        s = s.order_by('biological_source')
    elif req_sort == "factor":
        s = s.order_by("factor__name")
    elif req_sort == "pub":
        s = s.order_by("paper__reference")
    else:
        s = s.order_by('status')

    if req_allqc == "true":
        s = s.filter(qc_judge='{"map": true, "peaks": true, "fastqc": true, "frip": true, "pbc": true, "dhs": true}')
    elif req_peakqc == "true":
        s = s.filter(Q(qc_judge='{"map": true, "peaks": true, "fastqc": true, "frip": true, "pbc": true, "dhs": true}') | 
            Q(qc_judge='{"map": false, "peaks": true, "fastqc": true, "frip": true, "pbc": true, "dhs": true}') | 
            Q(qc_judge='{"map": true, "peaks": true, "fastqc": true, "frip": true, "pbc": false, "dhs": true}') | 
            Q(qc_judge='{"map": true, "peaks": true, "fastqc": false, "frip": true, "pbc": true, "dhs": true}') | 
            Q(qc_judge='{"map": false, "peaks": true, "fastqc": true, "frip": true, "pbc": false, "dhs": true}') | 
            Q(qc_judge='{"map": false, "peaks": true, "fastqc": false, "frip": true, "pbc": true, "dhs": true}') | 
            Q(qc_judge='{"map": true, "peaks": true, "fastqc": false, "frip": true, "pbc": false, "dhs": true}') | 
            Q(qc_judge='{"map": false, "peaks": true, "fastqc": false, "frip": true, "pbc": false, "dhs": true}'))
    else:
        s = s


    s_paginator = Paginator(s, 20)
    if req_p > s_paginator.num_pages:
        req_p = 1
    s_in_current_page = s_paginator.page(req_p)
    s_current = s_in_current_page.object_list.values("species__name", "factor__name",
                                                                   "cell_line__name", "cell_pop__name",
                                                                   "cell_type__name", "strain__name",
                                                                   "tissue_type__name","paper__reference",
                                                                   "status","paper__pub_summary","id",
                                                                   "biological_source", "qc_judge")
    content = list(s_current)

    for dataset in content:
        dataset["qc_judge"] = json.loads(dataset["qc_judge"])

    return HttpResponse(json.dumps({"species": list(species), "factors": list(factors), "cellinfos": list(cellinfos),
                                    "datasets": content, "num_pages": s_paginator.num_pages, "request_page": req_p}),
                        content_type='application/json')

@cache_page(60 * 60 * 24)
def inspector_ajax(request):
    """ inspector views 
    """
    req_dataset_id = request.GET.get("id", None)
    from django.core import signing
    sgn = signing.dumps({"id": req_dataset_id})

    if not req_dataset_id:
        return HttpResponse("Request denied!")
    else:
        s = Samples.objects.filter(id=req_dataset_id)
        if not s:
            return HttpResponse("Request denied!")

        status = s[0].status
        treat = s.values("species__name", "factor__name",
                         "cell_line__name", "cell_pop__name",
                         "cell_type__name", "strain__name",
                         "tissue_type__name",
                         "paper__journal__name", "disease_state__name",
                         "paper__reference", "paper__lab", "paper__pmid",
                         "name", "other_ids", "unique_id", "is_correcting").order_by("name")

        motif = False
        motif_url = ""

        s = s[0]
        if s.factor:
            if not os.path.exists(os.path.join('/data5/browser/motif_html/', req_dataset_id,'mdseqpos_index.html')):
                motif = False
            else:
                if os.path.getsize(os.path.join('/data5/browser/motif_html/', req_dataset_id, 'mdseqpos_index.html')) > 0:
                    motif = True
                    motif_url = "/motif_html/" + req_dataset_id + "/table.html"
            if not motif:
                if not os.path.exists(os.path.join('/data/motifs/', req_dataset_id,'mdseqpos_index.html')):
                    motif = False
                else:
                    if os.path.getsize(os.path.join('/data/motifs/', req_dataset_id, 'mdseqpos_index.html')) > 0:
                        motif = True
                        motif_url = "/motif_html2/" + req_dataset_id + "/table.html"
            if s.factor.type == 'hm':
                motif = False
        # Quality report and tooltip part, send json to Angular
        json_data = {}
        try:
            json_f = open("/data/home/qqin/12_data/json/%s.json" % req_dataset_id, "r")
            json_data = json.load(json_f)
            json_f.close()
        except IOError:
            json_data = {}
        #generate link for each sample
        for t in treat:
            if str(t['unique_id']).lower().startswith('gsm'):
                t['link'] = 'http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc='+str(t['unique_id'])
            else:
                t['link'] = "https://www.encodeproject.org/experiments/"+str(t['unique_id']).split('_')[0]
        return HttpResponse(json.dumps({"id": req_dataset_id, "treats": list(treat), 'sign': sgn, 
            "qc": json_data, "motif": motif, 'motif_url': motif_url, "status": status}), content_type='application/json')

@cache_page(60 * 60 * 24)
def show_image(request):
    req_id = request.GET.get("id", None)

    if not (req_id):
        return HttpResponse("Wrong parameter")

    for dir_path in inputdir:
        img = os.path.join(dir_path, 'dataset' + str(req_id), 'attic', str(req_id) + '_conserv_img.png')
        if os.path.exists(img):
            break
    # img = os.path.join(inputdir, 'dataset' + str(req_id), 'attic', str(req_id) + '_conserv_img.png')
    # if not os.path.exists(img):
    #     img = os.path.join(inputdir2, 'dataset' + str(req_id), 'attic', str(req_id) + '_conserv_img.png')
    #     if not os.path.exists(img):
    #         img = os.path.join(inputdir3, 'dataset' + str(req_id), 'attic', str(req_id) + '_conserv_img.png')
    #         if not os.path.exists(img):
    #             img = os.path.join(inputdir4, 'dataset' + str(req_id), 'attic', str(req_id) + '_conserv_img.png')
    #             if not os.path.exists(img):
    #                 img = os.path.join(inputdir1, 'dataset' + str(req_id), 'attic', str(req_id) + '_conserv_img.png')
    #                 if not os.path.exists(img):
    #                     img = os.path.join(inputdir0, 'dataset' + str(req_id), 'attic', str(req_id) + '_conserv_img.png')
    #                     if not os.path.exists(img):
    #                         img = os.path.join(inputdir5, 'dataset' + str(req_id), 'attic', str(req_id) + '_conserv_img.png')
    #                         if not os.path.exists(img):
    #                             img = os.path.join(inputdir6, 'dataset' + str(req_id), 'attic', str(req_id) + '_conserv_img.png')
    #                             if not os.path.exists(img):
    #                                 img = os.path.join(inputdir7, 'dataset' + str(req_id), 'attic', str(req_id) + '_conserv_img.png')
    #                                 if not os.path.exists(img):
    #                                     img = os.path.join(inputdir8, 'dataset' + str(req_id), 'attic', str(req_id) + '_conserv_img.png')
    #                                     if not os.path.exists(img):
    #                                         img = os.path.join(inputdir9, 'dataset' + str(req_id), 'attic', str(req_id) + '_conserv_img.png')



    if os.path.exists(img):
        with open(img, "rb") as f:
            return HttpResponse(f.read(), content_type="image/png")

    err = {req_id: "not found image"}
    return HttpResponse(json.dumps(err), content_type='application/json')

@cache_page(60 * 60 * 24)
def target_json(request):
    req_id = request.GET.get("id", None)
    req_gene = request.GET.get("gene", None)
    if not (req_id):
        return HttpResponse("Wrong parameter")

    for dir_path in inputdir:
        target_file = os.path.join(dir_path, 'dataset' + str(req_id), 'attic', str(req_id) + '_gene_score_5fold.txt')
        if os.path.exists(target_file):
            break
        else:
            if dir_path == inputdir[-1]:
                return HttpResponse("Dataset not processed")
    # target_file = os.path.join(inputdir, 'dataset' + str(req_id), 'attic', str(req_id) + '_gene_score_5fold.txt')
    # if not os.path.exists(target_file):    
    #     target_file = os.path.join(inputdir2, 'dataset' + str(req_id), 'attic', str(req_id) + '_gene_score_5fold.txt')
    #     if not os.path.exists(target_file):    
    #         target_file = os.path.join(inputdir3, 'dataset' + str(req_id), 'attic', str(req_id) + '_gene_score_5fold.txt')
    #         if not os.path.exists(target_file):    
    #             target_file = os.path.join(inputdir4, 'dataset' + str(req_id), 'attic', str(req_id) + '_gene_score_5fold.txt')
    #             if not os.path.exists(target_file):    
    #                 target_file = os.path.join(inputdir1, 'dataset' + str(req_id), 'attic', str(req_id) + '_gene_score_5fold.txt')
    #                 if not os.path.exists(target_file):    
    #                     target_file = os.path.join(inputdir0, 'dataset' + str(req_id), 'attic', str(req_id) + '_gene_score_5fold.txt')
    #                     if not os.path.exists(target_file):    
    #                         target_file = os.path.join(inputdir, 'dataset' + str(req_id), 'attic', str(req_id) + '_gene_score_5fold.txt')
    #                         if not os.path.exists(target_file):
    #                             target_file = os.path.join(inputdir5, 'dataset' + str(req_id), 'attic', str(req_id) + '_gene_score_5fold.txt')
    #                             if not os.path.exists(target_file):
    #                                 target_file = os.path.join(inputdir6, 'dataset' + str(req_id), 'attic', str(req_id) + '_gene_score_5fold.txt')
    #                                 if not os.path.exists(target_file):
    #                                     target_file = os.path.join(inputdir7, 'dataset' + str(req_id), 'attic', str(req_id) + '_gene_score_5fold.txt')
    #                                     if not os.path.exists(target_file):
    #                                         target_file = os.path.join(inputdir8, 'dataset' + str(req_id), 'attic', str(req_id) + '_gene_score_5fold.txt')
    #                                         if not os.path.exists(target_file):
    #                                             target_file = os.path.join(inputdir9, 'dataset' + str(req_id), 'attic', str(req_id) + '_gene_score_5fold.txt')
    #                                             if not os.path.exists(target_file):    
                                                    # return HttpResponse("Dataset not processed")
    target_cnt = 0
    gene_dict = {}
    target_list = []
    gene_scanned = []
    with open(target_file) as tf:
        for line in tf:
            if line.startswith("#"):
                continue
            if target_cnt == 100:
                break
            cols = line.strip().split("\t")

            gene_symbol = cols[6]
            if req_gene and cols[6].upper() != req_gene.upper():
                continue

            visual_coordinate = cols[0] + ":" + str(max(int(cols[1])-100000, 0)) + "-" + str(int(cols[2]) + 100000)
            if req_gene:
                target_list = [
                    {"coordinate":cols[0]+ ":" + cols[1] +"-" + cols[2], "score": cols[4], "symbol": gene_symbol,
                     "visual_coordinate": visual_coordinate}]
                break
            if gene_symbol in gene_scanned:
                continue
            else:
                gene_scanned.append(gene_symbol)

            if gene_dict.has_key(visual_coordinate):
                if gene_symbol not in gene_dict[visual_coordinate]["symbol"]:
                    target_list[gene_dict[visual_coordinate]["location"]]["symbol"] += " / %s" % gene_symbol

            else:
                gene_dict[visual_coordinate] = {"location": target_cnt, "symbol": [gene_symbol]}


            if float(cols[4]) <= 0:
                break

            target_list += [
                {"coordinate":cols[0] + ":" + cols[1] + "-" + cols[2], "score": cols[4], "symbol": cols[6],
                 "visual_coordinate": visual_coordinate}]
            target_cnt += 1
    return HttpResponse(json.dumps(target_list))


def ucsc_view(request, sample_id=None):
    """
    url example: h[<8;27;12m]ttp://genome.ucsc.edu/cgi-bin/hgTracks?db=hg18&position=chr21:33038447-33041505&hgct_customText=track%20type=bigBed%20name=myBigBedTrack%20description=%22a%20bigBed%20track%22%20visibility=full%20bigDataUrl=http://genome.ucsc.edu/goldenPath/help/examples/bigBedExample.bb
    """
    db = request.GET.get("db", None)
    position = request.GET.get("position", None)

    for dir_path in inputdir:
        if os.path.exists(os.path.join(dir_path, 'dataset' + sample_id)):
            folder = os.path.join(dir_path, 'dataset' + sample_id)
            break
    # if os.path.exists(os.path.join(inputdir, 'dataset' + sample_id)):
    #     folder = os.path.join(inputdir, 'dataset' + sample_id)
    # if os.path.exists(os.path.join(inputdir0, 'dataset' + sample_id)):
    #     folder = os.path.join(inputdir0, 'dataset' + sample_id)
    # if os.path.exists(os.path.join(inputdir2, 'dataset' + sample_id)):
    #     folder = os.path.join(inputdir2, 'dataset' + sample_id)
    # if os.path.exists(os.path.join(inputdir3, 'dataset' + sample_id)):
    #     folder = os.path.join(inputdir3, 'dataset' + sample_id)
    # if os.path.exists(os.path.join(inputdir4, 'dataset' + sample_id)):
    #     folder = os.path.join(inputdir4, 'dataset' + sample_id)
    # if os.path.exists(os.path.join(inputdir1, 'dataset' + sample_id)):
    #     folder = os.path.join(inputdir1, 'dataset' + sample_id)
    # if os.path.exists(os.path.join(inputdir5, 'dataset' + sample_id)):
    #     folder = os.path.join(inputdir5, 'dataset' + sample_id)
    # if os.path.exists(os.path.join(inputdir6, 'dataset' + sample_id)):
    #     folder = os.path.join(inputdir6, 'dataset' + sample_id)
    # if os.path.exists(os.path.join(inputdir7, 'dataset' + sample_id)):
    #     folder = os.path.join(inputdir7, 'dataset' + sample_id)
    # if os.path.exists(os.path.join(inputdir8, 'dataset' + sample_id)):
    #     folder = os.path.join(inputdir8, 'dataset' + sample_id)
    # if os.path.exists(os.path.join(inputdir9, 'dataset' + sample_id)):
    #     folder = os.path.join(inputdir9, 'dataset' + sample_id)
    
    if os.path.exists(os.path.join(folder, sample_id + '_b_sort_peaks.broadPeak.bed')):
        bed = os.path.join(folder, sample_id + '_b_sort_peaks.broadPeak.bed')
    if not os.path.exists(os.path.join(folder, sample_id + '_b_sort_peaks.broadPeak.bed')) and  os.path.exists(os.path.join(folder, sample_id + '_sort_peaks.narrowPeak.bed')):
        bed = os.path.join(folder, sample_id + '_sort_peaks.narrowPeak.bed')

    bw = os.path.join(folder, sample_id + '_treat.bw')
    tbw = os.path.join(datahub, os.path.basename(bw))

    huburl = []
    
    if db:
        genome = os.path.join(scripts, db + '.genome')
    else:
        HttpResponse("no species specified")

    anno_text = ''
    if os.path.exists(bed) and os.path.getsize(bed) > 0:
        tbed = os.path.join(datahub, os.path.basename(bed.replace('.bed','.bb')))
        tbed_name = os.path.basename(tbed)
        if 'narrow' in bed:
            os.system("{program} {genome} {bed} {bb}".format(
                program=os.path.join(scripts, 'peak2bb.sh'),
                genome=genome,
                bed=bed,
                bb=tbed))
        else:
            os.system("{program} {genome} {bed} {bb}".format(
                program=os.path.join(scripts, 'peak2bb.sh'),
                genome=genome,
                bed=bed,
                bb=tbed))

        if os.path.exists(tbed):
            anno_text += 'track visibility=2 type=bigBed bigDataUrl=http://dc2.cistrome.org{tbed} name="{tbed_name}" '.format(**locals())

    if os.path.exists(bw) and os.path.getsize(bw) > 0:
        os.system('cp {bw} {tbw}'.format(**locals()))
        bw_name = os.path.basename(bw)

        if os.path.exists(tbw):
            anno_text += 'track visibility=2 type=bigWig bigDataUrl=http://dc2.cistrome.org{tbw} name="{bw_name}"'.format(**locals())

    result_url = "http://genome.ucsc.edu/cgi-bin/hgTracks?db=" + db + "&hgct_customText="+urllib.quote_plus(anno_text)
    if position:
        result_url += "&position=" + position        
    return redirect(result_url)
    
def washu_view(request, sample_id=None):
    """ datahub for WashU browser

    deposited temporary files: /data5/datahub
    input directory: /data5/DC_results/Result_new/
    use json up to WashU browser to transfer bed and bigwig 
    """
    #sample = Samples.objects.get(id=int(sample_id))
    for dir_path in inputdir:
        if os.path.exists(os.path.join(dir_path, 'dataset' + sample_id)):
            folder = os.path.join(dir_path, 'dataset' + sample_id)
            break
    # if os.path.exists(os.path.join(inputdir0, 'dataset' + sample_id)):
    #     folder = os.path.join(inputdir0, 'dataset' + sample_id)
    # if os.path.exists(os.path.join(inputdir, 'dataset' + sample_id)):
    #     folder = os.path.join(inputdir, 'dataset' + sample_id)
    # if os.path.exists(os.path.join(inputdir2, 'dataset' + sample_id)):
    #     folder = os.path.join(inputdir2, 'dataset' + sample_id)
    # if os.path.exists(os.path.join(inputdir3, 'dataset' + sample_id)):
    #     folder = os.path.join(inputdir3, 'dataset' + sample_id)
    # if os.path.exists(os.path.join(inputdir4, 'dataset' + sample_id)):
    #     folder = os.path.join(inputdir4, 'dataset' + sample_id)
    # if os.path.exists(os.path.join(inputdir1, 'dataset' + sample_id)):
    #     folder = os.path.join(inputdir1, 'dataset' + sample_id)
    # if os.path.exists(os.path.join(inputdir5, 'dataset' + sample_id)):
    #     folder = os.path.join(inputdir5, 'dataset' + sample_id)
    # if os.path.exists(os.path.join(inputdir6, 'dataset' + sample_id)):
    #     folder = os.path.join(inputdir6, 'dataset' + sample_id)
    # if os.path.exists(os.path.join(inputdir7, 'dataset' + sample_id)):
    #     folder = os.path.join(inputdir7, 'dataset' + sample_id)
    # if os.path.exists(os.path.join(inputdir8, 'dataset' + sample_id)):
    #     folder = os.path.join(inputdir8, 'dataset' + sample_id)
    # if os.path.exists(os.path.join(inputdir9, 'dataset' + sample_id)):
    #     folder = os.path.join(inputdir9, 'dataset' + sample_id)


    if os.path.exists(os.path.join(folder, sample_id + '_b_sort_peaks.broadPeak.bed')):
        bed = os.path.join(folder, sample_id + '_b_sort_peaks.broadPeak.bed')
    if not os.path.exists(os.path.join(folder, sample_id + '_b_sort_peaks.broadPeak.bed')) and os.path.exists(os.path.join(folder, sample_id + '_sort_peaks.narrowPeak.bed')):
        bed = os.path.join(folder, sample_id + '_sort_peaks.narrowPeak.bed')

    tbed = os.path.join(datahub, os.path.basename(bed))
    
    bw = os.path.join(folder, sample_id + '_treat.bw')
    tbw = os.path.join(datahub, os.path.basename(bw))

    huburl = []
    
    # Shirley don't want this
    #if os.path.exists(bed) and os.path.getsize(bed) > 0:
    #    if 'narrow' in bed:
    #        os.system('narrowpeak.py %s %s' % (bed, tbed))
    #    else:
    #        os.system('broadpeak.py %s %s' % (bed, tbed))

    if os.path.exists(bw) and os.path.getsize(bw) > 0:
        os.system('cp {bw} {tbw}'.format(**locals()))

    time.sleep(0.3)

    #if os.path.exists(tbed + '.gz'):
    #    huburl += [{'type':'hammock',
    #                'url': "http://dc2.cistrome.org" + tbed + '.gz',
    #                'mode':'barplot',
    #                'name':bed.split('/')[-1],
    #                'showscoreidx':0,
    #                'scorenamelst':["signal value", "P value (-log10)","Q value (-log10)"],
    #                'boxcolor':'#210085',
    #                'strokecolor':'#ff6600'}]
    if os.path.exists(tbw):
        huburl += [{"type": "bigwig", "url": "http://dc2.cistrome.org" + tbw, "mode": "show", "height": 50, "name": bw.split("/")[-1]}]
                   
    if huburl:
        return HttpResponse(json.dumps(huburl), content_type = 'application/json')

    
def file_view(request):
    req_type = request.GET.get("type", None)
    req_id = request.GET.get("id", None)

    if not (req_id and req_type):
        return HttpResponse("Wrong parameter")

    for dir_path in inputdir:
        if os.path.exists(os.path.join(dir_path, 'dataset' + req_id)):
            folder = os.path.join(dir_path, 'dataset' + req_id)
            break
    # if os.path.exists(os.path.join(inputdir0, 'dataset' + req_id)):
    #     folder = os.path.join(inputdir0, 'dataset' + req_id)
    # if os.path.exists(os.path.join(inputdir5, 'dataset' + req_id)):
    #     folder = os.path.join(inputdir5, 'dataset' + req_id)
    # if os.path.exists(os.path.join(inputdir6, 'dataset' + req_id)):
    #     folder = os.path.join(inputdir6, 'dataset' + req_id)
    # if os.path.exists(os.path.join(inputdir7, 'dataset' + req_id)):
    #     folder = os.path.join(inputdir7, 'dataset' + req_id)
    # if os.path.exists(os.path.join(inputdir, 'dataset' + req_id)):
    #     folder = os.path.join(inputdir, 'dataset' + req_id)
    # if os.path.exists(os.path.join(inputdir2, 'dataset' + req_id)):
    #     folder = os.path.join(inputdir2, 'dataset' + req_id)
    # if os.path.exists(os.path.join(inputdir3, 'dataset' + req_id)):
    #     folder = os.path.join(inputdir3, 'dataset' + req_id)
    # if os.path.exists(os.path.join(inputdir4, 'dataset' + req_id)):
    #     folder = os.path.join(inputdir4, 'dataset' + req_id)
    # if os.path.exists(os.path.join(inputdir1, 'dataset' + req_id)):
    #     folder = os.path.join(inputdir1, 'dataset' + req_id)
    # if os.path.exists(os.path.join(inputdir8, 'dataset' + req_id)):
    #     folder = os.path.join(inputdir8, 'dataset' + req_id)
    # if os.path.exists(os.path.join(inputdir9, 'dataset' + req_id)):
    #     folder = os.path.join(inputdir9, 'dataset' + req_id)
        
    if req_type == "bed":
        f = os.path.join(folder, req_id + '_peaks.bed')
        fout = os.path.join(datahub, req_id + '_peaks.bed')

    if req_type == 'bw':
        f = os.path.join(folder, req_id + '_treat.bw')
        fout = os.path.join(datahub, req_id + '_treat.bw')

    if req_type == 'pt':
        f = os.path.join(folder, 'attic', req_id + '_gene_score_5fold.txt')
        fout = os.path.join(datahub, req_id + '_gene_score_5fold.txt')
        
    if os.path.exists(f) and os.path.getsize(f) > 0:
        os.system('cp ' + f + ' ' + fout)
        return redirect("http://dc2.cistrome.org" + fout)

def check_authenticated(req):
    if not req.user.is_authenticated():
        return HttpResponse(json.dumps({"status": "logout"}))
    else:
        return HttpResponse(
                json.dumps({"status": "login", "username": req.user.username}), content_type = 'application/json')


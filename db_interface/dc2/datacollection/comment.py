import time
import urllib
import sys
import os
from .models import Samples,models
import datetime
import json
from django.db.models import Q
from django.shortcuts import redirect
from django.http import Http404, JsonResponse, HttpResponse, HttpResponseRedirect
from datacollection.models import Samples, CellLines, CellTypes, Papers, Factors, CellPops, Strains, DiseaseStates, TissueTypes, Species, Corrections
from django.core.paginator import Paginator
from django.views.decorators.csrf import csrf_exempt
import pubmed
# from .views import logger
# import logging
# log = logging.getLogger(__name__)
# logging.basicConfig(level=logging.DEBUG,
#                     format='%(asctime)s %(levelname)s %(message)s',
#                     filemode='a')
# logger = logging.getLogger()
# handler = logging.FileHandler('/data/home/qqin/01_Projects/Programming/dc2/comments.txt')
# logger.addHandler(handler)


@csrf_exempt
def commentapi(request):
    if request.method == 'POST':
        obj = json.loads(request.body)
        ids = obj.get('ids', None)
        dcid = Samples.objects.get(id=ids)
        treatment = obj.get('treatment', None)
        species = obj.get('species', None)
        pmid = obj.get('pmid', None)
        factor = obj.get('factor', None)
        cellline = obj.get('cellline', None)
        celltype = obj.get('celltype', None)
        cellpop = obj.get('cellpop', None)
        strain = obj.get('strain', None)
        tissue = obj.get('tissue', None)
        disease = obj.get('disease', None)
        comment = obj.get('comment', None)
        email = obj.get('email', None)
        dic = {'dc_id': dcid, 'treatment': treatment, 'species': species, 'pmid': pmid, 'factor': factor,
               'cell_line': cellline, 'cell_type': celltype, 'cell_pop': cellpop, 'strain': strain,
               'tissue': tissue, 'disease': disease, 'comment': comment, 'email': email}
        Corrections.objects.create(**dic)
        Samples.objects.filter(id=ids).update(is_correcting=True)
        return HttpResponse("Succeeded! Please wait us to have a check!")
    else:
        return HttpResponse("Request denied!")


def view_comment(request):
    if not request.user.is_authenticated():
        return HttpResponse("Need to login!")
    else:
        req_correction_status = request.GET.get("status", None)
        if req_correction_status not in ['0', '-1', '1']:
            return HttpResponse("Request denied!")
        else:
            if req_correction_status != '1':
                selects = Corrections.objects.filter(status=req_correction_status)
                if not selects:
                    return HttpResponse(None)
                else:
                    corrects = selects.values("id", "treatment", "species", "pmid", "factor", "cell_line", "cell_type",
                                              "cell_pop", "strain", "tissue", "disease", "comment", "email", "time", "factor_type",
                                              "status", "dc_id_id", "dc_id__unique_id", "dc_id__name", "dc_id__species_id__name",
                                              "dc_id__paper_id__pmid", "dc_id__factor_id__name", "dc_id__factor_id__type",
                                              "dc_id__cell_line_id__name", "dc_id__cell_type_id__name", "dc_id__cell_pop_id__name",
                                              "dc_id__strain_id__name", "dc_id__tissue_type_id__name", "dc_id__disease_state_id__name"
                                              ).order_by("id")
                    for d in corrects:
                        d["time"] = d["time"].strftime("%Y-%m-%d %H:%M:%S")
                        d["raw_name"] = d.pop("dc_id__name")
                        d["raw_species"] = d.pop("dc_id__species_id__name")
                        d["raw_pmid"] = str(d.pop("dc_id__paper_id__pmid"))
                        d["raw_factor"] = d.pop("dc_id__factor_id__name")
                        d["raw_factor_type"] = d.pop("dc_id__factor_id__type")
                        d["raw_cell_line"] = d.pop("dc_id__cell_line_id__name")
                        d["raw_cell_type"] = d.pop("dc_id__cell_type_id__name")
                        d["raw_cell_pop"] = d.pop("dc_id__cell_pop_id__name")
                        d["raw_strain"] = d.pop("dc_id__strain_id__name")
                        d["raw_tissue"] = d.pop("dc_id__tissue_type_id__name")
                        d["raw_disease"] = d.pop("dc_id__disease_state_id__name")
                        if str(d['dc_id__unique_id']).lower().startswith('gsm'):
                            d['link'] = 'http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=' + str(d['dc_id__unique_id'])
                        else:
                            d['link'] = "https://www.encodeproject.org/experiments/" + str(d['dc_id__unique_id']).split('_')[0]
                    return HttpResponse(json.dumps({"status": req_correction_status, "corrects": list(corrects)}),
                                        content_type='application/json')
            else:
                selects = Corrections.objects.filter(status=req_correction_status)
                if not selects:
                    return HttpResponse(None)
                else:
                    corrects = selects.values("id", "comment", "email", "time",
                                              "status", "dc_id_id", "dc_id__unique_id", "dc_id__name",
                                              "dc_id__species_id__name", "dc_id__paper_id__pmid", "dc_id__factor_id__name",
                                              "dc_id__factor_id__type", "dc_id__cell_line_id__name",
                                              "dc_id__cell_type_id__name", "dc_id__cell_pop_id__name",
                                              "dc_id__strain_id__name", "dc_id__tissue_type_id__name",
                                              "dc_id__disease_state_id__name", "last_treatment", "last_species",
                                              "last_pmid", "last_factor", "last_factor_type", "last_cell_pop",
                                              "last_cell_line", "last_cell_type",
                                              "last_strain", "last_tissue", "last_disease").order_by("id")
                    for d in corrects:
                        d["time"] = d["time"].strftime("%Y-%m-%d %H:%M:%S")
                        d["raw_name"] = d.pop("last_treatment")
                        d["raw_species"] = d.pop("last_species")
                        d["raw_pmid"] = str(d.pop("last_pmid"))
                        d["raw_factor"] = d.pop("last_factor")
                        d["raw_factor_type"] = d.pop("last_factor_type")
                        d["raw_cell_line"] = d.pop("last_cell_line")
                        d["raw_cell_type"] = d.pop("last_cell_type")
                        d["raw_cell_pop"] = d.pop("last_cell_pop")
                        d["raw_strain"] = d.pop("last_strain")
                        d["raw_tissue"] = d.pop("last_tissue")
                        d["raw_disease"] = d.pop("last_disease")
                        d["treatment"] = d.pop("dc_id__name")
                        d["species"] = d.pop("dc_id__species_id__name")
                        d["pmid"] = str(d.pop("dc_id__paper_id__pmid"))
                        d["factor"] = d.pop("dc_id__factor_id__name")
                        d["factor_type"] = d.pop("dc_id__factor_id__type")
                        d["cell_line"] = d.pop("dc_id__cell_line_id__name")
                        d["cell_type"] = d.pop("dc_id__cell_type_id__name")
                        d["cell_pop"] = d.pop("dc_id__cell_pop_id__name")
                        d["strain"] = d.pop("dc_id__strain_id__name")
                        d["tissue"] = d.pop("dc_id__tissue_type_id__name")
                        d["disease"] = d.pop("dc_id__disease_state_id__name")
                        if str(d['dc_id__unique_id']).lower().startswith('gsm'):
                            d['link'] = 'http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=' + str(d['dc_id__unique_id'])
                        else:
                            d['link'] = "https://www.encodeproject.org/experiments/" + str(d['dc_id__unique_id']).split('_')[0]
                    return HttpResponse(json.dumps({"status": req_correction_status, "corrects": list(corrects)}),
                                        content_type='application/json')

@csrf_exempt
def verify(request):
    if not request.user.is_authenticated():
        return HttpResponse("Need to login!")
    else:
        if request.method == 'POST':
            obj = json.loads(request.body)
            corrects_id = obj.get('co_id', None)
            ids = obj.get('dc_id', None)
            c_treatment = obj.get('treatment', None)
            c_species = obj.get('species', None)
            c_pmid = obj.get('pmid', None)
            c_factor = obj.get('factor', None)
            c_factortype = obj.get('factor_type', None)
            c_cellline = obj.get('cell_line', None)
            c_celltype = obj.get('cell_type', None)
            c_cellpop = obj.get('cell_pop', None)
            c_strain = obj.get('strain', None)
            c_tissue = obj.get('tissue', None)
            c_disease = obj.get('disease', None)
            raw_selects = Samples.objects.filter(id=ids)
            if not raw_selects:
                return HttpResponse("Wrong!")
            else:
                if not c_treatment:
                    return HttpResponse("Invalid Treatment!")
                if c_species:
                    if not Species.objects.filter(name=c_species):
                        return HttpResponse('Invalid Species!')
                else:
                    return HttpResponse("Species can not be Null!")
                # save raw data
                raw_data = raw_selects.values('id', 'name', 'species_id__name', 'species_id', 'paper_id__pmid', 'paper_id',
                                              'factor_id', 'factor_id__name', 'factor_id__type', 'cell_line_id', 'cell_type_id',
                                              'cell_line_id__name', 'cell_type_id__name', 'cell_pop_id__name', 'cell_pop_id',
                                              'strain_id__name', 'strain_id', 'tissue_type_id__name', 'tissue_type_id',
                                              'disease_state_id__name', 'disease_state_id')[0]
                Corrections.objects.filter(id=corrects_id).update(factor_type=c_factortype, last_treatment=raw_data["name"],
                                                                  last_species=raw_data["species_id__name"],
                                                                  last_pmid=raw_data["paper_id__pmid"],
                                                                  last_factor=raw_data["factor_id__name"],
                                                                  last_factor_type=raw_data["factor_id__type"],
                                                                  last_cell_line=raw_data["cell_line_id__name"],
                                                                  last_cell_type=raw_data["cell_type_id__name"],
                                                                  last_cell_pop=raw_data["cell_pop_id__name"],
                                                                  last_strain=raw_data["strain_id__name"],
                                                                  last_tissue=raw_data["tissue_type_id__name"],
                                                                  last_disease=raw_data["disease_state_id__name"],
                                                                  treatment=c_treatment, species=c_species,
                                                                  pmid=c_pmid, factor=c_factor, cell_line=c_cellline,
                                                                  cell_type=c_celltype, cell_pop=c_cellpop,
                                                                  strain=c_strain, tissue=c_tissue, disease=c_disease)
                # pmid
                if Papers.objects.filter(pmid=c_pmid):
                    paper_id = Papers.objects.filter(pmid=c_pmid).values('id')[0]['id']
                    Samples.objects.filter(id=ids).update(paper_id=paper_id)
                else:
                    paper_id = pubmed.getOrCreatePaper(c_pmid)
                    Samples.objects.filter(id=ids).update(paper_id=paper_id)
                    #return HttpResponse("Can not find PMID in Cistrome Database!")
                # name
                Samples.objects.filter(id=ids).update(name=c_treatment)
                # Species
                species_id = Species.objects.filter(name=c_species).values('id')[0]['id']
                Samples.objects.filter(id=ids).update(species_id=species_id)
                # factor
                if c_factor:
                    if Factors.objects.filter(name=c_factor):
                        factor_id = Factors.objects.filter(name=c_factor).values('id')[0]['id']
                        Samples.objects.filter(id=ids).update(factor_id=factor_id)
                    else:
                        Factors.objects.create(name=c_factor, type=c_factortype)
                        factor_id = Factors.objects.filter(name=c_factor).values('id')[0]['id']
                        Samples.objects.filter(id=ids).update(factor_id=factor_id)
                else:
                    Samples.objects.filter(id=ids).update(factor_id=None)
                # cell line
                if c_cellline:
                    if CellLines.objects.filter(name=c_cellline):
                        cell_line_id = CellLines.objects.filter(name=c_cellline).values('id')[0]['id']
                        Samples.objects.filter(id=ids).update(cell_line_id=cell_line_id)
                    else:
                        CellLines.objects.create(name=c_cellline)
                        cell_line_id = CellLines.objects.filter(name=c_cellline).values('id')[0]['id']
                        Samples.objects.filter(id=ids).update(cell_line_id=cell_line_id)
                else:
                    Samples.objects.filter(id=ids).update(cell_line_id=None)
                # cell type
                if c_celltype:
                    if CellTypes.objects.filter(name=c_celltype):
                        cell_type_id = CellTypes.objects.filter(name=c_celltype).values('id')[0]['id']
                        Samples.objects.filter(id=ids).update(cell_type_id=cell_type_id)
                    else:
                        CellTypes.objects.create(name=c_celltype)
                        cell_type_id = CellTypes.objects.filter(name=c_celltype).values('id')[0]['id']
                        Samples.objects.filter(id=ids).update(cell_type_id=cell_type_id)
                else:
                    Samples.objects.filter(id=ids).update(cell_type_id=None)
                # cell pop
                if c_cellpop:
                    if CellPops.objects.filter(name=c_cellpop):
                        cell_pop_id = CellPops.objects.filter(name=c_cellpop).values('id')[0]['id']
                        Samples.objects.filter(id=ids).update(cell_pop_id=cell_pop_id)
                    else:
                        CellPops.objects.create(name=c_cellpop)
                        cell_pop_id = CellPops.objects.filter(name=c_cellpop).values('id')[0]['id']
                        Samples.objects.filter(id=ids).update(cell_pop_id=cell_pop_id)
                else:
                    Samples.objects.filter(id=ids).update(cell_pop_id=None)
                # strain
                if c_strain:
                    if Strains.objects.filter(name=c_strain):
                        strain_id = Strains.objects.filter(name=c_strain).values('id')[0]['id']
                        Samples.objects.filter(id=ids).update(strain_id=strain_id)
                    else:
                        Strains.objects.create(name=c_strain)
                        strain_id = Strains.objects.filter(name=c_strain).values('id')[0]['id']
                        Samples.objects.filter(id=ids).update(strain_id=strain_id)
                else:
                    Samples.objects.filter(id=ids).update(strain_id=None)
                # tissue
                if c_tissue:
                    if TissueTypes.objects.filter(name=c_tissue):
                        tissue_id = TissueTypes.objects.filter(name=c_tissue).values('id')[0]['id']
                        Samples.objects.filter(id=ids).update(tissue_type_id=tissue_id)
                    else:
                        TissueTypes.objects.create(name=c_tissue)
                        tissue_id = TissueTypes.objects.filter(name=c_tissue).values('id')[0]['id']
                        Samples.objects.filter(id=ids).update(tissue_type_id=tissue_id)
                else:
                    Samples.objects.filter(id=ids).update(tissue_type_id=None)
                # disease
                if c_disease:
                    if DiseaseStates.objects.filter(name=c_disease):
                        disease_id = DiseaseStates.objects.filter(name=c_disease).values('id')[0]['id']
                        Samples.objects.filter(id=ids).update(disease_state_id=disease_id)
                    else:
                        DiseaseStates.objects.create(name=c_disease)
                        disease_id = DiseaseStates.objects.filter(name=c_disease).values('id')[0]['id']
                        Samples.objects.filter(id=ids).update(disease_state_id=disease_id)
                else:
                    Samples.objects.filter(id=ids).update(disease_state_id=None)
                # change status
                Corrections.objects.filter(id=corrects_id).update(status=1)
                Samples.objects.filter(id=ids).update(is_correcting=False)
                return HttpResponse("Done!")
        else:
            return HttpResponse("Request Denied!")

@csrf_exempt
def edit(request):
    if not request.user.is_authenticated():
        return HttpResponse("Need to login!")
    else:
        if request.method == 'POST':
            obj = json.loads(request.body)
            req_edit_id = obj.get('co_id', None)
            raw_data = Corrections.objects.filter(id=req_edit_id).values("id", "dc_id_id", "last_treatment", "last_species",
                                                                         "last_pmid", "last_factor", "last_cell_pop",
                                                                         "last_cell_line", "last_cell_type", "last_strain",
                                                                         "last_tissue", "last_disease")[0]
            Samples.objects.filter(id=raw_data['dc_id_id']).update(name=raw_data['last_treatment'])
            species_id = Species.objects.filter(name=raw_data['last_species']).values('id')[0]['id']
            Samples.objects.filter(id=raw_data['dc_id_id']).update(species_id=species_id)
            if raw_data['last_pmid']:
                paper_id = Papers.objects.filter(pmid=raw_data['last_pmid']).values('id')[0]['id']
                Samples.objects.filter(id=raw_data['dc_id_id']).update(paper_id=paper_id)
            else:
                Samples.objects.filter(id=raw_data['dc_id_id']).update(paper_id=None)
            if raw_data['last_factor']:
                factor_id = Factors.objects.filter(name=raw_data['last_factor']).values('id')[0]['id']
                Samples.objects.filter(id=raw_data['dc_id_id']).update(factor_id=factor_id)
            else:
                Samples.objects.filter(id=raw_data['dc_id_id']).update(factor_id=None)
            if raw_data['last_cell_line']:
                cell_line_id = CellLines.objects.filter(name=raw_data['last_cell_line']).values('id')[0]['id']
                Samples.objects.filter(id=raw_data['dc_id_id']).update(cell_line_id=cell_line_id)
            else:
                Samples.objects.filter(id=raw_data['dc_id_id']).update(cell_line_id=None)
            if raw_data['last_cell_pop']:
                cell_pop_id = CellPops.objects.filter(name=raw_data['last_cell_pop']).values('id')[0]['id']
                Samples.objects.filter(id=raw_data['dc_id_id']).update(cell_pop_id=cell_pop_id)
            else:
                Samples.objects.filter(id=raw_data['dc_id_id']).update(cell_pop_id=None)
            if raw_data['last_cell_type']:
                cell_type_id = CellTypes.objects.filter(name=raw_data['last_cell_type']).values('id')[0]['id']
                Samples.objects.filter(id=raw_data['dc_id_id']).update(cell_type_id=cell_type_id)
            else:
                Samples.objects.filter(id=raw_data['dc_id_id']).update(cell_type_id=None)
            if raw_data['last_strain']:
                strain_id = Strains.objects.filter(name=raw_data['last_strain']).values('id')[0]['id']
                Samples.objects.filter(id=raw_data['dc_id_id']).update(strain_id=strain_id)
            else:
                Samples.objects.filter(id=raw_data['dc_id_id']).update(strain_id=None)
            if raw_data['last_disease']:
                disease_id = DiseaseStates.objects.filter(name=raw_data['last_disease']).values('id')[0]['id']
                Samples.objects.filter(id=raw_data['dc_id_id']).update(disease_state_id=disease_id)
            else:
                Samples.objects.filter(id=raw_data['dc_id_id']).update(disease_state_id=None)
            if raw_data['last_tissue']:
                tissue_id = TissueTypes.objects.filter(name=raw_data['last_tissue']).values('id')[0]['id']
                Samples.objects.filter(id=raw_data['dc_id_id']).update(tissue_type_id=tissue_id)
            else:
                Samples.objects.filter(id=raw_data['dc_id_id']).update(tissue_type_id=None)
            Corrections.objects.filter(id=req_edit_id).update(status=0)
            Samples.objects.filter(id=raw_data['dc_id_id']).update(is_correcting=True)
            return HttpResponse("Done! The record in Cistrome Database have been resumed. You can recheck the record in Unsolved Tab.")
        else:
            return HttpResponse("Request denied!")

@csrf_exempt
def change_status(request):
    if not request.user.is_authenticated():
        return HttpResponse("Need to login!")
    else:
        if request.method == 'POST':
            obj = json.loads(request.body)
            req_change_id = obj.get('id', None)
            req_change_status = obj.get('tostatus', None)
            if req_change_status in [1, 0]:
                Corrections.objects.filter(id=req_change_id).update(status=req_change_status)
                return HttpResponse("Success!")
            else:
                Corrections.objects.filter(id=req_change_id).update(status=req_change_status)
                dc_id = Corrections.objects.filter(id=req_change_id).values("dc_id_id")[0]["dc_id_id"]
                Samples.objects.filter(id=dc_id).update(is_correcting=False)
                return HttpResponse("Success!")
        else:
            return HttpResponse("Request denied!")

@csrf_exempt
def redirect(req):
    return HttpResponseRedirect(
        "http://cistrome.org/db/#/admin"
    )

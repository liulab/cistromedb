import os
import time
import urllib

from itertools import chain
import json as json
from django.core.paginator import Paginator
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.shortcuts import redirect
from django.template import RequestContext
from django.views.decorators.cache import cache_page
from datacollection.models import Samples
from .utils import get_query

def external_factor_id_view(request):
    """
    filter dc database information
    """
    req_f = request.GET.get("id", None)
    return 


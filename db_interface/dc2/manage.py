#!/usr/bin/env python
import os
import sys
from django.core.management.base import BaseCommand, CommandError
# from datacollection import models

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "dc2.settings")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)

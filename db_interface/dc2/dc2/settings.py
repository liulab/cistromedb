import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'mpbo&y$na^_m__eoww2(z6%1hk6)2#)gk-69#o3)@gz4a9!+cw'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True ## weird, cannot shut off

# Application definition

INSTALLED_APPS = [
    'suit',
    'adminactions',
    'django_select2',
    'django.contrib.admin',
    'django.contrib.messages',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.staticfiles',
    'django_extensions',

    ## internal apps
    'captcha',
    'datacollection',
    'corsheaders',
    
    ]

MIDDLEWARE_CLASSES = [
#    'django.middleware.security.SecurityMiddleware',
    'django.middleware.common.CommonMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

# ALLOWED_HOSTS = ['localhost', '127.0.0.1', 'dc2.cistrome.org']
CORS_ORIGIN_WHITELIST = (
    'localhost:9000/#/',
    'cistrome.org/db/#/',
)

CORS_ALLOW_CREDENTIALS = True
CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_HEADERS = (
    'x-requested-with',
    'content-type',
    'accept',
    'origin',
    'authorization',
    'x-csrftoken',
)
CORS_ALLOW_METHODS = (
    'GET',
    'POST',
    'PUT',
    'PATCH',
    'DELETE',
    'OPTIONS',
)

ROOT_URLCONF = 'dc2.urls'

WSGI_APPLICATION = 'dc2.wsgi.application'


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'datacollection', 'templates'),],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]


AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'newdc2',
        'USER': 'hanfei',
        'PASSWORD': 'cptbtptp',
        'HOST': '',
        'PORT': '',
        'OPTIONS': {'init_command': 'SET storage_engine=InnoDB;'},
        'STORAGE_ENGINE': 'INNODB'
    },
    'cistromeap':{
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'cistromeap',
        'USER': 'hanfei',
        'PASSWORD': 'cptbtptp',
    }
}

#CACHES = {
#    'default': {
#        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
#        'LOCATION': '127.0.0.1:11211',
#        }
#}
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.9/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

LOGIN_REDIRECT_URL = '/api/comment_redirect/'

STATIC_URL="/static/"
STATIC_ROOT = '/var/www/html/db/static/'
STATICFILES_DIRS=("/data/home/qqin/01_Projects/Programming/dc2/dc_statics/", )

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'django.contrib.staticfiles.finders.FileSystemFinder',
)

CAPTCHA_BACKGROUND_COLOR = 'transparent'
CAPTCHA_LENGTH = 3
CAPTCHA_CHALLENGE_FUNCT = 'captcha.helpers.math_challenge'
CAPTCHA_NOISE_FUNCTIONS = ()
CAPTCHA_FOREGROUND_COLOR = 'blue'

from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS as TCP
TEMPLATE_CONTEXT_PROCESSORS = TCP + (
    'django.core.context_processors.request',
    )


SUIT_CONFIG = {
    'ADMIN_NAME': 'Data Browser Admin',
    'MENU': (
        {'app': 'auth', 'label': 'Authorization', 'icon': 'icon-lock'},
        {'app':'datacollection','label':'DataCollection'},
        {'label': 'Widgets', 'icon':'icon-cog', 'url': '/dc/stat/',
     'models': (
        {'label': 'Statistics', 'url': '/dc/stat/'},
        )},
        )
}


SITE_ROOT = "/data/home/qqin/01_Projects/Programming/dc2"
# LOGGING = {
#     'version': 1,
#     'disable_existing_loggers': True,
#     'formatters': {
#         'standard': {
#             'format' : "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
#             'datefmt' : "%d/%b/%Y %H:%M:%S"
#         },
#     },
#     'handlers': {
#         'null': {
#             'level':'DEBUG',
#             'class':'django.utils.log.NullHandler',
#         },
#         'logfile': {
#             'level':'WARN',
#             'class':'logging.handlers.RotatingFileHandler',
#             'filename': BASE_DIR + "/logfile",
#             'maxBytes': 50000,
#             'backupCount': 2,
#             'formatter': 'standard',
#         },
#         'console':{
#             'level':'INFO',
#             'class':'logging.StreamHandler',
#             'formatter': 'standard'
#         },
#     },
#     'file':  # work with DEBUG=True
#         {
#             'level':
#             'INFO',
#             'class':
#             'logging.FileHandler',
#             'formatter': 'standard',
#             'filename': 'myapp.log'
#         },
#     'loggers': {
#         'django': {
#             'handlers':['console'],
#             'propagate': True,
#         },
#         'django.db.backends': {
#             'handlers': ['console'],
#             'level': 'DEBUG',
#             'propagate': False,
#         },
#         'datacollection': { # all app use ''
#             'handlers': ['console', 'logfile'],
#             'level': 'DEBUG'
#         },
#     }
# }


GRAPH_MODELS = {
  'all_applications': True,
  'group_models': True,
}

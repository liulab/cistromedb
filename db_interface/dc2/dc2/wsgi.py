"""
WSGI config for dc2 project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.9/howto/deployment/wsgi/
"""

import os
import sys

## mkvirtualenv dc2, and pip install -r requirements.txt
activate_env = '/data/home/qqin/.virtualenvs/dc2/bin/activate_this.py'
execfile(activate_env, dict(__file__=activate_env))

sys.path.append('/data/home/qqin/.virtualenvs/dc2/lib/python2.7/site-packages')
sys.path.append('/data/home/qqin/01_Projects/Programming/dc2')
sys.path.append('/data/home/qqin/01_Projects/Programming/dc2/dc2')
sys.path.append('/data/home/qqin/01_Projects/Programming/dc2/datacollection')

sys.path = sys.path[::-1]

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "dc2.settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

#def application(environ, start_response):
#    if environ['mod_wsgi.process_group'] != '': 
#        import signal
#        os.kill(os.getpid(), signal.SIGINT)
#    return ["killed"]

from django.conf.urls import patterns, url, include

from datacollection import views
from datacollection import utils
from datacollection import batchmode
from datacollection import comment

from django.contrib.auth.views import login, logout

# from datacollection import admin as datacollection_admin
from datacollection.admin_utils import SamplesSelect2View
from django.contrib import admin
import django_select2
import adminactions.urls

admin.autodiscover()

urlpatterns = [
    url(r'^main_filter_ng$', views.main_filter_ng),
    url(r'^inspector$', views.inspector_ajax),
    url(r'^putative_target_ng', views.target_json),
    url('^conserv', views.show_image),
    url(r'^file$', views.file_view),
    url(r'^datahub/(?P<sample_id>[0-9]+)$', views.washu_view),
    url(r'^hgtext/(?P<sample_id>[0-9]+)/$', views.ucsc_view),
    # url(r'^similarity$', views.similarity_ajax),
    url(r'^accounts/logout/$', logout,
        {'template_name':'registration/logged_out.html'}, name='logout'),
    url(r'^accounts/login/$', login, name='login'),
    url(r'accounts/check/', views.check_authenticated),

    url(r'^captcha/', include('captcha.urls')),
    url(r'^downloads/(?P<id>.+:.+:.+)/$', utils.some_view, name='downloads'),
    url(r'^files/(?P<fileid>.+:.+:.+)/$', utils.download_view, name='files'),
    url(r'^batch/$', batchmode.batch_filter, name='batch'),
    url(r'^comment/$', comment.commentapi, name='comment api'),
    url(r'^comment_filter/$', comment.view_comment),
    url(r'^verify/$', comment.verify),
    url(r'^edit/$', comment.edit),
    url(r'^change_status/$', comment.change_status),
    url(r'^comment_redirect/$', comment.redirect),

    url(r'^batchview2/(?P<sp>\w)/(?P<ids>.+)/(?P<type>\w)/(?P<pos>.+)/$', batchmode.washu_view, name='batch wash u browse'),
    url(r'^batchview/(?P<sp>\w)/(?P<ids>.+)/(?P<type>\w)/$', batchmode.washu_view, name='batch wash u browse'),
    # admin
    url(r'^new_admin/', include(admin.site.urls)),
    url(r'^admin/', include(admin.site.urls)),
    # url(r'^stat/$', DC_stat_view, name="stat"),
    # url(r'^adminactions/', include(include(adminactions.urls))),
    url(r'^adminactions/', include(adminactions.urls)),
    url(r'^select2/', include('django_select2.urls'))
    # url(r'^select2sample/$', SamplesSelect2View.as_view(),name='SamplesSelect2View'),
]


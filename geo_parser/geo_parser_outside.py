import json
import os
import re
import sys
import urllib
import traceback
from datetime import datetime
import time
import cPickle
import pickle
import commands
try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET

from xml.dom.minidom import parseString

#AUTO-load classifiers
#a trick to get the current module
_modname = globals()['__name__']
_this_mod = sys.modules[_modname]

_ppath = "/".join(_this_mod.__file__.split("/")[:-1])

from django.utils.encoding import smart_str

from datacollection import models
import geo_parser_newVersion
import ab_parser

def parseAntibody(description_dict):
    """Given a geoPost, will 1. try to parse out the antibody information
    2. create the new antibody if necessary with the name as:
    VENDOR Catalog# (TARGET) OR
    VENDOR Catalog# --if there is no target OR
    TARGET --if there is no vendor info

    To do this we key in on some key concepts

    Returns the sample's antibody, None otherwise
    """

    targetFlds = ["antibody source", "chip antibody", "antibody"]

    vendorFlds = ["antibody vendorname", "chip antibody provider",
                  "antibody vendor", "antibody manufacturer",
                  "chip antibody vendor", "chip antibody manufacturer", "antibody vendorcatalog#",
                  "antibody vendor and catalog number", "antibody vendor/catalog", "antobody vendor/catalog#"
    ]
    catalogFlds = ["antibody vendorid", "chip antibody catalog",
                   "antibody CATALOG NUMBER", "chip antibody cat #", "antibody catalog #"]
    lotFlds = ["chip antibody lot #"]

    #1. try to get the values
    vals = [None, None, None, None]
    used_fld = []
    for (i, ls) in enumerate([targetFlds, vendorFlds, catalogFlds, lotFlds]):
        for f in ls:
            tmp = description_dict.get(f)
            if tmp:
                vals[i] = tmp
                used_fld.append(f)
                break

    #2. get each term of the antibody separately
    (target, vendor, cat, lot) = tuple(vals)

    if target and "input" in target.lower():
        #ret, created = models.Antibodies.objects.get_or_create(name="input")
        #return 'input'
        return 'Input'


    def if_match_then_get(keyword, current_key):
        if keyword in current_key:
            current_value = description_dict[current_key]
            if current_value and current_key not in used_fld:
                used_fld.append(current_key)
                if current_value.startswith("catalog#"):
                    current_value = current_value.replace("catalog#, or reference):", "").strip()
                return current_value
        return None

    #3. sometimes the field name is not standard, in other words, it is not included in xxxFlds
    #If so, use string matching to parse these fields
    for k, v in description_dict.items():
        if not (k and v):
            continue

        if not vendor:
            vendor = if_match_then_get("vendor", k)

        if not cat:
            cat = if_match_then_get("catalog", k)

        if not lot:
            lot = if_match_then_get("lot", k)

    #4. compose the complete antibody name

    name_list = []
    if vendor:
        name_list.append(vendor)

    if cat:
        name_list.append(cat)

    if lot:
        name_list.append(lot)

    if not vendor and not cat and not lot:
        if not target:
            target = if_match_then_get('antibody', k)
        if target:
            name_list.append(target)
    name = ", ".join(name_list)

    if name:
        return name
        # ret, created = models.Antibodies.objects.get_or_create(name=name)
        # return ret
    else:
        return None


def _parse_a_field(description_dict, a_field, DCmodel, max_create_length=100, new=False):
    if  not description_dict.get(a_field, None):
        return None
    if len(description_dict.get(a_field, "")) > 0:
        if DCmodel in [models.CellTypes]: #remove '-' when match cell types, like T-cells > T cells
            description_dict_new = {}
            for v in description_dict.keys():
                description_dict_new[v] = description_dict[v].replace('-', ' ')
            description_dict = description_dict_new  
        result_searched_by_name = sorted(DCmodel.objects.extra(
            where={"%s REGEXP CONCAT('([^a-zA-Z0-9]|^)', `name`, '([^a-rt-zA-RT-Z0-9]|$)')"},
                                  params=[description_dict[a_field]]),
            key=lambda o: len(o.name),
            reverse=True)

        if result_searched_by_name and len(result_searched_by_name[0].name.strip()) > 0:
            return result_searched_by_name[0]

        if DCmodel not in [models.Factors, models.Aliases] :
            result_searched_by_aliases = sorted(DCmodel.objects.exclude(aliases=None).exclude(aliases="").extra(
                where={"%s REGEXP CONCAT('([^a-zA-Z0-9]|^)', `aliases`, '([^a-rt-zA-RT-Z0-9]|$)')"},
                params=[description_dict[a_field]]),
                    key=lambda o: len(o.name),
                    reverse=True)
            if result_searched_by_aliases and len(result_searched_by_aliases[0].name.strip()) > 0:
                return result_searched_by_aliases[0]

    if new and (len(description_dict.get(a_field, "")) > 0) and (len(description_dict.get(a_field, "")) <= max_create_length):
        #return description_dict[a_field]
        if DCmodel in [models.CellLines, models.CellTypes, models.TissueTypes]: # these three will check wheather they appare in each other
            return description_dict[a_field]
        else:
            # ret, created = DCmodel.objects.get_or_create(name=description_dict[a_field])
            # if created:
            #     ret.status = 'new'
            return description_dict[a_field]


    return None

def _parse_fields(description_dict, strict_fields, greedy_fields, DCmodel, greedy_length=100):
    for sf in strict_fields:
        ret = _parse_a_field(description_dict, sf, DCmodel, greedy_length, new=False)
        if ret:
            return ret

            
    for gf in greedy_fields:
        ret = _parse_a_field(description_dict, gf, DCmodel, greedy_length, new=True)
        if ret:
            return ret
    if DCmodel in [models.Factors, models.Aliases]: #do not use all description information for factor part, since factor should be parse more serious.
        return None
    tmp = {}
    for x in description_dict.keys(): # using all description to parse information
        if x not in ['antibody', 'chip_antibody', 'last update date', 'release date']:
            tmp[x] = description_dict[x]
    characteristics = {'characteristics':' '.join(tmp.values())}
    ret = _parse_a_field(characteristics, 'characteristics', DCmodel, greedy_length, new = False)
    if ret:
        return ret

    if DCmodel in [models.CellLines]: # search cell line by pubic database information
        ret = geo_parser_newVersion.search_cellline_from_out(characteristics, 'characteristics')
        if ret:
            if ret and (str(ret) not in ['OF', 'IP']):
                return ret
    return None

def _search_factor_by_pattern(field_name, field_content):
    field_content = field_content.upper()
    if "antibody" in field_name or 'Chip' in field_name or 'title' in field_name:
        if "NONE" in field_content or "INPUT" in field_content or "IGG" in field_content or "N/A" in field_content:
            #return models.Factors.objects.get_or_create(name="Input")[0]
            return "Input"
    if "POL2" in field_content or "POLYMERASE" in field_content or 'POLII' in field_content or 'POL II' in field_content:
        #return models.Factors.objects.get_or_create(name="POLR2A")[0]
        return "POLR2A"
    if "CTCF" in field_content:
#        return models.Factors.objects.get_or_create(name="CTCF")[0]
        return "CTCF"
    return None


def _guess_factor_boldly(field_name, field_content):
    factor_finder = re.compile(r"anti[^a-z]([a-z]+[-\.]?[a-z0-9]{0,4})", re.I)
    # get the word after anti
    factor_pattern = re.compile(r"^[a-z]+[-\.]?[a-z0-9]+$", re.I)

    factor_found = factor_finder.findall(field_content)

    stop_words_pattern = re.compile(r"(mouse)|(abcam)|(dmso)|(negative)|(seq)|(chip)", re.I)
    if factor_found and not stop_words_pattern.match(factor_found[0]):
        return models.Factors.objects.get_or_create(name=factor_found[0])[0]

    if len(field_content) < 10 and factor_pattern.match(field_content) and not stop_words_pattern.match(field_content):
        return models.Factors.objects.get_or_create(name=field_content)[0]
    return None


def parseFactor(description_dict):
    standard_fields = [i for i in
                       ["antibody targetdescription", "factor", "title", 'hgn', 'chip target'] if i in description_dict.keys()]
    non_standard_fields = [i for i in description_dict.keys() if "antibody" in i and i not in standard_fields]
    fields = standard_fields + non_standard_fields

    first_try = _parse_fields(description_dict, fields, [], models.Factors)
    if first_try and (str(first_try) != 'None'):
        return first_try

    print ".",
    second_try = _parse_fields(description_dict, fields, [], models.Aliases)
    if second_try and (str(second_try) != 'None'):
        return second_try.factor

    print ".",
    for f in fields:
        third_try = _search_factor_by_pattern(f, description_dict[f])
        if third_try and (str(third_try) != 'None'):
            return third_try

    # print ".",
    # for f in standard_fields:
    #     fourth_try = _guess_factor_boldly(f, description_dict[f])
    #     if fourth_try and (str(fourth_try) != 'None'):
    #         return fourth_try

    print ".",
    return None

def parseCellType(description_dict):
    return _parse_fields(description_dict,
                         ['cell type', 'cell lineage', 'cell', 'cell line', 'source name', 'cell description', 'title', ],
                         ['cell type'],
                         models.CellTypes)


def parseCellLine(description_dict):
    return _parse_fields(description_dict,
                         ['cell', 'cell line', 'source name', 'cell description', 'title', 'cell type', 'cell lineage'],
                         ['cell line'],
                         models.CellLines)


def parseCellPop(description_dict):
    return _parse_fields(description_dict, ['cell', 'source name', 'cell description', 'title', 'cell type', 'cell lineage'], [], models.CellPops)


def parseTissue(description_dict):
    return _parse_fields(description_dict,
                         ['tissue', 'tissue type', 'tissue depot', 'source name', 'cell description', 'title', 'cell type', 'cell lineage','cell', 'cell line'],
                         ['tissue', 'tissue type'],
                         models.TissueTypes)


def parseStrain(description_dict):
    return _parse_fields(description_dict,
                         ['strain', 'strain background', 'source name', 'cell description', 'title'],
                         ['strain'],
                         models.Strains)


def parseDisease(description_dict):
    return _parse_fields(description_dict,
                         ['disease', 'tumor stage', 'cell karotype', 'source name', 'title'],
                         ['disease'],
                         models.DiseaseStates)

def search_between_table(description_dict, strict_fields, serious_fields, DCmodel):
    for sf in strict_fields:
        sea = _parse_a_field(description_dict, sf, DCmodel, 100, new=False)
        if sea:
            return sea
    tmp = {}
    for k in description_dict.keys():
        if k not in ['antibody', 'chip_antibody', 'last update date', 'release date']:
            tmp[k] = description_dict[k]
    characteristics = {'characteristics':' '.join(tmp.values())}
    sea = _parse_a_field(characteristics, 'characteristics', DCmodel, 100, new = False)
    if sea:
        return sea
    if DCmodel in [models.CellTypes]:
        """try metamap if no cell type"""
        feature = characteristics['characteristics'].replace('(', ' ').replace(')', ' ')
        try:
            content = commands.getoutput('echo "%s" | metamap -y -I '%(feature))
            content = [x for x in content.split('\n') if x.endswith('[Cell]')]
            if content:
                proCell = content[0][content[0].index(':')+1:].rstrip('[Cell]').strip()
                if proCell:
                    Cell = proCell
                    if '(' in proCell:
                        Cell = proCell[:proCell.index('(')].strip('(').strip()
                    if Cell.lower() not in ['cell', 'cancer', 'tumor', 'clone', 'cell line', 'cells', 'human cell line', 'mouse cell line', 'cellline', 'celllines']:
                        if (not _parse_a_field({'cellType':Cell}, 'cellType', models.CellPops, 100, new = False)) and (not _parse_a_field({'cellType':Cell}, 'cellType', models.CellLines, 100, new = False)) : # in case metamap result in cell pop
                            print "metamap cell type"
                            return Cell
        except:
            pass
    if serious_fields:
        for sf in serious_fields:
            tmp = description_dict.get(sf, "")
            if tmp and (DCmodel == models.CellTypes) and (not _parse_a_field({'cell':tmp}, 'cell', models.CellLines, 100, new = False)) and (not _parse_a_field({'cell':tmp}, 'cell', models.TissueTypes, 100, new = False)) and (not _parse_a_field({'cell':tmp}, 'cell', models.CellPops, 100, new = False)):
                return tmp
            if tmp and (DCmodel == models.CellLines) and (not _parse_a_field({'cell':tmp}, 'cell', models.CellTypes, 100, new = False)):
                return tmp
            if tmp and (DCmodel == models.TissueTypes) and (not _parse_a_field({'cell':tmp}, 'cell', models.CellTypes, 100, new = False)) and (not _parse_a_field({'cell':tmp}, 'cell', models.CellLines, 100, new = False)):
                return tmp
            if tmp and (DCmodel not in [models.CellTypes, models.CellLines, models.TissueTypes]):
                return tmp
    return None

def parseAndsearch(description_dict, field):
    tmp_sea_cellLine = search_between_table(description_dict,
                         field, ['cell line'],
                         models.CellLines)
    tmp_sea_cellType = search_between_table(description_dict,
                         field, ['cell type'],
                         models.CellTypes)
    tmp_sea_tissueType = search_between_table(description_dict,
                         field, ['tissue', 'tissue type'],
                         models.TissueTypes)
    tmp_sea_cellpop = search_between_table(description_dict,
                         field, [],
                         models.CellPops)
    tmp_sea_disease = search_between_table(description_dict,
                         field, [],
                         models.DiseaseStates)
    if str(tmp_sea_tissueType).lower() in ['primary tumor', 'tumor']:
        tmp_sea_tissueType = None
    return {'cellType':tmp_sea_cellType, 'cellLine':tmp_sea_cellLine, 'tissueType':tmp_sea_tissueType, 'cellpop':tmp_sea_cellpop, 'disease':tmp_sea_disease}               
        

def update_one_sample(gsmid, factor_require=False, ddir='geo', seq_type = 'ChIP-Seq', factor_know = False, parse_fields=['other_ids', 'paper', 'name', 'species', 'description', 'antibody', 'factor',
                                           'cell type', 'cell line', 'cell pop', 'tissue', 'strain', 'disease','update date','release date']):
    """Given a gsmid, tries to create a new sample--auto-filling in the
    meta fields


    If overwrite is True and there is the sample that has the same gsmid, this function will overwrite that sample

    NOTE: will try to save the sample!!

    Returns newly created sample
    """
    
    
    print gsmid
    description_dict = geo_parser_newVersion.parseGeoInfo(gsmid, ddir)
    antibody = parseAntibody(description_dict)
    if 'factor' in parse_fields:
        if not seq_type and factor_know: # deal with those that we have known which factor it is, from a outside table
            factor, created = models.Factors.objects.get_or_create(name = factor_know)
            tmp_factor = factor
        else:
            try:
                if seq_type in ['ATAC-seq', 'ATAC-Seq']:
                    tmp_factor = models.Factors.objects.filter(name = 'ATAC-seq')[0]
                elif seq_type in ['DNase-Hypersensitivity']:
                    tmp_factor = models.Factors.objects.filter(name = 'DNase')[0]
                elif seq_type in ['MeDIP-Seq']:
                    tmp_factor = models.Factors.objects.get_or_create(name = 'MeDIP-Seq')
                else:
                    tmp_factor = parseFactor(description_dict)
            except:
                tmp_factor = parseFactor(description_dict)
    # compare parsed factor and gene name from antibody company parser
    if (str(tmp_factor) in ['None', 'none', 'NA']):
        antibody_factor_comp = [None, None]
        if str(antibody) != 'None':
            antibody_factor_comp = ab_parser._excute_antibody_factor(antibody)
            if str(antibody_factor_comp[0]) not in ['None', 'Flag']:
                compare_database = parseFactor({'factor':antibody_factor_comp[0]})
                if compare_database != antibody_factor_comp[0]:
                    antibody_factor_comp = [compare_database, antibody_factor_comp[1]]
                else:
                    antibody_factor_comp = [None, antibody_factor_comp[1]]
        if ('Flag' not in antibody_factor_comp) and (antibody_factor_comp[0] or antibody_factor_comp[1]):
            if antibody_factor_comp[0] and (antibody_factor_comp[0] != 'Existed') and (not antibody_factor_comp[1]):
                tmp_factor = str(antibody_factor_comp[0])
            else:
                tmp_factor = str(antibody_factor_comp[1])
    if tmp_factor and str(tmp_factor).lower() == 'input':
        return None
    if factor_require and (not ((len(factor_require) == 1) and factor_require[0].lower() == 'chip-seq')) and (str(tmp_factor).lower() not in [x.lower() for x in factor_require]):
        return None

    sraId = geo_parser_newVersion.gsmToSra(gsmid)
    sraXML = geo_parser_newVersion.sra.getSraXML(sraId) if sraId else None

    geoPost = geo_parser_newVersion.postProcessGeo(gsmid, ddir=ddir)
    if not geoPost:
        return None

    if 'species' in parse_fields:
        if geo_parser_newVersion.getFromPost(geoPost, "organism") == "HOMO SAPIENS":
            species = models.Species.objects.get(pk=1)
        else:
            species = models.Species.objects.get(pk=2)

    if ('other_ids' in parse_fields) or ('paper' in parse_fields):
        gseId = geo_parser_newVersion.gsmToGse(gsmid)
        pmid = geo_parser_newVersion.gseToPubmed(gseId) if gseId else None

    if 'other_ids' in parse_fields:
        import json 
        idList = {'sra': sraId, 'gse': gseId, 'pmid': pmid}
        print idList
        other_ids = json.dumps(idList)
        try:
            series_id = geo_parser_newVersion.gse_idToAcc(idList['gse'])#"GSE"+str(idList['gse'][4:])
        except:
            #s.series_id = 'NA'
            print 'cannot find GSE_id'
    
    paper = None
    if 'paper' in parse_fields and pmid:
        paper = geo_parser_newVersion.pubmed.getOrCreatePaper(pmid)

    if 'name' in parse_fields:
        name = None
        name = geo_parser_newVersion.getFromPost(geoPost, "title")

    #HERE is where I need to create a classifier app/module
    #FACTOR, platform, species--HERE are the rest of them!

 #   description_dict = geo_parser_newVersion.parseGeoInfo(gsmid, ddir)
    if 'description' in parse_fields:
        description = json.dumps(description_dict)

#    if 'antibody' in parse_fields:
#        antibody = parseAntibody(description_dict)
    special = ["_", "_ _", "__", "--", "-"]
    if 'strain' in parse_fields:
        strain = parseStrain(description_dict)
        if str(strain) in special:
            strain = None
        else:
            pass
    if 'cell type' in parse_fields:
        tmp_celltype = None
        searchCellType = parseAndsearch(description_dict, ['cell type', 'cell lineage', 'cell', 'cell line', 'source name', 'cell description', 'title'])
        if searchCellType['cellType'] and (str(searchCellType['cellType']).upper() not in [str(searchCellType['cellLine']).upper(), str(searchCellType['tissueType']).upper()]):
            tmp_celltype = searchCellType['cellType']
            tmp_tmp_celltype = None   
	
    if 'tissue' in parse_fields:
        searchTisssue = parseAndsearch(description_dict, ['tissue', 'tissue type', 'tissue depot', 'source name', 'cell description', 'title', 'cell type', 'cell lineage','cell', 'cell line'])
        if searchCellType['tissueType'] and (str(searchCellType['tissueType']).upper() not in [str(searchCellType['cellLine']).upper(), str(searchCellType['cellType']).upper(), str(searchCellType['cellpop']).upper(), str(searchCellType['disease']).upper()]):
            tmp_tissue = searchTisssue['tissueType']
        else:
            tmp_tissue = geo_parser_newVersion.semicheck('tiss', parseTissue(description_dict))
            if tmp_tissue:
                test_tissue = parseAndsearch({'cell type':str(tmp_tissue)}, ['cell type'])
            else:
                test_tissue = {'cellType':None}
            if test_tissue['cellType']:
                tmp_tissue = None

    if 'cell line' in parse_fields:
		
        tmp_cellline = geo_parser_newVersion.semicheck('cline', parseCellLine(description_dict))
        if searchCellType['cellLine'] and (searchCellType['cellLine'] != tmp_factor) and (str(searchCellType['cellLine']) not in [str(strain)]):
            tmp_cellline = searchCellType['cellLine']
        elif (not str(tmp_cellline) or str(tmp_cellline) in ['None', 'none', 'NA']):
            tmp_cellline = None
        elif (str(tmp_cellline) and (str(tmp_cellline) in [str(strain)])):
            tmp_cellline = None
        else:
            pass

        if (str(tmp_cellline) in special) or (not tmp_cellline):
            cell_line = None
            cell_type = str(tmp_celltype)
            tissue_type = str(tmp_tissue)
        else:
            m = geo_parser_newVersion.match(tmp_cellline, tmp_celltype, tmp_tissue)
            cell_line = str(m[0])
            cell_type = str(m[1])
            tissue_type = str(m[2])

    if 'disease' in parse_fields:
        disease_state = parseDisease(description_dict)
        if searchCellType['disease']:
            disease_state = searchCellType['disease']

    if 'cell pop' in parse_fields:
        cell_pop = parseCellPop(description_dict)
        if searchCellType['cellpop']:
            cell_pop = searchCellType['cellpop']
    if 'update date' in parse_fields:
        geo_last_update_date = geo_parser_newVersion.parseUpdateTime(description_dict)

    if 'release date' in parse_fields:
        geo_release_date = geo_parser_newVersion.parseReleaseTime(description_dict)

    # compare parsed factor and gene name from antibody company parser
    # antibody_factor_comp = [None, None]
    # if str(antibody) != 'None':
    #     antibody_factor_comp = ab_parser._excute_antibody_factor(antibody)
    #     if str(antibody_factor_comp[0]) not in ['None', 'Flag']:
    #         compare_database = parseFactor({'factor':antibody_factor_comp[0]})
    #         if compare_database != antibody_factor_comp[0]:
    #             antibody_factor_comp = [compare_database, antibody_factor_comp[1]]
    #         else:
    #             antibody_factor_comp = [None, antibody_factor_comp[1]]
    # if (str(tmp_factor) in ['None', 'none', 'NA']) and ('Flag' not in antibody_factor_comp) and (antibody_factor_comp[0] or antibody_factor_comp[1]):
    #     if antibody_factor_comp[0] and not antibody_factor_comp[1]:
    #         tmp_factor = antibody_factor_comp[0]
    #     else:
    #         tmp_factor = antibody_factor_comp[1]
    if (type(tmp_factor) == type(str())) or (str(tmp_factor) == 'None'):
        factor_type = 'None'
    elif (type(tmp_factor) != type(str())) and not str(tmp_factor.type):
        factor_type = 'None'
    else:
        factor_type = str(tmp_factor.type)
    # record_anti_factor = open('record_anti_factor.txt', 'a')
    # record_anti_factor.write('\t'.join([gsmid, tmp_factor, str(antibody_factor_comp[0]), str(antibody_factor_comp[1])]))
    # record_anti_factor.close()
    # if tmp_factor and tmp_factor in antibody_factor_comp:
    #     factor = tmp_factor
    #res = [gsmid, str(species), str(tmp_factor), str(antibody_factor_comp[0]), str(antibody_factor_comp[1]), str(factor_type), str(series_id), str(pmid), str(paper), str(name), str(cell_line), str(cell_type), str(tissue_type), str(strain), str(disease_state), str(cell_pop), str(geo_release_date), str(geo_last_update_date), str(antibody), str(description)]

    res = [gsmid, str(species), str(tmp_factor), str(factor_type), str(series_id), str(pmid), str(paper), str(name), str(cell_line), str(cell_type), str(tissue_type), str(strain), str(disease_state), str(cell_pop), str(geo_release_date), str(geo_last_update_date), str(antibody), str(description)]
    time.sleep(0.3)
    return res

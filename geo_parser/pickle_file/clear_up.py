import sys
f = sys.argv[1]
#f = open('cell_line_resource.xls')
f = open(f)
all = [x.rstrip().split('\t') for x in f if '[' not in x and ']' not in x]
f.close()
out = open('cell_line_new.xls', 'w')
for iterm in all:
	tmp = []
	for i in iterm[-1].split('; '):
		if i.lower() in ['cancer']:
			continue			
		if (not len(i.replace(' ', '')) <= 3) and (' ' not in i):
			tmp.append(i) 
	if tmp and len(tmp) == 1:
		print >>out, '\t'.join(iterm[:2]+tmp)
	elif tmp and len(tmp) > 1:
		print >>out, '\t'.join(iterm[:2]+['; '.join(tmp)])
	else:
		#	if (len(iterm[1]) >= 3) and (' ' in iterm[1]):
		print >>out, '\t'.join(iterm[:2]+['NA'])
		#	else:
		#		pass
out.close()

import sys
f = sys.argv[1]
search = {}
link = {}
#f = open('cell_line_new.xls')
f = open(f)
all = [x.rstrip().split('\t') for x in f]
f.close()
for iterm in all:
	if len(iterm[1]) > 3 and (' ' not in iterm[1]):
        	search[iterm[1]] = iterm[1]
	for i in iterm[-1].split('; '):
		if i != 'NA' and (' ' not in i) and (i.lower() not in ['cancer']):
			search[i] = iterm[1]
import pickle
out = open('search_cellline.pk', 'wb')
pickle.dump(search, out, True)
out.close()

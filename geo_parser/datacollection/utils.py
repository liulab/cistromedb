from django.shortcuts import render, render_to_response, redirect
from captcha.helpers import captcha_image_url
from captcha.models import CaptchaStore
from captcha.fields import CaptchaField
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse, Http404
import os
import re
from django.db.models import Q
from django.views.decorators.csrf import csrf_protect
from django import forms

inputdir0 = '/data1/DC_results/Result_new/'
inputdir = '/data5/DC_results/Result_new/'
inputdir1 = '/data5/DC_results/Result_new3/'
inputdir2 = '/data5/DC_results/Result_new2/'
inputdir3 = '/data2/DC_results/Result_new'
inputdir4 = '/data3/DC_results/Result_new'
inputdir5 = '/data6/DC_results/Result_2016_ATAC'
inputdir6 = '/data6/DC_results/Result_2016_k27ac'
inputdir7 = '/data6/DC_results/Result_encode3'

## datahub for download and genome browser
datahub = '/data5/browser'
scripts = '/data/home/qqin/dc2/scripts'


class CaptchaTestForm(forms.Form):
    captcha = CaptchaField()

import sys
@csrf_protect
def some_view(request, id):
    if request.POST:
        form = CaptchaTestForm(request.POST)
        if form.is_valid():
            return redirect('files', fileid=id)
    else:
        form = CaptchaTestForm()
    ## original link:     <!-- http://dc2.cistrome.org/api/file?type=bed&id={{ sample_id }} -->
    return render_to_response('test.html', locals(), context_instance=RequestContext(request))

def download_view(request, fileid):

    from django.http import StreamingHttpResponse
    from django.core import signing
    
    if fileid[-1] == 'p':
        req_type = 'plain'
    elif fileid[-1] == 'b':
        req_type = 'bed'
    else:
        return HttpResponse("Wrong parameter")
        
    try:
        req_id = signing.loads(fileid[:-1], max_age=120)['id']
    except signing.SignatureExpired:
        return HttpResponse("Time out!")
    
    if not (req_id and req_type):
        return HttpResponse("Wrong parameter")

    if os.path.exists(os.path.join(inputdir, 'dataset' + req_id)):
        folder = os.path.join(inputdir, 'dataset' + req_id)
    if os.path.exists(os.path.join(inputdir2, 'dataset' + req_id)):
        folder = os.path.join(inputdir2, 'dataset' + req_id)
    if os.path.exists(os.path.join(inputdir1, 'dataset' + req_id)):
        folder = os.path.join(inputdir1, 'dataset' + req_id)
    if os.path.exists(os.path.join(inputdir3, 'dataset' + req_id)):
        folder = os.path.join(inputdir3, 'dataset' + req_id)
    if os.path.exists(os.path.join(inputdir4, 'dataset' + req_id)):
        folder = os.path.join(inputdir4, 'dataset' + req_id)
    if os.path.exists(os.path.join(inputdir0, 'dataset' + req_id)):
        folder = os.path.join(inputdir0, 'dataset' + req_id)
    if os.path.exists(os.path.join(inputdir5, 'dataset' + req_id)):
        folder = os.path.join(inputdir5, 'dataset' + req_id)
    if os.path.exists(os.path.join(inputdir6, 'dataset' + req_id)):
        folder = os.path.join(inputdir6, 'dataset' + req_id)
    if os.path.exists(os.path.join(inputdir7, 'dataset' + req_id)):
        folder = os.path.join(inputdir7, 'dataset' + req_id)
        
    if req_type == "bed":
        f = os.path.join(folder, req_id + '_peaks.bed')
    if req_type == 'plain':
        f = os.path.join(folder, 'attic', req_id + '_gene_score_5fold.txt')
        
    if os.path.exists(f) and os.path.getsize(f) > 0:
        with open(f, 'r') as inf:
            response = StreamingHttpResponse(inf.readlines(), content_type="text/" + req_type)
        response['Content-Disposition'] = 'attachment; filename="%s"' % os.path.basename(f)
        return response
    else:
        raise Http404, ' no such file ' 


def normalize_query(query_string,
                    findterms=re.compile(r'"([^"]+)"|(\S+)').findall,
                    normspace=re.compile(r'\s{2,}').sub):
    ''' Splits the query string in invidual keywords, getting rid of unecessary spaces
        and grouping quoted words together.
        Example:
        
        >>> normalize_query('  some random  words "with   quotes  " and   spaces')
        ['some', 'random', 'words', 'with quotes', 'and', 'spaces']
    '''
    return [normspace(' ', (t[0] or t[1]).strip()) for t in findterms(query_string)] 

def get_query(query_string, search_fields):
    ''' Returns a query, that is a combination of Q objects. That combination
        aims to search keywords within a model by testing the given search fields.
    '''
    query = None # Query to search for every search term        
    terms = normalize_query(query_string)
    
    for term in terms:
        or_query = None # Query to search for a given term in each field
        for field_name in search_fields:
            q = Q(**{"%s__icontains" % field_name: term})
            if or_query is None:
                or_query = q
            else:
                or_query = or_query | q
        if query is None:
            query = or_query
        else:
            query = query & or_query
    return query


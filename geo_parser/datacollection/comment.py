import time
import urllib
import sys
import os
from .models import Samples
import time
import json
from django.db.models import Q
from django.shortcuts import redirect
from django.http import Http404, JsonResponse, HttpResponse
from datacollection.models import Samples
from django.core.paginator import Paginator
# from .views import logger
import logging
# log = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(levelname)s %(message)s',
                    filemode='a')
logger = logging.getLogger()
handler = logging.FileHandler('/data/home/qqin/01_Projects/Programming/dc2/comments.txt')
logger.addHandler(handler)

def commentapi(request):
    if request.method == 'POST':
        obj = json.loads(request.body)
        ids = obj.get('ids', None)
        comment = obj.get('comment', None)
        logger.info("%s\t%s" % (ids, comment))
        return HttpResponse("done")

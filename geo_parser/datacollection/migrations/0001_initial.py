# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-01-21 12:21
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Aliases',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
            ],
            options={
                'verbose_name_plural': 'aliases',
            },
        ),
        migrations.CreateModel(
            name='Antibodies',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, default=b'', max_length=255, null=True)),
                ('comments', models.CharField(blank=True, default=b'', max_length=255, null=True)),
                ('status', models.CharField(blank=True, choices=[('new', 'newly imported'), ('ok', 'validated')], default='new', max_length=255, null=True)),
                ('aliases', models.CharField(blank=True, default=None, max_length=255, null=True)),
            ],
            options={
                'verbose_name_plural': 'antibodies',
            },
        ),
        migrations.CreateModel(
            name='Assemblies',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, default=b'', max_length=255, null=True)),
                ('pub_date', models.DateField(blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='CellLines',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, default=b'', max_length=255, null=True)),
                ('status', models.CharField(blank=True, choices=[('new', 'newly imported'), ('ok', 'validated')], default='new', max_length=255, null=True)),
                ('comments', models.CharField(blank=True, default=b'', max_length=255, null=True)),
                ('aliases', models.CharField(blank=True, default=None, max_length=255, null=True)),
            ],
            options={
                'verbose_name_plural': 'cell lines',
            },
        ),
        migrations.CreateModel(
            name='CellPops',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, default=b'', max_length=255, null=True)),
                ('status', models.CharField(blank=True, choices=[('new', 'newly imported'), ('ok', 'validated')], default='new', max_length=255, null=True)),
                ('comments', models.CharField(blank=True, default=b'', max_length=255, null=True)),
                ('aliases', models.CharField(blank=True, default=None, max_length=255, null=True)),
            ],
            options={
                'verbose_name_plural': 'cell populations',
            },
        ),
        migrations.CreateModel(
            name='CellTypes',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, default=b'', max_length=255, null=True)),
                ('status', models.CharField(blank=True, choices=[('new', 'newly imported'), ('ok', 'validated')], default='new', max_length=255, null=True)),
                ('comments', models.CharField(blank=True, default=b'', max_length=255, null=True)),
                ('aliases', models.CharField(blank=True, default=None, max_length=255, null=True)),
            ],
            options={
                'verbose_name_plural': 'cell types',
            },
        ),
        migrations.CreateModel(
            name='CistromeUser',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('create_time', models.DateTimeField()),
                ('update_time', models.DateTimeField()),
                ('email', models.CharField(max_length=255L)),
                ('password', models.CharField(max_length=255L)),
                ('external', models.IntegerField(blank=True, null=True)),
                ('deleted', models.IntegerField(blank=True, null=True)),
                ('purged', models.IntegerField(blank=True, null=True)),
                ('username', models.CharField(blank=True, max_length=255L)),
                ('form_values_id', models.IntegerField(blank=True, null=True)),
                ('disk_usage', models.DecimalField(blank=True, decimal_places=0, max_digits=16, null=True)),
                ('active', models.IntegerField(blank=True, null=True)),
                ('activation_token', models.CharField(blank=True, max_length=64L)),
            ],
            options={
                'db_table': 'galaxy_user',
                'in_db': 'cistromeap',
            },
        ),
        migrations.CreateModel(
            name='Conditions',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('comments', models.CharField(blank=True, default=b'', max_length=255, null=True)),
                ('name', models.CharField(max_length=255)),
                ('status', models.CharField(blank=True, choices=[('new', 'newly imported'), ('ok', 'validated')], default='new', max_length=255, null=True)),
                ('aliases', models.CharField(blank=True, default=None, max_length=255, null=True)),
            ],
            options={
                'verbose_name_plural': 'conditions',
            },
        ),
        migrations.CreateModel(
            name='Datasets',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('full_text', models.TextField(blank=True, default=b'', null=True)),
                ('result_folder', models.CharField(blank=True, default=b'', max_length=255, null=True)),
                ('date_created', models.DateTimeField(blank=True, default=None, null=True)),
                ('status', models.CharField(choices=[('inherited', 'inherited from DC 1.0'), ('validated', 'meta-info validated awaiting file download'), ('auto-parsed', 'meta-info extracted automatically awaiting validation'), ('transfer', 'file is downloading'), ('downloaded', 'downloaded/closed'), ('complete', 'analysis complete'), ('error', 'error/hold- see comments'), ('?', 'not sure')], default=b'new', max_length=255)),
                ('comments', models.TextField(blank=True, default=b'')),
                ('fastqc', models.CharField(max_length=255, null=True)),
                ('mapped', models.CharField(max_length=255, null=True)),
                ('map_ratio', models.CharField(max_length=255, null=True)),
                ('pbc', models.CharField(max_length=255, null=True)),
                ('peaks', models.CharField(max_length=255, null=True)),
                ('frip', models.CharField(max_length=255, null=True)),
                ('dhs', models.CharField(max_length=255, null=True)),
                ('motif', models.CharField(max_length=255, null=True)),
                ('qc_summary', models.CharField(max_length=255, null=True)),
                ('cell_line', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, to='datacollection.CellLines')),
                ('cell_pop', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, to='datacollection.CellPops')),
                ('cell_type', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, to='datacollection.CellTypes')),
            ],
            options={
                'verbose_name_plural': 'datasets',
            },
        ),
        migrations.CreateModel(
            name='DiseaseStates',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, default=b'', max_length=255, null=True)),
                ('comments', models.CharField(blank=True, default=b'', max_length=255, null=True)),
                ('status', models.CharField(blank=True, choices=[('new', 'newly imported'), ('ok', 'validated')], default='new', max_length=255, null=True)),
                ('aliases', models.CharField(blank=True, default=None, max_length=255, null=True)),
            ],
            options={
                'verbose_name_plural': 'disease states',
            },
        ),
        migrations.CreateModel(
            name='Factors',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, default=b'', max_length=255, null=True)),
                ('type', models.CharField(blank=True, choices=[('tf', 'transcription factor'), ('hm', 'histone mark'), ('cr', 'chromatin regulator'), ('ca', 'chromatin accessibility'), ('predicted transcription factor', 'predicted transcription factor'), ('predicted chromatin regulator', 'predicted chromatin regulator'), ('both predicted transcription factor and chromatin regulator', 'both predicted transcription factor and chromatin regulator'), ('not sure', 'not sure'), ('other', 'other')], default=b'', max_length=255, null=True)),
                ('status', models.CharField(blank=True, choices=[('new', 'newly imported'), ('ok', 'validated')], default='new', max_length=255, null=True)),
                ('comments', models.CharField(blank=True, default=b'', max_length=255, null=True)),
            ],
            options={
                'verbose_name_plural': 'factors',
            },
        ),
        migrations.CreateModel(
            name='Journals',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, default=b'', max_length=255, null=True)),
                ('issn', models.CharField(blank=True, default=b'', max_length=9, null=True)),
                ('impact_factor', models.FloatField(default=0.0, null=True)),
            ],
            options={
                'verbose_name_plural': 'journals',
            },
        ),
        migrations.CreateModel(
            name='Papers',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pmid', models.IntegerField(blank=True, default=None, null=True)),
                ('unique_id', models.CharField(blank=True, default=b'', max_length=255, null=True)),
                ('title', models.CharField(blank=True, default=b'', max_length=255, null=True)),
                ('reference', models.CharField(blank=True, default=b'', max_length=255, null=True)),
                ('abstract', models.TextField(blank=True, default=b'', null=True)),
                ('pub_date', models.DateField(blank=True, default=None, null=True)),
                ('date_collected', models.DateTimeField(blank=True, default=None, null=True)),
                ('authors', models.CharField(blank=True, default=b'', max_length=1000, null=True)),
                ('last_auth_email', models.EmailField(blank=True, default=None, max_length=254, null=True)),
                ('status', models.CharField(blank=True, choices=[('imported', 'paper entered awaiting datasets'), ('datasets', 'datasets imported awaiting download'), ('transfer', 'datasets download in progress'), ('downloaded', 'datasets downloaded awaiting analysis'), ('complete', 'analysis complete/complete'), ('error', 'error/hold- see comments')], default=b'imported', max_length=255, null=True)),
                ('comments', models.TextField(blank=True, default=b'', null=True)),
                ('lab', models.CharField(blank=True, default=b'', max_length=1000, null=True)),
                ('pub_summary', models.CharField(blank=True, default=b'', max_length=1000, null=True)),
                ('journal', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='datacollection.Journals')),
                ('user', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'papers',
            },
        ),
        migrations.CreateModel(
            name='PaperSubmissions',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pmid', models.IntegerField(default=0)),
                ('gseid', models.CharField(blank=True, max_length=8)),
                ('status', models.CharField(choices=[('pending', 'Pending'), ('closed', 'Imported/Closed'), ('n/a', 'Not Appropriate')], max_length=255)),
                ('ip_addr', models.CharField(max_length=15)),
                ('submitter_name', models.CharField(blank=True, max_length=255)),
                ('comments', models.TextField(blank=True)),
                ('user', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Platforms',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('gplid', models.CharField(blank=True, default=b'', max_length=255, null=True)),
                ('status', models.CharField(blank=True, choices=[('new', 'newly imported'), ('ok', 'validated')], default='new', max_length=255, null=True)),
                ('name', models.CharField(blank=True, default=b'', max_length=255, null=True)),
                ('technology', models.CharField(blank=True, default=b'', max_length=255, null=True)),
                ('company', models.CharField(blank=True, default=b'', max_length=255, null=True)),
                ('experiment_type', models.CharField(blank=True, choices=[('chip', 'ChIP-Chip'), ('seq', 'ChIP-Seq')], default=b'', max_length=10, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Samples',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('unique_id', models.CharField(blank=True, default=b'', max_length=255, null=True)),
                ('other_ids', models.CharField(blank=True, default=b'', max_length=255, null=True)),
                ('series_id', models.CharField(blank=True, default=b'', max_length=255, null=True)),
                ('name', models.CharField(blank=True, default=b'', max_length=255, null=True)),
                ('fastq_file_url', models.CharField(blank=True, max_length=255, null=True)),
                ('description', models.TextField(blank=True, default=b'', null=True)),
                ('status', models.CharField(blank=True, choices=[('new', 'sample created'), ('checked', 'sample checked, awaiting importation'), ('inherited', 'sample inherited from DC 1.0'), ('running', 'analysis is running, awaiting completion'), ('complete', 'analysis complete'), ('ignored', 'ignored or wrong sample')], default=b'new', max_length=255, null=True)),
                ('comments', models.TextField(blank=True, default=b'', null=True)),
                ('dc_collect_date', models.DateTimeField(blank=True, default=None, null=True)),
                ('dc_upload_date', models.DateTimeField(blank=True, default=None, null=True)),
                ('geo_last_update_date', models.DateTimeField(blank=True, default=None, null=True)),
                ('geo_release_date', models.DateTimeField(blank=True, default=None, null=True)),
                ('re_check', models.TextField(blank=True, default=None, null=True)),
                ('antibody', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='antibody', to='datacollection.Antibodies')),
                ('assembly', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, to='datacollection.Assemblies')),
                ('cell_line', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, to='datacollection.CellLines')),
                ('cell_pop', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, to='datacollection.CellPops')),
                ('cell_type', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, to='datacollection.CellTypes')),
                ('condition', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, to='datacollection.Conditions')),
                ('curator', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='curator', to=settings.AUTH_USER_MODEL)),
                ('disease_state', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, to='datacollection.DiseaseStates')),
                ('factor', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, to='datacollection.Factors')),
                ('paper', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, to='datacollection.Papers')),
                ('platform', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, to='datacollection.Platforms')),
            ],
            options={
                'verbose_name_plural': 'samples',
            },
        ),
        migrations.CreateModel(
            name='Species',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, default=b'', max_length=255, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Strains',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, default=b'', max_length=255, null=True)),
                ('status', models.CharField(blank=True, choices=[('new', 'newly imported'), ('ok', 'validated')], default='new', max_length=255, null=True)),
                ('comments', models.CharField(blank=True, default=b'', max_length=255, null=True)),
                ('aliases', models.CharField(blank=True, default=None, max_length=255, null=True)),
            ],
            options={
                'verbose_name_plural': 'strains',
            },
        ),
        migrations.CreateModel(
            name='TissueTypes',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, default=b'', max_length=255, null=True)),
                ('status', models.CharField(blank=True, choices=[('new', 'newly imported'), ('ok', 'validated')], default='new', max_length=255, null=True)),
                ('comments', models.CharField(blank=True, default=b'', max_length=255, null=True)),
                ('aliases', models.CharField(blank=True, default=None, max_length=255, null=True)),
            ],
            options={
                'verbose_name_plural': 'tissues',
            },
        ),
        migrations.CreateModel(
            name='UserProfiles',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('team', models.CharField(blank=True, choices=[('admin', 'Administrators'), ('paper', 'paper collection team'), ('data', 'data collection team')], default=None, max_length=255, null=True)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='profile', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='samples',
            name='species',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, to='datacollection.Species'),
        ),
        migrations.AddField(
            model_name='samples',
            name='strain',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, to='datacollection.Strains'),
        ),
        migrations.AddField(
            model_name='samples',
            name='tissue_type',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, to='datacollection.TissueTypes'),
        ),
        migrations.AddField(
            model_name='samples',
            name='user',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='datasets',
            name='conts',
            field=models.ManyToManyField(blank=True, default=None, related_name='CONTS', to='datacollection.Samples'),
        ),
        migrations.AddField(
            model_name='datasets',
            name='disease_state',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, to='datacollection.DiseaseStates'),
        ),
        migrations.AddField(
            model_name='datasets',
            name='factor',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, to='datacollection.Factors'),
        ),
        migrations.AddField(
            model_name='datasets',
            name='paper',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, to='datacollection.Papers'),
        ),
        migrations.AddField(
            model_name='datasets',
            name='species',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, to='datacollection.Species'),
        ),
        migrations.AddField(
            model_name='datasets',
            name='strain',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, to='datacollection.Strains'),
        ),
        migrations.AddField(
            model_name='datasets',
            name='tissue_type',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_NULL, to='datacollection.TissueTypes'),
        ),
        migrations.AddField(
            model_name='datasets',
            name='treats',
            field=models.ManyToManyField(related_name='TREATS', to='datacollection.Samples'),
        ),
        migrations.AddField(
            model_name='datasets',
            name='user',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='aliases',
            name='factor',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='aliases', to='datacollection.Factors'),
        ),
    ]

import argparse
from optparse import OptionParser
import os, sys
import django
import cPickle as p
#sys.path.append('/mnt/Storage/home/zhengrongbin/anaconda2/lib/python2.7/site-packages')
sys.path.append('/data/home/qqin/01_Projects/Programming/dc2/lib/python2.7/site-packages')
sys.path = sys.path[::-1]
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
django.setup()
from geo_parser_newVersion import sync_samples
from geo_parser_newVersion import sync_samples_from_gsm_factor
from geo_parser_newVersion import getLocalGeo
import time
import datetime
import signal
def handler(signum, frame):
    print "time out!"
def getSyncLog(string):
    """ouput the record to DoneGsmXml.log file
    """
    print string
    f2 = open('aliveParser_time.log', 'a')
    f2.write(string+'\n')
    f2.close()

def main():
	try:
		new_parser = argparse.ArgumentParser(description="""DB parser: parse ChIP-seq, DNase- and ATAC-seq samples from GEO""")
		new_parser.add_argument('-start', dest='startPoint', type=str, required=False, help="a date formats like 2018-12-08 which is (Y-M-D), collect data starting from this date point. Default start from the day before the job is submited.")
		#new_parser.add_argument('-d', dest='date_region', type=str, required = True, help='input how many seconds, Parser will alive and execute alternately by your date region set here, eg: 86400 for 24hours.')
		new_parser.add_argument('-o', dest='fsave', type=str, required = False, help='The table you want to save the new sample information, the default is "DC_new_collection.xls" which will be built in the working directory.')
		new_parser.add_argument('-fi', dest='fill', action="store_true", default=False, help="This option should be given or not. add this option means parse the new samples from GEO and add in the CistromeDB MySQL database at same time, or means just parse new samples save in outside table, default is False.")
		new_parser.add_argument('-ty', dest='type', type=str, required=False, help="using this option to set data type you want, please select in [ChIP-seq, DNase, ATAC-seq, MeDIP-seq, and other factors, eg: H3K27ac], and separate by comma, default: [ChIP-seq, DNase, ATAC-seq]")

		args = new_parser.parse_args()
		startDate, file_save, fill_or_not, typo = args.startPoint, args.fsave, args.fill, args.type
		#time_split, file_save, fill_or_not, typo = args.date_region, args.fsave, args.fill, args.type
		#check save file	
		if not file_save:
			file_save = './DC_new_collection.xls'
		# check wanted data type
		if not typo:
			typo = False
		elif ',' not in typo:
			typo = [typo]
		else:
			typo = typo.split(',')
		# check date region
		if not startDate:
			time_one = str(datetime.date.today() - datetime.timedelta(days=1)).replace('-', '/') # parse data of the day before yesterday, to aviod raw data upload delay on current day.
		else:
			try:
				year, month, day = [int(x) for x in startDate.split('-')]
				time_one = datetime.date(year, month, day)
			except:
				getSyncLog('unrecognized date format in the -start option')
				sys.exit(1)
		
		while 1:
			time_two = str(datetime.date.today() - datetime.timedelta(days=1)).replace('-', '/') 
			getSyncLog('++new checking at %s'%str(datetime.date.today()))
			if time_two != time_one:
				dregion = '%s-%s'%(time_one, time_two)
				getSyncLog('++start collection of %s'%dregion)
				if fill_or_not:
					getSyncLog('parse new sample and add in database')
					sync_samples(file_save, fill_or_not, typo, dregion, refresh=True)
				elif not fill_or_not:
					getSyncLog("parse new samples and do not add in database")
					sync_samples(file_save, fill_or_not, typo, dregion, refresh=True)
				else:
					getSyncLog('unrecognized -fi option')
					sys.exit(1)
				time_one = time_two # make day point is continuous
			else:   
                getSyncLog('++sleep 12 hours')
                time.sleep(43200)

	except KeyboardInterrupt:
		sys.stderr.write("User interrupted me!\n")
		sys.exit(0)

if __name__ == '__main__':
    main()
    #signal.signal(signal.SIGALRM, handler)

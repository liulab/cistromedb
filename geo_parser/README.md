#CistromeDB parser's README#

#This is a parser script for collectting ChIP-seq, ATAC-seq, and DNase-seq data from GEO.#
## For the preparation:##
* please install the tools in "requirements.txt" and build a MySQL database.
* To make this parser script link with your MySQL database, please modify the MySQL information in line 98-100 in "setting.py".
* Install biomedical text mining software ([Metamap](https://mmtx.nlm.nih.gov/Installation.shtml))
```
git clone https://RongbinZheng@bitbucket.org/RongbinZheng/db-parser.git
```

# The main script is sync_new_samples_from_geo.py, and the usage is shown as follew:#
## +--------overview of the GEO parser script ##
```
python sync_new_samples_from_geo.py -h

usage: sync_new_samples_from_geo.py [-h] {parser,known,local} ...

DB parser: parse ChIP-seq, DNase- and ATAC-seq samples from GEO

positional arguments:
  {parser,known,local}  sub-command help
    parser              parse new sample
    known               add samples, known gsm id and factor
    local               go through the XML files in the given path, and parse
                        the detail sample information.

optional arguments:
  -h, --help            show this help message and exit
```
## +--------the first module: parse GEO ChIP-seq samples in a given date range, the scripts will get the XML files first, then justify which sequencing type the sample is, and parse details for the needed samples.##
```
python sync_new_samples_from_geo.py parser -h

usage: sync_new_samples_from_geo.py parser [-h] [-d DATE_REGION] [-o FSAVE]
                                           [-fi FILL] [-ty TYPE]

parse new data from GEO with option of filling in CistromeDB MySQL or not.

optional arguments:
  -h, --help      show this help message and exit
  -d DATE_REGION  Parser will get the pubic samples in this given date region,
                  Please use the format: 2016/01/01-2017/01/01. Default is the
                  recent 100000 entries in GEO.
  -o FSAVE        The table you want to save the new sample information, the
                  default is "DC_new_collection.xls" which will be built in
                  the working directory.
  -fi FILL        This option should be given as 'Fill' or 'Outside'. Fill
                  means parse the new samples from GEO and add in the
                  CistromeDB MySQL database at same time, while Outside means
                  just parse new samples save in outside table. The default is
                  'Fill'.
  -ty TYPE        using this option to set data type you want, please select
                  in [ChIP-Seq, DNase-Hypersensitivity, ATAC-Seq], and
                  seperate with comma if more than two you want.
```
## +--------the second module: update or parse the detail information if GSM number is known. ##
```
python sync_new_samples_from_geo.py known -h


usage: sync_new_samples_from_geo.py known [-h] -i INFILE -gc GSM_COL
                                          [-fc FACTOR_COL] -p PATH_OF_XML

add samples to CistromeDB MySQL database, those samples are with known gsm id
and facotr names.

optional arguments:
  -h, --help      show this help message and exit
  -i INFILE       The file contains at least two column, one is gsm id, one is
                  factor name with offical gene symbol.
  -gc GSM_COL     The column for gsm id in the -i table, start with 0.
  -fc FACTOR_COL  Optional, the column for factor name in the -i table, start
                  with 0.
  -p PATH_OF_XML  The folder path contain all the xml files, eg: "./geo", the
                  xml storage format should be:
                  "./geo/GSM1000/GSM1000102.xml".
```
## +--------the third module: design for if XML files have been obtained and saved in "PATH_OF_XML", the scripts will go through all the files and justify what user needed samples, then parse details.##
```
python sync_new_samples_from_geo.py local -h

usage: sync_new_samples_from_geo.py local [-h] -p PATH_OF_XML [-fi FILL]
                                          [-o FSAVE] [-ty TYPE]

optional arguments:
  -h, --help      show this help message and exit
  -p PATH_OF_XML  The folder path contain all the xml files, eg: "./geo", the
                  xml storage format should be:
                  "./geo/GSM1000/GSM1000102.xml".
  -fi FILL        This option should be given as 'Fill' or 'Outside'. Fill
                  means parse the new samples from GEO and add in the
                  CistromeDB MySQL database at same time, while Outside means
                  just parse new samples save in outside table. The default is
                  'Fill'.
  -o FSAVE        The table you want to save the new sample information, the
                  default is "DC_new_collection.xls" which will be built in
                  the working directory.
  -ty TYPE        using this option to set data type you want, please select
                  in [ChIP-Seq, DNase-Hypersensitivity, ATAC-Seq], and
                  seperate with comma if more than two you want.
```
"""Library to interface with the GEO (GDS- GEO DataSets) repository"""
import json
import os
import re
import sys
import urllib
import traceback
from datetime import datetime
import time
#import enchant
import cPickle
import pickle
import commands
from operator import itemgetter
#from classifiers import *
#from django.db.models import Q
import re
import random
#from datacollection.models import Samples

try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET

from xml.dom.minidom import parseString

#AUTO-load classifiers
#a trick to get the current module
_modname = globals()['__name__']
_this_mod = sys.modules[_modname]

_ppath = "/".join(_this_mod.__file__.split("/")[:-1])

# d = enchant.Dict("en_US")
# #CAN drop this if this is an app!
# DEPLOY_DIR="/home/lentaing/envs/newdc1.4/src"
# sys.path.insert(0, DEPLOY_DIR)
# from django.core.management import setup_environ
from django.utils.encoding import smart_str
# import settings
# setup_environ(settings)
#from django.utils.encoding import smart_str

from datacollection import models

#dynamically load classifiers
#import classifiers
import sra
import pubmed
import geo_parser_outside 
import ab_parser

import signal
def handler(signum, frame):
     print 'time over'

def getSyncLog(string):
    """ouput the record to DoneGsmXml.log file
    """
    f2 = open('DoneGsmXML.log', 'a')
    f2.write(string+'\n')
    f2.close()

### HELPER fns
def getFromPost(geoPost, cat):
    """tries to search for cat(VALUE) returns VALUE if found, otherwise ""
    NOTE: categories can be in UPPER or lowercase, eg. TITLE or title
    """
    m = re.search("%s\((.+)\)" % cat.upper(), geoPost)
    if m:
        return m.group(1)
    else:
        return ""


def cleanCategory(s):
    """Given a string, replaces ' ' with '_'
    '/', '&', '.', '(', ')'with ''
    """
    tmp = s.replace(" ", "_")
    for bad in ['/', '&', '.', '(', ')', ',']:
        tmp = tmp.replace(bad, "")
    return tmp


def isXML(doc):
    """TEST if it is a valid geo XML record
    NOTE: first lines are-
    <?xml version="1.0" encoding="UTF-8" standalone="no"?>
    """
    f = doc.split("\n")
    return f[0].strip() == """<?xml version="1.0" encoding="UTF-8" standalone="no"?>"""


def readGeoXML(path, docString=None):
    """
    Input: a file path or a string--default is to use the path

    Tries to read in the geo xml record,
    **KEY: REMOVES the xmlns line
    Returns the xml record text WITHOUT the xmlns line!!!
    """

    if docString:
        f = docString.split("\n")
    else:
        if path:
            f = open(path)
        else:
            f = ''
    tmp = []
    for l in f:
        if l.find("xmlns=\"http://www.ncbi.nlm.nih.gov/geo/info/MINiML\"") == -1:
            tmp.append(l)

    if not docString:
        f.close()

    return "".join(tmp)

def proxyInstead(link, using=False):
    """using proxy to aviod forbidden
    """
    context = ''
    if using:
        #using proxy first
        try: # using proxy first, or using the read ip
            agent = [x.rstrip() for x in open('./pickle_file/proxy.txt')]
            proxy = {'http':'http://%s'%random.sample(agent, 1)[0]}
            urlf = urllib.urlopen(link, proxies = proxy)
            getSyncLog('.')
        except:
            urlf = urllib.urlopen(link)
            proxy = {'proxy':'local IP'}
            getSyncLog('.') # use for record, so that we can know what happened if error occured
        # check whether we get the correct inf
        context = urlf.read()
        urlf.close()
        if ('404 - File or directory not found' in context) or ('ERR_ACCESS_DENIED' in context) or (context.strip() == ''):
            urlf = urllib.urlopen(link)
            context = urlf.read()
            urlf.close()
            proxy = {'proxy':'local IP'}
            getSyncLog('.')
        getSyncLog('%s: %s'%(proxy.values()[0], link))
        return context
    try:
        urlf = urllib.urlopen(link)
        context = urlf.read()
        urlf.close()
        getSyncLog('local IP: %s'%link)
        return context
    except:
        print 'link problem: %s'%link
    return None

# def proxyInstead(link, using=True):
#     """using proxy to aviod forbidden
#     """
#     context = ''
#     if not using:
#         getSyncLog('.')
#         urlf = urllib.urlopen(link)
#         context = urlf.read()
#         urlf.close()
#         getSyncLog('local IP: %s'%link)
#         return context
#     #using proxy first
#     try: # using proxy first, or using the read ip
#         agent = [x.rstrip() for x in open('./pickle_file/proxy.txt')]
#         proxy = {'http':'http://%s'%random.sample(agent, 1)[0]}
#         urlf = urllib.urlopen(link, proxies = proxy)
#         getSyncLog('.')
#     except:
#         urlf = urllib.urlopen(link)
#         proxy = {'proxy':'local IP'}
#         getSyncLog('.') # use for record, so that we can know what happened if error occured
#     # check whether we get the correct inf
#     context = urlf.read()
#     if ('404 - File or directory not found' in context) or ('ERR_ACCESS_DENIED' in context) or (context.strip() == ''):
#         urlf = urllib.urlopen(link)
#         context = urlf.read()
#         proxy = {'proxy':'local IP'}
#         getSyncLog('.')
#     urlf.close()
#     getSyncLog('%s: %s'%(proxy.values()[0], link))
#     return context

### GDS interface
def getGDSSamples(date_region=False):
    """Will run the predefined query and return a list of GDS ids
    NOTE: this returns ALL GDS samples which are of SRA type i.e.
    ALL CHIP-SEQ, RNA-SEQ, etc.
    """
    #expireDate = now - 30 days in seconds
    #ref: http://stackoverflow.com/questions/7430928/python-comparing-date-check-for-old-file
    # _expireDate = time.time() - 60 * 60 * 24 * 30

    ret = []
    #
    # #TRY: to read a file first -- IF IT IS NOT STALE
    path = os.path.join(_ppath, "gdsSamples.txt")
    # if os.path.exists(path) and not os.path.getctime(path) < _expireDate:
    #     f = open(path)
    #     for l in f:
    #         ret.append(l.strip())
    #     f.close()
    # else:
    #     #REFRESH!
    #REAL URL
    URL = """http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=gds&term=SRA[Sample Type] AND gsm[Entry Type] AND (homo sapiens[Organism] OR mus musculus[Organism])&retmax=100000&usehistory=y"""
    if date_region:
        maxTime = date_region.split('-')[1]
        minTime = date_region.split('-')[0]
        print date_region
        URL = """http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=gds&term=SRA[Sample Type] AND gsm[Entry Type] AND (homo sapiens[Organism] OR mus musculus[Organism])&mindate=%s&maxdate=%s&datetype=pdat&retmax=100000&usehistory=y"""%(minTime, maxTime)
    try:
        getSyncLog("getGDSSample: %s" % URL) # output record
        f = urllib.urlopen(URL)
        root = ET.fromstring(f.read())
        f.close()

        #Get the IDList
        tmp = root.findall("IdList/Id")
        ret = [i.text for i in tmp]

        #write to disk
        f = open(path, "w")
        for l in ret:
            f.write("%s\n" % l)
        f.close()
        print "Refresh %s"%path
    except:
        print "Exception in user code:"
        print '-' * 60
        traceback.print_exc(file=sys.stdout)
        print '-' * 60

    return ret

### Translation fns
def gsm_idToAcc(gdsId):
    """Given a GDS id, e.g. 300982523, tries to give a GDS accession, e.g.
    GSM982523

    NOTE: there is an algorithm: acc = "GSM"+gdsId[1:] (strip leading 0s)
    """
    #Cut = dropping of the "3" (which indicates sample) and removal of leading
    #leading 0s
    cut = gdsId[1:].lstrip("0")
    return "GSM%s" % cut

def gse_idToAcc(gdsId):
    """Given a GDS id, e.g. 200030833, tries to give a GDS accession, e.g.
    GSE30833

    NOTE: there is an algorithm: acc = "GSE"+gdsId[1:] (strip leading 0s)
    """
    #Cut = dropping of the "2" (which indicates series) and removal of leading
    #leading 0s
    cut = gdsId[1:].lstrip("0")
    return "GSE%s" % cut
    
### Librarian fns
def gsmToGse(gsmid):
    """Given a gsmid, will try to get the geo series id (GSE) that the
    sample is associated with; if it is associated with several GSEs, then
    returns the first.

    STORES the GSE ID, e.g. 200030833 in the file
    NOTE: if we want GSEs then we need to translate IDs to GSEXXXXX just
          like we do for GSMs above

    uses this query:
    http://www.ncbi.nlm.nih.gov/gds/?term=gse%5BEntry+Type%5D+AND+GSM764990%5BGEO+Accession%5D&report=docsum&format=text
    """
    ret = None
    path = os.path.join(_ppath, "GSM_GSE")
    if not os.path.exists(path):
        os.mkdir(path)
    subdir = os.path.join(path, gsmid[:7])
    if not os.path.exists(subdir):
        os.mkdir(subdir)
    path = os.path.join(subdir, "%s.txt" % gsmid)
    if os.path.exists(path):
        f = open(path)
        ret = f.read().strip()
        f.close()
    else:
        #This URL is slow!
        #NOTE: for every ncbi query, try to find the eutils equivalent!
        #--it's faster
        #URL = "http://www.ncbi.nlm.nih.gov/gds/?term=gse[Entry Type] AND %s[GEO Accession]&report=docsum&format=text" % gsmid
        URL = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=gds&term=gse[Entry Type] AND %s[GEO Accession]" % gsmid

        try:
            #print "gsmToGse: %s" % URL
            #signal.alarm(180)
            urlf = proxyInstead(link=URL)
            root = ET.fromstring(urlf)

            #Get the IDList
            tmp = root.findall("IdList/Id")
            if tmp:
                ret = tmp[0].text.strip()
                f = open(path, "w")
                f.write(ret)
                f.close()

                #FIRST URL
                # m = re.search("ID:\ (\d+)", urlf.read())
                # urlf.close()
                # if m:
                #     ret = m.group(1).strip()
                #     f = open(path, "w")
                #     f.write(ret)
                #     f.close()
        except:
            print "gsmToGse"
            print "URL is" + URL
            print '-' * 60
            traceback.print_exc(file=sys.stdout)
            print '-' * 60

    return ret


def gseToPubmed(gseid):
    """Given a gseid, will try to get the pubmed id
    """
    ret = None
    path = os.path.join(_ppath, "GSE_PUB")
    if not os.path.exists(path):
        os.mkdir(path)
    subdir = os.path.join(path, gseid[:6])
    if not os.path.exists(subdir):
        os.mkdir(subdir)
    path = os.path.join(subdir, "%s.txt" % gseid)
    if os.path.exists(path):
        f = open(path)
        ret = f.read().strip()
        f.close()
    else:
        URL = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/elink.fcgi?id=%s&db=pubmed&dbfrom=gds" % gseid

        try:
            #print "gseToPubmed: %s" % URL
            #signal.alarm(180)
            urlf = proxyInstead(link=URL)
            root = ET.fromstring(urlf)

            #Get the IDList
            tmp = root.findall("LinkSet/LinkSetDb/Link/Id")
            if tmp:
                ret = tmp[0].text.strip()
                f = open(path, "w")
                f.write(ret)
                f.close()
        except:
            print "gsmToGse"
            print '-' * 60
            traceback.print_exc(file=sys.stdout)
            print '-' * 60
    return ret


def gsmToSra(gsmid):
    """Given a gsm id, will try to get an SRA id, using this query:
    http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=sra&term=GSM530220 to get the SRA id

    Returns the SRA id corresponding to the GSM, None otherwise
    """
    ret = None
    path = os.path.join(_ppath, "GSM_SRA")
    if not os.path.exists(path):
        os.mkdir(path)
    subdir = os.path.join(path, gsmid[:7])
    if not os.path.exists(subdir):
        os.mkdir(subdir)
    path = os.path.join(subdir, "%s.txt" % gsmid)

    if os.path.exists(path):
        f = open(path)
        ret = f.read().strip()
        f.close()
    else:
        URL = "http://www.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=sra&term=%s" % gsmid
        try:
            #print "gsmToSra: %s" % URL
            #signal.alarm(180)
            urlf = proxyInstead(link=URL)
            root = ET.fromstring(urlf)

            #Get the IDList--should just be one
            tmp = root.findall("IdList/Id")
            if tmp:
                ret = tmp[0].text.strip()
                f = open(path, "w")
                f.write(ret)
                f.close()
        except:
            print '-' * 60
            traceback.print_exc(file=sys.stdout)
            print '-' * 60
    return ret


def getGeoXML(accession, path='geo'):
    """HANDLES GEO XML records--i.e. our GEO XML librarian!
    Given a GEO ACCESSION ID, return the xml record for it
    (making the urllib call)"""

    #path pattern: EXAMPLE-GSM1126513 geo/GSM1126/GSM1126513
    #path = os.path.join(_ppath, ddir)
    if not os.path.exists(path):
        os.mkdir(path)
    subdir = os.path.join(path, accession[:7])
    if not os.path.exists(subdir):
        os.mkdir(subdir)
    path = os.path.join(subdir, "%s.xml" % accession)
    if os.path.exists(path):
        f = open(path)
        docString = f.read()
        f.close()
    else:
        #print accession
        URL = "http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=%s&view=quick&form=xml&targ=self" % accession

        try:
            #print "getGeoXML: %s" % URL
            #signal.alarm(180)
            docString = proxyInstead(link=URL)
            if not isXML(docString): # try again
                #signal.alarm(180)
                getSyncLog('.')
                docString = proxyInstead(link=URL)
            if isXML(docString):
                #write to file
                f = open(path, "w")
                f.write(docString)
                f.close()
                #getSyncLog(proxy.values()[0]+'\t'+accession + '\n')# output record
            else:
                print accession
                print "ERROR: accession is NOT xml. (The accession may be deleted from GEO repository)"
                f1 = open('gsm_notXML.txt', 'a')
                f1.write(accession + '\n')
                f1.close()

                return None

        except:
            print "Exception in user code:"
            print '-' * 60
            traceback.print_exc(file=sys.stdout)
            print '-' * 60
            docString = None
    return docString


def parseGeoInfo(accession, ddir='geo'):
    """parse necessary information (detailed description from the Geo XML file"""
    xmlString = readGeoXML(None, getGeoXML(accession, path=ddir))
    tree = ET.fromstring(xmlString)
    ret = {}
    for node in tree.findall("Sample/Channel/Characteristics"):
        if node.get("tag"):
            ret[node.get("tag").replace("_", " ")] = node.text.strip()
    if not ret:
        ret["Characteristics"] = tree.findall("Sample/Channel/Characteristics")[0].text.strip()
    ret["source name"] = tree.find("Sample/Channel/Source").text.strip()
    ret["title"] = tree.find("Sample/Title").text.strip()
    ret['last update date'] = tree.find("Sample/Status/Last-Update-Date").text.strip()
    ret['release date'] = tree.find("Sample/Status/Release-Date").text.strip()
    return ret


def postProcessGeo(accession, ddir='geo', docString=None):
    """post processes the GEO record to feed into the classifiers
    uses docString IF it is available, otherwise will read the record
    """
    #ignore these tags
    ignore = ["Growth-Protocol", "Extract-Protocol", "Treatment-Protocol"]
    _thresh = 10 #max 10 words

    path = os.path.join(_ppath, "geoPost")
    if not os.path.exists(path):
        os.mkdir(path)
    subdir = os.path.join(path, accession[:7])
    if not os.path.exists(subdir):
        os.mkdir(subdir)
    path = os.path.join(subdir, "%s.txt" % accession)

    if os.path.exists(path):
        f = open(path)
        docString = f.read()
        f.close()
        return docString
    else:
        #need to build the doc
        if not docString:
            #read the document from geo
            docString = getGeoXML(accession, ddir)
            #process the string

        if not docString:
            return None

        text = readGeoXML(None, docString=docString)

        try:
            root = ET.fromstring(text)
        except:
            print "Could not parse: %s" % accession
            print '-' * 60
            traceback.print_exc(file=sys.stdout)
            print '-' * 60
            return None

        #3. collect all of the information under Sample/Channel
        tmp = []

        #Things in sample to get:
        ls = ['Title', 'Source', 'Library-Strategy', 'Library-Source',
              'Library-Selection', 'Supplementary-Data']
        for t in ls:
            tag = root.findall("Sample/%s" % t)
            for tt in tag:
                tmp.append("%s(%s)" % (t.upper(), tt.text.strip().upper()))

        channels = root.findall("Sample/Channel")

        for c in channels:
            for child in c:
                category = ""

                if child.tag in ignore:
                    continue

                #Special case: Characteristic--take the tag attrib
                if child.tag == "Characteristics":
                    if "tag" in child.attrib and child.attrib["tag"]:
                        category = child.attrib["tag"].lstrip().rstrip()
                else:
                    category = child.tag.lstrip().rstrip()

                #convert categories like "cell line" to "CELL_LINE"
                #tmp.append("%s(%s)" % (category.replace(" ","_").upper(),
                #                       child.text.strip()))
                val = child.text.strip()
                #THRESHOLD: values can be at most 10 words
                if len(val.split()) <= _thresh:
                    tmp.append("%s(%s)" % (cleanCategory(category).upper(),
                                           val.upper()))
                else:
                    #take first 10 words
                    tmp.append("%s(%s)" % (cleanCategory(category).upper(),
                                           " ".join(val.split()[:_thresh]).upper()))


        #4. write the information to file
        f = open(path, "w")
        f.write("%s" % smart_str("\n".join(tmp)))
        f.close()
	
        return "\n".join(tmp)

def checkType(acc, sample_path, type_need = ["ChIP-Seq", "DNase-Hypersensitivity", "ATAC-Seq"]):
    type_need = [x.upper() for x in type_need]
    if os.path.isfile(sample_path): #it's a file--check if it's ChIP-Seq
        ret = {}
 #       acc = gsmid #df.split(".")[0]
        #NOTE: we need readGeoXML to process
        text = readGeoXML(sample_path)
        try:
            rt = ET.fromstring(text)
            tag = rt.findall("Sample/Library-Strategy")
            tag_Ds = rt.findall("Sample/Description")
            tag_Dp = rt.findall("Sample/Data-Processing")
            #check ENCODE and reject it
            email = rt.findall('Contributor/Email') # jusitify ENCODE based on email: encode-help@lists.stanford.edu
            if email and ('encode' in email[0].text.strip()):
                # judge whether we collected this encode sample before from ENCODE website.
                #1. remove cistrome existed encode sample based on gsm
                f = open('./pickle_file/encodeInCistrome_butEncodeIDin_gsm.pk', 'rb')
                encode_aleady_gsm = pk.load(f)
                f.close()
                if acc in encode_aleady_gsm:
                    return None
                #2. remove cistrome existed encode sample based on encode id in geo
                encode_acc = re.findall(r'experiment encode accession: ENC\S*\n', tag_Ds[0].text.strip())
                if len(encode_acc)>0:
                    encode_acc = [x.lstrip('experiment encode accession: ').rstrip() for x in encode_acc][0]
                    if encode_acc in list(set(models.Samples.objects.values_list('series_id', flat = True))):
                        return None
                #return None
            
            #check for Library-Strategy
            a = None
            if tag_Dp and re.findall(r'Library strategy:\s\S*\n', tag_Dp[0].text.strip(), re.I):
                a=re.findall(r'Library strategy:\s\S*\n', tag_Dp[0].text.strip(), re.I)[0].strip('Library strategy: \n').strip('library strategy: \n')
            tag_Dt = rt.findall("Sample/Title")
            getSyncLog(sample_path+' : '+str(tag[0].text.strip()))
            if tag and (tag[0].text.strip().upper() in type_need) and ("SCATAC" not in tag_Dt[0].text.strip().upper()) and ("SCATAC" not in tag_Ds[0].text.strip().upper()) and ("SINGLE" not in tag_Dt[0].text.strip().upper()) and ("SINGLE" not in tag_Ds[0].text.strip().upper()):
#                        ret.append(acc)
                ret[acc] = tag[0].text.strip()
#                    print (tag and str(tag[0].text.strip()) == "OTHER")
            if tag and str(tag[0].text.strip()) == "OTHER" and ("ATAC-SEQ" in type_need):
                #match ATAC-seq
                m=["ATAC-SEQ", "ATAC SEQ", "ATAC_SEQ"]
                for i in m:
                    if a and a.upper() == i and (i in tag_Dt[0].text.strip().upper()) and ("SCATAC" not in tag_Dt[0].text.strip().upper()) and ("SCATAC" not in tag_Ds[0].text.strip().upper()) and ("SINGLE" not in tag_Dt[0].text.strip().upper()) and (acc not in ret):
                        #ret.append(acc)
                        ret[acc] = 'ATAC-seq'
                    if tag_Dt and (i in tag_Dt[0].text.strip().upper()) and ("SCATAC" not in tag_Dt[0].text.strip().upper()) and ("SCATAC" not in tag_Ds[0].text.strip().upper()) and ("SINGLE" not in tag_Dt[0].text.strip().upper()) and (acc not in ret):
                        #ret.append(acc)
                        ret[acc] = 'ATAC-seq'
                    if tag_Ds and (i in tag_Ds[0].text.strip().upper()) and ("SCATAC" not in tag_Ds[0].text.strip().upper())  and ("SCATAC" not in tag_Ds[0].text.strip().upper()) and ("SINGLE" not in tag_Ds[0].text.strip().upper()) and (acc not in ret):
                        #ret.append(acc)
                        ret[acc] = 'ATAC-seq'
            return ret
        except:
            #ignored!
            return None
    return None
def getGeoSamples_byType(ddir="geo", ttype=["ChIP-Seq", "DNase-Hypersensitivity", "ATAC-Seq", "MeDIP-Seq"], unique_ids=False, refresh=False):
    """A filter for our Geo model; searches our db for the specific sample
    type.
    NOTE: hones in on Library-Strategy tag

    Returns a list of samples fitting the specified

    NOTE: building this up takes time, around 10 secs almost 1 minute!
    TRY: caching the result, reading from a cached file takes only 1 minute
    Store them in files by the .ttype--in the local dir
    """
    ret = {}

    #check for a cached file:
    p = os.path.join(_ppath, ddir, ".%s" % '_'.join(ttype))
    if not refresh and os.path.exists(p):
        f = open(p)
        for l in f:
            ret.append(l.strip())
        f.close()
    else:
        if not unique_ids: #qury all local samples
            #NEED to generate the file, and make the call recursively
            #actually, just one level of recursion b/c geo is pretty flat
            p = os.path.join(_ppath, ddir)
            ls = os.listdir(p)
            for df in ls:
                path = os.path.join(p, df)
                if os.path.isfile(path): #it's a file--check if it's ChIP-Seq
                    typo = checkType(acc=df.split(".")[0], sample_path=path, type_need=ttype) # check whether the seq type is chip-seq, atac, or dnase
                    if typo:
                        ret[typo.keys()[0]] = typo.values()[0]
                    else:
                        pass
                else:
                    #it's a dir recur
                    newd = os.path.join(ddir, df)
                    newdict = getGeoSamples_byType(ddir=newd, ttype=ttype, refresh=refresh)
                    if newdict and (type(newdict) == type(ret)):
                        ret = dict(ret, **newdict)
                    #ret = dict(ret + getGeoSamples_byType(ddir=newd, ttype=ttype, refresh=refresh))
        elif unique_ids: # query just for the collected gsm
            for gsmid in unique_ids:
                p = os.path.join(_ppath, ddir+'/'+gsmid[:7]+'/'+gsmid+'.xml')
                typo = checkType(acc=gsmid, sample_path=p, type_need=ttype)
                if typo:
                    ret[typo.keys()[0]] = typo.values()[0]
                else:
                    pass
        #write the local file:
        # f = open(os.path.join(_ppath, ddir, ".%s" % ttype), "w")
        # for gsm in ret:
        #     f.write("%s\n" % gsm)
        # f.close()
    return ret


def getGeoSamples_byTypes(path, ddir="geo", datatype=False, gsmids=False, refresh=False): #ttypes = ["ATAC-Seq"]): #"ChIP-Seq", "DNase-Hypersensitivity"]):

    ret = []
    if not refresh and os.path.exists(path):
        ret = cPickle.load(open(path))
        return ret

#    for t in ttypes:
    if datatype and gsmids:
        ret = getGeoSamples_byType(ddir=ddir, ttype=datatype, unique_ids=gsmids, refresh=refresh)
    elif datatype and not gsmids:
        ret = getGeoSamples_byType(ddir=ddir, ttype=datatype, refresh=refresh)
    elif gsmids and not datatype:
        ret = getGeoSamples_byType(ddir=ddir, unique_ids=gsmids, refresh=refresh)
    else:
        ret = getGeoSamples_byType(ddir=ddir, refresh=refresh)

    cPickle.dump(ret, open(path, "w"))

    return ret


def parseUpdateTime(description_dict):
    # a trick to convert time string into a datetime object
    time_struct = time.strptime(description_dict["last update date"], "%Y-%m-%d")
    return datetime.fromtimestamp(time.mktime(time_struct))


def parseReleaseTime(description_dict):
    # a trick to convert time string into a datetime object
    time_struct = time.strptime(description_dict["release date"], "%Y-%m-%d")
    return datetime.fromtimestamp(time.mktime(time_struct))


def parseAntibody(description_dict):
    """Given a geoPost, will 1. try to parse out the antibody information
    2. create the new antibody if necessary with the name as:
    VENDOR Catalog# (TARGET) OR
    VENDOR Catalog# --if there is no target OR
    TARGET --if there is no vendor info

    To do this we key in on some key concepts

    Returns the sample's antibody, None otherwise
    """

    targetFlds = ["antibody source", "chip antibody", "antibody"]

    vendorFlds = ["antibody vendorname", "chip antibody provider",
                  "antibody vendor", "antibody manufacturer",
                  "chip antibody vendor", "chip antibody manufacturer", "antibody vendorcatalog#",
                  "antibody vendor and catalog number", "antibody vendor/catalog", "antobody vendor/catalog#"
    ]
    catalogFlds = ["antibody vendorid", "chip antibody catalog",
                   "antibody CATALOG NUMBER", "chip antibody cat #", "antibody catalog #"]
    lotFlds = ["chip antibody lot #"]

    #1. try to get the values
    vals = [None, None, None, None]
    used_fld = []
    for (i, ls) in enumerate([targetFlds, vendorFlds, catalogFlds, lotFlds]):
        for f in ls:
            tmp = description_dict.get(f)
            if tmp:
                vals[i] = tmp
                used_fld.append(f)
                break

    #2. get each term of the antibody separately
    (target, vendor, cat, lot) = tuple(vals)

    if target and "input" in target.lower():
        ret, created = models.Antibodies.objects.get_or_create(name="input")
        #return 'input'
        return ret


    def if_match_then_get(keyword, current_key):
        if keyword in current_key:
            current_value = description_dict[current_key]
            if current_value and current_key not in used_fld:
                used_fld.append(current_key)
                if current_value.startswith("catalog#"):
                    current_value = current_value.replace("catalog#, or reference):", "").strip()
                return current_value
        return None

    #3. sometimes the field name is not standard, in other words, it is not included in xxxFlds
    #If so, use string matching to parse these fields
    for k, v in description_dict.items():
        if not (k and v):
            continue

        if not vendor:
            vendor = if_match_then_get("vendor", k)

        if not cat:
            cat = if_match_then_get("catalog", k)

        if not lot:
            lot = if_match_then_get("lot", k)

    #4. compose the complete antibody name

    name_list = []
    if vendor:
        name_list.append(vendor)

    if cat:
        name_list.append(cat)

    if lot:
        name_list.append(lot)

    if not vendor and not cat and not lot:
        if not target:
            target = if_match_then_get('antibody', k)
        if target:
            name_list.append(target)
    name = ", ".join(name_list)

    if name:
        ret, created = models.Antibodies.objects.get_or_create(name=name)
        return ret
    else:
        return None
              
def add_iterm_mysql(Name, DCmodel):
    """add name into mysql if just a string have
    """
    if Name == 'None':
        return None
    result_searched_by_name = sorted(
        DCmodel.objects.extra(where={"%s REGEXP CONCAT('([^a-zA-Z0-9]|^)', `name`, '([^a-rt-zA-RT-Z0-9]|$)')"},
                                params=[Name]),
        key=lambda o: len(o.name),
        reverse=True)

    if result_searched_by_name and len(result_searched_by_name[0].name.strip()) > 0:
        return result_searched_by_name[0]

    if DCmodel not in [models.Factors, models.Aliases] :
        result_searched_by_aliases = sorted(DCmodel.objects.exclude(aliases=None).exclude(aliases="").extra(
             where={"%s REGEXP CONCAT('([^a-zA-Z0-9]|^)', `aliases`, '([^a-rt-zA-RT-Z0-9]|$)')"},
            params=[Name]),
                key=lambda o: len(o.name),
                reverse=True)
        if result_searched_by_aliases and len(result_searched_by_aliases[0].name.strip()) > 0:
            return result_searched_by_aliases[0]

    ret, created = DCmodel.objects.get_or_create(name=Name)
    if created:
        ret.status = 'new'
        return ret

    return None 


def _parse_a_field(description_dict, a_field, DCmodel, max_create_length=100, new=False):
    if  not description_dict.get(a_field, None):
        return None
    if len(description_dict.get(a_field, "")) > 0:
        if DCmodel in [models.CellTypes]: #remove '-' when match cell types, like T-cells > T cells
            description_dict_new = {}
            for v in description_dict.keys():
                description_dict_new[v] = description_dict[v].replace('-', '').replace('/', '')
            description_dict = description_dict_new  
            
        result_searched_by_name = sorted(DCmodel.objects.extra(where={"%s REGEXP CONCAT('([^a-zA-Z0-9]|^)', `name`, '([^a-rt-zA-RT-Z0-9]|$)')"},
                                  params=[description_dict[a_field]]),
            key=lambda o: len(o.name),
            reverse=True)

        if result_searched_by_name and len(result_searched_by_name[0].name.strip()) > 0:
            return result_searched_by_name[0]

        if DCmodel not in [models.Factors, models.Aliases] :
            result_searched_by_aliases = sorted(DCmodel.objects.exclude(aliases=None).exclude(aliases="").extra(
                where={"%s REGEXP CONCAT('([^a-zA-Z0-9]|^)', `aliases`, '([^a-rt-zA-RT-Z0-9]|$)')"},
                params=[description_dict[a_field]]),
                    key=lambda o: len(o.name),
                    reverse=True)
            if result_searched_by_aliases and len(result_searched_by_aliases[0].name.strip()) > 0:
                return result_searched_by_aliases[0]

    if new and (len(description_dict.get(a_field, "")) > 0) and (len(description_dict.get(a_field, "")) <= max_create_length):
        #return description_dict[a_field]
        if DCmodel in [models.CellLines, models.CellTypes, models.TissueTypes]: # these three will check wheather they appare in each other
            return description_dict[a_field]
        else:
            ret, created = DCmodel.objects.get_or_create(name=description_dict[a_field])
            if created:
                ret.status = 'new'
            return ret

    return None

def search_cellline_from_out(description_dict, a_field):
#    """last step, search cell line name from the outside resource 
 #   which contains more 50K cell lines get from web
#    """
    import pickle, re
    f_search = file('./pickle_file/search_cellline.pk', 'rb')
    database_search = pickle.load(f_search)
    f_search.close()
    tmp = None
    for i in database_search:
        if (')' not in i) and ('(' not in i):
            ii = ''.join([x for x in str(i) if x != "[" and x != '+' and x != ']'])
            cellline_pattern = re.compile(r' %s '%ii, re.I)
            m = cellline_pattern.findall(' '+description_dict[a_field]+' ')
            if m:
                #print 'cell:', i
                return database_search[i]
    return None

def _parse_fields(description_dict, strict_fields, greedy_fields, DCmodel, greedy_length=100):
    for sf in strict_fields:
        ret = _parse_a_field(description_dict, sf, DCmodel, greedy_length, new=False)
        if ret:
            return ret

            
    for gf in greedy_fields:
        ret = _parse_a_field(description_dict, gf, DCmodel, greedy_length, new=True)
        if ret:
            return ret
    if DCmodel in [models.Factors, models.Aliases]: #do not use all description information for factor part, since factor should be parse more serious.
        return None
    tmp = {}
    for x in description_dict.keys():# using all description to parse information
        if x not in ['last update date', 'release date', 'antibody', 'chip_antibody']:
            tmp[x] = description_dict[x]
    characteristics = {'characteristics':' '.join(tmp.values())}
    ret = _parse_a_field(characteristics, 'characteristics', DCmodel, greedy_length, new = False)
    if ret:
        return ret

    if DCmodel in [models.CellLines]: # search cell line by pubic database information
        ret = search_cellline_from_out(characteristics, 'characteristics')
        if ret:
            if ret and (str(ret) not in ['OF', 'IP']):
                return add_iterm_mysql(ret, models.CellLines)

    return None


def _search_factor_by_pattern(field_name, field_content):
    field_content = field_content.upper()
    if "antibody" in field_name or 'Chip' in field_name or 'title' in field_name:
        if "NONE" in field_content or "INPUT" in field_content or "IGG" in field_content or "N/A" in field_content:
            return models.Factors.objects.get_or_create(name="Input")[0]

    if "POL2" in field_content or "POLYMERASE" in field_content or 'POLII' in field_content or 'POL II' in field_content:
        return models.Factors.objects.get_or_create(name="POLR2A")[0]

    if "CTCF" in field_content:
        return models.Factors.objects.get_or_create(name="CTCF")[0]
    return None


def _guess_factor_boldly(field_name, field_content):
    factor_finder = re.compile(r"anti[^a-z]([a-z]+[-\.]?[a-z0-9]{0,4})", re.I)
    # get the word after anti
    factor_pattern = re.compile(r"^[a-z]+[-\.]?[a-z0-9]+$", re.I)

    factor_found = factor_finder.findall(field_content)

    stop_words_pattern = re.compile(r"(mouse)|(abcam)|(dmso)|(negative)|(seq)|(chip)", re.I)
    if factor_found and not stop_words_pattern.match(factor_found[0]):
        return models.Factors.objects.get_or_create(name=factor_found[0])[0]

    if len(field_content) < 10 and factor_pattern.match(field_content) and not stop_words_pattern.match(field_content):
        return models.Factors.objects.get_or_create(name=field_content)[0]
    return None


def parseFactor(description_dict):
    standard_fields = [i for i in
                       ["antibody targetdescription", "factor", "title", 'hgn', 'chip target'] if i in description_dict.keys()]
    non_standard_fields = [i for i in description_dict.keys() if "antibody" in i and i not in standard_fields]
    fields = standard_fields + non_standard_fields

    first_try = _parse_fields(description_dict, fields, [], models.Factors)
    if first_try and (str(first_try) != 'None'):
        return first_try

    print ".",
    second_try = _parse_fields(description_dict, fields, [], models.Aliases)
    if second_try and (str(second_try) != 'None'):
        return second_try.factor

    print ".",
    for f in fields:
        third_try = _search_factor_by_pattern(f, description_dict[f])
        if third_try and (str(third_try) != 'None'):
            return third_try

    # print ".",
    # for f in standard_fields:
    #     fourth_try = _guess_factor_boldly(f, description_dict[f])
    #     if fourth_try and (str(fourth_try) != 'None'):
    #         return fourth_try

    print ".",
    return None

def parseCellType(description_dict):
    return _parse_fields(description_dict,
                         ['cell type', 'cell lineage', 'cell', 'cell line', 'source name', 'cell description', 'title', ],
                         ['cell type'],
                         models.CellTypes)


def parseCellLine(description_dict):
    return _parse_fields(description_dict,
                         ['cell', 'cell line', 'source name', 'cell description', 'title', 'cell type', 'cell lineage'],
                         ['cell line'],
                         models.CellLines)


def parseCellPop(description_dict):
    return _parse_fields(description_dict, ['cell', 'source name', 'cell description', 'title', 'cell type', 'cell lineage'], [], models.CellPops)


def parseTissue(description_dict):
    return _parse_fields(description_dict,
                         ['tissue', 'tissue type', 'tissue depot', 'source name', 'cell description', 'title', 'cell type', 'cell lineage','cell', 'cell line'],
                         ['tissue', 'tissue type'],
                         models.TissueTypes)


def parseStrain(description_dict):
    return _parse_fields(description_dict,
                         ['strain', 'strain background', 'source name', 'cell description', 'title'],
                         ['strain'],
                         models.Strains)


def parseDisease(description_dict):
    return _parse_fields(description_dict,
                         ['disease', 'tumor stage', 'cell karotype', 'source name', 'title'],
                         ['disease'],
                         models.DiseaseStates)

def _getSample(cell_line_id, max_annotation):
    """using in find_red_cellAnnotation. 
    return sample SQL which the most known cell type tissue type annotation for that cell line
    max_annotation should be a list
    """
    cell_type_name = max_annotation[0].split(';')[0]
    cell_tissue_name = max_annotation[0].split(';')[1]
    if cell_type_name != 'None' and cell_tissue_name != 'None':
        cell_type = models.CellTypes.objects.filter(name = max_annotation[0].split(';')[0])[0]
        cell_tissue = models.TissueTypes.objects.filter(name = max_annotation[0].split(';')[1])[0]
        return models.Samples.objects.filter(cell_line_id = cell_line_id).filter(cell_type_id = cell_type.id).filter(tissue_type_id = cell_tissue.id)[0]
    elif cell_type_name == 'None' and cell_tissue_name != 'None':
        cell_tissue = models.TissueTypes.objects.filter(name = max_annotation[0].split(';')[1])[0]
        return models.Samples.objects.filter(cell_line_id = cell_line_id).filter(cell_type_id = None).filter(tissue_type_id = cell_tissue.id)[0]
    elif cell_type_name != 'None' and cell_tissue_name == 'None':
        cell_type = models.CellTypes.objects.filter(name = max_annotation[0].split(';')[0])[0]
        return models.Samples.objects.filter(cell_line_id = cell_line_id).filter(cell_type_id = cell_type.id).filter(tissue_type_id = None)[0]

    return None
def find_ref_cellAnnotation(cell_line_id, samples):
    """deal with those cell line may have multiple cell type and tissue, just match the one with more cases
    """
    dic = {}
    for s in samples:
        if str(s.status) not in ['run', 'completed', 'curated']:
            continue
        pair = str(s.cell_type)+';'+str(s.tissue_type)
        if pair != 'None;None':
            if pair in dic:
                dic[pair] += 1
            else:
                dic[pair] = 1
    if len(list(dic)) == 1:
        return samples[0]
    elif len(list(dic)) > 1:
        max_annotation = [x for x in dic if (dic[x] == max(dic.values()))]
#        print '1', max_annotation
        if len(max_annotation) == 1 and (not all([x=='None' for x in max_annotation[0].split(';')])):
            return _getSample(cell_line_id, max_annotation)
        if len(max_annotation) > 1:
            max_annotation = [i for i in max_annotation if (not all([x=='None' for x in max_annotation[0].split(';')]))]
            if max_annotation:
#                print '2', max_annotation
                return _getSample(cell_line_id, max_annotation)
    return None

def match(tmp_cellline, tmp_celltype, tmp_tissue):
    """correct cell type and tissue infomation based on our existed information (cellline ~ celltype ~ tissue)"""
    all_factor = [str(x.name) for x in models.Factors.objects.all()]
    all_aliases = [str(x.name) for x in models.Aliases.objects.all()]
    if str(tmp_cellline) != 'None' and (str(tmp_cellline) not in all_factor) and (str(tmp_cellline) not in all_aliases):
        ret_cellline,  cell_status= {}, {}
        for c in models.CellLines.objects.all():
            reload(sys)
            sys.setdefaultencoding("utf-8")
            r = ''.join([x for x in str(c.name) if x != " " and x != '/' and x != '-'])
            ret_cellline[r] = c
            cell_status[r] = str(c.status)
        ll = ''.join([x for x in str(tmp_cellline) if x != " " and x != '/' and x != '-'])
        if (ll in ret_cellline) and cell_status[ll] == 'ok':
            samples = models.Samples.objects.filter(cell_line_id = ret_cellline[ll].id)
            sample = find_ref_cellAnnotation(ret_cellline[ll].id, samples)
            if sample:
                #if (str(tmp_celltype) not in [str(sample.cell_type), str(sample.tissue_type)]) and (str(tmp_tissue) not in [str(sample.cell_type), str(sample.tissue_type)]):
                #    return [None, tmp_celltype, tmp_tissue] # means cell line may wrongly parsed
                #risk_cell = ['E14'] #risk cell which length more than 2
                #if (len(ll) <= 2) and (str(tmp_celltype) not in [str(sample.cell_type), str(sample.tissue_type)]):
                #    return [None, tmp_celltype, tmp_tissue]
                #if (ll in risk_cell) and (str(tmp_celltype) != str(sample.cell_type)):
                #    return [None, tmp_celltype, tmp_tissue]
                s_cell_line = sample.cell_line
                s_cell_type = sample.cell_type
                s_tissue_type = sample.tissue_type
                risk_cell = ['E14', 'CAT', 'LOG']#risk cell which length more than 2
                if (str(tmp_celltype) == 'None') and (str(tmp_tissue)) == 'None':
                    return [s_cell_line, s_cell_type, s_tissue_type] # give the linked cell annotation if only cell line parsed
                elif (str(tmp_celltype) != 'None' and str(tmp_celltype) in [str(sample.cell_type), str(sample.tissue_type)]) or (str(tmp_tissue) != 'None' and str(tmp_tissue) in [str(sample.cell_type), str(sample.tissue_type)]):
                    return [s_cell_line, s_cell_type, s_tissue_type]# if one of cell type and tissue = existed, use existed linked cell annotation
                elif (str(tmp_celltype) not in [str(sample.cell_type), str(sample.tissue_type)]) and (str(tmp_tissue) not in [str(sample.cell_type), str(sample.tissue_type)]):
                    if str(sample.cell_type) == 'Embryonic Stem Cell':
                        return [None, tmp_celltype, tmp_tissue] # throw cell line, if none of cell type and tissue = existed, because cell line may differatiate to other type
                    elif (len(ll) <= 2) or (ll.upper() in risk_cell):
                        return [None, tmp_celltype, tmp_tissue] # throw cell line, because short length cell line easily and wrongly parsed
                else:
                    return [tmp_cellline, tmp_celltype, tmp_tissue]
    elif (str(tmp_cellline) != 'None') and ((str(tmp_cellline) in all_factor) or (str(tmp_cellline) in all_aliases)):
        return [None, tmp_celltype, tmp_tissue] # aviod author fill in factor name in cell line conlumn
    return [tmp_cellline, tmp_celltype, tmp_tissue]

def semicheck(type, content):
    """correct content by our experience which get from previously manual checking.
    """
    cline = file('./pickle_file/cellline_semicheck.pk', 'rb')
    ctype = file('./pickle_file/celltype_semicheck.pk', 'rb')
    tiss = file('./pickle_file/tissue_semicheck.pk', 'rb')
    cline = pickle.load(cline)
    ctype = pickle.load(ctype)
    tiss = pickle.load(tiss)
    content1 = str(content)
    if (content1 not in cline) and (content1 not in ctype) and (content1 not in tiss):
        return content
    if type == 'cline' and content1 in cline:
        return cline[content]
    if type == 'ctype' and content1 in cline:
        return ctype[content]
    if type == 'tiss' and content1 in tiss:
        return tiss[content]
        
    return content

def search_between_table(description_dict, strict_fields, serious_fields, DCmodel):
    """authors may label cell annotation with wrong order, eg: cell type in cell line column or tissue column,
     we can address this case by searching parsed cell line in Cistrome cell type database. Also for cell type and tissue
    """
    for sf in strict_fields:
        sea = _parse_a_field(description_dict, sf, DCmodel, 100, new=False)
        if sea:
        	return sea
    tmp = {}
    for k in description_dict.keys():
        if k not in ['antibody', 'chip_antibody', 'last update date', 'release date']:
            tmp[k] = description_dict[k]
    characteristics = {'characteristics':' '.join(tmp.values())}
    sea = _parse_a_field(characteristics, 'characteristics', DCmodel, 100, new = False)
    if sea:
        return sea

    if DCmodel in [models.CellTypes]:
        """try metamap if no cell type"""
        feature = characteristics['characteristics'].replace('(', ' ').replace(')', ' ')
        try: # in case the software problem
            content = commands.getoutput('echo "%s" | metamap -y -I '%(feature))
            content = [x for x in content.split('\n') if x.endswith('[Cell]')]
            if content:
                proCell = content[0][content[0].index(':')+1:].rstrip('[Cell]').strip()
                if proCell:
                    if '(' in proCell:
                        Cell = proCell[:proCell.index('(')].strip('(').strip()
                    Cell = proCell[:proCell.index('(')].strip('(').strip()
                    if Cell.lower() not in ['cell', 'cancer', 'cancer cell', 'cancer cells', 'tumor', 'clone', 'cell line', 'clone cell', 'cells', 'human cell line', 'mouse cell line', 'cellline', 'celllines']:
                        if (not _parse_a_field({'cellType':Cell}, 'cellType', models.CellPops, 100, new = False)) and (not _parse_a_field({'cellType':Cell}, 'cellType', models.CellLines, 100, new = False)): # in case metamap result in cell pop
                            print "metamap cell type"
                            return Cell
        except:
            pass
    if serious_fields:
        for sf in serious_fields:
            tmp = description_dict.get(sf, "")
            if tmp and (DCmodel == models.CellTypes) and (not _parse_a_field({'cell':tmp}, 'cell', models.CellLines, 100, new = False)) and (not _parse_a_field({'cell':tmp}, 'cell', models.TissueTypes, 100, new = False)) and (not _parse_a_field({'cell':tmp}, 'cell', models.CellPops, 100, new = False)):
                return tmp
            if tmp and (DCmodel == models.CellLines) and (not _parse_a_field({'cell':tmp}, 'cell', models.CellTypes, 100, new = False)):
                return tmp
            if tmp and (DCmodel == models.TissueTypes) and (not _parse_a_field({'cell':tmp}, 'cell', models.CellTypes, 100, new = False)) and (not _parse_a_field({'cell':tmp}, 'cell', models.CellLines, 100, new = False)):
                return tmp
            if tmp and (DCmodel not in [models.CellTypes, models.CellLines, models.TissueTypes]):
                return tmp
    return None
        
def parseAndsearch(description_dict, field):
    """this function is simaliar with _parse_fields, but this is specific for cell annotation, just easy for search cell line,
    cell type, and tissue interactively.
    """
    tmp_sea_cellLine = search_between_table(description_dict,
                         field, ['cell line'],
                         models.CellLines)
    tmp_sea_cellType = search_between_table(description_dict,
                         field, ['cell type'],
                         models.CellTypes)
    tmp_sea_tissueType = search_between_table(description_dict,
                         field, ['tissue', 'tissue type'],
                         models.TissueTypes)
    tmp_sea_cellpop = search_between_table(description_dict,
                         field, [],
                         models.CellPops)
    tmp_sea_disease = search_between_table(description_dict,
                         field, [],
                         models.DiseaseStates)
    if str(tmp_sea_tissueType).lower() in ['primary tumor', 'tumor']:
        tmp_sea_tissueType = None
    return {'cellType':tmp_sea_cellType, 'cellLine':tmp_sea_cellLine, 'tissueType':tmp_sea_tissueType, 'cellpop':tmp_sea_cellpop, 'disease':tmp_sea_disease}               
        
def update_one_sample(gsmid, factor_require=False, ddir='geo', seq_type = 'ChIP-Seq', factor_know = False, parse_fields=['other_ids', 'paper', 'name', 'species', 'description', 'antibody', 'factor',
                                           'cell type', 'cell line', 'cell pop', 'tissue', 'strain', 'disease','update date','release date']):
    """Given a gsmid, tries to create a new sample--auto-filling in the
    meta fields

    factor_require means just need factor type is ..
    If overwrite is True and there is the sample that has the same gsmid, this function will overwrite that sample

    NOTE: will try to save the sample!!

    Returns newly created sample
    """
    
#    print gsmid
    description_dict = parseGeoInfo(gsmid, ddir)
    antibody = parseAntibody(description_dict)
    if 'factor' in parse_fields:
        if not seq_type and factor_know: # deal with those that we have known which factor it is, from a outside table
            factor, created = models.Factors.objects.get_or_create(name = factor_know)
            tmp_factor = factor
        else:
            try:
                if seq_type in ['ATAC-seq', 'ATAC-Seq']:
                    tmp_factor = models.Factors.objects.filter(name = 'ATAC-seq')[0]
                elif seq_type in ['DNase-Hypersensitivity']:
                    tmp_factor = models.Factors.objects.filter(name = 'DNase')[0]
                elif seq_type in ['MeDIP-Seq']:
                    tmp_factor = models.Factors.objects.get_or_create(name = 'MeDIP-Seq')
                else:
                    tmp_factor = parseFactor(description_dict)
            except:
                tmp_factor = parseFactor(description_dict)
    # compare parsed factor and gene name from antibody company parser
    if (str(tmp_factor) in ['None', 'none', 'NA']):
        antibody_factor_comp = [None, None]
        if str(antibody) != 'None':
            antibody_factor_comp = ab_parser._excute_antibody_factor(str(antibody))
            if str(antibody_factor_comp[0]) not in ['None', 'Flag']:
                compare_database = parseFactor({'factor':antibody_factor_comp[0]})
                if compare_database != antibody_factor_comp[0]:
                    antibody_factor_comp = [compare_database, antibody_factor_comp[1]]
                else:
                    antibody_factor_comp = [None, antibody_factor_comp[1]]
        if ('Flag' not in antibody_factor_comp) and (antibody_factor_comp[0] or antibody_factor_comp[1]):
            if antibody_factor_comp[0] and (antibody_factor_comp[0] != 'Existed') and (not antibody_factor_comp[1]):
                tmp_factor = str(antibody_factor_comp[0])
            else:
                tmp_factor = str(antibody_factor_comp[1])
    if tmp_factor and str(tmp_factor).lower() == 'input':
        return None
    if factor_require and ('chip-seq' not in [x.lower() for x in factor_require]) and (str(tmp_factor).lower() not in [x.lower() for x in factor_require]):
        return None
        
    sraId = gsmToSra(gsmid)
    sraXML = sra.getSraXML(sraId) if sraId else None

    geoPost = postProcessGeo(gsmid, ddir=ddir)
    if not geoPost:
        return None

    s, created = models.Samples.objects.get_or_create(unique_id=gsmid)
    assert isinstance(s, models.Samples)
    if tmp_factor:
        s.factor = add_iterm_mysql(str(tmp_factor), models.Factors)

    if 'species' in parse_fields:
        if getFromPost(geoPost, "organism") == "HOMO SAPIENS":
            s.species = models.Species.objects.get(pk=1)
        else:
            s.species = models.Species.objects.get(pk=2)

    if ('other_ids' in parse_fields) or ('paper' in parse_fields):
        gseId = gsmToGse(gsmid)
        pmid = gseToPubmed(gseId) if gseId else None

    if 'other_ids' in parse_fields:
        import json 
        idList = {'sra': sraId, 'gse': gseId, 'pmid': pmid}
        print idList
        s.other_ids = json.dumps(idList)
        try:
            if idList['gse']:
                s.series_id = gse_idToAcc(idList['gse'])#str(idList['gse'][4:])
        except:
            print 'cannot find GSE_id'
    
    paper = None
    if 'paper' in parse_fields and pmid:
        s.paper = pubmed.getOrCreatePaper(pmid)

    if 'name' in parse_fields:
        s.name = getFromPost(geoPost, "title")

    if 'species' in parse_fields:
        if getFromPost(geoPost, "organism") == "HOMO SAPIENS":
            s.species = models.Species.objects.get(pk=1)
        else:
            s.species = models.Species.objects.get(pk=2)

    #HERE is where I need to create a classifier app/module
    #FACTOR, platform, species--HERE are the rest of them!

    if 'description' in parse_fields:
        s.description = json.dumps(description_dict)

    if 'antibody' in parse_fields:
        s.antibody = antibody
    special = ["_", "_ _", "__", "--", "-"]
    if 'strain' in parse_fields:
        s.strain = parseStrain(description_dict)
        if str(s.strain) in special:
            s.strain = None
        else:
            pass
    if 'cell type' in parse_fields:
        tmp_celltype = None
        # get first parsed cell type information
        searchCellType = parseAndsearch(description_dict, ['cell type', 'cell lineage', 'cell', 'cell line', 'source name', 'cell description', 'title'])
        if searchCellType['cellType'] and (str(searchCellType['cellType']).upper() not in [str(searchCellType['cellLine']).upper(), str(searchCellType['tissueType']).upper()]):
            tmp_celltype = searchCellType['cellType'] # use the cell type if parsed information not in other tables. else use "None" defined before
            tmp_tmp_celltype = None
	
    if 'tissue' in parse_fields:
        searchTisssue = parseAndsearch(description_dict, ['tissue', 'tissue type', 'tissue depot', 'source name', 'cell description', 'title', 'cell type', 'cell lineage','cell', 'cell line'])
        if searchCellType['tissueType'] and (str(searchCellType['tissueType']).upper() not in [str(searchCellType['cellLine']).upper(), str(searchCellType['cellType']).upper(), str(searchCellType['cellpop']).upper(), str(searchCellType['disease']).upper()]):
            tmp_tissue = searchTisssue['tissueType']
        else:
            tmp_tissue = semicheck('tiss', parseTissue(description_dict))
            if tmp_tissue:
                test_tissue = parseAndsearch({'cell type':str(tmp_tissue)}, ['cell type']) # test parsed tissue information whether in cell type table
            else:
                test_tissue = {'cellType':None}
            if test_tissue['cellType']: # means parsed tissue information in cell type table, then ignore tissue
                tmp_tissue = None

    if 'cell line' in parse_fields:		
        tmp_cellline = semicheck('cline', parseCellLine(description_dict))
        if searchCellType['cellLine'] and (searchCellType['cellLine'] != tmp_factor) and (str(searchCellType['cellLine']) not in [str(s.strain)]): # if celltype contain cell line information, use it
            tmp_cellline = searchCellType['cellLine']
        elif (not str(tmp_cellline) or str(tmp_cellline) in ['None', 'none', 'NA']): # if information in specical sign, or in none, ignore it. 
            tmp_cellline = None
        elif (str(tmp_cellline) and (str(tmp_cellline) in [str(s.strain)])):
            tmp_cellline = None
        else:
            pass

        if (str(tmp_cellline) in special) or (not tmp_cellline):
            s.cell_line = None
            s.cell_type = add_iterm_mysql(str(tmp_celltype), models.CellTypes)
            s.tissue_type = add_iterm_mysql(str(tmp_tissue), models.TissueTypes)
        else:
            m = match(tmp_cellline, tmp_celltype, tmp_tissue)
            s.cell_line = add_iterm_mysql(str(m[0]), models.CellLines)
            s.cell_type = add_iterm_mysql(str(m[1]), models.CellTypes)
            s.tissue_type = add_iterm_mysql(str(m[2]), models.TissueTypes)

    if 'disease' in parse_fields:
        s.disease_state = parseDisease(description_dict)
        if searchCellType['disease']:
            s.disease_state = searchCellType['disease']

    if 'cell pop' in parse_fields:
        s.cell_pop = parseCellPop(description_dict)
        if searchCellType['cellpop']:
            s.cell_pop = searchCellType['cellpop']
    if 'update date' in parse_fields:
        s.geo_last_update_date = parseUpdateTime(description_dict)

    if 'release date' in parse_fields:
        s.geo_release_date = parseReleaseTime(description_dict)



    # compare parsed factor and gene name from antibody company parser, anti-factor parser return (A, B), A from title of web, B = gene symbol
    # antibody_factor_comp = [None, None]
    # if str(antibody) != 'None':
    #     antibody_factor_comp = ab_parser._excute_antibody_factor(antibody)
    #     if str(antibody_factor_comp[0]) not in ['None', 'Flag']:
    #         compare_database = parseFactor({'factor':antibody_factor_comp[0]})
    #         if compare_database != antibody_factor_comp[0]:
    #             antibody_factor_comp = [compare_database, antibody_factor_comp[1]]
    #         else:
    #             antibody_factor_comp = [None, antibody_factor_comp[1]]
    # if (str(tmp_factor) in ['None', 'none', 'NA']) and ('Flag' not in antibody_factor_comp) and (antibody_factor_comp[0] or antibody_factor_comp[1]):
    #     if antibody_factor_comp[0] and not antibody_factor_comp[1]:
    #         s.factor = add_iterm_mysql(str(antibody_factor_comp[0]), models.Factors)
    #     else:
    #         s.factor = add_iterm_mysql(str(antibody_factor_comp[1]), models.Factors)
    # else:
    #     s.factor = tmp_factor

    s.save()
    time.sleep(0.3)
    return s
def parse_detail_linker(fsave, fill_or_not, need_added_samples, FactorType=False, ddir='geo'):
    """update sample information by function "update_one_sample", and record the entry to output
    """
    need_added_samples_gsm = need_added_samples.keys()
    for s in need_added_samples_gsm:
        getSyncLog('parse details: %s'%s)
        try:
            if fill_or_not:
                sample = update_one_sample(gsmid=s, factor_require=FactorType, ddir=ddir, seq_type=need_added_samples[s]) # local_repo_samples_dict[s] can give the information of ChIP-seq or ATAC-seq or DNase-seq.
                if not sample:
                    continue # means the sample is not the factor user want
                if (str(sample.factor) == 'None') or (str(sample.factor) != 'None' and not str(sample.factor.type)):
                    factor_type = 'None'
                else:
                    factor_type = str(sample.factor.type)
                list_sample = [str(sample.id), str(sample.species), str(sample.factor), str(factor_type), str(sample.unique_id), str(sample.name), str(sample.paper), str(sample.series_id), str(sample.cell_line), str(sample.cell_type), str(sample.tissue_type), str(sample.antibody), str(sample.disease_state), str(sample.geo_last_update_date), str(sample.geo_release_date), str(sample.cell_pop), str(sample.strain), str(sample.description)]
            else:
#                import geo_parser_outside
                list_sample = geo_parser_outside.update_one_sample(gsmid=s, factor_require=FactorType, ddir=ddir, seq_type=need_added_samples[s])
                if not list_sample:
                    continue
            try:
                list_sample = [x.strip() for x in list_sample]
                if ''.join(list_sample[8:11]) == 'NoneNoneNone' and list_sample[2] != 'None': # cell annotation are all none, only
                    fnone = open('DC_NoneCellAnn.xls', 'a')
                    fnone.write('\t'.join(list_sample)+'\n')
                    fnone.close()
                    f = open(fsave, 'a')
                    f.write('\t'.join(list_sample)+'\n')
                    f.close()    
                elif list_sample[2] == 'None' and ''.join(list_sample[8:11]) != 'NoneNoneNone': #factor none, only
                    fnone = open('DC_NoneFactor.xls', 'a')
                    fnone.write('\t'.join(list_sample)+'\n')
                    fnone.close()
                elif ''.join(list_sample[8:11]) == 'NoneNoneNone' and list_sample[2] == 'None': # both factor and cell none
                    fnone = open('DC_NoneFactorCell.xls', 'a')
                    fnone.write('\t'.join(list_sample)+'\n')
                    fnone.close()
                else:
                    f = open(fsave, 'a')
                    f.write('\t'.join(list_sample)+'\n')
                    f.close()
            except:
                getSyncLog("Error when writing in table: %s" % s)

        except:
            print "problems in %s"%s
            #sys.exit(1)

def sync_samples(fsave, fill_or_not=False, DataType=False, dateRegion = False, refresh=False):
    """Will run through the whole flow of this package, trying to update
    the db w/ new samples if any.

    refresh: whether need to sync local repository (pickle file) with GEO.
        If yes, sync local pickle file with GEO, then sync database with local pickle file.
        If no, only sync database with local pickle file.

    Returns: list of newly created samples

    """

    if refresh:
        datatype = []
        if DataType:
            for x in DataType:
                if x.lower() == 'atac-seq':
                    datatype.append('ATAC-Seq')
                elif x.lower() == 'dnase':
                    datatype.append('DNase-Hypersensitivity')
                elif x.lower() == 'medip-seq':
                    datatype.append('MeDIP-Seq')
                else:
                    datatype.append('ChIP-Seq')
        else:
            datatype = False
        getSyncLog("# 1. resync the repository from Internet")# output record to log file
        local_repo_path = "repository_samples.pickle"
        local_db_samples = set(models.Samples.objects.values_list('unique_id', flat=True))
    
        gdsSamples = getGDSSamples(dateRegion)
        begin = datetime.now()
        getSyncLog('start %s: There are %s GDS Samples in sum'%(dateRegion, len(gdsSamples)))# output record to log file
        #getSyncLog("There are %s GDS Samples in sum"%len(gdsSamples))# output record to log file

        gsm_collect = []
        one_percent = len(gdsSamples)/100
        cnt = 0
        for gdsid in gdsSamples:
            cnt += 1
            if cnt % one_percent == 0:
                getSyncLog("%s%%"%(cnt/one_percent))
            gsm = gsm_idToAcc(gdsid)
            if gsm and (gsm not in local_db_samples) and (gsm not in gsm_collect):
                geoXML = getGeoXML(gsm)
                
                """if get one xml of gsm, then parser the information immediately"""
                getType = getGeoSamples_byTypes(path=local_repo_path, datatype=datatype, gsmids=[gsm], refresh=refresh)
                if getType:
                    gsm_collect.append(gsm) # collect gsm of just parsed samples
                    #getSyncLog('parse details: %s'%gsm)
                    parse_detail_linker(fsave, fill_or_not, need_added_samples=getType, FactorType = DataType)
                """remove all the follew sync part part to update_one_sample function, so that just query those we needed"""
                #geoPost = postProcessGeo(gsm) 
                # "2a. sync geo and sra"
                # sraId = gsmToSra(gsm)
                # if sraId:
                #     sra.getSraXML(sraId)

    # getSyncLog("# 2. get all samples")# output record to log file
    # local_repo_path = "repository_samples.pickle"
    # local_repo_samples_dict = getGeoSamples_byTypes(path=local_repo_path, datatype=DataType, gsmids=gsm_collect, refresh=refresh)
    # local_repo_samples = set(local_repo_samples_dict.keys())
    

    # local_db_samples = set(models.Samples.objects.values_list('unique_id', flat=True))

    # getSyncLog("# 3. try to calculate new samples")# output record to log file
    # getSyncLog("There are %d samples in local repo." % len(local_repo_samples))
    # getSyncLog("There are %d samples in local db." % len(local_db_samples))
    
    # need_added_samples = sorted(list(local_repo_samples - local_db_samples))
    # need_added_samples_new = [x for x in need_added_samples if x in gsm_collect] # just add the samples we wanted based on gsm_collect
    # getSyncLog("%d samples will be added." % len(need_added_samples_new))

    # print "# 4. try to add new samples"
    # for s in need_added_samples_new:
    #     getSyncLog('parse details: %s'%s)
    #     f = open(fsave, 'a')
    #     try:
    #         if (not fill_or_not) or (fill_or_not and fill_or_not == 'Fill'):
    #             sample = update_one_sample(gsmid=s, seq_type=local_repo_samples_dict[s]) # local_repo_samples_dict[s] can give the information of ChIP-seq or ATAC-seq or DNase-seq.
    #             if (str(sample.factor) == 'None') or (str(sample.factor) != 'None' and not str(sample.factor.type)):
    #                 factor_type = 'None'
    #             else:
    #                 factor_type = str(sample.factor.type)
    #             list_sample = [str(sample.id), str(sample.species), str(sample.name), str(sample.paper), str(sample.unique_id), str(sample.series_id), str(sample.cell_line), str(sample.cell_type), str(sample.tissue_type), str(sample.antibody), str(sample.factor), str(factor_type), str(sample.disease_state), str(sample.geo_last_update_date), str(sample.geo_release_date), str(sample.cell_pop), str(sample.strain), str(sample.description)]
    #         else:
    #             import geo_parser_outside
    #             list_sample = geo_parser_outside.update_one_sample(gsmid=s, seq_type=local_repo_samples_dict[s])

    #         try:
    #             f.write('\t'.join(list_sample)+'\n')
    #             f.close()
    #         except:
    #             getSyncLog("Error when writing in table: %s" % s)

    #     except KeyboardInterrupt:
    #         print "User interrupts!"
    #         sys.exit(1)
   # getSyncLog('Fianlly, we get new samples: %d'%len([x for x in gsm_collect if x not in local_db_samples]))
    end = datetime.now()  
    getSyncLog('++++++finish time %s'%end)
    getSyncLog('spend time: %s'%(end-begin))
   
def sync_samples_from_gsm_factor(infile, gsm_col, factor_col, fsave, xmlPath='geo', fill_or_not = False, refresh = False):
    getSyncLog("try to add samples based on outside table which contain gsm ID and factor name")

    need_added_samples = [x.rstrip().split('\t') for x in open(infile)]
    local_db_samples = set(models.Samples.objects.values_list('unique_id', flat=True))
    getSyncLog('totally %d samples need to be added'%len(need_added_samples))
    n = 1
    for iterm in need_added_samples:
        print n # let me know which the process 
        try:
            if (not refresh) and (iterm[int(gsm_col)] in local_db_samples):
                continue
            if factor_col:
                factor_know = iterm[int(factor_col)]
            else:
                factor_know = False
            if fill_or_not:
                sample = update_one_sample(gsmid=iterm[int(gsm_col)], ddir=xmlPath, seq_type=False, factor_know=factor_know)
                if (str(sample.factor) == 'None') or (str(sample.factor) != 'None' and not str(sample.factor.type)):
                    factor_type = 'None'
                else:
                    factor_type = str(sample.factor.type)
                list_sample = [str(sample.id), str(sample.species), str(sample.factor), str(factor_type),  str(sample.unique_id), str(sample.name), str(sample.paper), str(sample.series_id), str(sample.cell_line), str(sample.cell_type), str(sample.tissue_type), str(sample.antibody), str(sample.disease_state), str(sample.geo_last_update_date), str(sample.geo_release_date), str(sample.cell_pop), str(sample.strain), str(sample.description)]
            else:
                list_sample = geo_parser_outside.update_one_sample(gsmid=iterm[int(gsm_col)], ddir=xmlPath, seq_type=False, factor_know=factor_know)
                if not list_sample:
                    out = open('Failure_in.txt', 'a')
                    print >>out, '\t'.join(iterm)
                    out.close()
            try:
                list_sample = [x.strip() for x in list_sample]
                if ''.join(list_sample[8:11]) == 'NoneNoneNone' and list_sample[2] != 'None': # cell annotation are all none, only
                    fnone = open('DC_NoneCellAnn.xls', 'a')
                    fnone.write('\t'.join(list_sample)+'\n')
                    fnone.close()
                    f = open(fsave, 'a')
                    f.write('\t'.join(list_sample)+'\n')
                    f.close()    
                elif list_sample[2] == 'None' and ''.join(list_sample[8:11]) != 'NoneNoneNone': #factor none, only
                    fnone = open('DC_NoneFactor.xls', 'a')
                    fnone.write('\t'.join(list_sample)+'\n')
                    fnone.close()
                elif ''.join(list_sample[8:11]) == 'NoneNoneNone' and list_sample[2] == 'None': # both factor and cell none
                    fnone = open('DC_NoneFactorCell.xls', 'a')
                    fnone.write('\t'.join(list_sample)+'\n')
                    fnone.close()
                else:
                    f = open(fsave, 'a')
                    f.write('\t'.join(list_sample)+'\n')
                    f.close()
            except:
                getSyncLog("Error when writing in table: %s" % s)
        except:
            out = open('Failure_in.txt', 'a')
            print >>out, '\t'.join(iterm)
            out.close()
        n = n + 1

def getLocalGeo(fsave, fill_or_not=False, xmlPath="geo", DataType=False, refresh = False):
    """This function can be used if we have some xml file of GEO, then 
    recursion all local existed geo xml and get we wanted samples (ChIP, ATAC, DNase)
    """
    getSyncLog("# 1. go through all the xml and check the type in the path of %s"%xmlPath)
    local_repo_path = "repository_samples.pickle"
    datatype = []
    if DataType:
        for x in DataType:
            if x.lower() == 'atac-seq':
                datatype.append('ATAC-Seq')
            elif x.lower() == 'dnase':
                datatype.append('DNase-Hypersensitivity')
            elif x.lower() == 'medip-seq':
                datatype.append('MeDIP-Seq')
            else:
                datatype.append('ChIP-Seq')
    else:
        datatype = False
    local_repo_samples_dict = getGeoSamples_byTypes(path=local_repo_path, datatype=datatype, ddir=xmlPath, refresh=refresh)
    local_repo_samples = set(local_repo_samples_dict.keys())


    getSyncLog("# 2. try to calculate new samples")

    local_db_samples = set(models.Samples.objects.values_list('unique_id', flat=True))

    getSyncLog("There are %d samples in local repo." % len(local_repo_samples))
    getSyncLog("There are %d samples in local db." % len(local_db_samples))
    need_added_samples = sorted(list(local_repo_samples - local_db_samples))
    #local2 = [x.rstrip().split('\t')[0] for x in open('./stuffs/DC_data/collection_2016/DC_parser_0105_0920.xls')]
    #need_added_samples = [x for x in need_added_samples if (x not in local2)]
    getSyncLog("%d samples will be added." % len(need_added_samples))
    getSyncLog("The first 10 of them are " + str(need_added_samples[:10]))

    getSyncLog("# 4. try to add new samples")
    need_added_samples_new = {x:local_repo_samples_dict[x] for x in need_added_samples} # just add the samples we wanted based on gsm_collect

    parse_detail_linker(fsave, fill_or_not, need_added_samples=need_added_samples_new, FactorType = DataType, ddir=xmlPath)




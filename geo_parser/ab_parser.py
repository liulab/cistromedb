# import os
# import sys
# import django
# import cPickle as p
# sys.path.append('/data/home/zhengrb/anaconda2/lib/python2.7/site-packages')
# sys.path.append('/data/home/zhengrb/cistromeDB/database/program')
# sys.path.append('/data/home/zhengrb/cistromeDB/database/program/dc2')
# sys.path.append('/data/home/zhengrb/cistromeDB/database/program/datacollection')
# sys.path = sys.path[::-1]
# os.environ.setdefault("DJANGO_SETTINGS_MODULE", "dc2.settings")
# django.setup()
# from datacollection import models

import geo_parser_outside
import pickle as pk
import requests
from bs4 import BeautifulSoup
import sys
import re
import time
reload(sys)
sys.setdefaultencoding("utf-8")

def Antibody_Info(antibody_cont):
	#antibody_cont = antibody_cont.replace(',', '').replace(' ', '')
	# first try with santa cruz and abcam
	# antibody_cont = antibody_cont.replace('#', ' ').replace('(', ' ').replace(')', '').replace(';', '')
	# antibody_cont = ''.join([x for x in antibody_cont if x not in trash])
	antiInf = ''
	antibody_cont = antibody_cont.replace('#', ' ').replace('(', ' ').replace(')', ' ').replace(';', ' ').replace(',', ' ').replace(':', ' ').replace('no.',' ').replace('/','').replace('cat.','')
	link_comp = {r'sc-?\s?\d+':'Santa Cruz', r'[^a-z]ab-?\s?[1-9]{3,} ':'ABcam', r'[^a-z]ab-?\s?[1-9]{3,}$':'ABcam', r'hpa-?\s?\d+':'Sigma', r'[^a-z0-9]f-?\s?\d{2,}':'Sigma',
	r'[^0-9a-z]\d{2}-\d{3,4}':'Millipore', r'A\d{3}-\.?\d{3}A?':'Bethyl' , r'^f-?\s?\d{2,}':'Sigma',
	r'^ab-?\s?\d{3,} ':'ABcam', r'^ab-?\s?\d{3,}$':'abcam', r'^\d{2}-\d{3,4}':'Millipore'}
	for field in link_comp.keys():
		antiInf_found = re.findall(field, antibody_cont, re.I)
		# stop_words_pattern = ["sc", "ab", "hpa","hpa","f","A"]
		if antiInf_found: #and not antiInf_found[0].isalpha() and antiInf_found[0].lower() not in stop_words_pattern:
			if link_comp[field] == 'ABcam' or link_comp[field] == 'Santa Cruz':
				antiInf  = link_comp[field]+' '+antiInf_found[0].strip().replace('-','').replace(' ','')
			elif link_comp[field] == 'Bethyl':
				if re.findall(r'bethyl',antibody_cont,re.I):
					if antiInf_found[0].strip().replace('.','-')[-1] == 'a':
						antiInf  = link_comp[field]+' '+antiInf_found[0].strip().replace('.','-')
					else:
						antiInf  = link_comp[field]+' '+antiInf_found[0].strip().replace('.','-')+'a'
			elif link_comp[field] == 'Sigma':
				if re.findall(r'sigma',antibody_cont,re.I):
					antiInf  = link_comp[field]+' '+antiInf_found[0].strip().replace('-','')
			else:
				antiInf  = link_comp[field]+' '+antiInf_found[0].strip()        
			return antiInf

	# second try with more
	trash = ['catalog','cat','lot','no','number','catalogue','biotechnology','biotechnologies','technology','technologies','aldrich']
	antiInf = ''
	# antibody_cont = antibody_cont.replace('#', '').replace('(', '').replace(')', '').replace(';', '').replace(',', '').replace(':', '')
	antibody_cont = ' '.join([x for x in antibody_cont.split(' ') if x not in trash])
	fields = ['Sigma', 'Active Motif', 'Cell Signaling','ActiveMotif']
	for field in fields:
		antiInf_finder = re.compile(r"%s\s+[a-z]*[-\.,]?[ ]?[a-z0-9]+"%field, re.I)
		antiInf_found = antiInf_finder.findall(antibody_cont)
		# stop_words_pattern = ["biotechnology", "sigma", "scientific", "Active Motif", "Cell Signaling","biotechnologies"]
		if antiInf_found: #and not antiInf_found[0].replace(' ', '').isalpha() and antiInf_found[0].lower() not in stop_words_pattern:
			if field.replace(' ','') == 'ActiveMotif':
				if len(antiInf_found[0].replace(field.lower(),'').replace('am','').strip()) == 5:
					antiInf  = 'Active Motif '+antiInf_found[0].replace(field.lower(),'').replace('am','').strip()
			elif field.replace(' ','') == 'CellSignaling':
				antiInf  = field+' '+antiInf_found[0].replace(field.lower(),'').replace('s','').replace('bf','').replace('b','').replace('p','').replace('lot','').replace('l','').strip()
			else:
				antiInf  = field+' '+antiInf_found[0].replace(field.lower(),'').strip()        
			return antiInf

	return None

def SantaC(ab_info):
	'''
	get the tf corresponding to the antibody in Santa Cruze web site
	'''
	# import requests
	# from bs4 import BeautifulSoup
	abid = ab_info.rstrip().split(' ')[-1].replace('sc','sc-').rstrip('X')+'-X' # head abid should add - before X
	html = 'https://www.labome.com/product/Santa-Cruz-Biotechnology/%s.html'%abid
	test = re.compile('Gene Name&nbsp;: </div>\n.*</div>',re.I)
	# print html
	try:
		tmp = requests.get(html)
		soup = BeautifulSoup(tmp.text,'html.parser')
		if soup.title.text.strip() == 'Error':
			html = 'https://www.labome.com/product/Santa-Cruz-Biotechnology/%s.html'%ab_info.rstrip().split(' ')[-1].rstrip('-X')
			tmp = requests.get(html)
			soup = BeautifulSoup(tmp.text,'html.parser')
			if soup.title.text.strip() == 'Error':
				return Citeab(ab_info)
			elif test.search(tmp.text):
				return soup.title.text.strip(), test.search(tmp.text).group().strip().split('<')[-2].split('>')[-1] #
			else:
				return Citeab(ab_info)
		elif test.search(tmp.text):
			return soup.title.text.strip(), test.search(tmp.text).group().strip().split('<')[-2].split('>')[-1] #
		else:
			return Citeab(ab_info)
	except:
		return Citeab(ab_info)



def Abcam(ab_info):
	'''
	get the tf corresponding to the antibody in abcam web site
	'''
	requests.session().keep_alive = False
	headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.109 Safari/537.36','Connection': "keep-alive"} 
	time.sleep(2)
	abid = ab_info.rstrip().split(' ')[-1].replace('-','') #here the abid should remove - first
	html = 'http://www.abcam.com/products?keywords=%s'%abid
	test = re.compile('href.*Entrez Gene',re.I)
	# test1 = re.compile('<dd class=.*<span')
	histone = re.compile('histone',re.I)
	# print html
	try:
		tmp = requests.get(html,headers = headers)
		soup = BeautifulSoup(tmp.text,'html.parser')
		if tmp.status_code != 200:
			# return tmp.status_code
			return Citeab(ab_info)
		# elif soup.title.text == 'Abcam - antibodies and reagents supplier, find any antibody':
		# 	return 'Not supply'
		elif histone.search(soup.title.text.strip()):
			return soup.title.text.strip(), Histone(soup.title.text.strip())
		elif test.search(tmp.text):
			tmp1 = requests.get(test.search(tmp.text).group().split('\"')[1])
			soup1 = BeautifulSoup(tmp1.text,'html.parser')
			if tmp1.status_code == 200:
				return soup.title.text.strip(), soup1.select('#summaryDl > dd.noline')[0].text.split('provided')[0]
			else:
				return Citeab(ab_info)
		else:
			return Citeab(ab_info)
	except:
		return Citeab(ab_info)


def Sigma(ab_info):
	'''
	get the tf corresponding to the antibody in Sigma web site
	'''
	# import requests
	# from bs4 import BeautifulSoup
	abid = ab_info.rstrip().split(' ')[-1] #here the abid should remove - first
	html = 'http://www.sigmaaldrich.com/catalog/product/sigma/%s'%abid
	flag = re.compile('flag',re.I)
	test = re.compile('catalog/genes.*',re.I)
	# print html
	try:
		tmp = requests.get(html)
		soup = BeautifulSoup(tmp.text,'html.parser')
		if tmp.status_code != 200:
			return Citeab(ab_info)
		elif flag.search(soup.title.text.strip()):
			return 'Flag', None
		elif test.search(tmp.text):
			return soup.title.text.strip(), test.search(tmp.text).group().split('<')[0].split('>')[-1] #soup.title.text.split('|')[0]+', %s'%
		else:
			return Citeab(ab_info)
	except:
		return Citeab(ab_info)


def Millipore(ab_info):
	'''
	get the tf corresponding to the antibody in Millipore and Upstate web site
	'''
	# import requests
	# from bs4 import BeautifulSoup
	abid = ab_info.rstrip().split(' ')[-1] #here the abid should remove - first
	html = 'http://www.merckmillipore.com/CN/zh/product/,MM_NF-%s#'%abid
	flag = re.compile('flag',re.I)
	test = re.compile('Gene Symbol.*\n.*?</li>',re.I)
	# test_histone = re.compile('Alternate Names.*\n.*?</li>',re.I)
	histone = re.compile('histone',re.I)
	# print html
	try:
		tmp = requests.get(html)
		soup = BeautifulSoup(tmp.text,'html.parser')
		if tmp.status_code != 200:
			# print tmp.status_code
			return Citeab(ab_info)#'Antibody without ID'
		elif flag.search(soup.title.text.strip()):
			return 'Flag', None
		elif histone.search(soup.title.text.strip()):
			# if test_histone.search(tmp.text):
			return soup.title.text.strip(), Histone(soup.title.text.strip()) #test_histone.search(tmp.text).group().split('<')[-2].split('>')[-1] #soup.title.text.split('|')[0]+', %s'%
			# else:
				# return soup.title.text.split('|')[0]
		elif test.search(tmp.text):
			return soup.title.text.strip(), test.search(tmp.text).group().split('<')[-2].split('>')[-1]
		else:
			return Citeab(ab_info)
	except:
		return Citeab(ab_info)#'Url error'


def Activemotif(ab_info):
	'''
	get the tf corresponding to the antibody in Activemotif web site
	'''
	abid = ab_info.rstrip().split(' ')[-1] #here the abid should remove - first
	html = 'http://www.activemotif.com/catalog/details/%s'%abid
	flag = re.compile('flag',re.I)
	test = re.compile('<strong>Aliases: </strong>.*</td>',re.I)
	test_title = re.compile('<h1>.*</h1>',re.I)
	# print html
	try:
		tmp = requests.get(html)
		soup = BeautifulSoup(tmp.text,'html.parser')
		if tmp.status_code != 200:
			# print tmp.status_code
			return Citeab(ab_info)#'None %s'%tmp.status_code#'Antibody without ID'
		elif re.search(r'histone',soup.title.text.strip(),re.I):
			return soup.title.text, Histone(soup.title.text.strip())
		elif flag.search(soup.title.text.strip()):
			return 'Flag', None
		# elif test.search(tmp.text) and test_title.search(tmp.text):
		# 	return test.search(tmp.text).group().split('>')[1].split('<')[0]+', %s'%test.search(tmp.text).group().split('<')[-2].split('>')[-1]
		# elif test_title.search(tmp.text):
		# 	return soup.title.text.strip()+', %s'%test.search(tmp.text).group().split('>')[1].split('<')[0]
		else:
			return Citeab(ab_info)#soup.title.text.strip()
	except:
		return Citeab(ab_info)#'Url error'


def Bethyl(ab_info):
	'''
	get the tf corresponding to the antibody in Bethyl lab web site
	'''
	abid = ab_info.rstrip().split(' ')[-1] #here the abid should remove - first
	html = 'https://www.bethyl.com/product/%s'%abid
	flag = re.compile('flag',re.I)
	test = re.compile('<label>Gene Symbol</label>\r\n.*</span>',re.I)
	# print html
	try:
		tmp = requests.get(html)
		soup = BeautifulSoup(tmp.text,'html.parser')
		if tmp.status_code != 200:
			# print tmp.status_code
			return Citeab(ab_info)#'None %s'%tmp.status_code#'Antibody without ID'
		elif flag.search(soup.title.text.strip()):
			return 'Flag', None
		elif test.search(tmp.text):
			return soup.title.text.strip(), test.search(tmp.text).group().split('<')[-2].split('>')[-1]
		else:
			return Citeab(ab_info)#soup.title.text.strip()
	except:
		return Citeab(ab_info)#'Url error'


def Cellsignal(ab_info):
	'''
	get the tf corresponding to the antibody in Bethyl lab web site
	'''
	requests.session().keep_alive = False
	headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.109 Safari/537.36','Connection': "keep-alive" } 
	time.sleep(2)
	abid = ab_info.rstrip().split(' ')[-1] #here the abid should remove - first
	html = 'https://www.cellsignal.com/products/%s'%abid
	flag = re.compile('flag',re.I)
	test = re.compile('<b>Entrez-Gene Id </b>\s*<a href=.*</a>',re.I)
	# test1 = re.compile('<dd class=.*<span')
	# print html
	try:
		tmp = requests.get(html)
		soup = BeautifulSoup(tmp.text,'html.parser')
		if tmp.status_code != 200:
			# print tmp.status_code
			return Citeab(ab_info)#'None %s'%tmp.status_code#'Antibody without ID'
		elif re.search(r'histone',soup.title.text.strip(),re.I):
			return soup.title.text.strip(), Histone(soup.title.text.strip())
		elif flag.search(soup.title.text.strip()):
			return 'Flag', None
		elif test.search(tmp.text):
			tmp1 = requests.get(test.search(tmp.text).group().split('\"')[1])
			soup1 = BeautifulSoup(tmp1.text,'html.parser')
			if tmp1.status_code != 200 :
				# print tmp1.status_code
				return Citeab(ab_info)#soup.title.text.strip()+' '+tmp1.status_code
			elif soup1.select('#summaryDl > dd.noline'):
				return soup.title.text.strip(), soup1.select('#summaryDl > dd.noline')[0].text.split('provided')[0]
			else:
				return Citeab(ab_info)#soup.title.text.strip()
		else:
			return Citeab(ab_info)#soup.title.text.strip()
	except:
		return Citeab(ab_info)#'None'#'Url error'


def Citeab(ab_info):
	'''
	get the tf corresponding to the antibody in CiteAb web site
	'''
	requests.session().keep_alive = False
	# proxies = {'http':'http://121.8.98.201:8080','https':'https://134.213.221.177:5007'}
	headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.109 Safari/537.36','Connection': "keep-alive"} 
	time.sleep(5)
	abid = ab_info.rstrip().split(' ')#[-1].replace('sc','sc-').rstrip('X')+'-X' # head abid should add - before X
	html = 'https://www.citeab.com/search?q=%s'%'+'.join(abid)
	print html
	try:
		tmp = requests.get(html,headers=headers)#,proxies=proxies
		soup = BeautifulSoup(tmp.text,'html.parser')
		if tmp.status_code != 200:
			# return '%s : %s'%(html,tmp.status_code)
			return None, None
		elif soup.find_all(class_ ='ab-name'):
			if re.search(r'histone',soup.find_all(class_ ='ab-name')[0].text,re.I):
				return soup.find_all(class_ ='ab-name')[0].text.strip(), Histone(soup.find_all(class_ ='ab-name')[0].text.strip())
			else:
				ab_html = 'https://www.citeab.com%s'%soup.find_all(class_ ='ab-name')[0].a.attrs['href']
				# print ab_html
				ab_tmp = requests.get(ab_html,headers=headers)
				ab_soup = BeautifulSoup(ab_tmp.text,'html.parser')
				if ab_tmp.status_code != 200:
					# return '%s : %s'%(ab_html,ab_tmp.status_code)
					return None, None
				elif ab_soup.select('#panel1 > ul > li'):
					for li in ab_soup.select('#panel1 > ul > li'):
						if li.h5.text == 'Protein':
							gene_html = li.a.attrs['href']
							gene_tmp = requests.get(gene_html,headers=headers)
							gene_soup = BeautifulSoup(gene_tmp.text,'html.parser')
							if gene_tmp.status_code != 200:
								# return '%s : %s'%(gene_html,gene_tmp.status_code)
								return None, None
							elif gene_soup.select('#content-gene > h2'):
								return None, gene_soup.select('#content-gene > h2')[0].text
					return str(ab_soup.title.text.strip()), None
				return str(ab_soup.title.text.strip()), None
		return None, None
	except:
		return None, None


def Histone(title):
	'''
	get things about hsitone mark corrected
	'''
	model = re.compile('H(3|2A|2B|4)K\d{1,3}(me1|me2|me3|ac|ub1)',re.I)
	if model.search(title.replace(' ','')):
		return model.search(title.replace(' ','')).group()
	else:
		title = title.replace(' ','').lower()
		histone = re.compile('H(3|2A|2B|4)(\.(X|\d))?',re.I)
		lys = re.compile('(lys|lysine|k|r)\d{1,3}',re.I)
		methyl = re.compile('(mono|di|tri)-?methyl',re.I)
		cir = re.compile('citrulline',re.I)
		acetyl = re.compile('acetyl',re.I)
		ub = re.compile('ubiquityl',re.I)
		ser = re.compile('ser')
		if histone.search(title):
			if lys.search(title):
				if methyl.search(title):
					return histone.search(title).group().upper()+lys.search(title).group().replace('lysine','K').replace('lys','K').upper()+methyl.search(title).group().replace('-','').replace('monomethyl','me1').replace('dimethyl','me2').replace('trimethyl','me3')
				elif acetyl.search(title):
					return histone.search(title).group().upper()+lys.search(title).group().replace('lysine','K').replace('lys','K').upper()+acetyl.search(title).group().replace('acetyl','ac')
				elif ub.search(title):
					return histone.search(title).group().upper()+lys.search(title).group().replace('lysine','K').replace('lys','K').upper()+ub.search(title).group().replace('ubiquityl','ub')
				else:
					return histone.search(title).group().upper()+lys.search(title).group().replace('lysine','K').replace('lys','K').upper()
			elif acetyl.search(title):
				return histone.search(title).group().upper()+acetyl.search(title).group().replace('acetyl','ac')
			elif ub.search(title):
				return histone.search(title).group().upper()+acetyl.search(title).group().replace('ubiquityl','ub1')
			elif cir.search(title):
				return histone.search(title).group().upper()+lys.search(title).group().replace('lysine','K').replace('lys','K').upper()+cir.search(title).group().replace('citrulline','Cit')
			else:
				return histone.search(title).group().upper()
		else:
			return None


def _excute_antibody_factor(antibody_content):
	anti_catlog = Antibody_Info(str(antibody_content))
	if not anti_catlog:
		return None, None # if no catlog id get, the reson may: the antibody is not in the company we covered.
	f = open('./pickle_file/existed_antibody.pk', 'rb')
	existed_antibody = pk.load(f)
	f.close() 
	if anti_catlog in existed_antibody.keys():
		return 'Existed', existed_antibody[anti_catlog]
	ab_info = anti_catlog.lower()
	if ab_info.split(' ')[0] == 'abcam':
		return Abcam(ab_info)
	elif ab_info.split(' ')[0] == 'active':
		return Activemotif(ab_info)
	elif ab_info.split(' ')[0] == 'bethyl':
		return Bethyl(ab_info)
	elif ab_info.split(' ')[0] == 'cell':
		return Cellsignal(ab_info)
	elif ab_info.split(' ')[0] == 'millipore':
		return Millipore(ab_info)
	elif ab_info.split(' ')[0] == 'santa':
		return SantaC(ab_info)
	elif ab_info.split(' ')[0] == 'sigma':
		return Sigma(ab_info)
	else:
		return None, None



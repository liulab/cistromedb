import os
import sys
import django
import cPickle as p
  ## mkvirtualenv dc2, and pip install -r requirements.txt
sys.path.append('/data/home/qqin/01_Projects/Programming/dc2/lib/python2.7/site-packages')
sys.path.append('/data/home/qqin/01_Projects/Programming/dc2')
sys.path.append('/data/home/qqin/01_Projects/Programming/dc2/dc2')
sys.path.append('/data/home/qqin/01_Projects/Programming/dc2/datacollection')
sys.path = sys.path[::-1]
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "dc2.settings")
django.setup()
from datacollection import models

Finput = sys.argv[1] # a table at least two columns, DCid + Factor
cols = sys.argv[2]

idCol, factorCol = [int(x) for x in cols.split(',')]

with open(Finput) as f:
	for line in f:
		print(line)
		line = line.rstrip().split('\t')
		ID = line[idCol]
		factor = line[factorCol]
		# get factor id
		factor_sql, created = models.Factors.objects.get_or_create(name = factor.strip())
		if created:
			factor_sql.status = 'new'
		samples = models.Samples.objects.filter(id = ID)
		if samples:
			for s in samples:
				s.factor = factor_sql
				s.save()







import os
import sys
import django
import cPickle as p
  ## mkvirtualenv dc2, and pip install -r requirements.txt
sys.path.append('/data/home/qqin/01_Projects/Programming/dc2/lib/python2.7/site-packages')
sys.path.append('/data/home/qqin/01_Projects/Programming/dc2')
sys.path.append('/data/home/qqin/01_Projects/Programming/dc2/dc2')
sys.path.append('/data/home/qqin/01_Projects/Programming/dc2/datacollection')
sys.path = sys.path[::-1]
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "dc2.settings")
django.setup()
from datacollection import models
get = []
reload(sys)
sys.setdefaultencoding("utf-8")

inputdir = ['/data1/DC_results/Result_new', '/data5/DC_results/Result_new', '/data5/DC_results/Result_new3', '/data5/DC_results/Result_new2',
'/data2/DC_results/Result_new', '/data3/DC_results/Result_new', '/data6/DC_results/Result_2016_ATAC', '/data6/DC_results/Result_2016_k27ac',
'/data6/DC_results/Result_encode3', '/data6/DC_results/Result_2016All', '/data5/DC_results/Result_2016All2']
datahub = '/data5/browser'
scripts = '/data/home/qqin/01_Projects/Programming/dc2/scripts/'

def get_query(query_string, search_fields):
    ''' Returns a query, that is a combination of Q objects. That combination
        aims to search keywords within a model by testing the given search fields.
    '''
    query = None # Query to search for every search term        
    terms = normalize_query(query_string)
    for term in terms:
        or_query = None # Query to search for a given term in each field
        for field_name in search_fields:
            q = Q(**{"%s__icontains" % field_name: term})
            if or_query is None:
                or_query = q
            else:
                or_query = or_query | q
        if query is None:
            query = or_query
        else:
            query = query & or_query
    return query

def normalize_query(query_string,
                    findterms=re.compile(r'"([^"]+)"|(\S+)').findall,
                    normspace=re.compile(r'\s{2,}').sub):
    ''' Splits the query string in invidual keywords, getting rid of unecessary spaces
        and grouping quoted words together.
        Example:
        
        >>> normalize_query('  some random  words "with   quotes  " and   spaces')
        ['some', 'random', 'words', 'with quotes', 'and', 'spaces']
    '''
    return [normspace(' ', (t[0] or t[1]).strip()) for t in findterms(query_string)] 

req_s = None #request.GET.get("species", None)
req_c = None #request.GET.get("cellinfos", None)
req_f = None #request.GET.get("factors", None)
req_p = 1 #int(request.GET.get("page", 1))

req_run = "false" #request.GET.get("run", "false")
req_curated = "false" #request.GET.get("curated", "false")
req_completed = "false" #request.GET.get("completed", "false")

req_keyword = "" #request.GET.get("keyword", "")
clicked = None #request.GET.get("clicked", None)

if req_completed == "true" or req_run == "true" or req_curated == "true":
    if req_completed == "true" and req_run == "true" and req_curated == "true":
        q_common = Q(status="completed") | Q(status="run") | Q(status="curated")
    if req_completed == "false" and req_run == "true" and req_curated == "true":
        q_common = Q(status="run") | Q(status="curated")
    if req_completed == "false" and req_run == "false" and req_curated == "true":
        q_common = Q(status="curated")
    if req_completed == "true" and req_run == "true" and req_curated == "false":
        q_common = Q(status="completed") | Q(status="run")
    if req_completed == "true" and req_run == "false" and req_curated == "false":
        q_common = Q(status="completed")
    if req_completed == "true" and req_run == "false" and req_curated == "true":
        q_common = Q(status="completed") | Q(status="curated")
    if req_completed == "false" and req_run == "true" and req_curated == "false":
        q_common = Q(status="run")
else:
    q_common = Q() ## all options are turned off

# if request.is_ajax() and (not (req_s and req_c and req_f)):
#     return HttpResponse("Request denied!")

if req_s == "all" or req_s == None:
    q_s = Q()
else:
    q_s = Q(species__name=req_s)

if req_f == "all" or req_f == None:
    q_f = Q()
elif req_f == "None":
    q_f = Q(factor__isnull=True)
else:
    q_f = Q(factor__name=req_f)

q_f = q_f & ~Q(factor__name__icontains='input') & ~Q(factor__name__icontains='igg')
q_f = q_f & ~Q(factor__name__in=['552-SKD', '598-SKD', '5HMC', '5MC', '7SK'])

if req_c == "all" or req_c == None:
    q_c = Q()
else:
    clip = lambda x: x[3:]
    if req_c.startswith(ab_cellline):
        q_c = Q(cell_line__name=clip(req_c))
    #elif req_c.startswith(ab_cellpop):
    #    q_c = Q(cell_pop__name=clip(req_c))
    elif req_c.startswith(ab_celltype):
        q_c = Q(cell_type__name=clip(req_c))
    elif req_c.startswith(ab_tissue):
        q_c = Q(tissue_type__name=clip(req_c))
    #elif req_c.startswith(ab_strain):
    #    q_c = Q(strain__name=clip(req_c))

## s is the search term for samples
# s = Samples.objects.filter(q_s & q_f & q_c & q_common & ~Q(status='hidden') & ~Q(status='new')) ## filter to get completed one
s = Samples.objects.filter(q_s & q_f & q_c & q_common & (Q(status='completed') | Q(status='run')))

for sample in s:
	if str(sample.factor) != 'None':
		list=[str(sample.id), str(sample.status), str(sample.species), str(sample.name), str(sample.paper), str(sample.unique_id), str(sample.series_id), str(sample.cell_line), str(sample.cell_type), str(sample.tissue_type), str(sample.antibody), str(sample.factor), str(sample.factor.type), str(sample.disease_state), str(sample.geo_release_date), str(sample.strain)]
		#get.append(list)
	else:
		list=[str(sample.id), str(sample.status), str(sample.species), str(sample.name), str(sample.paper), str(sample.unique_id), str(sample.series_id), str(sample.cell_line), str(sample.cell_type), str(sample.tissue_type), str(sample.antibody), str(sample.factor), 'None', str(sample.disease_state), str(sample.geo_release_date), str(sample.strain)]
		#get.append(list)			
	out=open('DC_output_20170707.xls', 'a')
	#for x in get:
	print >>out, '\t'.join([x.strip() for x in list])
	out.close()


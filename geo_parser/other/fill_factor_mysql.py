import os, sys
import django
import cPickle as p
sys.path.append('/mnt/Storage/home/zhengrongbin/anaconda2/lib/python2.7/site-packages')
sys.path = sys.path[::-1]
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
django.setup()
import geo_parser_newVersion
import geo_parser_outside
from datacollection import models

fi = sys.argv[1] # the file with three column, 1 = gene symbol, 2 = aliases (NA means no), 3 = factor type

f = [x.rstrip().split('\t') for x in open(fi, 'r')]
n = 1
for iterm in f:
	print n
	symbol = iterm[0]
	alias = iterm[1].split('|')
	try:
		if symbol not in list(set(models.Factors.objects.values_list('name', flat=True))):
			factor, created = models.Factors.objects.get_or_create(name = symbol)
			if created:
				factor.status = 'ok'
				factor.type = iterm[2]
				factor.save()
		if alias[0] == 'NA':
			continue
		for a in alias:
			if a.upper() not in [str(x).upper() for x in list(set(models.Aliases.objects.values_list('name', flat=True)))]:
				Aliases, created = models.Aliases.objects.get_or_create(name = a)
				Aliases.factor_id = models.Factors.objects.filter(name = symbol)[0].id
				Aliases.save()
	except:
		print 'problem %s'%symbol
	n = n + 1

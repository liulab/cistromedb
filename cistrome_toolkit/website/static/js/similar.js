// var vmProcess = new Vue({
//     el: "#processBar",
//     data: {
//         processData: {},
//     },
//     methods: {
//         queryProcess: function(){
//             this.$http.get('http://cistrome.org/similar/process/').then(function(response){
//                 this.processData = response.data
//             })
//         },
//         getProcess: function(){
//             setInterval(this.queryProcess() ,1000)
//             return true
//         },
//     }
// })

// var vmResult = new Vue({
//     el: "#resultTable",
//     data: {
//         datas: {},
//         batchIds: [],
//         url: "http://dc2.cistrome.org/api/",
//         curPage: 1,
//     },
//     methods: {
//         dataInfo: function (ids) {
//             // var resInspector = this.$el.querySelector("$resinspector")
//             $.blockUI({ message: $('#spinner') }),
//             document.documentElement.scrollTop = document.body.scrollTop = 641
//             this.$http.get(this.url+'inspector?id='+ids).then(function (response) {
//                 insResult.inspector = response.data
//                 toolPanel.inspector = response.data
//             }),
//             this.$http.get(this.url+'putative_target_ng?id='+ids).then(function (response) {
//                 toolPanel.targets = response.data
//                 $.unblockUI()
//             })
//         },
//         viewIds: function(){
//             return this.batchIds.join('_')
//         },
//         loadData: function (query) {
//             this.datas = query
//             return this.datas.status
//         },
//         curData: function (){
//             if (this.curPage*20 >= this.datas.items.length){
//                 return this.datas.items.slice(this.curPage*20-20, this.datas.items.length)
//             } else {
//                 return this.datas.items.slice(this.curPage*20-20, this.curPage*20)
//             }
//         },
//         jumpPage: function (page) {
//             this.curPage = Number(page)
//         },
//         prePage: function () {
//             if (this.curPage > 1) {
//                 --this.curPage
//             } else {
//                 this.curPage = 1
//             }
//         },
//         nextPage: function () {
//             if (this.curPage < this.datas.npage) {
//                 ++this.curPage
//             } else {
//                 this.curPage = this.datas.npage
//             }
//         },
//         bioResource: function(a, b, c) {
//             var biotmp = a+";"+b+";"+c
//             return biotmp.replace(/null\;?/, "").replace(/^null\;/, "").replace(/\;$/,"").replace("null","")
//         },
//     }
// })

var insResult = new Vue({
    el: "#resultInspector",
    data: {
        inspector: {},
    },
    methods: {
        send_bed: function(){
            return $("#bed_sender").submit()
        },
        send_bw: function(){
            return $("#bw_sender").submit()
        },
    }
});

var toolPanel = new Vue({
    el: "#resultTool",
    data: {
        inspector: {},
        targets: {},
        tools: {
            qc: true,
            motif: false,
            gtargets: false,
            ctargets: false
        },
        inputText: '',
        genes: {},
        ctargets: []
    },
    methods: {
        setTools: function (choose) {
            for (var item in this.tools){
                this.tools[item] = false
            }
            this.tools[choose] = true
        },
        showGene: function() {
            this.$http.get('http://mygene.info/v2/query?fields=symbol%2Cname&species=human&q='+this.inputText).then(function(response){
                this.genes = response.data
            })
        },
        goTarget: function(gene,ids) {
            this.$http.get('http://dc2.cistrome.org/api/putative_target_ng?gene='+gene+'&id='+ids).then(function(response){
                this.ctargets = response.data
            })
        },
    }
});


var vmResult2 = new Vue({
    el: "#resultGsm",
    data: {
        datas: {},
        batchIds: [],
        url: "http://dc2.cistrome.org/api/",
        curPage: 1,
    },
    methods: {
        dataInfo: function (ids) {
            // var resInspector = this.$el.querySelector("$resinspector")
            document.documentElement.scrollTop = document.body.scrollTop = 641
            this.$http.get(this.url+'inspector?id='+ids).then(function (response) {
                insResult.inspector = response.data
                toolPanel.inspector = response.data
            }),
            this.$http.get(this.url+'putative_target_ng?id='+ids).then(function (response) {
                toolPanel.targets = response.data
            })
        },
        viewIds: function(){
            return this.batchIds.join('_')
        },
        loadData: function (query) {
            this.datas = query
            return true
        },
        curData: function (){
            if (this.curPage*20 >= this.datas.items.length){
                return this.datas.items.slice(this.curPage*20-20, this.datas.items.length)
            } else {
                return this.datas.items.slice(this.curPage*20-20, this.curPage*20)
            }
        },
        jumpPage: function (page) {
            this.curPage = Number(page)
        },
        prePage: function () {
            if (this.curPage > 1) {
                --this.curPage
            } else {
                this.curPage = 1
            }
        },
        nextPage: function () {
            if (this.curPage < this.datas.npage) {
                ++this.curPage
            } else {
                this.curPage = this.datas.npage
            }
        },
        bioResource: function(a, b, c) {
            var biotmp = a+";"+b+";"+c
            return biotmp.replace(/null\;?/, "").replace(/^null\;/, "").replace(/\;$/,"").replace("null","")
        },
    }
});

var vmResult = new Vue({
    el: "#resultTable",
    data: {
        datas: {},
        batchIds: [],
        url: "http://dc2.cistrome.org/api/",
        curPage: 1,
        currentSort:'distance',
        currentSortDir:'asc',
    },
    methods: {
        dataInfo: function (ids) {
            // var resInspector = this.$el.querySelector("$resinspector")
            $.blockUI({ message: $('#spinner') }),
            document.documentElement.scrollTop = document.body.scrollTop = 641
            this.$http.get(this.url+'inspector?id='+ids).then(function (response) {
                insResult.inspector = response.data
                toolPanel.inspector = response.data
            }),
            this.$http.get(this.url+'putative_target_ng?id='+ids).then(function (response) {
                toolPanel.targets = response.data
                $.unblockUI()
            })
        },
        viewIds: function(){
            return this.batchIds.join('_')
        },
        loadData: function (query) {
            this.datas = query
            return true
        },
        jumpPage: function (page) {
            this.curPage = Number(page)
        },
        prePage: function () {
            if (this.curPage > 1) {
                this.curPage--
            } else {
                this.curPage = 1
            }
        },
        nextPage: function () {
            if (this.curPage < this.datas.npage) {
                this.curPage++
            } else {
                this.curPage = this.datas.npage
            }
        },
        sort: function(s) {
            //if s == current sort, reverse
            if(s === this.currentSort) {
              this.currentSortDir = this.currentSortDir==='asc'?'desc':'asc';
            }
            this.currentSort = s;
        },
    },
    computed: {
        sortTable: function() {
            return this.datas.items.sort((a,b) => {
                let modifier = 1;
                if(this.currentSortDir === 'desc') modifier = -1;
                if(a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
                if(a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
                return 0;
            }).filter((row, index) => {
                let start = (this.curPage-1)*20;
                let end = this.curPage*20;
                if(index >= start && index < end) return true;
            });
        }
    }
});

"""dc3 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings

from  datacollection import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    # url(r'^$', views.upload),
    # url(r'^demo/$', views.demo),
    # url(r'^process/$', views.update_process),
    url(r'^download/$', views.get_download),
    url(r'^gsm$', views.gsm_query),
    url(r'^$', views.toolkit),
    url(r'^document', views.index)
    # url(r'^similar/$', views.similar), 
    # url(r'^ginterval/$', views.giggle_interval),
    # url(r'^rpsearch/$', views.rp_search),
]


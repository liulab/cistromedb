"""
WSGI config for dc3 project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/howto/deployment/wsgi/
"""

import os
import sys

#activate_env = '/data5/home/rongbin/miniconda2/bin/activate'
#execfile(activate_env, dict(__file__=activate_env))

sys.path.append('/data5/home/changxin/miniconda2/lib/python2.7/site-packages')
sys.path.append('/data5/home/rongbin/wancx/project_01/dc3/dc3')
sys.path.append('/data5/home/rongbin/wancx/project_01/dc3')
sys.path.append('/data5/home/rongbin/wancx/project_01/dc3/datacollection')

sys.path = sys.path[::-1]

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "dc3.settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
# from itertools import chain
import json as json
# from django.core.paginator import Paginator
# from django.db.models import Q
from django.http import HttpResponse
from django.http import StreamingHttpResponse
# from django.shortcuts import render_to_response
# from django.shortcuts import redirect
# from django.template import RequestContext
# from django.views.decorators.cache import cache_page
from datacollection.models import Samples
# from .utils import get_query
import re
import os,sys
import pandas as pd
import numpy as np
import math
import time
import cistromeSearch
reload(sys)
sys.setdefaultencoding("utf-8")

def index(request):
    return render(request, 'document.html')

def get_download(request):
    file_name = request.GET.get("fname", None)
    result_path = './wancx/project_01/dc3/sim_file/download/%s_result.csv'%file_name
    def file_iterator(file_name, chunk_size=512):
        with open(file_name) as f:
            while True:
                c = f.read(chunk_size)
                if c:
                    yield c
                else:
                    break
    try:
        response = StreamingHttpResponse(file_iterator(result_path))
        response["Content-Type"] = 'application/octet-stream'
        response["Content-Disposition"] = 'attachment;filename="{0}_result.csv"'.format(file_name.rstrip().split("___")[-1])
        return response
    except:
        return render(request, '404.html')


def gsm_query(request):
    gsm_id = request.GET.get("uid", None)
    try:
        gsm_id = int(gsm_id)
    except:
        pass
    if gsm_id:
        if isinstance(gsm_id, int):
            s = Samples.objects.filter(id__iexact=gsm_id)
        else:
            s = Samples.objects.filter(unique_id__iexact=gsm_id.lower())
        if s:
            result = s.values("id")[0]
            # return HttpResponse(json.dumps({"id": s.values("id")[0], "uid": gsm_id}))
            return render(request, 'gsm.html', {'sid': result})
        else:
            return render(request, '404.html')
    else:
        return render(request, '404.html')

def _similar_function(request):
    """This is for similar data searching
    """
    try:
        peak_file = request.FILES['peak']
        peak_path = './wancx/project_01/dc3/sim_file/tmp/%s_%s'%(time.asctime(time.localtime(time.time())).replace(" ", "_"), os.path.basename(peak_file.name))
        if peak_file.name.split(".")[-1] != "bed":
            peak_path_new = './wancx/project_01/dc3/sim_file/tmp/'+'_'.join((peak_file.name.split(".")[:-1]))+'.bed'
            os.system('mv %s %s'%(peak_path, peak_path_new))
            peak_path = './wancx/project_01/dc3/sim_file/tmp/%s_%s'%(time.asctime(time.localtime(time.time())).replace(" ", "_"), '_'.join((peak_file.name.split(".")[:-1])))+'.bed'
        fname = peak_path.split("/")[-1]
        sfile = open(peak_path, 'w')
        sfile.write(peak_file.read())
        sfile.close()
        sel_specie = str(request.POST['specie'])
        num_neighbor = int(request.POST['par'])
        tpeak = request.POST['tpeak']
        ftype = request.POST['factor']
        indexFile = sel_specie+ftype+'_gindex_'+tpeak
    except:
        return render(request, 'warning.html',{'status': False, 'wns': 'Please check all your parameters!!!'})
    # if peak_file.name.split(".")[-1] != "bed" and  peak_file.name.split(".")[-1] != "txt":
    #     return render(request, 'warning.html', {'status': False, 'wns': 'We only support bed file!!!'})
    try:
        os.system("/data5/home/rongbin/software/htslib-1.7/bgzip -c %s > %s.gz"%(peak_path, peak_path))
        os.system("/data5/home/rongbin/software/giggle/bin/giggle search -i /data6/home/rongbin2/changxin/gindex/%s -q %s.gz -s > %s.result.xls"%(indexFile, peak_path, peak_path))
        result_df = pd.read_csv("%s.result.xls"%peak_path, sep="\t", index_col=False)
        result_df.index = [i.replace("_5foldPeak.bed.gz", "").split("/")[-1] for i in result_df["#file"]]
    except:
        return render(request, 'warning.html', {'status': False, 'wns': 'Please check your upload peak file!!!'})

    result_df = result_df.sort_values(by="combo_score", ascending=False).head(200)
    ### here I should add outer annotation file ?
    npage = math.ceil(result_df.shape[0]/20.0)
    items = []
    for i in result_df.index:
        s = Samples.objects.filter(id=str(i))
        treat = s.values("species__name",
                     "factor__name",
                     "cell_line__name",
                     "cell_type__name",
                     "tissue_type__name",
                     "disease_state__name",
                     "unique_id")[0]
        biosource = "%s;%s;%s"%(treat['cell_line__name'], treat['cell_type__name'], treat['tissue_type__name'])
        biosource = biosource.replace("None;", "").replace("None", "").strip(";")
        tmp = {"sid":str(i),"specie":treat['species__name'], "uid": treat['unique_id'],
                "factor": treat['factor__name'],
                "biosource": biosource, "distance": result_df.loc[i, 'combo_score']}
        items.append(tmp)
    try:
        items_df = pd.DataFrame(items)
        items_df.to_csv('./wancx/project_01/dc3/static/html/similar/%s.csv'%fname)
    except:
        return render(request, 'warning.html', {'status': False, 'wns': 'No result was found!'})
    plot_file = open('./wancx/project_01/dc3/static/html/similar/similar.html').read().replace('SimilarResult', fname)
    out = open('./wancx/project_01/dc3/static/html/similar/%s.html'%fname, 'w')
    out.write(plot_file)
    out.close()
    ### here to add code to remove files do not need
    form = {"link": '/static/html/similar/%s'%peak_path.split("/")[-1]+'.html', "table": '/static/html/similar/%s'%peak_path.split("/")[-1]+'.csv'}
    return render(request, 'similar.html', {'datas':json.dumps({'items': items, 'npage': npage, 'fname': fname,'giggle': True, 'rpsearch': False, 'interval': False}), 'status': True, 'form': form})        

def _rpsearch_function(request):
    """This is for rp searching
    """
    keyword=request.GET.get('keyword', None).upper()
    species=request.GET.get('specie', None)
    distance=request.GET.get('distance')
    idistance = int(distance.strip('kb'))*1000
    ftype = request.GET.get('factor')
   # t = re.compile('^N[A-Z].*[0-9]$')
   # if keyword and not t.match(keyword) and species == 'mm10':
   #     keyword = keyword[0] + keyword[1:].lower()
    transcript = cistromeSearch.get_transcript(keyword, species)
    if not transcript:
        wns = "We cannot understand your keyword: "+request.GET.get('keyword', None)+'. Please provide offical gene name or refseq ID.'
        return render(request,'warning.html', {'status': False, 'wns': wns})
    if len(transcript) > 1:
        result = []
        for i in transcript:
            tmp = {'nm':i.split(":")[-2], 'item':i}
            result.append(tmp)
        return render(request,'warning.html', {'transcript':result, 'species':species, 'distance':distance, 'status': True})
    if len(transcript) == 1:
        transcript = transcript[0]
        position = "%s:%s-%s"%(transcript.split(':')[0], int(transcript.split(':')[1])-idistance, int(transcript.split(':')[2])+idistance)
    saveName = transcript.replace(':', '_')+species+distance+'_'+ftype
    chromosome, start, end = transcript.split(':')[:3]
    htmlFile, csvFile = './wancx/project_01/dc3/static/html/rpsearch/'+saveName+'.html', './wancx/project_01/dc3/static/html/rpsearch/'+saveName+'.csv'
    if os.path.exists(htmlFile) and os.path.exists(csvFile):
        search_result = pd.read_csv(csvFile)
        search_result.index = [str(x) for x in search_result['/IDs']]
        form = {"link":'/static/html/rpsearch/%s'%saveName+'.html',"table":'/static/html/rpsearch/%s'%saveName+'.csv'}
    #     return render(request, 'rpsearch/home.html', {'form': form})
    else:
        search_result = cistromeSearch.search(transcript, species, distance, ftype)
        if search_result is None:
            wns = "Sorry! regulatory potential scores on your queried transcript are 0 across all the sample."
            return render(request,'warning.html', {'status': False, 'wns': wns})
        search_result.to_csv('./wancx/project_01/dc3/static/html/rpsearch/'+saveName+'.csv')
        plot_file = open('./wancx/project_01/dc3/static/html/rpsearch/d3scatter.html').read().replace('d3Scatter', saveName).replace('transcript', transcript+' (%s distance to TSS)'%distance).replace('Species', species).replace('chromosome', chromosome).replace('startSite', start).replace('endSite', end)
        out = open('./wancx/project_01/dc3/static/html/rpsearch/'+saveName+'.html', 'w')
        out.write(plot_file)
        out.close()
        form = {"link": '/static/html/rpsearch/%s'%saveName+'.html', "table": '/static/html/rpsearch/%s'%saveName+'.csv'}
    result_df = search_result.sort_values(by="RP", ascending=False).head(200)
    npage = math.ceil(result_df.shape[0]/20.0)
    items = []
    for i in result_df.index:
        s = Samples.objects.filter(id=str(i))
        treat = s.values("species__name",
                     "factor__name",
                     "cell_line__name",
                     "cell_type__name",
                     "tissue_type__name",
                     "disease_state__name",
                     "unique_id")[0]
        biosource = "%s;%s;%s"%(treat['cell_line__name'], treat['cell_type__name'], treat['tissue_type__name'])
        biosource = biosource.replace("None;", "").replace("None", "").strip(";")
        tmp = {"sid":str(i),"specie":treat['species__name'], "uid": treat['unique_id'],
                "factor": treat['factor__name'],
                "biosource": biosource, "distance": float(result_df.loc[i, 'RP'])}
        items.append(tmp)
    return render(request, 'rpsearch.html', {'datas':json.dumps({'items': items, 'npage': npage, 'rpsearch': True, 'giggle': False, 'interval': False, 'position': position}), 'status': True, 'form': form})


def _interval_function(request):
    """This is for rp searching
    """
    interval = request.GET.get("interval", None).lower().replace("y", "Y").replace("x", "X").replace(",", "").strip()
    specie = request.GET.get("specie", None)
    ftype = request.GET.get('factor', None)
    indexFile = specie+ftype+'_gindex_all'
    if re.match(r'^chr[0-9XY]{1,2}\:[0-9]+\:[0-9]+$', interval, re.I): #chr1:num-num
        interval = "%s:%s-%s"%(interval.split(":")[0], interval.split(":")[1], interval.split(":")[2])
    ntmp = "./wancx/project_01/dc3/sim_file/tmp/%s_%s.result.xls"%(specie, interval)
    saveFile = interval.replace(':', '_').replace('-', '_')+'_'+specie+'_'+ftype
    if os.path.exists(ntmp): # if file exist, not need to run again
        pass
    else:
        os.system("/data5/home/rongbin/software/giggle/bin/giggle search -i /data6/home/rongbin2/changxin/gindex/%s -r %s > %s"%(indexFile, interval, ntmp))
    try:
        result_df = pd.read_csv(ntmp , sep="\t", index_col=False, header=None)
        result_df[2] = [int(i.replace("overlaps:", "")) for i in result_df[2]]
        result_df = result_df[result_df[2]>0]
        result_df.index = [i.replace("_5foldPeak.bed.gz", "").split("/")[-1] for i in result_df[0]]
        result_df[1] = [int(i.replace("size:", "")) for i in result_df[1]]
        result_df[3] = result_df[2]/result_df[1]
        result_df = result_df.sort_values(by=3, ascending=False).head(200)
        npage = math.ceil(result_df.shape[0]/20.0)
        items = []
        for i in result_df.index:
            s = Samples.objects.filter(id=str(i))
            treat = s.values("species__name",
                 "factor__name",
                 "cell_line__name",
                 "cell_type__name",
                 "tissue_type__name",
                 "disease_state__name",
                 "unique_id")[0]
            biosource = "%s;%s;%s"%(treat['cell_line__name'], treat['cell_type__name'], treat['tissue_type__name'])
            biosource = biosource.replace("None;", '').replace("None", "").strip(";")
            tmp = {"sid":str(i),"specie":treat['species__name'], "uid": treat['unique_id'],
            "factor": treat['factor__name'], "biosource": biosource,
            "apeak": result_df.loc[i,1], "opeak": result_df.loc[i,2], "distance": result_df.loc[i,3]}
            items.append(tmp)
        try:
            items_df = pd.DataFrame(items)
            items_df = items_df.sort_values(by = 'opeak', ascending=False)
            items_df.to_csv('./wancx/project_01/dc3/static/html/interval/%s.csv'%saveFile)
        except:
            return render(request, 'warning.html', {'status': False, 'wns': 'No result was found!'})
        plot_file = open('./wancx/project_01/dc3/static/html/interval/interval.html').read().replace('interval.csv', saveFile+'.csv').replace('Species', specie).replace('chromosome:startSite-endSite', interval)
        out = open('./wancx/project_01/dc3/static/html/interval/%s.html'%saveFile, 'w')
        out.write(plot_file)
        out.close()
        form = {"link": '/static/html/interval/%s'%saveFile+'.html', "table": '/static/html/interval/%s'%saveFile+'.csv'}
        return render(request, 'interval.html', {'datas':json.dumps({'items': items, 'npage': npage, 'interval': True, 'giggle': False, 'rpsearch': False, 'position': interval}), 'status': True, 'form': form})
    except:
        return render(request, 'warning.html', {'status': False, 'wns': 'Please make sure your interval is valid or is from %s.'%specie})

def _convert_file(peak_path, sep = '\t'):
    """convert data to the format we want
    """
    try:
        tmp = pd.read_csv(peak_path, header = None, sep = sep)
        if tmp.shape[0] >= 3:
            return tmp
        else:
            _convert_file(peak_path, ' ')
    except:
        return False

def toolkit(request):
    prefix = str(time.time()).split(".")[0]
    # return render(request, "datacollection/dbtools.html")
    if request.method == "POST":
        ### This is for similar data searching
        # _similar_function(request)
        try:
            peak_file = request.FILES['peak']
            peak_path = './wancx/project_01/dc3/sim_file/tmp/%s___%s'%(prefix, os.path.basename(peak_file.name))
            if peak_file.name.split(".")[-1] != "bed":
                peak_path_new = './wancx/project_01/dc3/sim_file/tmp/'+'_'.join((peak_file.name.split(".")[:-1]))+'.bed'
                os.system('mv %s %s'%(peak_path, peak_path_new))
                peak_path = './wancx/project_01/dc3/sim_file/tmp/%s___%s'%(prefix, '_'.join((peak_file.name.split(".")[:-1])))+'.bed'
            fname = peak_path.split("/")[-1]
            pd_peak = pd.read_csv(peak_file, sep="\t", header=None, comment="#")
            # Check file format here
            if pd_peak.shape[1] < 3:
                return render(request, 'warning.html',{'status': False, 'wns': 'Your file should be delimited by tad and has at least 3 columns!!!'})
            elif not re.match(r'^chr[XY0-9]+$', pd_peak.iloc[1, 0], re.I):
                return render(request, 'warning.html',{'status': False, 'wns': 'Please check first column of your file!!!'})
            else:
                pd_peak.to_csv(peak_path, index=False, header=False, sep=str(u"\t").encode('utf-8'))
            # return render(request, 'warning.html', {'status': False, 'wns': 'TEST'})
            # elif pd_peak[1].dtype != 'int' or pd_peak[2].dtype != 'int':
            #     return render(request, 'warning.html',{'status': False, 'wns': 'Please check second and third column of your file!!!'})
            # else:
            #     # return render(request, 'warning.html', {'status': False, 'wns': 'TEST'})
            #     pd_peak.to_csv(peak_path, sep="\t", header=False, index=False)
            #     return render(request, 'warning.html', {'status': False, 'wns': 'TEST'})

            # sfile = open(peak_path, 'w')
            # sfile.write(peak_file.read())
            # sfile.close()
            #sfile = _convert_file(peak_path, '\t')
            #if not sfile:
            #    return 
            sel_specie = str(request.POST['specie'])
            # num_neighbor = int(request.POST['par'])
            tpeak = request.POST['tpeak']
            ftype = request.POST['factor']
            indexFile = sel_specie+ftype+'_gindex_'+tpeak
        except:
            return render(request, 'warning.html',{'status': False, 'wns': 'Please check all your parameters!!!'})
        # if peak_file.name.split(".")[-1] != "bed" and  peak_file.name.split(".")[-1] != "txt":
        #     return render(request, 'warning.html', {'status': False, 'wns': 'We only support bed file!!!'})
        try:
            os.system("/data5/home/rongbin/software/htslib-1.7/bgzip -c %s > %s.gz"%(peak_path, peak_path))
            os.system("/data5/home/rongbin/software/giggle/bin/giggle search -i /data6/home/rongbin2/changxin/gindex/%s -q %s.gz -s > %s.result.xls"%(indexFile, peak_path, peak_path))
            result_df = pd.read_csv("%s.result.xls"%peak_path, sep="\t", index_col=False)
            result_df.index = [i.replace("_5foldPeak.bed.gz", "").split("/")[-1] for i in result_df["#file"]]
        except:
            return render(request, 'warning.html', {'status': False, 'wns': 'Please check your upload peak file!!!'})

        result_df = result_df.sort_values(by="combo_score", ascending=False).head(200)
        npage = math.ceil(result_df.shape[0]/20.0)
        items = []
        for i in result_df.index:
            s = Samples.objects.filter(id=str(i))
            treat = s.values("species__name",
                         "factor__name",
                         "cell_line__name",
                         "cell_type__name",
                         "tissue_type__name",
                         "disease_state__name",
                         "unique_id")[0]
            biosource = "%s;%s;%s"%(treat['cell_line__name'], treat['cell_type__name'], treat['tissue_type__name'])
            biosource = biosource.replace("None;", "").replace("None", "").strip(";")
            tmp = {"sid":str(i),"specie":treat['species__name'], "uid": treat['unique_id'],
                    "factor": treat['factor__name'],
                    "biosource": biosource, "distance": result_df.loc[i, 'combo_score']}
            items.append(tmp)
        try:
            items_df = pd.DataFrame(items)
            items_df.to_csv('./wancx/project_01/dc3/static/html/similar/%s.csv'%fname)
            # plot the result
            os.system('/data5/home/rongbin/software/R-3.1.3/bin/Rscript /data5/home/rongbin/wancx/project_01/dc3/datacollection/plot.R /data5/home/rongbin/wancx/project_01/dc3/static/html/similar/%s.csv similar /data5/home/rongbin/wancx/project_01/dc3/static/html/similar/%s'%(fname, fname))
            items_df = items_df.loc[:,['uid', 'factor', 'biosource', 'distance']]
            items_df.to_csv('./wancx/project_01/dc3/sim_file/download/%s_result.csv'%fname, index=False, header=['GSM_ID', 'Factor', 'Biosource', 'GIGGLE_score'])
        except:
            return render(request, 'warning.html', {'status': False, 'wns': 'No result was found!'})
        plot_file = open('./wancx/project_01/dc3/static/html/similar/similar.html').read().replace('SimilarResult', fname).replace('staticPlot', fname)
        out = open('./wancx/project_01/dc3/static/html/similar/%s.html'%fname, 'w')
        out.write(plot_file)
        out.close()
        form = {"link": '/static/html/similar/%s'%fname+'.html', "table": '/static/html/similar/%s'%fname+'.csv'}
        ### Here may add code to remove files not need!!!
        return render(request, 'similar.html', {'datas':json.dumps({'items': items, 'npage': npage, 'fname': fname,'giggle': True, 'rpsearch': False, 'interval': False}), 'status': True, 'form': form})        

    elif request.method == "GET" and request.GET.get('keyword', None):
        ### This is for rp searching
        # _rpsearch_function(request)
        keyword=request.GET.get('keyword', None)
        species=request.GET.get('specie', None)
        distance=request.GET.get('distance')
        idistance = int(distance.strip('kb'))*1000
        ftype = request.GET.get('factor')
       # t = re.compile('^N[A-Z].*[0-9]$')
       # if keyword and not t.match(keyword) and species == 'mm10':
       #     keyword = keyword[0] + keyword[1:].lower()
        transcript = cistromeSearch.get_transcript(keyword, species)
        if not transcript:
            wns = "We cannot understand your keyword: "+request.GET.get('keyword', None)+'. Please provide offical gene name or refseq ID.'
            return render(request,'warning.html', {'status': False, 'wns': wns})
        if len(transcript) > 1:
            result = []
            for i in transcript:
                # tmp = {'nm':i.split(":")[-2], 'item':i}
                tmp = {'nm':i, 'item':i}
                result.append(tmp)
            return render(request,'warning.html', {'transcript':result, 'species':species, 'distance':distance, 'factor':ftype, 'status': True})
        if len(transcript) == 1:
            transcript = transcript[0]
            position = "%s:%s-%s"%(transcript.split(':')[0], int(transcript.split(':')[1])-idistance, int(transcript.split(':')[2])+idistance)
        saveName = transcript.replace(':', '_')+species+distance+'_'+ftype
        chromosome, start, end = transcript.split(':')[:3]
        htmlFile, csvFile = './wancx/project_01/dc3/static/html/rpsearch/'+saveName+'.html', './wancx/project_01/dc3/static/html/rpsearch/'+saveName+'.csv'
        if os.path.exists(htmlFile) and os.path.exists(csvFile):
            search_result = pd.read_csv(csvFile)
            search_result.index = [str(x) for x in search_result['/IDs']]
            form = {"link":'/static/html/rpsearch/%s'%saveName+'.html',"table":'/static/html/rpsearch/%s'%saveName+'.csv'}
        #     return render(request, 'rpsearch/home.html', {'form': form})
        else:
            search_result = cistromeSearch.search(transcript, species, distance, ftype)
            if search_result is None:
                wns = "Sorry! regulatory potential scores on your queried transcript are 0 across all the sample."
                return render(request,'warning.html', {'status': False, 'wns': wns})
            search_result.to_csv('./wancx/project_01/dc3/static/html/rpsearch/'+saveName+'.csv')
            os.system('/data5/home/rongbin/software/R-3.1.3/bin/Rscript /data5/home/rongbin/wancx/project_01/dc3/datacollection/plot.R /data5/home/rongbin/wancx/project_01/dc3/static/html/rpsearch/%s.csv rp /data5/home/rongbin/wancx/project_01/dc3/static/html/rpsearch/%s'%(saveName, saveName))
            plot_file = open('./wancx/project_01/dc3/static/html/rpsearch/d3scatter.html').read().replace('d3Scatter', saveName).replace('staticPlot', saveName).replace('transcript', transcript+' (%s distance to TSS)'%distance).replace('Species', species).replace('chromosome', chromosome).replace('startSite', start).replace('endSite', end)
            out = open('./wancx/project_01/dc3/static/html/rpsearch/'+saveName+'.html', 'w')
            out.write(plot_file)
            out.close()
            form = {"link": '/static/html/rpsearch/%s'%saveName+'.html', "table": '/static/html/rpsearch/%s'%saveName+'.csv'}
        result_df = search_result.sort_values(by="RP", ascending=False).head(200)
        npage = math.ceil(result_df.shape[0]/20.0)
        items = []
        for i in result_df.index:
            s = Samples.objects.filter(id=str(i))
            treat = s.values("species__name",
                         "factor__name",
                         "cell_line__name",
                         "cell_type__name",
                         "tissue_type__name",
                         "disease_state__name",
                         "unique_id")[0]
            biosource = "%s;%s;%s"%(treat['cell_line__name'], treat['cell_type__name'], treat['tissue_type__name'])
            biosource = biosource.replace("None;", "").replace("None", "").strip(";")
            tmp = {"sid":str(i),"specie":treat['species__name'], "uid": treat['unique_id'],
                    "factor": treat['factor__name'],
                    "biosource": biosource, "distance": float(result_df.loc[i, 'RP'])}
            items.append(tmp)
        download_df = pd.DataFrame(items)
        download_df = download_df.loc[:,['uid', 'factor', 'biosource', 'distance']]
        download_df.to_csv('./wancx/project_01/dc3/sim_file/download/'+saveName+'_result.csv', index=False, header=['GSM_ID', 'Factor', 'Biosource', 'RP_score'])
        return render(request, 'rpsearch.html', {'datas':json.dumps({'items': items, 'npage': npage, 'rpsearch': True, 'giggle': False, 'interval': False, 'position': position, 'fname': saveName}), 'status': True, 'form': form})
    elif request.method == "GET" and request.GET.get('interval', None):
        ### This is for interval giggle searching
        # _interval_function(request)
        interval = request.GET.get("interval", None).lower().replace("y", "Y").replace("x", "X").replace(",", "").strip()
        specie = request.GET.get("specie", None)
        ftype = request.GET.get('factor', None)
        indexFile = specie+ftype+'_gindex_all'
        if re.match(r'^chr[0-9XY]{1,2}\:[0-9]+\:[0-9]+$', interval, re.I): #chr1:num-num
            interval = "%s:%s-%s"%(interval.split(":")[0], interval.split(":")[1], interval.split(":")[2])
        ntmp = "./wancx/project_01/dc3/sim_file/tmp/%s_%s_%s.result.xls"%(specie, interval, ftype)
        saveFile = interval.replace(':', '_').replace('-', '_')+'_'+specie+'_'+ftype
        if os.path.exists(ntmp): # if file exist, not need to run again
            pass
        else:
            os.system("/data5/home/rongbin/software/giggle/bin/giggle search -i /data6/home/rongbin2/changxin/gindex/%s -r %s > %s"%(indexFile, interval, ntmp))
        try:
            result_df = pd.read_csv(ntmp , sep="\t", index_col=False, header=None)
            result_df[2] = [int(i.replace("overlaps:", "")) for i in result_df[2]]
            result_df = result_df[result_df[2]>0]
            result_df.index = [i.replace("_5foldPeak.bed.gz", "").split("/")[-1] for i in result_df[0]]
            result_df[1] = [int(i.replace("size:", "")) for i in result_df[1]]
            result_df[3] = result_df[2]/result_df[1]
            result_df = result_df.sort_values(by=3, ascending=False).head(200)
            npage = math.ceil(result_df.shape[0]/20.0)
            items = []
            for i in result_df.index:
                s = Samples.objects.filter(id=str(i))
                treat = s.values("species__name",
                     "factor__name",
                     "cell_line__name",
                     "cell_type__name",
                     "tissue_type__name",
                     "disease_state__name",
                     "unique_id")[0]
                biosource = "%s;%s;%s"%(treat['cell_line__name'], treat['cell_type__name'], treat['tissue_type__name'])
                biosource = biosource.replace("None;", '').replace("None", "").strip(";")
                tmp = {"sid":str(i),"specie":treat['species__name'], "uid": treat['unique_id'],
                "factor": treat['factor__name'], "biosource": biosource,
                "apeak": result_df.loc[i,1], "opeak": result_df.loc[i,2], "distance": result_df.loc[i,3]}
		# distance means the overlap ratio
                items.append(tmp)
            try:
                items_df = pd.DataFrame(items)
                items_df = items_df.sort_values(by = 'distance', ascending=False)
                items_df.to_csv('./wancx/project_01/dc3/static/html/interval/%s.csv'%saveFile)
                os.system('/data5/home/rongbin/software/R-3.1.3/bin/Rscript /data5/home/rongbin/wancx/project_01/dc3/datacollection/plot.R /data5/home/rongbin/wancx/project_01/dc3/static/html/interval/%s.csv interval /data5/home/rongbin/wancx/project_01/dc3/static/html/interval/%s'%(saveFile, saveFile))
                items_df = items_df.loc[:,['uid', 'factor', 'biosource', 'apeak', 'opeak', 'distance']]
                items_df.to_csv('./wancx/project_01/dc3/sim_file/download/%s_result.csv'%saveFile, index=False, header=['GSM_ID', 'Factor', 'Biosource', 'All_peak_number', 'Overlap_peak_number', 'Overlap_ratio'])
            except:
                return render(request, 'warning.html', {'status': False, 'wns': 'No result was found!'})
            plot_file = open('./wancx/project_01/dc3/static/html/interval/interval.html').read().replace('interval.csv', saveFile+'.csv').replace('staticPlot', saveFile).replace('Species', specie).replace('chromosome:startSite-endSite', interval)
            out = open('./wancx/project_01/dc3/static/html/interval/%s.html'%saveFile, 'w')
            out.write(plot_file)
            out.close()
            form = {"link": '/static/html/interval/%s'%saveFile+'.html', "table": '/static/html/interval/%s'%saveFile+'.csv'}
            return render(request, 'interval.html', {'datas':json.dumps({'items': items, 'npage': npage, 'interval': True, 'giggle': False, 'rpsearch': False, 'position': interval, 'fname': saveFile}), 'status': True, 'form': form})
        except:
            return render(request, 'warning.html', {'status': False, 'wns': 'Please make sure your interval is valid or is from %s.'%specie})

    else:
        return render(request, "dbtools.html")



    



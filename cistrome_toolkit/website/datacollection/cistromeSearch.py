import os, sys 
import argparse
import traceback
import time
import pandas as pd
import h5py
import numpy as np
import json
from pandas import DataFrame as DF
import re
from collections import Counter 
import json

def order(sample_inf_table):
	"""input is a data.frame, will ranked by the maxiumn value of same factor
	"""
	hm = sample_inf_table[sample_inf_table['Type'] == 'Histone Marks']
	CA = sample_inf_table[sample_inf_table['Type'] == 'Chromatin Accessibility']
	TF = sample_inf_table[sample_inf_table['Type'] == 'TF/CR/Other']
	# rank factors by the max RP sample
	tf = TF[['Factor', 'RP']].groupby(['Factor']).max().sort_values(by = 'RP').index.tolist()
	hm = hm[['Factor', 'RP']].groupby(['Factor']).max().sort_values(by = 'RP').index.tolist()
	ca = CA[['Factor', 'RP']].groupby(['Factor']).max().sort_values(by = 'RP').index.tolist()
	factor_rank_max_RP = tf+hm+ca
	orders = list(range(1, len(tf)+1)) + list(range(len(tf), len(tf)+len(hm))) + list(range(len(tf)+len(hm), len(tf)+len(hm)+len(ca)))
	# rank_factor_df = DF(data = np.array(range(1, len(factor_rank_max_RP)+1)), index = factor_rank_max_RP)
	rank_factor_df = DF(data = np.array(orders), index = factor_rank_max_RP)
	rank_factor_df = rank_factor_df.loc[sample_inf_table.Factor.tolist(),:]
	return rank_factor_df


def search(query_gene_transcript, species, distance, request_type):
	'''query the transcript in RP matrix, and return the ordered data.frame
	'''
	hg = {'1k': 'human_1kRP.hd5', '10k': 'human_10kRP.hd5', '100k':'human_100kRP.hd5'}
	mm = {'1k': 'mouse_1kRP.hd5', '10k': 'mouse_10kRP.hd5', '100k':'mouse_100kRP.hd5'}
	if species == 'hg38':
		# all_h5File = '/data6/home/rongbin2/GeneSearch/hd5/%s'%hg[distance]
		all_h5File = '/data5/home/rongbin/wancx/project_01/dc3/static/db/%s'%hg[distance]
	else:
		# all_h5File = '/data6/home/rongbin2/GeneSearch/hd5/%s'%mm[distance]
		all_h5File = '/data5/home/rongbin/wancx/project_01/dc3/static/db/%s'%mm[distance]
	RP_matrix = h5py.File(all_h5File, 'r')
	# data_ann = pd.read_table('/data6/home/rongbin2/GeneSearch/DCall_20180122.xls', header = None)
	data_ann = pd.read_table('/data5/home/rongbin/wancx/project_01/dc3/static/db/DC_narrow1kpeak_broad.xls', header = None)
	data_ann.index = np.array(data_ann[0], dtype = 'S22')
	gene_annotation = np.array(list(map(lambda x: x.decode('utf-8'),
	                                                RP_matrix['RefSeq'][...])))
	map_gene = {}
	for i, c in enumerate(gene_annotation):
	    map_gene[c] = i
	values = RP_matrix['RPnormalize'][map_gene[query_gene_transcript]]	
	values[np.isnan(values)] = 0 # if RP of all gene is 0, so the nomalized value will be nan, so need to transfer back to 0
	if np.all(values == 0):
		return None
	tmp_RP = DF(values, index = RP_matrix['IDs'])
	tmp_RP = tmp_RP.sort_values(by = 0, ascending = False)
	tmp_RP = tmp_RP[tmp_RP[0] != 0]
	sample_inf = data_ann.loc[tmp_RP.index.tolist(), [11,12,5,7,8,9]]
	sample_inf_new = pd.concat([sample_inf, tmp_RP], axis=1)
	sample_inf_new.columns = np.array(['Factor', 'Type','GSMid', 'Cell_Line', 'Cell_Type', 'Tissue', 'RP'])
	# avoid some back factors
	sample_inf_new = sample_inf_new[[(x not in ['598-SKD', '552-SKD', '5HMC', '5MC', '7SK']) for x in sample_inf_new['Factor'].tolist()]]
	# extract requested type
	if request_type == 'hmca':
		request_type = ['Histone Marks', 'Chromatin Accessibility']
	else:
		request_type = ['TF/CR/Other']
	sample_inf_new = sample_inf_new[sample_inf_new['Type'].isin(request_type)]
	rank_factor = order(sample_inf_new)
	sample_inf_new['Factor_label'] = rank_factor[0].tolist()
	return sample_inf_new


def _normalize_query_res(all_transcript, index, query_keyWords):
	'''normalize query result by remove the abnormal chr
	'''
	allChr = ['chr' + str(x) for x in range(1,23)] + ['chrX', 'chrY']
	all_transcript.index = index
	query_res = all_transcript.loc[query_keyWords,:]
	if type(query_res) == DF and query_res.shape[0] > 1:
		query_res = query_res[[(x in allChr) for x in query_res[0]]]
		t = [] # save the transcript in list with format chr:start:end:refseq:gene
		for n in range(query_res.shape[0]):
			t.append(':'.join([str(x) for x in query_res.iloc[n,:].tolist()]))
		return t
	return [':'.join([str(x) for x in query_res.tolist()])]

def get_transcript(query_keyWords, species):
	"""input the key word and species, return the tranacript
	"""
	if species == 'hg38':
		# all_transcript = pd.read_table('/data6/home/rongbin2/GeneSearch/hg38_promoters_beta.bed', header = None)
		all_transcript = pd.read_table('/data5/home/rongbin/wancx/project_01/dc3/static/db/hg38_promoters_beta.bed', header = None)
	else:
		# all_transcript = pd.read_table('/data6/home/rongbin2/GeneSearch/mm10_promoters_beta.bed', header = None)
		all_transcript = pd.read_table('/data5/home/rongbin/wancx/project_01/dc3/static/db/mm10_promoters_beta.bed', header = None)
	if re.match('^chr[0-9|X|Y]*:[0-9]*:[0-9]*:[A-Z]*_[0-9]*:[A-Z|a-z|0-9]*', query_keyWords): # if alrealy fit the format, means the actual transcript
		return [query_keyWords]
	query_keyWords = query_keyWords.upper()
	if query_keyWords in all_transcript[3].tolist(): # query with unique transcript id
		return _normalize_query_res(all_transcript, all_transcript[3], query_keyWords)
	elif query_keyWords in [x.upper() for x in all_transcript[4].tolist()]:# query with gene name, so maybe multiple transcript
		return _normalize_query_res(all_transcript, [x.upper() for x in all_transcript[4].tolist()], query_keyWords)
	else:
		return None


import os,sys
import h5py
import pandas as pd
import numpy as np
import commands

if len(sys.argv) == 1 or sys.argv[1] in ['-h', '--help']:
	print('+++help:\n a table includes at least one column of BETA result\nthree parameters are needed:\npython hs_beta_sum.py path_of_table path_of_save beta_col')
	sys.exit(1)

data_table = sys.argv[1]#'/data5/home/rongbin/cistromeDB/newtable/DCall_20180122.xls.path'
saveFile = sys.argv[2]
beta_col = sys.arge[3]
data_inf = pd.read_table(data_table, header = None)

def _get_sample_rp(beta_rp_path):
	"""get the RP based on path
	"""
	sample_values, refseq_index = [], []
	with open(beta_rp_path) as f:
		for line in f:
			if line.startswith('#'):
				continue
			line = line.rstrip().split('\t')
			refseq_index.append(':'.join(line[:4])+":"+line[-1])
			sample_values.append(float(line[4]))
	sample_rp = pd.DataFrame(sample_values,index=refseq_index)
	return sample_rp

all_beta_files = [x for x in data_inf[int(beta_col)].tolist() if str(x) != 'nan']
all_beta_files_good = []
for a in all_beta_files:
	if int(commands.getoutput('wc -l %s'%a).split(' ')[0]) == 54648:
		all_beta_files_good.append(a)
	else:
		print(a)
with h5py.File(saveFile, 'a') as store:
	ID = os.path.basename(all_beta_files_good[0]).replace('_gene_score_5fold.txt', '')
	beta_rp_path = all_beta_files_good[0]
	sample_rp = _get_sample_rp(beta_rp_path)
	orders = sample_rp.index.tolist()
	refseq_arr = store.create_dataset("RefSeq", shape=(len(orders), ), dtype='S200', compression='gzip', shuffle=True, fletcher32=True)
	refseq_arr[...] = np.array(orders, dtype='S200')
	RP = store.create_dataset("RP", dtype=np.float32, shape=(len(orders), len(all_beta_files_good)), compression='gzip', shuffle=True, fletcher32=True)
	ids = store.create_dataset("IDs", shape=(len(all_beta_files_good), ), dtype='S50', compression='gzip', shuffle=True, fletcher32=True)
	iids = []
	for x, p in enumerate(all_beta_files_good):
		ID = os.path.basename(p).replace('_gene_score_5fold.txt', '')
		beta_rp_path = p
		sample_rp = _get_sample_rp(beta_rp_path)
		try:
			sample_rp = sample_rp.loc[orders, : ]
		except:
			print(p)
			continue
		RP[:,x] = np.array(sample_rp[0].tolist(), dtype='float32').T
		store.flush()
		#iids.append(str.encode(i, 'utf-8'))
		iids.append(ID)
	ids[...] = np.array(iids)
	store.flush()



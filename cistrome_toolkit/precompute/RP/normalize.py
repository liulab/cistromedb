import h5py
import numpy as np
import os,sys
#all_h5File = '/Users/zhengrongbin/Documents/Workplace/Projects/DB_update/GeneSearch/Files/DC_mouse_180122.xls.good.hd5'
if len(sys.argv) == 1 or sys.argv[1] in ['-h', '--help']:
        print('+++help:\n a path of hd5 file that generated from hs_beta_sum.py or mm_beta_sum.py\npython normalize.py mm_rp.hd5')
        sys.exit(1)

all_h5File = sys.argv[1]
RP_matrix = h5py.File(all_h5File, 'r+')

RP_new = RP_matrix.create_dataset("RPnormalize", dtype=np.float32, shape=(RP_matrix['RP'].shape[0], RP_matrix['RP'].shape[1]), compression='gzip', shuffle=True, fletcher32=True)
for i in range(RP_matrix['RP'].shape[1]):
    print(i)
    max_rp = max(RP_matrix['RP'][:,i])
    min_rp = min(RP_matrix['RP'][:,i])
    val = (RP_matrix['RP'][:,i] - min_rp) / (max_rp - min_rp)
    RP_new[:,i] = val
RP_matrix.flush()

#!/usr/bin/python
# -*- coding: UTF-8 -*-
import os, sys 
import traceback
import time
import argparse
import pandas as pd
import h5py
import numpy as np
from pandas import DataFrame as DF

#form = cgi.FieldStorage() 

def _search(query_gene_transcript, species):
	# all_transcript = RP_matrix['RefSeq'][:,].tolist()
	# query_gene_transcript = [x for x in all_transcript if x.endswith(query_gene)]
	if species == 'hg38':
		all_h5File = '/data6/home/rongbin2/DC_human_180122.xls.good.hd5'
	else:
		all_h5File = '/data6/home/rongbin2/DC_mouse_180122.xls.good.hd5'
	RP_matrix = h5py.File(all_h5File, 'r')
	data_ann = pd.read_table('/data5/home/rongbin/cistromeDB/newtable/DCall_20180122.xls', header = None)
	data_ann.index = np.array(data_ann[0], dtype = 'S22')
	all_transcript = RP_matrix['RefSeq'][:,].tolist()
	#for t in query_gene_transcript:
	tmp_RP = DF(RP_matrix['RP'][all_transcript.index(query_gene_transcript)], index = RP_matrix['IDs'])
	tmp_RP = tmp_RP.sort_values(by = 0, ascending = False)
	#tmp_RP = tmp_RP.iloc[0:100,:]
	# filter 0
	tmp_RP = tmp_RP[tmp_RP[0] > 1]
	sample_inf = data_ann.loc[tmp_RP.index.tolist(), [11,12,5,7,8,9]]
	sample_inf_new = pd.concat([sample_inf, tmp_RP], axis=1)
	#sample_inf_new.cloumns = np.array(['Factor', 'GSMid', 'Cell Line', 'Cell Type', 'Tissue', 'RP'])
	#sample_inf_new.to_csv('./search_result.txt')
	sample_inf_new.to_csv('/data5/home/rongbin/public_html/cgi-bin/result/'+query_gene_transcript.split(':')[3]+'.txt', header = None, sep = '\t')
	return 'http://cistrome.org/~rongbin/cgi-bin/result/'+query_gene_transcript.split(':')[3]+'.txt'

def _get_transcript(query_keyWords, species):
	if species == 'hg38':
		all_transcript = pd.read_table('/data6/home/rongbin2/hg38_promoters_beta.bed', header = None)
	else:
		all_transcript = pd.read_table('/data6/home/rongbin2/mm10_promoters_beta.bed', header = None)
	if query_keyWords in all_transcript[3].tolist():
		return ':'.join([str(x) for x in all_transcript[all_transcript[3] == query_keyWords].iloc[0,:].tolist()])
	elif query_keyWords in all_transcript[4].tolist():
		t = ':'.join([str(x) for x in all_transcript[all_transcript[4] == query_keyWords].iloc[0,:].tolist()])
		print('You are search using gene, we will give you: %s'%t)
		return t
	else:
		return None
def _run(config):
	transcript = _get_transcript(config['keyword'], config['species'])
	result_link = _search(transcript, config['species'])
	print('please check your result here: '+result_link)

def main():
	try:
		parser = argparse.ArgumentParser(description="""search gene and rank sample by RP""")
		parser.add_argument( '-w', dest='keyword', type=str, required=True, help='the keyword use to search, should be gene name or transcript ID.')
		parser.add_argument('-s',dest='species',type=str,required=True,help='select one from [hg38, mm10]')
		args = parser.parse_args()

		_run({'keyword':args.keyword, 'species':args.species})

	except KeyboardInterrupt:
		sys.stderr.write("User interrupted me!\n")
		sys.exit(0)

if __name__ == '__main__':
	main()
# species = form.getvalue('checkbox1')
# keyword = form.getvalue('input1')
# transcript = _get_transcript(keyword)

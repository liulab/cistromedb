library(Gviz)
library(GenomicRanges)

hg38=read.csv('/Users/zhengrongbin/Documents/Workplace/Projects/DB_update/toolkit/draw_transcript/hg38.refGene', header = F, sep = '\t')
# hg38 = read.csv('/data5/home/rongbin/software/genome/hg38.refGene', header = F, sep = '\t')

format = function(gene, mat){
  mat = as.matrix(mat)
  df = data.frame()
  for (i in 1:nrow(mat)){
    transcripts = as.character(mat[i,2])
    chromosome = as.character(mat[i,3])
    strand = as.character(mat[,4])
    exons = t(do.call(rbind, strsplit(mat[i,10:11], '\\,')))
    transcripts = rep(transcripts, nrow(exons))
    chromosome = rep(chromosome, nrow(exons))
    strand = rep(strand, nrow(exons))
    df = rbind(df, data.frame('chr' = chromosome, 'start' = as.numeric(exons[,1]),
                    'end' = as.numeric(exons[,2]), 'strand' = strand, 'transcript' = transcripts))
  }
  chr = chromosome[1]
  gen = c(chr='hg38') 
  # itrack <- IdeogramTrack(genome = gen, chromosome = chr)
  gtrack <- GenomeAxisTrack(name="Axis",
                            range <- IRanges(start=df$start, end=df$end))
  
  grtrack <- GeneRegionTrack(df, genome = gen,
                             chromosome = chr, name = gene, 
                             strand = df$strand,
                             transcriptAnnotation = "transcript",
                             background.panel = "#FFFEDB",
                             background.title = 'darkblue')
  plotTracks(list(itrackList[[chr]], gtrack, grtrack), col = 'red',
             margin = 10, innerMargin = 10, fontcolor = 'black', fontface = 'bold',
             stackHeight = 0.1, extend.left = 50)
}

chrs = paste0('chr', c(1:22, 'X', 'Y'))
itrackList=list()
for (c in chrs){
  itrackList[[c]] <- IdeogramTrack(genome = 'hg38', chromosome = c)
}
hg38 = subset(hg38, V3 %in% chrs)
multiple_chr = NULL
for (gene in unique(as.vector(hg38$V13))){
  print(gene)
  mat = subset(hg38, V13 == gene)
  if (length(unique(mat$V3)) != 1){
    multiple_chr = c(multiple_chr, gene)
  }
  if (nrow(mat) > 1 & length(unique(mat$V3)) == 1){
    if (length(unique(mat$V2)) == 1){
      mat$V2 = paste(unique(mat$V2), seq(1, nrow(mat)), sep = '.')
    }
    png(paste0('./hg38/hg38_', gene, '.png'), res = 300, height = 280*nrow(mat), width = 1800)
    format(gene, mat)
    dev.off()
  }
}
saveRDS(multiple_chr, file = 'multiple_chr_hg38.rds')


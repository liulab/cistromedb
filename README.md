# Welcome to the instructions of Cistrome DB maintaining!
## The entire CistromeDB maitaining includes **four** sections:
* Section1. Data collection from GEO database, under ./geo_parser
* Section2. Data processing for the new collected samples, under ./dc_process
* Section3. CistromeDB web interface, under ./db_interface
* Section4. CistromeDB Toolkit, under ./cistrome_toolkit

## Section1: do geo parser to collect public ChIP-seq, DNase-seq, and ATAC-seq
This is a framework that collects meta information of ChIP-seq, DNase-seq, and ATAC-seq from GEO as default, in addition, this framework can also collect MeDIP-Seq samples from GEO if it is specified.
### Language: python2.7
### Precedures of parser:
* query the SRA database at """http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=gds&term=SRA[Sample Type] AND gsm[Entry Type] AND (homo sapiens[Organism] OR mus musculus[Organism])&mindate=%s&maxdate=%s&datetype=pdat&retmax=100000&usehistory=y"""%(minTime, maxTime), where minTime and maxTime can be set as the format of 2019-01-27.
* get GSM accessions from SRA ids that obtained from last step.
* get new GSM accessions by comparing with the existed samples in MySQL.
* parse detail annotations, i.e. factor, cell, pmid, gseId, etc., and add into MySQL for each of new GSM. In this step, each sample will have a **XML** file which contains all the details and saved under a folder named geo as default.

### Running the framework:
* overall
```shell
cd ./geo_parser
python sync_new_samples_from_geo.py -h
usage: sync_new_samples_from_geo.py [-h] {parser,known,local} ...

DB parser: parse ChIP-seq, DNase- and ATAC-seq samples from GEO

positional arguments:
  {parser,known,local}  sub-command help
    parser              parse new sample
    known               add samples, known gsm id and factor
    local               go through the XML files in the given path, and parse
                        the detail sample information.

optional arguments:
  -h, --help            show this help message and exit
```
* sub-function #1 (the main function):
```shell
python sync_new_samples_from_geo.py parser -h
usage: sync_new_samples_from_geo.py parser [-h] [-d DATE_REGION] [-o FSAVE]
                                           [-fi] [-ty TYPE]

parse new data from GEO with option of filling in CistromeDB MySQL or not.

optional arguments:
  -h, --help      show this help message and exit
  -d DATE_REGION  Parser will get the pubic samples in this given date region,
                  Please use the format: 2016/01/01-2017/01/01. Default is the
                  recent 100000 entries in GEO.
  -o FSAVE        The table you want to save the new sample information, the
                  default is "DC_new_collection.xls" which will be built in
                  the working directory.
  -fi             This option should be given or not. add this option means
                  parse the new samples from GEO and add in the CistromeDB
                  MySQL database at same time, or means just parse new samples
                  save in outside table, default is False.
  -ty TYPE        using this option to set data type you want, please select
                  in [ChIP-seq, DNase, ATAC-seq, MeDIP-seq, and other factors,
                  eg: H3K27ac]
```
example:
```shell
python sync_new_samples_from_geo.py parser -d 2016/01/01-2017/01/01 -o ./DC_newCollection.xls -fi -ty ChIP-seq,ATAC-seq
```
* sub-function #2 (if you know the GSM ids, this can be used for specific dataset collection)
```shell
python sync_new_samples_from_geo.py known -h
usage: sync_new_samples_from_geo.py known [-h] -i INFILE -gc GSM_COL
                                          [-fc FACTOR_COL] [-fi] [-o FSAVE]
                                          [-p PATH_OF_XML] [-rf]

add samples to CistromeDB MySQL database, those samples are with known gsm id
and facotr names.

optional arguments:
  -h, --help      show this help message and exit
  -i INFILE       The file contains at least two column, one is gsm id, one is
                  factor name with offical gene symbol.
  -gc GSM_COL     The column for gsm id in the -i table, start with 0.
  -fc FACTOR_COL  Optional, the column for factor name in the -i table, start
                  with 0.
  -fi             This option should be given or not. add this option means
                  parse the new samples from GEO and add in the CistromeDB
                  MySQL database at same time, or means just parse new samples
                  save in outside table, default is False.
  -o FSAVE        The table you want to save the new sample information, the
                  default is "DC_new_collection.xls" which will be built in
                  the working directory.
  -p PATH_OF_XML  The folder path contain all the xml files, eg: "./geo", the
                  xml storage format should be:
                  "./geo/GSM1000/GSM1000102.xml".
  -rf             whether you want to update if gsmid existed already.
                  Optional.
```
example:
```shell
python sync_new_samples_from_geo.py known -i /data5/home/rongbin/cistromeDB/newtable/2018_update/sync_ENCODE/encode_have_geoAccession_notInCistrome_unique_gse_gsms.xls -gc 0 -fi -o encode_have_geoAccession_notInCistrome_unique_gse_gsms_chipseq.xls
```
* sub-function #3 (if you get a set of XML files, and want to check whether there are ChIP-seq or other data you want, this function will help)
```shell
python sync_new_samples_from_geo.py local -h
usage: sync_new_samples_from_geo.py local [-h] -p PATH_OF_XML [-fi] [-o FSAVE]
                                          [-ty TYPE]

optional arguments:
  -h, --help      show this help message and exit
  -p PATH_OF_XML  The folder path contain all the xml files, eg: "./geo", the
                  xml storage format should be:
                  "./geo/GSM1000/GSM1000102.xml".
  -fi             This option should be given or not. add this option means
                  parse the new samples from GEO and add in the CistromeDB
                  MySQL database at same time, or means just parse new samples
                  save in outside table, default is False.
  -o FSAVE        The table you want to save the new sample information, the
                  default is "DC_new_collection.xls" which will be built in
                  the working directory.
  -ty TYPE        using this option to set data type you want, please select
                  in [ChIP-seq, DNase, ATAC-seq, MeDIP-seq, and other factors,
                  eg: H3K27ac]
```
example:
```shell
python sync_new_samples_from_geo.py local -p /data5/home/rongbin/cistromeDB/syncOdyssey/geo_180208_181203/ -fi -o ./DC_from_local.xls -ty H3K27ac 
```
* automatically do geo parser every day:
```shell
python sync_new_samples_from_geo_alive.py -h
usage: sync_new_samples_from_geo_alive.py [-h] [-start STARTPOINT] [-o FSAVE]
                                          [-fi] [-ty TYPE]

DB parser: parse ChIP-seq, DNase- and ATAC-seq samples from GEO

optional arguments:
  -h, --help         show this help message and exit
  -start STARTPOINT  a date formats like 2018-12-08 which is (Y-M-D), collect
                     data starting from this date point. Default start from
                     the day before the job is submited.
  -o FSAVE           The table you want to save the new sample information,
                     the default is "DC_new_collection.xls" which will be
                     built in the working directory.
  -fi                This option should be given or not. add this option means
                     parse the new samples from GEO and add in the CistromeDB
                     MySQL database at same time, or means just parse new
                     samples save in outside table, default is False.
  -ty TYPE           using this option to set data type you want, please
                     select in [ChIP-seq, DNase, ATAC-seq, MeDIP-seq, and
                     other factors, eg: H3K27ac]
```
example:
```shell
python sync_new_samples_from_geo_alive.py -start 2018-12-08 -o ./DC_keepCollection.xls -fi -ty ChIP-seq,DNase,ATAC-seq
```

## +++++++++++++

## Section 2: data processing for the new collected samples
Currently, we process samples using ChiLin pipeline, and only support running jobs on SLUM cluster type of server [Odyssey and O2]. All the parameter of the framework can be found in **dc.conf** file under ./dc_process.
### Install work evironment and ChiLin
1. build miniconda2 environment, this will help you separate dc processing environment and install softwares
2. install chilin and requested software in [chilin](http://cistrome.org/chilin/Installation.html), and you must need to test chilin demo and make sure that chilin can be run and all the required result can be generated smoothly.
3. download sratools (the newest version are suggested) and copy the tools into miniconda2 environment, or install sratools by conda, you can do like this:
4. make sure that R is available in your environment, and **seqLogo** package is installed and can work smoothly, seqLogo will be used for drawing motif sequence logo 
5. make sure that your png() can write out smoothly in R, if something reports to you about X11 problems, you can try to resolve by:

sratools:
```
ln -s ./sratools/bin/* ~miniconda2/bin/
# or
conda install -c bioconda sra-tools 
```
png() X11 problem in R
```
echo "options(bitmapType='cairo')" >> ~/.Rprofile
```

### how this pipeline works?
* modes, this piepline can either receive sample table from Daisy or local sample table:
	* query meta-data table from Daisy server and process in Odyssey or O2 server, because we do geo parser only on Daisy, we have to query new collected samples real-timely if we process data in Odyssey or O2. **To make this, please set a ssh-keygen between Daisy and Odyssey to make password free login and transferring  from Diasy to Odyssey [[ref](http://www.linuxproblem.org/art_9.html)]**
	* process data from a local table which contains meta annotation.
	* Note: DCid, GSMid, species, and factor names are required in the given table.
* precedures of this pipeline:
	* read-in meta information and judge which samples haven't been processed by comparing with table which records samples that has been received in Odyssey or O2.
	* get raw data FASTQ file: 
		* parse SRR id (this we needs GSMid) and downlaod sra by "prefetch SRR.sra -o fsave.sra" (these will in sbatch file)
		* check sra file completeness by vdb-validate (one of the sratools)
		* get fastq file by fastq-dump (one of the sratools)
	* generate ChiLin command line (a separate python script handles this). Up to current step, all the command lines will be saved in a sbatch file, one of the example in sbatch file at sbatch1:
	* get chilin command and run ChiLin
	* check completeness of ChiLin by output files, and re-do QC part if any json files (records qc values) lost
	* tansfer chilin result back to Daisy. An new sbatch file for these 3 steps will be generated, one of the example at sbatch2:
	* connect to Daisy and check the completeness of sample result transferring, it has to transfer again if not finished, and delete result folder to freeup new space if finished. This is handled by script "connect_daisy.py", example at below. <b>This has to be submit separately.</b>
	* **Corresponding to the checking in Odyssey or O2 side in last step, an another script in Daisy side should keep running.** This script will check the transferring completeness by checking md5 file, and save ids of completed samples to "sampleList.txt" and problematic samples to "again.txt", so that Odyssey side can check based on these two file. The scripts of this step is ./support_script/data_transferAndBackup/connect_umass_odyssey.py. In addition, this script also response for backup to Umass server. More information at ./support_script/data_transferAndBackup. **Currently, this step is running in Daisy under /data6/DC_results/FIles/**

script examples:
#### check transferring (these must be submitted by bash and keep alive)
```shell
# Odyssey side:
python connect_daisy.py dc.conf
# Daisy side (check and backup):
python connect_umass_odyssey.py /data6/DC_results/DC_201812,/data6/DC_results/DC_new
```

#### sbatch1:

```shell
source /home/rz92/miniconda3/bin/activate chilin

module load gcc/6.2.0
module load R/3.3.3
module load texlive/2007


prefetch SRR7101490 -o /n/scratch2/rz92/test_chilin/cistromedc_v2/./tmp_sra_fastq/GSM3128275_1.sra
python /n/scratch2/rz92/test_chilin/cistromedc_v2/modules/checkSRA.py /n/scratch2/rz92/test_chilin/cistromedc_v2/./tmp_sra_fastq/GSM3128275_1.sra
if [ $? == 1 ];then
echo GSM3128275,ftp://ftp-trace.ncbi.nih.gov/sra/sra-instant/reads/ByRun/sra/SRR/SRR710/SRR7101490/SRR7101490.sra,/n/scratch2/rz92/test_chilin/cistromedc_v2/./tmp_sra_fastq/GSM3128275_1.sra >> /n/scratch2/rz92/test_chilin/cistromedc_v2/./record/SRAchecking_fail.xls
exit
fi
fastq-dump /n/scratch2/rz92/test_chilin/cistromedc_v2/./tmp_sra_fastq/GSM3128275_1.sra -O /n/scratch2/rz92/test_chilin/cistromedc_v2/./tmp_sra_fastq
prefetch SRR7101489 -o /n/scratch2/rz92/test_chilin/cistromedc_v2/./tmp_sra_fastq/GSM3128275_2.sra
python /n/scratch2/rz92/test_chilin/cistromedc_v2/modules/checkSRA.py /n/scratch2/rz92/test_chilin/cistromedc_v2/./tmp_sra_fastq/GSM3128275_2.sra
if [ $? == 1 ];then
echo GSM3128275,ftp://ftp-trace.ncbi.nih.gov/sra/sra-instant/reads/ByRun/sra/SRR/SRR710/SRR7101489/SRR7101489.sra,/n/scratch2/rz92/test_chilin/cistromedc_v2/./tmp_sra_fastq/GSM3128275_2.sra >> /n/scratch2/rz92/test_chilin/cistromedc_v2/./record/SRAchecking_fail.xls
exit
fi
fastq-dump /n/scratch2/rz92/test_chilin/cistromedc_v2/./tmp_sra_fastq/GSM3128275_2.sra -O /n/scratch2/rz92/test_chilin/cistromedc_v2/./tmp_sra_fastq
cat /n/scratch2/rz92/test_chilin/cistromedc_v2/./tmp_sra_fastq/GSM3128275_1.fastq /n/scratch2/rz92/test_chilin/cistromedc_v2/./tmp_sra_fastq/GSM3128275_2.fastq > /n/scratch2/rz92/test_chilin/cistromedc_v2/./fastq_final/GSM3128275.fastq
rm /n/scratch2/rz92/test_chilin/cistromedc_v2/./tmp_sra_fastq/GSM3128275_*

python /n/scratch2/rz92/test_chilin/cistromedc_v2/modules/chilinComand.py -s odyssey -c dc.conf -g GSM3128275 -spe hg38 -factor H3K27ac -end SINGLE -fastq /n/scratch2/rz92/test_chilin/cistromedc_v2/./fastq_final/GSM3128275.fastq -out /n/scratch2/rz92/test_chilin/cistromedc_v2/./tmp_chilin_result/dataset93253 -broad h3k27me3,h3k36me3,h3k9me3
```
#### sbatch2:
```shell
source /home/rz92/miniconda3/bin/activate chilin

module load gcc/6.2.0
module load R/3.3.3
module load texlive/2007


chilin simple -u rongbin -s hg38 --threads 8 -i 94395 -o /n/scratch2/rz92/test_chilin/cistromedc_v2/./tmp_chilin_result/dataset94395 -t /n/scratch2/rz92/test_chilin/cistromedc_v2/./fastq_final/GSM2887587.fastq -p narrow -r tf

python /n/scratch2/rz92/test_chilin/cistromedc_v2/.//modules/checkChilin.py -cf dc.conf -d /n/scratch2/rz92/test_chilin/cistromedc_v2/./tmp_chilin_result/dataset94395 -p /n/scratch2/rz92/test_chilin/cistromedc_v2/././final_chilin_result -c "chilin simple -u rongbin -s hg38 --threads 8 -i 94395 -o /n/scratch2/rz92/test_chilin/cistromedc_v2/./tmp_chilin_result/dataset94395 -t /n/scratch2/rz92/test_chilin/cistromedc_v2/./fastq_final/GSM2887587.fastq -p narrow -r tf"
```

* data backup to Umass server. This step will be handled by two scripts in Daisy and Umass server respectively. **Currently these two are located in Daisy: /data6/DC_results/Files, and Umass: /data/projects/Cistromesrc**, Bam files will be deleted on Daisy once backup is finished.
	* connect_umass_odyssey.py for Daisy
	* connect_umass_side.py for Umass

## +++++++++++++

## Section3: CistromeDB interface and backend
### basics:
* Language：AngularJS, coffeeScript, python2.7 
* main path in daisy: /data/home/qqin/01_Projects/Programming/cistrangular, /data/home/qqin/01_Projects/Programming/dc2 (django), /var/www/html/db/

* please do below scripts every time after any revision on the interface part under ./cistrangular, and please restart apache anytime after you revise the backend django part under data/home/qqin/01_Projects/Programming/dc2: 
```
cd /data/home/qqin/01_Projects/Programming/cistrangular
grunt build
cp -r dist/* /var/www/html/db/
#'grunt build' works based on Yeoman（http://yeoman.io）
```
* To install the whole platform if you want migrate to a new place or server, a separate evironment of miniconda2 are suggested strongly, and do steps as below:
```shell
#install dependencies
cd cistrangular
npm install
bower install
#Serve Development Version
grunt serve
Serve Producton Version
grunt serve:build
#Deploy to Web Server
#Change host and port to your own ones, change dest key to your deployed server path, then run,
grunt sftp-deploy
```

### how this database works (the HTML: http://getbootstrap.com/components/):

1. go to /data/home/qqin/01_Projects/Programming/cistrangular/app/scripts/app.coffee if the cistrome.org/db be queried
2. refresh http://cistrome.org/db/#/, get html. As default, cistromeDB request all dc id / samples
3. /data/home/qqin/01_Projects/Programming/cistrangular/app/scripts/app.coffee receive the request, and then request /data/home/qqin/01_Projects/Programming/cistrangular/app/scripts/controllers/main_controller.coffee to interact with html
4. /data/home/qqin/01_Projects/Programming/cistrangular/app/scripts/main_service.coffee will query django by $http
5. /data/home/qqin/01_Projects/Programming/dc2/dc2/urls.py in dyango recieves the query from coffee
6. /data/home/qqin/01_Projects/Programming/dc2/datacollection/views.py, line 194 def inspector_ajax(request):, search in mysql
7. return the data using json format
8. get html showing data in json
9. /data/home/qqin/01_Projects/Programming/cistrangular/app/scripts/app.coffee returns the data

### CGI is used for batch download function in CistromeDB
* This cgi script responses for the information delivary between front-end (user information) and back-end (download links) in batch download function.
* current daisy path: /data5/home/rongbin/public_html/cgi-bin/, if you change the cgi path, please revise in interface.html.
* For the security of data sharing, we only provide downloads for the users whose email domain are in world_universities_and_domains.json. Users can contact cistromedb@gmail.com if they cannot receive download link, **you have to add the domain into the world_universities_and_domains.json in this case**.
* prepare batch BED zip files and download links if you want to update batch download.
	* make tables which includes sample annotation by data types, columns like: ID      Species GSMID   Cell_line       Cell_type       Tissue_type     Factor  File
	* tar and zip the BED files (narrowPeak or broadPeak) and annotation file by types: human factor, human ca, human hm, mouse factor, mouse ca, mouse hm.
	* rename each tar.gz file into random name like below.
	* copy the tar.gz files into /var/www/html/db/batchdata/, then links will be http://cistrome.org/db/batchdata/24KRO157XZ5Y.tar.gz for example
	* revise the correspondence links in cgi script named validate2.py
```
human_ca_zip.tar.gz     R56Q7GGRZEY7L.tar.gz
human_hm_zip.tar.gz     GTYPP2KEMBOVQ.tar.gz
mouse_factor_zip.tar.gz R9MXVUTB72SQ.tar.gz
human_factor_zip.tar.gz 24KRO157XZ5Y.tar.gz
mouse_ca_zip.tar.gz     FGRVH30PLYTN.tar.gz
mouse_hm_zip.tar.gz     DPOUA6WA6SNL.tar.gz 
```
revise links in validate2.py
```python
if form.getvalue("checkbox1"):
	link = 'http://cistrome.org/db/batchdata/24KRO15.tar.gz'
	dtype = "Human FACTOR, 4.6G"
elif form.getvalue("checkbox2"):
	link = 'http://cistrome.org/db/batchdata/GTYPP2KE.tar.gz'
	dtype = "Human HISTONE MARK AND VARIANT, 7.2G"
elif form.getvalue("checkbox3"):
	link = 'http://cistrome.org/db/batchdata/R56Q7GG.tar.gz'
	dtype = "Human CHROMATIN Accessibility, 3.0G"
elif form.getvalue('checkbox5'):
	link = 'http://cistrome.org/db/batchdata/R9MXVUT.tar.gz'
	dtype = "Mouse FACTOR, 3.0G"
elif form.getvalue('checkbox6'):
	link = 'http://cistrome.org/db/batchdata/DPOUA6WA6.tar.gz'
	dtype = "Mouse HISTONE MARK AND VARIANT, 5.2G"
elif form.getvalue('checkbox7'):
	link = 'http://cistrome.org/db/batchdata/FGRVH30.tar.gz'
	dtype = "Mouse CHROMATIN Accessibility, 1.5G"
```

### What do you need to do if you want to update CistromeDB after new data collection and processing:
* get a list of samples that are processed ok and the correspondence path on Daisy, save to file DC_collection.txt
* copy the motif folders to a specific directory at **/data5/browser/motif_html** on Daisy
```shell
DCID=199
if [ -d /path/dataset${DCID}/attic/${DCID}_seqpos ]; then
  mv /path/dataset${DCID}/attic/${DCID}_seqpos /data5/browser/motif_html/${DCID}
fi
```
* get the QC matrix for each sample and save to json file under a specific folder at /data/home/qqin/12_data/json/, this can be handled by python script named. The generated json will be used for qc visulization in website.
```
cd ./db_interface/other/
cp ../../geo_parser/datacollection/models.py ./
python ./db_interface/other/tojson_web_onejson.py DC_collection.txt /data/home/qqin/12_data/json/
```
* add biological source combination (cellLine;cellType;Tissue) in MySQL column. This columns will be used for sorting in CistromeDB result page. The sorting can be fast only based on one column information. Please do:
```
python add_biological_source.py
```
* change the status of new samples in MySQL as 'completed'. Only 'completed' samples can be shown in CistromeDB website. So please make sure you have finished the last two steps before releasing.
```
python change_status.py
```
* You might also want to refresh the figure of CistromeDB visits in recent years like http://cistrome.org/db/#/stat. The monitor of cistrome DB webpage could be found here at http://cistrome.org/~qqin/piwik/index.php?module=CoreHome&action=index&idSite=2&period=day&date=yesterday#?module=Dashboard&action=embeddedIndex&idSite=2&period=day&date=yesterday&idDashboard=1. To get the figure of visiting summary, please do:
```shell
cd ./db_interface/other/statistic
python visit_barplot.py
sudo cp visit_barplot.png /var/www/html/db/
```
* You also want to update "Cistrome Data Browser Samples statistics". Run the command line below and replace the numbers in html at ./db_interface/cistrangular/app/views/stat.html  
```shell
python ./db_interface/other/statistic/stats_cistromedb.py
cat stat_db.csv
```
### The administration of CistromeDB samples.

We have two administration systems: 
* one is for our internal members at http://dc2.cistrome.org/api/new_admin/, this system include each tables in MySQL and can allow maintainers to revise sampel information convinently. 
* another one is for the administration of sample annotation correction submitted by users at http://cistrome.org/db/#/admin, the more instructions can be found in the page.

## +++++++++++++

## Section4: CistromeDB Toolkit

### basics:
* Language：python2.7, vueJS, R
* framework: Django
* main path in daisy: /data5/home/rongbin/wancx/project_01/dc3

### Preparation:
* This toolkit includes three functions that built based on existed samples in CistromeDB. Therefore, some pre-computation needs to be done if you want to update the function.
* summarize RP scores for the existed good quality samples (# of 5 fold peaks > 1000 for narrow peaks, all peaks for broad peaks) into a matrix (sample by genes) and save hd5 file, this preparation benefits the searching in website.
```shell
python ./cistrome_toolkit/precompute/RP/hs_beta_sum.py path_of_table path_of_save beta_col
# path_of_table: the table path includes at least one column which is the BETA result file of cistrome sample
# path_of_save: the path of hd5 file for saving combined RP score
python ./cistrome_toolkit/precompute/RP/normalize.py path_of_hd5
# normalized RP score into 0-1 range, so the RP can be compared across samples.
```
* draw transcript figures which will be used for the visualization of multiple transcripts in relative genome location of a gene in first function.
```shell
Rscript ./cistrome_toolkit/precompute/transcript/drawhg38.R
```
* build giggle index for those good quality samples using BED files according to the command lines in the CistromeDB toolkit document page.
* The prepared files need to move the specific path under Toolkit framework:
	* RP: ./cistrome_toolkit/website/static/db/
	* transcript: ./cistrome_toolkit/website/static/transcripts/hg38/ and ./cistrome_toolkit/website/static/transcripts/mm10/


















